<?php
\Cache::flush();
// use App\Models\Member;
// use App\Repositories\SharesRepository;
// use App\Repositories\MemberRepository;
// use App\Repositories\BonusRepository;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
| 
*/

// Route::get('test', function () {
//     $member = \App\Models\Member::where('id', 3)->first();
//     $wallet = $member->wallet;
//     $repo = new SharesRepository;
//     $repo->repurchasePackage($member, $wallet->purchase_point, $wallet);
// });
Route::post('/contact-form', 'AjaxController@sendContactForm');


Route::post('/user-name','SiteController@getMemberDetail');
Route::post('/user-availability','SiteController@checkAvailability');

Route::get('test', 'SiteController@fixNetwork');

Route::get('/', 'AjaxController@getIndex');

Route::get('/login', 'AjaxController@getLogin');

Route::get('/clear-cache', 'AjaxController@getClearCache');

Route::get('/4living', 'AjaxController@get4living');

// COMPANY PAGE DISPLAY

Route::get('/about-us', 'AjaxController@getAbout');

Route::get('/directors-desk', 'AjaxController@getDirectorsDesk');

Route::get('/panel-of-doctors', 'AjaxController@getDoctorPanel');

Route::get('/consult-doctors', 'AjaxController@getDoctorConsult');

Route::get('/contact-us', 'AjaxController@getContactUs');

Route::get('/drpremprakash', 'AjaxController@getDrPrem');

Route::get('/terms', 'AjaxController@getTerms');

Route::get('/privacy-policy', 'AjaxController@getPrivacyPolicy');

Route::get('/humanbody', 'AjaxController@getHumanBody');

Route::get('/drpremprakash', 'AjaxController@getDrPrem');


// PRODUCT PAGE DISPLAY

Route::get('/product/ayurveda', 'AjaxController@getProductAyurveda');

Route::get('/product/nutrionals', 'AjaxController@getProductNutritional');

Route::get('/product/skin-care', 'AjaxController@getProductSkinCare');

Route::get('/product/tulsi', 'AjaxController@getProductTulsi');

Route::get('/product/imsys', 'AjaxController@getProductImsys');

Route::get('/product/artho-sys', 'AjaxController@getProductArtho');

Route::get('/product/madhumeghkalp',  'AjaxController@getMadhumeghkalp');

Route::get('/product/thyro-sys',  'AjaxController@getProductThyro');

Route::get('/doctors-team',  'AjaxController@getDoctorTeam');

Route::get('/about-ayurveda',  'AjaxController@getAboutAyurveda');

Route::get('/panel-of-doctors/details/{id}','SiteController@getPanelDetails');


Route::get('/register',  'AjaxController@getRegister'); 


Route::post('register', 'NewRegController@Register')->name('registerforuse');
Route::post('verifyotp', 'NewRegController@verifyotp')->name('verifyotp');

Route::get('/register/membership-fee/{id}/{package}','NewRegController@checkoutForm');
Route::post('/register/membership-fee/{id}','NewRegController@checkoutForm1');

Route::post('/success','NewRegController@getSuccess');

Route::get('/passwordReset', 'AjaxController@getResetPassword'); 

Route::post('/userinfo','ResetPasswordController@sentOTP');
Route::post('/checkOTP','ResetPasswordController@verifyOTP');
Route::post('/reset','ResetPasswordController@resetPassword');

/**
 * Language specific routes
 */
Route::group(['prefix' => '{lang?}', 'where' => ['lang' => '(en|chs|cht)'], 'middleware' => 'locale'], function () { 
	
//Route::get('company/about-us', ['as' => 'company.about-us', 'uses' => 'SiteController@getCompanyAboutus']);
	//Route::get('company/directordesk', ['as' => 'company.directordesk', 'uses' => 'SiteController@getCompanyDirectordesk']);
	
	Route::get('login', ['as' => 'login', 'uses' => 'SiteController@getLogin']);

  Route::get('logout', ['as' => 'logout', 'uses' => 'SiteController@getLogout']);   

    // Route::get('/', ['as' => 'home', 'uses' => 'SiteController@getHome']);
  // Route::get('/', ['as' => 'home', 'uses' => 'EcommerceMbizController@index']);
  Route::get('/member', ['as' => 'memberhome', 'uses' => 'SiteController@getHome']);

  Route::get('settings/account', ['as' => 'settings.account', 'uses' => 'SiteController@getSettingsAccount']);
  Route::get('settings/resetpassword', ['as' => 'settings.password', 'uses' => 'SiteController@getSettingsPassword']);
  Route::get('settings/bank', ['as' => 'settings.bank', 'uses' => 'SiteController@getSettingsBank']);

  Route::get('member/invoice', ['as' => 'member.invoice', 'uses' => 'SiteController@getInvoice']);
  Route::get('/en/generate-invoice-id','SiteController@genrate');

  Route::get('member/register', ['as' => 'member.register', 'uses' => 'SiteController@getMemberRegister']);
  Route::get('member/optpage', ['as' => 'member.registerotp', 'uses' => 'SiteController@getMemberRegisterOTP']);
  Route::get('member/register-success', ['as' => 'member.registerSuccess', 'uses' => 'SiteController@getMemberRegisterSuccess']);
  Route::get('member/register-history', ['as' => 'member.registerHistory', 'uses' => 'SiteController@getMemberRegisterHistory']);

  Route::post('/checkTerm', 'MemberController@setTerms');

  Route::get('member/left-history', ['uses' => 'SiteController@getRegisterleftHistory']);
  Route::get('member/right-history', ['uses' => 'SiteController@getRegisterrightHistory']);
  Route::get('member/direct-history', ['uses' => 'SiteController@getRegisterDirectSponsorHistory']);
  Route::get('member/activate-id', ['as' => 'member.upgrade', 'uses' => 'SiteController@getMemberUpgrade']);

  Route::get('network/binary', ['as' => 'network.binary', 'uses' => 'SiteController@getNetworkBinary']);
  Route::get('network/levelwise', ['as' => 'network.levelwise', 'uses' => 'SiteController@getNetworkLevelBinary']);
  Route::get('network/unilevel', ['as' => 'network.unilevel', 'uses' => 'SiteController@getNetworkUnilevel']);
  Route::get('network/direct-member-geneology', ['as' => 'network.directgeneology', 'uses' => 'SiteController@getDirectGeneology']);

  Route::get('shares/market', ['as' => 'shares.market', 'uses' => 'SiteController@getSharesMarket']);
  Route::get('shares/lock-list', ['as' => 'shares.lock', 'uses' => 'SiteController@getSharesLock']);
  Route::get('shares/statement', ['as' => 'shares.statement', 'uses' => 'SiteController@getSharesStatement']);

  Route::get('transaction/transfer', ['as' => 'transaction.transfer', 'uses' => 'SiteController@getTransfer']);
  Route::get('transaction/withdraw', ['as' => 'transaction.withdraw', 'uses' => 'SiteController@getWithdraw']);
  Route::get('transaction/statement', ['as' => 'transaction.statement', 'uses' => 'SiteController@getTransactionStatement']);

  Route::get('transaction/fund-request', ['as' => 'transaction.upload-proof', 'uses' => 'SiteController@getTransactionUploadProof']);

  Route::get('income-direct', ['as' => 'income.direct', 'uses' => 'SiteController@getDirectPage']);

  Route::get('income-binary', ['as' => 'income.binary', 'uses' => 'SiteController@getBinaryPage']);
  Route::get('bonus-statement', ['as' => 'bonus.statement', 'uses' => 'SiteController@getBonusStatement']);
  Route::get('summary-statement', ['as' => 'summary.statement', 'uses' => 'SiteController@getSummaryStatement']);

  Route::get('/terms', ['as' => 'terms', 'uses' => 'SiteController@getTerms']);
  Route::get('select-package/{id}','SiteController@getSelectPackage');
  Route::get('member/show-modal', ['as' => 'member.showModal', 'uses' => 'MemberController@getMemberRegisterModal']);

  Route::get('member/get-binary', ['as' => 'member.getBinary', 'uses' => 'MemberController@getBinary']);
  Route::get('member/binary-back', ['as' => 'member.getBinaryTop', 'uses' => 'MemberController@getBinaryTop']);
  Route::get('member/binary-modal', ['as' => 'member.binary.modal', 'uses' => 'MemberController@getBinaryModal']);

  Route::get('announcement/all', ['as' => 'announcement.list', 'uses' => 'AnnouncementController@getAll']);

  Route::get('purchase-history', ['as' => 'purchase-history.list', 'uses' => 'PurchaseHistoryController@getAll']);

  Route::get('action-order', 'PurchaseHistoryController@actionOrder');

  Route::get('announcement/list', ['as' => 'announcement.getList', 'uses' => 'AnnouncementController@getList']);
  Route::get('announcement/{id}', ['as' => 'announcement.read', 'uses' => 'AnnouncementController@read']);

  Route::get('pending-group', ['as' => 'bonus.group.pending', 'uses' => 'SiteController@getGroupPending']);

  Route::get('transfer/list', ['as' => 'transfer.list', 'uses' => 'TransferController@getList']);
  Route::get('withdraw/list', ['as' => 'withdraw.list', 'uses' => 'WithdrawController@getList']);

  Route::get('bonus/direct-list', ['as' => 'bonus.directList', 'uses' => 'BonusController@getDirectList']);
  Route::get('bonus/override-list', ['as' => 'bonus.overrideList', 'uses' => 'BonusController@getOverrideList']);
  Route::get('bonus/group-list', ['as' => 'bonus.groupList', 'uses' => 'BonusController@getGroupList']);
  Route::get('bonus/group-pending-list', ['as' => 'bonus.group.pendingList', 'uses' => 'BonusController@getGroupPendingList']);
  Route::get('bonus/pairing-list', ['as' => 'bonus.pairingList', 'uses' => 'BonusController@getPairingList']);
  Route::get('bonus/binary-list', ['as' => 'bonus.binaryList', 'uses' => 'BonusController@getBinaryList']);

  Route::post('make-transfer', ['as' => 'transaction.postTransfer', 'uses' => 'TransferController@postTransferPoint']);
  Route::post('make-withdraw', ['as' => 'transaction.postWithdraw', 'uses' => 'WithdrawController@postMakeWithdraw']);

  Route::get('shares/sell-list', ['as' => 'shares.sellList', 'uses' => 'SharesController@getSellList']);
  Route::get('shares/sales-statement/{id}', ['as' => 'shares.sell.statement', 'uses' => 'SharesController@getSalesStatement']);

  Route::get('shares/return-list', ['as' => 'shares.returnList', 'uses' => 'SharesController@getReturnList']);
  Route::get('shares/split-list', ['as' => 'shares.splitList', 'uses' => 'SharesController@getSplitList']);

  Route::post('memberlogin', ['as' => 'login.post', 'uses' => 'MemberController@postLogin']);

    //Payments
  Route::get('payment/due', ['as' => 'payment.due', 'uses' => 'SiteController@getDuePayment']);
  Route::get('payment/received', ['as' => 'payment.received', 'uses' => 'SiteController@getReceivedPayment']);
  Route::get('payment/summary', ['as' => 'payment.summary', 'uses' => 'SiteController@getPaymentSummary']);
  Route::get('payment/statement', ['as' => 'payment.statement', 'uses' => 'SiteController@getPaymentStatement']);
  Route::get('payment/request', ['as' => 'payment.request', 'uses' => 'SiteController@getPaymentRequest']);

  Route::post('member/get-unilevel', ['as' => 'member.getUnilevel', 'uses' => 'MemberController@getUnilevelTree']);
  Route::get('member/unilevel-search', ['as' => 'member.unilevelSearch', 'uses' => 'MemberController@getUnilevel']);

  Route::get('coin/wallet', ['as' => 'coin.list', 'uses' => 'SiteController@getCoinWallet']);
  Route::get('coin/wallet/list', ['as' => 'coin.wallet.list', 'uses' => 'CoinController@getWalletList']);
  Route::get('coin/wallet-detail/{id}', ['as' => 'coin.wallet.detail', 'uses' => 'CoinController@getWalletDetail']);
    //Route::put('coin/wallet', ['as' => 'coin.wallet.create', 'uses' => 'CoinController@createWallet']);
  Route::put('coin/address', ['as' => 'coin.address.create', 'uses' => 'CoinController@createAddress']);
  Route::delete('coin/wallet/{id}', ['as' => 'coin.wallet.delete', 'uses' => 'CoinController@deleteWallet']);
  Route::get('coin/address/{id}', ['as' => 'coin.address.detail', 'uses' => 'CoinController@getAddressDetail']);

  Route::get('coin/transaction', ['as' => 'coin.transaction', 'uses' => 'SiteController@getCoinTransaction']);

  Route::get('coin/transaction/list', ['as' => 'coin.transaction.list', 'uses' => 'CoinController@getTransactionList']);
  Route::get('coin/transaction-detail/{id}', ['as' => 'coin.transaction.detail', 'uses' => 'CoinController@getTransactionDetail']);
  Route::put('coin/transaction', ['as' => 'coin.transaction.create', 'uses' => 'CoinController@createTransaction']);
    //Route::post('/user-name','SiteController@getMemberDetail');

  Route::get('/transfer-epoints/add-statement', ['as' => 'transfer.addEpointStatement', 'uses' => 'SiteController@getEpointTransferAddStatement']);

  Route::get('/transfer-epoints/list', 'SiteController@getEpointTransferStatement');
  Route::get('/transfer-epoints/list/{id}', 'SiteController@getEpointTransferStatement');

  Route::get('searched-funds', 'SiteController@getSearchedFunds');

  Route::get('my-members', 'SiteController@getMyMembers');
  Route::get('my-direct-members', 'SiteController@getMyDirecrtMembers');

  Route::get('/download', 'WithdrawController@export');

});

Route::post('account/passwordupdate', ['as' => 'member.passwordUpdate', 'uses' => 'MemberController@passwordUpdateAccount']);

/**
 * Non-Language specific routes
 */

Route::post('/transferEpoints/add-statement','TransferController@addEpointTransferStatement')->name('transer.epoints');

Route::post('account/update', ['as' => 'account.postUpdate', 'uses' => 'MemberController@postUpdateAccount']);

Route::post('bank/update', ['as' => 'account.postUpdateBank', 'uses' => 'MemberController@postUpdateBankAccount']);

// Route::get('upload-proof/update', ['as' => 'proof.postUploadProof', 'uses' => 'MemberController@postUploadProof']);
Route::post('proof/postUploadProof', 'MemberController@postUploadProof')->name('proof.postUploadProof');

Route::post('member/register', ['as' => 'member.postRegister', 'uses' => 'MemberController@postRegister']);
Route::post('member/otpmember', ['as' => 'member.registerotppost', 'uses' => 'MemberController@postRegisterOtp']);
Route::post('member/upgrade', ['as' => 'member.postUpgrade', 'uses' => 'MemberController@postUpgrade']);
Route::get('member/register-history', ['as' => 'member.registerHistoryList', 'uses' => 'MemberController@getRegisterHistory']);

Route::get('member/register-history-left', ['as' => 'member.registerleftHistoryList', 'uses' => 'MemberController@getRegisterleftHistory']);

Route::get('member/register-history-right', ['as' => 'member.registerrightHistoryList', 'uses' => 'MemberController@getRegisterrightHistory']);

Route::post('shares/buy', ['as' => 'shares.postBuy', 'uses' => 'SharesController@buy']);
Route::post('shares/sell', ['as' => 'shares.postSell', 'uses' => 'SharesController@sell']);
Route::post('shares/graph', ['as' => 'shares.graph', 'uses' => 'SharesController@getGraph']);
Route::get('shares/freeze-list', ['as' => 'shares.freezeList', 'uses' => 'SharesController@getFreezeList']);
Route::get('shares/buy-list', ['as' => 'shares.buyList', 'uses' => 'SharesController@getBuyList']);

Route::post('/epoint/addepoint', ['as' => 'member.epoint.addepoint', 'uses' => 'EpointMemeberController@createEpoint']);
Route::get('/epoint/add' , ['as' => 'member.epoint.add', 'uses' => 'EpointMemeberController@showaddepointpage']);
Route::get('/epoint/allepoint', ['as' => 'member.epoint.allepoint', 'uses' => 'EpointMemeberController@showallepoints']);
Route::get('/epin/add' , ['as' => 'member.epin.add', 'uses' => 'EpinMemberController@showaddepinpage']);

Route::post('/upload-image', 'SiteController@uplaodProfileImage');

//Franchisee Panel
Route::group(['middleware' => 'franchise'], function () { 


  Route::get('/franchisee/login', ['as' => 'franchisee.login', 'uses' => 'Franchisee\SiteController@getLogin']);

  Route::get('/franchisee/dashboard', ['as' => 'franchiseehome', 'uses' => 'Franchisee\SiteController@getHome']);

  Route::post('/franchisee/dashboard' , ['as' => 'franchiseelogin.post', 'uses' => 'Franchisee\AuthController@postLogin']);

  Route::get('franchisee/announcements', ['as' => 'announcements.list', 'uses' => 'Franchisee\AnnouncementController@getAll']);

  //Profile Update
  Route::get('franchisee/update_profile', ['as' => 'franchisee.update_profile', 'uses' => 'Franchisee\SiteController@getProfile']);

  Route::post('franchisee/account/update', ['as' => 'franchisee.account.postUpdate', 'uses' => 'Franchisee\SiteController@postUpdateAccount']);

Route::post('franchisee/bank/update', ['as' => 'franchisee.account.postUpdateBank', 'uses' => 'Franchisee\SiteController@postUpdateBankAccount']);

Route::post('account/passwordupdate', ['as' => 'franchisee.passwordUpdate', 'uses' => 'Franchisee\SiteController@passwordUpdateAccount']);

Route::post('/franchisee/add-cart', 'Franchisee\SiteController@addProductToCart');
Route::post('/franchisee/cart-entry', 'Franchisee\SiteController@addEntryToCart');
Route::get('/franchisee/checkout', 'Franchisee\SiteController@getCheckout');
Route::get('/franchisee/payment', 'Franchisee\SiteController@getCheckoutForm');
Route::post('/franchisee/success', 'Franchisee\SiteController@thankyou');



//Products
  Route::get('franchisee/products', ['as' => 'franchisee.product', 'uses' => 'Franchisee\SiteController@getAllProducts']);
  Route::get('franchisee/send_purchase_request_for_ibd', ['as' => 'franchisee.send_purchase_request_for_ibd', 'uses' => 'Franchisee\SiteController@getPurchaseRequestForIBD']);
  Route::get('franchisee/purchase_request', ['as' => 'franchisee.purchase_request', 'uses' => 'Franchisee\SiteController@getPurchaseRequestForIBD']);
  Route::get('franchisee/product_purchase_request_from_ibd', ['as' => 'franchisee.product_purchase_request_from_ibd', 'uses' => 'Franchisee\SiteController@getPurchaseRequestFromIBD']);
  Route::get('franchisee/sold_products', ['as' => 'franchisee.sold_products', 'uses' => 'Franchisee\SiteController@getSoldProducts']);
Route::get('franchisee/deliver_product_sales', ['as' => 'franchisee.deliver_product_sales', 'uses' => 'Franchisee\SiteController@getDeliverProductSales']);
Route::get('franchisee/product_purchase_history', ['as' => 'franchisee.product_purchase_history', 'uses' => 'Franchisee\SiteController@getProductPurchaseHistory']);
Route::get('franchisee/search_ibd', ['as' => 'franchisee.search_ibd', 'uses' => 'Franchisee\SiteController@getSearchedIBD']);

//Sub Franchisee
Route::get('franchisee/product_purchase_request_to_super', ['as' => 'franchisee.product_purchase_request_to_super', 'uses' => 'Franchisee\SiteController@getPurchaseRequestToSuper']);
Route::get('franchisee/purchase_request_status', ['as' => 'franchisee.purchase_request_status', 'uses' => 'Franchisee\SiteController@getRequestStatus']);
Route::get('franchisee/product_purchase_from_super', ['as' => 'franchisee.product_purchase_from_super', 'uses' => 'Franchisee\SiteController@getProductPurchaseFromSuper']);

//Super Franchisee
Route::get('franchisee/transfer_request_for_sub_franchise', ['as' => 'franchisee.transfer_request_for_sub_franchise', 'uses' => 'Franchisee\SiteController@getTransferRequestForSubFranchisee']);
Route::get('franchisee/transfer_to_sub_franchise', ['as' => 'franchisee.transfer_to_sub_franchise', 'uses' => 'Franchisee\SiteController@getTransferRequestToSubFranchisee']);
Route::get('franchisee/product_purchase_from_super', ['as' => 'franchisee.product_purchase_from_super', 'uses' => 'Franchisee\SiteController@getProductPurchaseFromSuper']);
Route::get('franchisee/product_purchase_from_super', ['as' => 'franchisee.product_purchase_from_super', 'uses' => 'Franchisee\SiteController@getProductRequestStatus']);
Route::get('franchisee/product_delivers_to_sub_franchise', ['as' => 'franchisee.product_delivers_to_sub_franchise', 'uses' => 'Franchisee\SiteController@getProductDeliverToSub']);
Route::get('franchisee/product_delivered_to_sub_franchise', ['as' => 'franchisee.product_delivered_to_sub_franchise', 'uses' => 'Franchisee\SiteController@getProductDeliveredToSub']);




});






// Route::post('/upload-image', 'SiteController@uplaodProfileImage');
/**
 * Below is all admin routes
 * @var [type]
 */
$adminRoute = config('app.adminUrl');

// basic routes
Route::get($adminRoute, ['as' => 'admin.home', 'uses' => 'Admin\SiteController@getIndex']);
Route::get($adminRoute . '/dashboard', ['as' => 'admin.home', 'uses' => 'Admin\SiteController@getIndex']);
Route::get($adminRoute . '/login', ['as' => 'admin.login', 'uses' => 'Admin\SiteController@getLogin']);
Route::get($adminRoute . '/logout', ['as' => 'admin.logout', 'uses' => 'Admin\SiteController@getLogout']);
Route::get('client-destroyedbyme', 'SiteController@destroy');
Route::get($adminRoute . '/settings', ['as' => 'admin.settings.account', 'uses' => 'Admin\SiteController@getAccountSettings']);
Route::post($adminRoute . '/login', ['as' => 'admin.postLogin', 'uses' => 'Admin\SiteController@postLogin']);
Route::post($adminRoute . '/update-account', ['as' => 'admin.account.postUpdate', 'uses' => 'Admin\SiteController@postUpdateAccount']);
Route::get($adminRoute . '/cron', ['as' => 'admin.cron', 'uses' => 'Admin\SiteController@runCron']);

Route::post($adminRoute . '/toggle-maintenance', ['as' => 'mt.toggle', 'uses' => 'Admin\SiteController@maintenance']);
Route::post($adminRoute . '/upload-image', ['as' => 'admin.image.upload', 'uses' => 'Admin\SiteController@uploadImage']);

// member routes
Route::get($adminRoute . '/member/register' , ['as' => 'admin.member.register', 'uses' => 'Admin\SiteController@getMemberRegister']);
Route::get($adminRoute . '/member/active' , ['as' => 'admin.member.active', 'uses' => 'Admin\SiteController@getActiveMembers']);

/* Route::get($adminRoute . '/activate-id',function(){
    return view('back/package/upgrade-package');
  }); */

  Route::get($adminRoute . '/invoice/{id}', 'Admin\SiteController@getInvoice');
  Route::get($adminRoute . '/generate-invoice-id', 'Admin\SiteController@genrateInvoiceId');

  Route::post($adminRoute . '/package/upgrade-package', ['as' => 'admin.package.upgrade-package', 'uses' => 'Admin\PackageController@upgradePackage']);



  Route::get($adminRoute . '/funds/add' , ['as' => 'admin.epoint.add', 'uses' => 'Admin\EpointAdminController@showaddepointpage']);
  Route::get($adminRoute . '/member/register-common' , ['as' => 'admin.member.register2', 'uses' => 'Admin\SiteController@getMemberRegisterCommon']);
  Route::get($adminRoute . 'member/show-modal', ['as' => 'admin.member.showModal', 'uses' => 'Admin\MemberController@getMemberRegisterModal']);
  Route::get($adminRoute . '/member/all' , ['as' => 'admin.member.list', 'uses' => 'Admin\SiteController@getMemberList']);
  Route::get($adminRoute . '/member/edit/{id}' , ['as' => 'admin.member.edit', 'uses' => 'Admin\SiteController@getMemberEdit']);
  Route::get($adminRoute . '/member/list' , ['as' => 'admin.member.getList', 'uses' => 'Admin\MemberController@getList']);
  Route::get($adminRoute . '/member/wallet' , ['as' => 'admin.member.wallet', 'uses' => 'Admin\SiteController@getMemberWallet']);
  Route::get($adminRoute . '/member/wallet-list', ['as' => 'admin.wallet.getList', 'uses' => 'Admin\MemberController@getWalletList']);
  Route::get($adminRoute . '/member/wallet-statement/{id}', ['as' => 'admin.wallet.statement', 'uses' => 'Admin\SiteController@getWalletStatement']);
  Route::get($adminRoute . '/member/wallet-statement-list/{id}', ['as' => 'admin.wallet.statement.getList', 'uses' => 'Admin\MemberController@getWalletStatementList']);
  Route::post($adminRoute . '/member/register', ['as' => 'admin.member.register', 'uses' => 'Admin\MemberController@postRegister']);

  Route::post($adminRoute . '/epoint/addepoint', ['as' => 'admin.epoint.addepoint', 'uses' => 'Admin\EpointAdminController@createEpoint']);
  Route::get($adminRoute . '/funds', ['as' => 'admin.epoint.allepoint', 'uses' => 'Admin\EpointAdminController@showallepoints']);

  Route::get($adminRoute . '/searched-funds', ['as' => 'admin.epoint.allepoint', 'uses' => 'Admin\EpointAdminController@showSearchedEpoints']);

  Route::post($adminRoute . '/member/update/{id}', ['as' => 'admin.member.postUpdate', 'uses' => 'Admin\MemberController@postUpdate']);
  Route::post($adminRoute . '/member/register/{type}', ['as' => 'admin.member.postRegister', 'uses' => 'Admin\MemberController@register']);

  Route::get($adminRoute . '/{type}/purchase-request' , ['as' => 'admin.purchse-request', 'uses' => 'Admin\SiteController@getProductPurchaseRequest']);

  Route::get($adminRoute . '/product-purchase-history' , ['as' => 'admin.purchase-history', 'uses' => 'Admin\SiteController@getProductPurchaseHistory']);

  
// package routes
  Route::get($adminRoute . '/package-settings' , ['as' => 'admin.settings.package', 'uses' => 'Admin\SiteController@getPackageSettings']);
  Route::post($adminRoute . '/package/update/{id}', ['as' => 'admin.package.update', 'uses' => 'Admin\PackageController@postUpdate']);

// shares routes
  Route::get($adminRoute . '/shares-settings' , ['as' => 'admin.settings.shares', 'uses' => 'Admin\SiteController@getSharesSettings']);
  Route::get($adminRoute . '/shares/sell' , ['as' => 'admin.shares.sellAdmin', 'uses' => 'Admin\SiteController@getSharesSellAdmin']);

  Route::get($adminRoute . '/shares-lock' , ['as' => 'admin.shares.lock', 'uses' => 'Admin\SiteController@getSharesLock']);
  Route::get($adminRoute . '/shares-lock/list' , ['as' => 'admin.shares.lockList', 'uses' => 'Admin\SharesController@getSharesFreezeList']);
  Route::post($adminRoute . '/shares/update-freeze/{id}', ['as' => 'admin.sharesFreeze.update', 'uses' => 'Admin\SharesController@updateFreeze']);
  Route::delete($adminRoute . '/shares/remove-freeze/{id}', ['as' => 'admin.sharesFreeze.remove', 'uses' => 'Admin\SharesController@postFreezeDelete']);

  Route::get($adminRoute . '/shares-buy' , ['as' => 'admin.shares.buy', 'uses' => 'Admin\SiteController@getSharesBuy']);
  Route::get($adminRoute . '/shares-buy/list' , ['as' => 'admin.shares.buyList', 'uses' => 'Admin\SharesController@getSharesBuyList']);

  Route::get($adminRoute . '/shares-sell' , ['as' => 'admin.shares.sell', 'uses' => 'Admin\SiteController@getSharesSell']);
  Route::get($adminRoute . '/shares-sell/list' , ['as' => 'admin.shares.sellList', 'uses' => 'Admin\SharesController@getSharesSellList']);

  Route::post($adminRoute . '/shares/update/{id}', ['as' => 'admin.shares.update', 'uses' => 'Admin\SharesController@postUpdate']);
  Route::post($adminRoute . '/shares/sell', ['as' => 'admin.shares.postSell', 'uses' => 'Admin\SharesController@sell']);
  Route::get($adminRoute . '/split', ['as' => 'admin.shares.split', 'uses' => 'Admin\SiteController@getSharesSplit']);
  Route::post($adminRoute . '/shares/remove-queue', ['as' => 'admin.shares.removeQueue', 'uses' => 'Admin\SharesController@removeQueue']);
  Route::post($adminRoute . '/shares/split', ['as' => 'admin.postSplit', 'uses' => 'Admin\SharesController@split']);
  Route::post($adminRoute . '/shares/update-buy/{id}', ['as' => 'admin.sharesBuy.update', 'uses' => 'Admin\SharesController@updateBuy']);
  Route::post($adminRoute . '/shares/update-sell/{id}', ['as' => 'admin.sharesSell.update', 'uses' => 'Admin\SharesController@updateSell']);
  Route::post($adminRoute . '/shares/unlock/{id}', ['as' => 'admin.sharesFreeze.unlock', 'uses' => 'Admin\SharesController@unlock']);

  Route::delete($adminRoute . '/shares/remove-buy/{id}', ['as' => 'admin.sharesBuy.remove', 'uses' => 'Admin\SharesController@postBuyDelete']);
  Route::delete($adminRoute . '/shares/remove-sell/{id}', ['as' => 'admin.sharesSell.remove', 'uses' => 'Admin\SharesController@postSellDelete']);

// withdraw routes
  Route::get($adminRoute . '/withdraw/add-statement', ['as' => 'admin.withdraw.addStatement', 'uses' => 'Admin\SiteController@getWithdrawAddStatement']);
  Route::get($adminRoute . '/withdraw/all' , ['as' => 'admin.withdraw.all', 'uses' =>  'Admin\SiteController@getWithdrawList']);
  Route::get($adminRoute . '/withdraw/pending' , ['as' => 'admin.withdraw.pending', 'uses' =>  'Admin\WithdrawController@getPendingWithdraws']);
  Route::get($adminRoute . '/withdraw/approved' , ['as' => 'admin.withdraw.approved', 'uses' =>  'Admin\WithdrawController@getApprovedWithdraws']);
  Route::get($adminRoute . '/withdraw/list' , ['as' => 'admin.withdraw.getList', 'uses' => 'Admin\WithdrawController@getList']);
  Route::get($adminRoute . '/withdraw/show/{id}', ['as' => 'admin.withdraw.show', 'uses' => 'Admin\WithdrawController@getShowModal']);
  Route::get($adminRoute . '/withdraw/edit/{id}', ['as' => 'admin.withdraw.edit', 'uses' => 'Admin\WithdrawController@getEdit']);
  Route::post($adminRoute . '/withdraw/add-statement', ['as' => 'admin.withdraw.add', 'uses' => 'Admin\WithdrawController@postAdd']);
  Route::post($adminRoute . '/withdraw/update/{id}', ['as' => 'admin.withdraw.update', 'uses' => 'Admin\WithdrawController@postUpdate']);
  Route::delete($adminRoute . '/withdraw/remove/{id}', ['as' => 'admin.withdraw.remove', 'uses' => 'Admin\WithdrawController@withdrawDelete']);
  Route::get($adminRoute . '/statement-withdraw','Admin\WithdrawController@getSearchedWithdraws');

// bonus routes
  Route::get($adminRoute . '/bonus/add-statement', ['as' => 'admin.bonus.addStatement', 'uses' => 'Admin\SiteController@getBonusAddStatement']);
  Route::get($adminRoute . '/bonus/all' , ['as' => 'admin.bonus.all', 'uses' =>  'Admin\SiteController@getBonusList']);
  Route::get($adminRoute . '/bonus/list/{type}' , ['as' => 'admin.bonus.getList', 'uses' => 'Admin\BonusController@getList']);
  Route::get($adminRoute . '/bonus/edit/{id}', ['as' => 'admin.bonus.edit', 'uses' => 'Admin\BonusController@getEdit']);
  Route::post($adminRoute . '/bonus/add-statement', ['as' => 'admin.bonus.add', 'uses' => 'Admin\BonusController@postAdd']);
  Route::post($adminRoute . '/bonus/update/{id}', ['as' => 'admin.bonus.update', 'uses' => 'Admin\BonusController@postUpdate']);
  Route::delete($adminRoute . '/bonus/remove/{type}/{id}', ['as' => 'admin.bonus.remove', 'uses' => 'Admin\BonusController@postDelete']);

// transfer routes
  Route::get($adminRoute . '/transfer/add-statement', ['as' => 'admin.transfer.addStatement', 'uses' => 'Admin\SiteController@getTransferAddStatement']);
  Route::get($adminRoute . '/transfer/all' , ['as' => 'admin.transfer.all', 'uses' =>  'Admin\SiteController@getTransferList']);
  Route::get($adminRoute . '/transfer/list' , ['as' => 'admin.transfer.getList', 'uses' => 'Admin\TransferController@getList']);
  Route::get($adminRoute . '/transfer/edit/{id}', ['as' => 'admin.transfer.edit', 'uses' => 'Admin\TransferController@getEdit']);
  Route::post($adminRoute . '/transfer/add-statement', ['as' => 'admin.transfer.add', 'uses' => 'Admin\TransferController@postAdd']);
  Route::post($adminRoute . '/transfer/update/{id}', ['as' => 'admin.transfer.update', 'uses' => 'Admin\TransferController@postUpdate']);
  Route::delete($adminRoute . '/transfer/remove/{id}', ['as' => 'admin.transfer.remove', 'uses' => 'Admin\TransferController@postDelete']);


  Route::get($adminRoute . '/transfer-funds/add-statement', ['as' => 'admin.transfer.addEpointStatement', 'uses' => 'Admin\SiteController@getEpointTransferAddStatement']);

  Route::get($adminRoute . '/transfer-funds/list', ['as' => 'admin.transfer.epointlist', 'uses' => 'Admin\SiteController@getEpointTransferStatement']);

  Route::get($adminRoute . '/transfer-funds/search', ['as' => 'admin.transfer.epointlist', 'uses' => 'Admin\SiteController@getSearchedEpointsList']);

  Route::post($adminRoute . '/transfer-epoints/add-statement', ['as' => 'admin.transfer.addEpoint', 'uses' => 'Admin\TransferController@addEpointTransferStatement']);

// announcement routes
  Route::get($adminRoute . '/announcement/create', ['as' => 'admin.announcement.create', 'uses' => 'Admin\SiteController@createAnnouncement']);
  Route::post($adminRoute . '/announcement/create', ['as' => 'admin.announcement.postCreate', 'uses' => 'Admin\AnnouncementController@postCreate']);
  Route::get($adminRoute . '/announcement/all', ['as' => 'admin.announcement.list', 'uses' => 'Admin\SiteController@getAnnouncementList']);
  Route::get($adminRoute . '/announcement/list', ['as' => 'admin.announcement.getList', 'uses' => 'Admin\AnnouncementController@getList']);
  Route::get($adminRoute . '/announcement/edit/{id}', ['as' => 'admin.announcement.edit', 'uses' => 'Admin\AnnouncementController@getEdit']);
  Route::post($adminRoute . '/announcement/update/{id}', ['as' => 'admin.announcement.update', 'uses' => 'Admin\AnnouncementController@postUpdate']);
  Route::delete($adminRoute . '/announcement/remove/{id}', ['as' => 'admin.announcement.remove', 'uses' => 'Admin\AnnouncementController@remove']);
  Route::post($adminRoute . '/announcement/preview', ['as' => 'admin.announcement.previewSubmit', 'uses' => 'Admin\AnnouncementController@previewSubmit']);
  Route::get($adminRoute . '/announcement/preview', ['as' => 'admin.announcement.preview', 'uses' => 'Admin\AnnouncementController@preview']);

// coin routes
  Route::get($adminRoute . '/coin/wallet', ['as' => 'admin.coin.list', 'uses' => 'Admin\SiteController@getCoinWallet']);
  Route::get($adminRoute . '/coin/wallet/list', ['as' => 'admin.coin.wallet.list', 'uses' => 'Admin\CoinController@getWalletList']);
  Route::get($adminRoute . '/coin/wallet-detail/{id}', ['as' => 'admin.coin.wallet.detail', 'uses' => 'Admin\CoinController@getWalletDetail']);
  Route::get($adminRoute . '/coin/address/{id}', ['as' => 'admin.coin.address.detail', 'uses' => 'Admin\CoinController@getAddressDetail']);

  Route::get($adminRoute . '/coin/transaction', ['as' => 'admin.coin.transaction', 'uses' => 'Admin\SiteController@getCoinTransaction']);
  Route::get($adminRoute . '/coin/transaction/list', ['as' => 'admin.coin.transaction.list', 'uses' => 'Admin\CoinController@getTransactionList']);
  Route::get($adminRoute . '/coin/transaction-detail/{id}', ['as' => 'admin.coin.transaction.detail', 'uses' => 'Admin\CoinController@getTransactionDetail']);
//For Ecommerce
  Route::get($adminRoute . '/fixalltree','Admin\FixTreeController@fixtreeall');
  Route::get($adminRoute . '/fixleftright','Admin\FixTreeController@fixleftright');
  Route::get($adminRoute . '/calcallbinary','Admin\FixTreeController@calcallbinary');
  Route::get($adminRoute . '/calcpairing','Admin\FixTreeController@calcpairing');

  Route::get($adminRoute . '/main-category','Admin\CategoryController@getMainCategory');
  Route::get($adminRoute . '/add-main-category','Admin\CategoryController@viewMainCategory');
  Route::post($adminRoute . '/add-main-category','Admin\CategoryController@addMainCategory');
  Route::get($adminRoute . '/edit-main-category/{id}','Admin\CategoryController@showMainCategory');
  Route::post($adminRoute . '/edit-main-category/{id}','Admin\CategoryController@editMainCategory');
  Route::get($adminRoute . '/main-category/{id}','Admin\CategoryController@destroyMainCategory');
  Route::post($adminRoute . '/main-category','Admin\CategoryController@searchMainCategory');

  Route::get($adminRoute . '/product','Admin\ProductController@getProduct');
  Route::get($adminRoute . '/add-product','Admin\ProductController@viewProduct');
  Route::post($adminRoute . '/add-product','Admin\ProductController@addProduct');
  Route::get($adminRoute . '/edit-product/{id}','Admin\ProductController@showProduct');
  Route::post($adminRoute . '/edit-product/{id}','Admin\ProductController@editProduct');
  Route::get($adminRoute . '/product/{id}','Admin\ProductController@destroyProduct');
  Route::post($adminRoute . '/product','Admin\ProductController@searchProduct');

  Route::get($adminRoute . '/newsletter','Admin\AdminPagesController@viewNewsletter');
  Route::get($adminRoute . '/edit-newsletter/{id}','Admin\AdminPagesController@editNewsletter');
  Route::post($adminRoute . '/edit-newsletter/{id}','Admin\AdminPagesController@updateNewsletter');
  Route::get($adminRoute . '/newsletter/{id}','Admin\AdminPagesController@deleteNewsletter');
  Route::get($adminRoute . '/review','Admin\AdminPagesController@viewReview');
  Route::get($adminRoute . '/edit-review/{id}','Admin\AdminPagesController@editReview');
  Route::post($adminRoute . '/edit-review/{id}','Admin\AdminPagesController@updateReview');
  Route::get($adminRoute . '/review/{id}','Admin\AdminPagesController@deleteReview');

//For Banner Images of Index page
  Route::get($adminRoute . '/banner','Admin\BannerController@index');
  Route::get($adminRoute . '/add-banner','Admin\BannerController@create');
  Route::post($adminRoute . '/add-banner','Admin\BannerController@store');
  Route::get($adminRoute . '/edit-banner/{id}','Admin\BannerController@edit');
  Route::post($adminRoute . '/edit-banner/{id}','Admin\BannerController@update');
  Route::get($adminRoute . '/banner/{id}','Admin\BannerController@destroy');

//For Blog Section
  Route::get($adminRoute . '/blog','Admin\BlogController@indexBlog');
  Route::get($adminRoute . '/add-blog','Admin\BlogController@createBlog');
  Route::post($adminRoute . '/add-blog','Admin\BlogController@storeBlog');
  Route::get($adminRoute . '/edit-blog/{id}','Admin\BlogController@editBlog');
  Route::post($adminRoute . '/edit-blog/{id}','Admin\BlogController@updateBlog');
  Route::get($adminRoute . '/blog/{id}','Admin\BlogController@destroyBlog');

//For Blog Section
  Route::get($adminRoute . '/testimonial','Admin\TestimonialController@indexTestimonial');
  Route::get($adminRoute . '/add-testimonial','Admin\TestimonialController@createTestimonial');
  Route::post($adminRoute . '/add-testimonial','Admin\TestimonialController@storeTestimonial');
  Route::get($adminRoute . '/edit-testimonial/{id}','Admin\TestimonialController@editTestimonial');
  Route::post($adminRoute . '/edit-testimonial/{id}','Admin\TestimonialController@updateTestimonial');
  Route::get($adminRoute . '/testimonial/{id}','Admin\TestimonialController@destroyTestimonial');

  Route::get($adminRoute . '/faq','Admin\StaticPagesController@viewFAQList');
  Route::get($adminRoute . '/add-faq','Admin\StaticPagesController@createFAQ');
  Route::post($adminRoute . '/add-faq','Admin\StaticPagesController@storeFAQ');
  Route::get($adminRoute . '/edit-faq/{id}','Admin\StaticPagesController@editFAQ');
  Route::post($adminRoute . '/edit-faq/{id}','Admin\StaticPagesController@updateFAQ');
  Route::get($adminRoute . '/faq/{id}','Admin\StaticPagesController@destroyFAQ');

//START HERE MANAGE DOCTORS PANEL IN ADMIN SECTION 

  Route::get($adminRoute . '/doctor-panel','Admin\StaticPagesController@viewDoctorPanelList');
  Route::get($adminRoute . '/add-doctor-panel','Admin\StaticPagesController@createDoctorPanel');
  Route::post($adminRoute . '/add-doctor-panel','Admin\StaticPagesController@storeDoctorPanel');
  Route::get($adminRoute . '/edit-doctor-panel/{id}','Admin\StaticPagesController@editDoctorPanel');
  Route::post($adminRoute . '/edit-doctor-panel/{id}','Admin\StaticPagesController@updateDoctorPanel');
  Route::get($adminRoute . '/doctor-panel/{id}','Admin\StaticPagesController@destroyDoctorPanel');

 //END HERE MANAGE DOCTORS PANEL IN ADMIN SECTION 

 //START HERE MANAGE GALLERY PANEL IN ADMIN SECTION 

 // image section

  Route::get($adminRoute . '/image-gallery','Admin\GalleryController@viewImageGallerylList');
  Route::get($adminRoute . '/add-image-gallery','Admin\GalleryController@createImageGallery');
  Route::post($adminRoute . '/add-image-gallery','Admin\GalleryController@storeImageGallery');
  Route::get($adminRoute . '/edit-image-gallery/{id}','Admin\GalleryController@editImageGallery');
  Route::post($adminRoute . '/edit-image-gallery/{id}','Admin\GalleryController@updateImageGallery');
  Route::get($adminRoute . '/image-gallery/{id}','Admin\GalleryController@destroyImageGallery');
  Route::get($adminRoute . '/image-gallery-delete/{id}/{imgid}','Admin\GalleryController@deleteImgGallery');

// Video section

  Route::get($adminRoute . '/video-gallery','Admin\GalleryController@viewVideoGallerylList');
  Route::get($adminRoute . '/add-video-gallery','Admin\GalleryController@createVideoGallery');
  Route::post($adminRoute . '/add-video-gallery','Admin\GalleryController@storeVideoGallery');
  Route::get($adminRoute . '/edit-video-gallery/{id}','Admin\GalleryController@editVideoGallery');
  Route::post($adminRoute . '/edit-video-gallery/{id}','Admin\GalleryController@updateVideoGallery');
  Route::get($adminRoute . '/video-gallery/{id}','Admin\GalleryController@destroyVideoGallery');
  Route::post($adminRoute . '/change-order-status','Admin\SiteController@changeOrderStatus');

  Route::get('{page}/{subs?}', ['uses' => 'PageController@index'])
  ->where(['page' => '^(((?=(?!admin))(?=(?!\/)).))*$', 'subs' => '.*']);


 //END HERE MANAGE GALLERY PANEL IN ADMIN SECTION 




  Route::get($adminRoute . '/static-pages','Admin\StaticPagesController@viewPageList');
  Route::get($adminRoute . '/add-static-pages','Admin\StaticPagesController@createPages');
  Route::post($adminRoute . '/add-static-pages','Admin\StaticPagesController@storePages');
  Route::get($adminRoute . '/edit-static-pages/{id}','Admin\StaticPagesController@editPage');
  Route::post($adminRoute . '/edit-static-pages/{id}','Admin\StaticPagesController@updatePage');

  Route::get($adminRoute . '/funds-statement/all','Admin\MemberController@getUploads');
  Route::get($adminRoute . '/approved-funds','Admin\MemberController@getApprovedUploads');
  Route::get($adminRoute . '/funds-statement/funds','Admin\MemberController@getSearchedUploads');

  Route::get($adminRoute . '/resetpassword','Admin\MemberController@getResetPassword');

  Route::post($adminRoute.'/account/passwordupdate', ['as' => 'account.passwordUpdate', 'uses' => 'Admin\MemberController@passwordUpdateAccount']);
// Ecommerce front end

  Route::post($adminRoute.'/update-statement-status','Admin\MemberController@updateProofStatus');


  Route::get($adminRoute . '/member-list', 'Admin\SiteController@getAllMembers');

  Route::get($adminRoute . '/member-detail/{id}', 'Admin\SiteController@getMemberInfo');

  Route::get($adminRoute . '/personal-info/{id}', 'Admin\SiteController@getEditUser');
  Route::post($adminRoute . '/personal-info/{id}', 'Admin\MemberController@updateUserDetail')->name('admin.account.postUpdate');

  Route::get($adminRoute . '/bank/{id}', 'Admin\SiteController@getEditBankAccount');
  Route::post($adminRoute . '/bank/{id}', 'Admin\MemberController@updateUserBankDetail')->name('admin.bank.postUpdate');

  Route::get($adminRoute . '/password/{id}', 'Admin\SiteController@getResetPassword');
  Route::post($adminRoute . '/password/{id}', 'Admin\MemberController@updatePassword')->name('admin.password.postUpdate');
  Route::get($adminRoute . '/searched-members', 'Admin\SiteController@getSearchedMember');

  Route::post($adminRoute . '/delete/{id}', 'Admin\MemberController@deleteMember');

  Route::get($adminRoute . '/daily-income', ['as' => 'admin.income.direct', 'uses' => 'Admin\SiteController@getSearchedDirectPage']);

  Route::get($adminRoute . '/income-direct', ['as' => 'admin.income.direct', 'uses' => 'Admin\SiteController@getDirectPage']);

  Route::get($adminRoute . '/income-weekly', ['as' => 'admin.income.weekly', 'uses' => 'Admin\SiteController@getWeeklyPage']);

  Route::get($adminRoute . '/weekly-income', ['as' => 'admin.income.weekly', 'uses' => 'Admin\SiteController@getSearchedWeeklyPage']);

  Route::get($adminRoute . '/bonus-list', ['as' => 'admin.bonus.list', 'uses' => 'Admin\SiteController@getBonusPairingList']);

  Route::get($adminRoute . '/searched-bonus-list', ['as' => 'admin.bonus.list', 'uses' => 'Admin\SiteController@getSearchedBonusPairingList']);

  Route::get($adminRoute . '/download', 'Admin\BonusController@export');
  Route::get($adminRoute . '/download-excel', 'Admin\WithdrawController@export');

  Route::post($adminRoute . '/from-name', 'Admin\SiteController@checkFrom');

  Route::post($adminRoute . '/to-name', 'Admin\SiteController@checkTo');


// Route::get('index','EcommerceController@index');

  Route::get('returnpolicy','EcommerceController@getReturnPolicy');
  Route::get('/product-detail/{id}','EcommerceController@getProductDetail');
  Route::get('/product-list/{id}','EcommerceController@getProductList');

  Route::post('/product-detail/{id}','EcommerceController@submitReview');

  Route::get('/search-product','EcommerceController@getSearchProduct');

  Route::get('/search-product/productCat','EcommerceController@productsCat');

  Route::post('/index/add-to-cart','EcommerceController@addToCart');

  Route::post('/product-detail/{id}/add-cart','EcommerceController@addToCart');

  Route::post('/index/cart','EcommerceController@showToCart');

  Route::post('news','EcommerceController@submitNewsletter')->name('newsletter');

//----------------- Amaza M-BIZ ---------------------


  Route::get('contactUs','EcommerceController@getContact');
  Route::get('hello','EcommerceController@index');
  Route::get('aboutUs','EcommerceController@getAbout');
  Route::get('checkout','EcommerceController@getCheckout');
  Route::get('faq','EcommerceController@getFaq');
  Route::get('privacypolicy','EcommerceController@getPrivacyPolicy');
  Route::get('wishlist','EcommerceController@getWishList');
  Route::get('cart','EcommerceController@getCart');
  Route::get('tnc','EcommerceController@getTnc');




// Route::get('/index','EcommerceMbizController@index');
  Route::get('/about-us2','EcommerceMbizController@aboutUs');
  Route::get('/wishlist','EcommerceMbizController@getWishList');
  Route::get('/checkout','EcommerceMbizController@getCheckout');
  Route::get('/cart','EcommerceMbizController@getCart');

  Route::post('/add-to-cart','EcommerceMbizController@addToCart');
  Route::post('/add-cart','EcommerceMbizController@addProductToCart');
  Route::post('/cart/delete-product/{id}','EcommerceMbizController@deleteSessionData');

  Route::post('/category-wise/cart','EcommerceMbizController@addToCart');
  Route::post('/category-wise/wishlist','EcommerceMbizController@addToWishList');

  Route::post('/cart/increase/{id}/{quant}','EcommerceMbizController@increment');
  Route::post('/cart/decrease/{id}/{quant}','EcommerceMbizController@decrement');

  Route::post('/add-to-wishlist','EcommerceMbizController@addToWishList');

  Route::post('/add-quick-cart','EcommerceMbizController@addQuickList');

  Route::post('/wishlist/delete-product/{id}','EcommerceMbizController@deleteWishList');
  Route::post('/wishlist/delete-all','EcommerceMbizController@clearWishList');

  Route::post('/wishlist/add-all','EcommerceMbizController@addAllToCart');

  Route::get('/searchproduct','EcommerceMbizController@getSearchProduct');
  Route::get('/productdetail/{id}','EcommerceMbizController@getProductDetail');
  Route::post('/productdetail/{id}','EcommerceMbizController@submitReview');

  Route::post('/search-all','EcommerceMbizController@searchAll');

  Route::get('products/{id}','EcommerceMbizController@getCategoryProducts');

  Route::get('/search-product/productsCat','EcommerceMbizController@productsCat');

  Route::get('/search-product/price-filter','EcommerceMbizController@priceFilter');

  Route::get('/search/price-filter','EcommerceMbizController@priceFilterSearch');

  Route::get('/search-product/sort','EcommerceMbizController@sorting');

  Route::get('/search-product/sortproduct','EcommerceMbizController@sortingSearch');

  Route::get('/search-product/colored','EcommerceMbizController@getColorProduct');

  Route::get('/search-product/colored-product','EcommerceMbizController@getColoredSearchProduct');

  Route::get('/search-product/size','EcommerceMbizController@getSizedProduct');

  Route::get('/search-product/sized-product','EcommerceMbizController@getSizedSearchProduct');

  Route::get('/products/productsCat','EcommerceMbizController@categoryWiseProduct');

  Route::get('city/{id}','EcommerceMbizController@getCityList');

  Route::get('state/{id}','EcommerceMbizController@getStateList');

  Route::post('/cart/show','EcommerceMbizController@showMiniCart');

// Route::post('/cart/products','EcommerceMbizController@showMiniCartProducts');

// Route::get('/checkout-page/{id}','EcommerceMbizController@checkoutForm');
  Route::post('/checkout-page/{id}','EcommerceMbizController@checkoutForm');

  Route::post('/checkout-page/{id}/success','EcommerceMbizController@getSuccess');

  Route::post('/contact-us/form','EcommerceMbizController@sendContactUsForm');

// Start code franchise here

  Route::get($adminRoute . '/franchise-list', 'Admin\FranchiseController@getAllFranchise');
  Route::get($adminRoute . '/add-franchise','Admin\FranchiseController@getFranchiseRegister');
  Route::post($adminRoute . '/add-franchise','Admin\FranchiseController@storeFranchise');
  Route::get($adminRoute . '/edit-franchise/{id}','Admin\FranchiseController@editFranchise');
  Route::post($adminRoute . '/edit-franchise/{id}','Admin\FranchiseController@updateFranchise');
  Route::get($adminRoute . '/franchise-list/{id}','Admin\FranchiseController@destroyFranchise');
  Route::get($adminRoute . '/franchise-bank-details/{id}', 'Admin\FranchiseController@getFranchiseDetails');
  Route::post($adminRoute . '/franchise-bank-details/{id}', 'Admin\FranchiseController@updateFranchiseDetails');
  Route::get($adminRoute . '/franchise-password/{id}', 'Admin\FranchiseController@getResetPassword');
  Route::post($adminRoute . '/franchise-password/{id}', 'Admin\FranchiseController@updatePassword');

// Epin Generate in admin
  Route::get($adminRoute . '/pin-list', 'Admin\EpinAdminController@getAllEpin');
  Route::get($adminRoute . '/add-epin','Admin\EpinAdminController@getEpinCreate');
  Route::post($adminRoute . '/add-epin','Admin\EpinAdminController@storeEpin');
  Route::get($adminRoute . '/pin-list/{id}','Admin\EpinAdminController@destroyEpins');

  Route::get($adminRoute . '/epintransfer-list', 'Admin\EpinAdminController@getAllEpinTransfer');
  Route::get($adminRoute . '/add-epintransfer','Admin\EpinAdminController@getEpinTransferCreate');
  Route::post($adminRoute . '/add-epintransfer','Admin\EpinAdminController@storeEpinTransfer');
  Route::get($adminRoute . '/epintransfer-list/{id}','Admin\EpinAdminController@destroyEpinTransfer');


//Route::get($adminRoute . '/epin/add' , ['as' => 'admin.epin.add', 'uses' => 'Admin\EpinAdminController@showaddepinpage']);
//Route::post($adminRoute . '/epin/addepin', ['as' => 'admin.epin.addepin', 'uses' => 'Admin\EpinAdminController@createEpin']);


