<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Epoint extends Model
{
    protected $table = 'epoint';   
    protected $fillable =[
      'epoint',
      'member_id'
    ];
    protected $primaryKey = 'member_id';

    protected static $memberModel = 'App\Models\Member';

    public function member () {
        return $this->belongsTo(static::$memberModel, 'member_id');
    }
}
