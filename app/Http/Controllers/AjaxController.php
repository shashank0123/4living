<?php

namespace App\Http\Controllers;
use App\Contact;
use App\Models\Member;

use Illuminate\Http\Request;

class AjaxController extends Controller
{
     public function sendContactForm(Request $request ){
                $message = 'Something Went Wrong';
                $query = new Contact;
                $query->name = $request->name;
                $query->email = $request->email;    
                $query->subject = $request->subject;
                $query->message_content = $request->message;
                $query->response_status = $request->name;

                $query->save();
                if($query){
                    $message = "Thank you . We will reply you Soon.";
                }
                return response()->json(["message" => $message]);
            }
            
     public function getIndex(){
        return  view('index');
     }

     public function get4living(){
        return  view('4living');
     }

     public function getAbout(){
        return  view('about-us');
     }

     public function getDirectorsDesk(){
        return  view('directors-desk');
     }

     public function getDoctorConsult(){
        return  view('consult-doctors');
     }

     public function getContactUs(){
        return  view('contact-us');
     }

     public function getDrPrem(){
        return  view('dr-prem-prakash');
     }

     public function getTerms(){
        return  view('terms');
     }

     public function getPrivacyPolicy(){
        return  view('privacy');
     }

     public function getHumanBody(){
        return  view('humanbody');
     }

     public function getProductAyurveda(){
        return  view('products.ayurveda');
     }

     public function getProductNutritional(){
        return  view('products.nutrionals');
     }

     public function getProductSkinCare(){
        return  view('products.skin-care');
     }

     public function getProductTulsi(){
        return  view('products.tulsi');
     }

     public function getProductImsys(){
        return  view('products.imsys');
     }

     public function getProductArtho(){
        return  view('products.artho-sys');
     }

     public function getMadhumeghkalp(){
        return  view('products.madhumeghkalp');
     }

     public function getProductThyro(){
        return  view('products.thyro-sys');
     }

     public function getDoctorTeam(){
        return  view('doctors-team');
     }

     public function getAboutAyurveda(){
        return  view('about-ayurveda');
     }

     public function getRegister(){
        return  view('register');
     }

     public function getLogin(){
        return redirect()->route('login', ['lang' => \App::getLocale()]);
     }

     public function getResetPassword(){
        return  view('front.forgetPassword');
        }

     public function getClearCache(){
      $exitCode = Artisan::call('config:clear');
  $exitCode = Artisan::call('cache:clear');
  $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
}
     
}
