<?php

namespace App\Http\Controllers;
use App\Models\Withdraw;
use App\Models\BonusPairing;
use App\Models\BonusDirect;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\WithdrawRepository;

class WithdrawController extends Controller
{
    /**
     * The WithdrawRepository instance.
     *
     * @var \App\Repositories\WithdrawRepository
     */
    protected $WithdrawRepository;

    /**
     * Create a new WithdrawController instance.
     *
     * @param \App\Repositories\WithdrawRepository $WithdrawRepository
     * @return void
     */
    public function __construct(WithdrawRepository $WithdrawRepository) {
        $this->WithdrawRepository = $WithdrawRepository;
        $this->middleware('member');
    }

    /**
     * Member Withdraw Cash Point
     * @return [type] [description]
     */
    public function postMakeWithdraw () {
        $now = Carbon::now();
        $date = $now->day;

        // if ($date != '10' && $date != '25') {
        //     return \Response::json([
        //         'type'  =>  'error',
        //         'message'   =>  \Lang::get('error.wdDateError')
        //     ]);
        // }

        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;

        $bp = BonusPairing::where('member_id',$member->id)->sum('total');
$bd = BonusDirect::where('member_id',$member->id)->sum('total');
$wd = Withdraw::where('member_id',$member->id)->sum('amount');


$total = $bp+$bd - $wd;


if($total<100){
    return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.enoughcash')
            ]);
}

        if($data['amount']>2000 || $data['amount']<100){
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.limitError')
            ]);
        }

        if (is_null($member->detail->bank_name) || is_null($member->detail->bank_account_holder) || is_null($member->detail->bank_account_number)) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.bankError')
            ]);
        }

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        try {
            $check = Withdraw::where('member_id',$member->id)->first();
            if(!empty($check)){
                if($check->amount>=2000){
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  \Lang::get('error.nextDayLimit')
                    ]);
                }
                else{
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount']);
                }
            }
            else{
                 $this->WithdrawRepository->makeWithdraw($member, $data['amount']);
            }
           
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.withdrawSuccess')
        ]);
    }

    /**
     * Withdraw list - DataTable
     * @return [type] [description]
     */
    public function getList () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->WithdrawRepository->getList($member);
    }



     /*Export Data*/
    public function export(Request $request){   
    $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;    
        $withdraw=Withdraw::where('member_id',$member->id)->get();

        echo $withdraw;
        die;
       
        $tot_record_found=0;
        if(count($withdraw)>0){
            $tot_record_found=1;
            $i=1;

            //First Methos          
            $export_data="SNo,DATE,MEMBER ID,USERNAME,AMOUNT,PROMOTION\n";
            foreach($withdraw as $value){
                $date = explode(' ',$value->created_at);
                $export_data.=$i++ .','.$date[0].','. $value->member_id.','.$value->username.','.$value->amount_cash.','.$value->amount_promotion."\n";
            }
            return response($export_data)
                ->header('Content-Type','application/csv')               
                ->header('Content-Disposition', 'attachment; filename="download.csv"')
                ->header('Pragma','no-cache')
                ->header('Expires','0');                     
        }
        return view('download',['record_found' =>$tot_record_found]);    
    }
}
