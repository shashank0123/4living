<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TransferRepository;
use App\Repositories\MemberRepository;
use App\Models\Member;
use App\Models\TransferEpoint;
use App\Models\Epoint;

class TransferController extends Controller
{
    /**
     * The TransferRepository instance.
     *
     * @var \App\Repositories\TransferRepository
     */
    protected $TransferRepository;

    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * Create a new TransferController instance.
     *
     * @param \App\Repositories\TransferRepository $TransferRepository
     * @return void
     */
    public function __construct(
        TransferRepository $TransferRepository,
        MemberRepository $MemberRepository
    ) {
        $this->TransferRepository = $TransferRepository;
        $this->MemberRepository = $MemberRepository;
        $this->middleware('member');
    }

    /**
     * Transfer register, promo, or cash point
     * @return json
     */
    public function postTransferPoint () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        if (!$toMember = $this->MemberRepository->findByUsername(trim($data['to_username']))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.memberNotFound')
            ]);
        }
        
        try {
            $this->TransferRepository->makeTransfer($member, $toMember, $data['amount'], $data['type']);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.transferSuccess')
        ]);
    }

    /**
     * Transfer list - DataTable
     * @return [type] [description]
     */
    public function getList () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->TransferRepository->getList($member);
    }

    public function addEpointTransferStatement(Request $request){
        $message = "Something Went Wrong";
        $from = $request->sender_id;
        $to = Member::where('username',$request->to)->first();
        
        $epointSender = Epoint::where('member_id',$from)->orderBy('created_at','DESC')->first();

        if($epointSender){
        if($epointSender->epoint >= $request->epoint){

            $transfer = new TransferEpoint;
            $transfer->sender_id = $from;
            $transfer->receiver_id = $to->id;
            $transfer->no_of_epoint = $request->epoint;
            $transfer->save();

            if($transfer){
                $epointReceiver = Epoint::where('member_id',$to->id)->orderBy('created_at','DESC')->first();

                if(!$epointReceiver){
                    $epointReceiver = new Epoint;
                    $epointReceiver->epoint = $request->epoint;
                    $epointReceiver->member_id = $to->id;
                    $epointReceiver->save();                    
                }
                else{
                    $epointReceiver->epoint = $epointReceiver->epoint + $request->epoint;
                    $epointReceiver->update();
                }

                $epointSender->epoint = $epointSender->epoint - $request->epoint;
                $epointSender->update();

                $message = "Epins Transfer Successfully";
            }            
        }

        else{ 
            $message = "Not Enough Epins to Transfer";
        }
    }


    else{
        $message = "Not Enough Epins to Transfer";
    }
        
        return redirect()->back()->with('message',$message);
    }
}
