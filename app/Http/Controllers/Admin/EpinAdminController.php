<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Epin;
use App\Models\transferEpin;


class EpinAdminController extends Controller
{
	public function getAllEpin() {
					
        $epin = Epin::orderBy('created_at','DESC')->paginate(10);
        return view('back.epin.pin-list',compact('epin'));
    }
	
	
	
	
	public function getEpinCreate(){
		return view('back.epin.add-epin');
    
	}
	
	public function storeEpin(Request $request){
		
		$this->validate($request, [
       
       	'no_of_epins' => 'required|integer',
				
		]);
		$n=1;
        for ($i=0; $i < $request->no_of_epins; $i++) { 
			
			$generateepins = new Epin;
			$generateepins->epin_type = $request->epin_type;
			$generateepins->no_of_pins = $request->no_of_epins;
			if($request->epin_type == 'Purchase'){
				$generateepins->epin_type_id = $request->epin_type_id;
			}else{
				$generateepins->epin_type_id = '0';
			}
			
			$randtime = rand(1000000000, 9999999999);
			
			$nexid = strtoupper(substr($request->epin_type, 0, 1)).$request->package_id;
			
			$generateepins->epin = $randtime.$nexid.$n;
			$generateepins->package_id = $request->package_id;
			$generateepins->save();
			$n++;
			
		}         
			
    			
    	
		return redirect()->action('Admin\EpinAdminController@getAllEpin')->with('message','Data Inserted Successfully');
    	
    }
	
	
	
		
	/* 
	* function rout 'Admin\EpinAdminController@destroyEpins'
	* delete data through id
	*/
	public function destroyEpins($id)
    {
        $deletepins = Epin::find($id);
        $deletepins->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }
	
	public function getAllEpinTransfer() {
					
        $transferepin = TransferEpin::orderBy('created_at','DESC')->paginate(10);
        return view('back.epin.epintransfer-list',compact('transferepin'));
    }
	
	
	
	public function getEpinTransferCreate(){
		return view('back.epin.add-epintransfer');
    
	}
	
	public function storeEpinTransfer(Request $request){
		
		$this->validate($request, [
       
       	'no_of_epins' => 'required|integer',
				
		]);
		
		$epinlist = Epin::where('package_id', $request->package_id)->where('status', 'Available')->paginate($request->no_of_epins);
		
		if(count($epinlist) == $request->no_of_epins){
			
			foreach($epinlist as $key => $val){
				$transferepins = new TransferEpin;
				$transferepins->receiver_id = $request->receiver_id;
				$transferepins->epin_id = $val['id'];
				$transferepins->epin = $val['epin'];
				$transferepins->epin_type = $val['epin_type'];
				$transferepins->epin_type_id = $request->package_id;
				$transferepins->no_of_epins = $request->no_of_epins;
				
				$transferepins->save();
				
				$updateFranchise = Epin::where('id', $val['id'])->update(['status' => 'Used']);
				
				
			}
		    
			
		}else{
			return redirect()->action('Admin\EpinAdminController@getAllEpinTransfer')->with('message','Data not Inserted Successfully');
			
			exit;
		}
			
    			
    	
		return redirect()->action('Admin\EpinAdminController@getAllEpinTransfer')->with('message','Data Inserted Successfully');
		exit;
    	
    }
	
	
	/* 
	* function rout 'Admin\EpinAdminController@destroyEpinTransfer'
	* delete data through id
	*/
	public function destroyEpinTransfer($id)
    {
        $deletepins = TransferEpin::find($id);
        $deletepins->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }
	
	
	public function showaddepinpage()
	{
		 return view('back.epin.addEpin');
	}

	public function createEpin(Request $request)
	{
		$data = $request->data;
		$member_id = $data['member_id'];
		$package_id = $data['package_id'];
		$no_of_epins = $data['no_of_epins'];
		$createepins = new TransferEpin;
		$createepins->sender_id = 'admin';
		$createepins->receiver_id = $member_id;
		$createepins->no_of_epins = $no_of_epins;
		$createepins->save();

		for ($i=1; $i < $no_of_epins; $i++) { 
			$generateepins = new Epin;
			$generateepins->member_id = $member_id;
			$generateepins->epin = time().$member_id.$i;
			$generateepins->package_id = $package_id;
			$generateepins->save();
		}
		return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.epincreation'),
        ]);
		//epin created by admin will also be saved in the transfer epin table.
		var_dump($member_id);
		die();
	}
	public function buyEpin(Request $request)
	{
		$agent_id = $_SESSION['uid']['id'];   
		$epin_package_info = $_POST['epin_package'];
		$paymentMode = $_POST['payment'];
		$no_of_epin = $_POST['no_of_epin'];
		$types = explode('&', $epin_package_info);
		$epinType = $types[0];
		$amount = $types[1]; 
		$epin_id = purchaseEpin($con, $agent_id, $paymentMode, $no_of_epin, $epinType, $amount, 0);
		if($epin_id != 0) { 
			$prize = epinPrize($amount);
			$insertPrizeQr = "INSERT into epin_prize(epin_id,agent_id,rewards) values ('$epin_id','$agent_id','$prize')";
			$insertPrizeRs = mysqli_query($con, $insertPrizeQr);
			$franchiseIncome = ($no_of_epin + 10) * $no_of_epin;
			$insertEpinIncomeQr = "INSERT into epin_franchise_income(epin_id,agent_id,income,left_income) values ('$epin_id','$agent_id','$franchiseIncome','$franchiseIncome')";
			$insertEpinIncomeRs = mysqli_query($con, $insertEpinIncomeQr);
			echo "<script>document.location='manageEpins.php?m=All epin were successfully inserted.';</script>"; 
		} else {
			$msg = "Only_".$count."_were_inserted._Contact_support_with_your_payment_details";
			echo "<script>document.location='manageEpins.php?m=$msg;</script>"; 
		}
	} 

	function purchaseEpin($con, $agent_id, $paymentMode, $no_of_epin, $epinType, $amount, $discount) { 
		$sql = "INSERT into user_epin(user_id,no_of_epins,epin_type,total_amount, discount,payment_mode,status) values ('$agent_id','$no_of_epin','$epinType','$amount','$discount','$paymentMode',1)";
		$rs = mysqli_query($con, $sql);
		if($rs) {
			$epinResult = mysqli_query($con, "SELECT id FROM user_epin WHERE user_id='$agent_id'");
			$epin_id = 0;
			while($row = mysqli_fetch_array($epinResult)) {
				$epin_id = $row['id'];
			}
			$count = 0;
			for ($i=0; $i < $no_of_epin; $i++) {
				$epin = time().$agent_id.$i;
				$epin = $epin++;
				$insertEpins = "insert into all_epins (user_id, epin_no, epin_id) values ('$agent_id', '$epin', '$epin_id')";
				$result = mysqli_query($con, $insertEpins);
				if ($result) {
					$count++;
				}
			}
			$msgQr = "SELECT * from epin_message where status='1' AND buyer_id='$agent_id'";
			$msgRs = mysqli_query($con, $msgQr);
			$msgResult = mysqli_fetch_array($msgRs);
			if($msgResult) {
				$msgNoOfEpin = $msgResult['no_of_epin'];
				if($msgNoOfEpin >= $no_of_epin) {
					$upMsgQr = "UPDATE epin_message SET `status`= '2', epin_id='$epin_id' where buyer_id='$agent_id'";
					$upMsgRs = mysqli_query($con, $upMsgQr);
				}
			}
			return $epin_id;
		}
		return 0;
	}
	
	
	
	
	
	

}

