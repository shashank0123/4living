<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\WithdrawRepository;
use App\Repositories\MemberRepository;
use App\Models\Withdraw;
use App\Models\MemberDetail;

class WithdrawController extends Controller
{
	/**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\WithdrawRepository
     */
    protected $WithdrawRepository;

    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    public function __construct(
        WithdrawRepository $WithdrawRepository,
        MemberRepository $MemberRepository
    ) {
        $this->middleware('admin');
        $this->WithdrawRepository = $WithdrawRepository;
        $this->MemberRepository = $MemberRepository;
    }

    /**
     * Datatable member admin
     * @return object
     */
    public function getList () {
        return $this->WithdrawRepository->findAll(true);
    }

    public function getPendingWithdraws(){
        $withdraw = Withdraw::where('status','process')->get();
        $status = 'Pending';
        return view('back.withdraw.statused_statements',compact('withdraw','status'));
    }
    
    public function getApprovedWithdraws(){
            $withdraw = Withdraw::where('status','done')->get();
        $status = 'Approved';
        return view('back.withdraw.statused_statements',compact('withdraw','status'));
    }

    public function getSearchedWithdraws(Request $request){
        $status = $request->status;
        echo $request->status;
        if($status)
        echo $request->search;
        die;
            $withdraw = Withdraw::where('status',$status)->orWhere('username','LIKE',$request->searched_username)->orWhere('amount','LIKE',$request->searched_username)->get();
        
        return view('back.withdraw.statused_statements',compact('withdraw','status'));
    }
    /**
     * Detail for modal
     * @param integer $id
     * @return html
     */
    public function getShowModal ($id) {
        if (!$model = $this->WithdrawRepository->findById(trim($id))) {
            return 'Withdraw not found.';
        }
        return view('back.withdraw.show')->with('model', $model);
    }

    /**
     * Get edit Withdraw
     * @param  integer $id
     * @return html
     */
    public function getEdit ($id) {
        if (!$model = $this->WithdrawRepository->findById(trim($id))) {
            return redirect()->back()->with('flashMessage', [
                'class' =>  'danger',
                'message' => 'Withdraw not found.'
            ]);
        }
        return view('back.withdraw.edit')->with('model', $model);
    }

    /**
     * Add statement
     * @return json
     */
    public function postAdd () {
        $data = \Input::get('data');
        if (!$target = $this->MemberRepository->findByUsername(trim($data['username']))) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Member not found.'
            ]);
        }

        try {
            $this->WithdrawRepository->store([
                'member_id' => $target->id,
                'username' => $target->username,
                'status' => $data['status'],
                'amount' => (float) $data['amount'],
                'admin' => (float) $data['amount']/10
            ]);
        } catch (\Exception $e) {
            return \Response::json([
                'type' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        return \Response::json([
            'type'  =>  'success',
            'message' => 'Withdraw statement added.'
        ]);
    }

    /**
     * Update shares settings
     * @param integer $id
     * @return json
     */
    public function postUpdate ($id) {
        $data = \Input::get('data');
        if (!$model = $this->WithdrawRepository->findById(trim($id))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  'Withdraw not found.'
            ]);
        }

        $this->WithdrawRepository->update($model, $data);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Withdraw #' . $model->id . ' updated.'
        ]);
    }

    /**
     * Remove transfer statement
     * @param integer $id
     * @return json
     */
    public function postDelete ($id) {
        $model = new \App\Models\Transfer;
        $model->where('id', trim($id))->delete();

        return \Response::json([
            'type' => 'success',
            'message' => 'Deleted Successfully.'
        ]);
    }

    public function withdrawDelete ($id) {
        $model = new \App\Models\Withdraw;
        $model->where('id', trim($id))->delete();

        return \Response::json([
            'type' => 'success',
            'message' => 'Deleted Successfully.'
        ]);
    }

     /*Export Data*/
    public function export(Request $request){   
     
        $withdraw = Withdraw::where('status','process')->get();

        
        $tot_record_found=0;
        if(count($withdraw)>0){
            $tot_record_found=1;
            $i=1;

            //First Methos          
            $export_data="SNo,Transaction Type (N – NFET; R – RTGS;I-HDFC to HDFC),Beneficiary Code * (Inter - HDFC to HDFC),Beneficiary Account Number,Instrument Amount,Beneficiary Name (Upto 40 character withput any special character),Customer Reference Number(Any alpha numeric character upto 20), Chq / Trn Date (DD/MM/YYYY),IFSC Code,Bene Bank Name( Not Madatory),Bene Bank Branch Name( Not Madatory)\n";
            foreach($withdraw as $value){

                $getInfo = MemberDetail::where('member_id',$value->member_id)->first();
                $tds = $value->amount/20;
                $admin = $value->amount/10;
                $amount = $value->amount-$tds-$admin;

                $date = explode(' ',$value->created_at);
                $export_data.=$i++ .',N,,'.$getInfo->bank_account_number.','.$amount.','.$getInfo->bank_account_holder.',,'.$date[0].','.$getInfo->ifsc.','.$getInfo->bank_name.','.$value->bank_branch."\n";

                $value->status  = 'done';
                $value->update();
            }

            return response($export_data)
                ->header('Content-Type','application/csv')               
                ->header('Content-Disposition', 'attachment; filename="download.csv"')
                ->header('Pragma','no-cache')
                ->header('Expires','0');                     
        }
        return view('download',['record_found' =>$tot_record_found]);    
    }
}
