<?php

namespace App\Http\Controllers\Admin;

use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Epoint;
use App\Models\TransferEpoint;

class EpointAdminController extends Controller
{
	public function showaddepointpage (){
		return view('back.epoint.addEpoint');

	}
	public function createEpoint(Request $request)
	{  
		$data = $request->data;
		$member_id = $data['member_id'];
		$no_of_epoint = $data['no_of_epoint'];
		$createepoint = new TransferEpoint;
		$createepoint->sender_id = 'admin';
		$createepoint->receiver_id = $member_id;
		$createepoint->no_of_epoint = $no_of_epoint;
		$createepoint->save();

		$epoints = Epoint::where('member_id',$member_id)->first();
		if ($epoints){
			$epoints->epoint += $no_of_epoint;
			$epoints->update();
		}
		else{
			$epoints = new Epoint;
			$epoints->member_id = $member_id;
			$epoints->epoint = $no_of_epoint;
			$epoints->save();
		}
		return \Response::json([
			'type'  => 'success',
			'message' => 'Epins Added',
		]);
	}
	public function showallepoints() {
		$members = Member::all();		
		$epoints = TransferEpoint::all();
		return view('back.epoint.allepoint',compact('members','epoints'));
	}

	public function showSearchedEpoints(Request $request) {
		$search = $request->search;
		$members = "";
		$epoints = "";
		$members = Member::where('username',$search)->first();
		if(!empty($members))
		$epoints = TransferEpoint::where('receiver_id',$members->id)->get();
		
		return view('back.epoint.allepoint',compact('members','epoints'));
	}

	
}
