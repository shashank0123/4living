<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\Leads;
use App\User;
use App\Payment;
use App\Models\Member;
use App\RegistrationSponsorId;
use App\MainCategory;
use App\Jobs\SharesAfterRegisterJob;
use App\Jobs\NetworkAfterRegisterJob;
use Mail;
use StdClass;
use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SharesRepository;
use App\Repositories\BonusRepository;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

class NewRegController extends BaseController
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;

    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $BonusRepository;

    /**
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        PackageRepository $PackageRepository,
        SharesRepository $SharesRepository,
        BonusRepository $BonusRepository
    ) {
        $this->MemberRepository = $MemberRepository;
        $this->PackageRepository = $PackageRepository;
        $this->SharesRepository = $SharesRepository;
        $this->BonusRepository = $BonusRepository;
    }
    public function Register(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->username = "";
        // $response->message = 'Something went wrong';
        $response->message = '';
        $lead = new Leads;
        
        // Setup the validator
$rules = array('mobile' => 'required|regex:/[0-9]{10}/', 'email' => 'required|email');
$validator = Validator::make(Input::all(), $rules);


// Validate the input and return correct response
if ($validator->fails())
{
    return \Response::json([
        'success' => 'false',
        'type' => 'error',
        'message' => $validator->getMessageBag()->toArray()

    ]); // 400 being the HTTP code for an invalid request.
}
// return Response::json(array('success' => true), 200);
        $data = array();
        $data['package_id'] = 1;
        $data['email']  = $request->email;
        // $data['username']= $request->username;
        $data['name']= $request->fullname;
        $data['username'] = strtoupper(substr($data['name'],0,1)).rand(999999, 1000000000);
        $data['password'] =  $request->password;        
        $data['direct_id'] = strtolower($request->referal_id);
        $data['parent_id'] = strtolower($request->referal_id);
        $data['phone'] = $request->mobile;            
        $data['secret_password'] = 'asd123';
        //$data['position'] = $request->placement;
        $data['position'] = 1;
        $data['pan'] = $request->pan;
        $data['address'] = $request->address;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['district'] = $request->district;
        $data['pin'] = $request->pin;
        $data['dob'] = $request->dob;
        


        $repass = $request->cpassword;
        $spass = $request->spassword;

        if (!$package = $this->PackageRepository->findById($data['package_id'])) {

            return \Response::json([
                'type' => 'error',
                'message' => 'Package not found.'
            ]);
        }
        
        try {
            if ($check = $this->MemberRepository->findByUsername($data['username'])) {
                throw new \Exception('Username already exists.', 1);
                return false;
            }
            if($repass == $data['password']){

                $user = \Sentinel::registerAndActivate([
                    'email'   => $data['email'],
                    'username'  =>  $data['username'],
                    'password'  =>  $data['password'],
                    'phone'  =>  $data['phone'],
                    'is_ban' => '1',
                    'permissions' =>  [
                        'member' => true,
                    ]
                ]);
            if (1) { // lower member
                if (!$direct = $this->MemberRepository->findByUsername($data['direct_id'])) {
                    throw new \Exception('Member upline not found.', 1);
                    return false;
                }

                if (!$parent = $this->MemberRepository->findByUsername($data['parent_id'])) {
                    throw new \Exception('Member binary not found.', 1);
                    return false;
                }

                $data['parent_id'] = $this->MemberRepository->findParentId($direct->id);
                // dd($data['parent_id']);

                $member = $this->MemberRepository->saveModel(new \App\Models\Member, [
                    'username'  =>  $user->username,
                    'name'  =>  $data['name'],
                    'referral_id'  =>  $user->username,
                    'register_by' => $direct->username,
                    'secret_password' =>  $data['secret_password'],
                    'user_id'   =>  $user->id,
                    'package_id'    =>  $package->id,
                    'direct_id' =>  $direct->id,
                    'parent_id' =>  $direct->id,
                    'root_id'   =>  ($parent->position == $data['position']) ? $parent->root_id : $parent->id,
                    'level'     =>  $parent->level + 1,
                    'direct_percent'    =>  $package->direct_percent,
                    'pairing_percent'   =>  $package->pairing_percent,
                    'group_level'   =>  $package->group_level,
                    'max_pair'  =>  $package->max_pair,
                    'max_pairing_bonus' =>  $package->max_pairing_bonus,
                    'original_amount'  =>  $package->package_amount,
                    'package_amount' =>  $package->package_amount,
                    'position'  =>  $data['position'],
                    'password'  =>  $data['password'],
                ]);

                $this->MemberRepository->saveModel(new \App\Models\MemberWallet, [
                    'member_id' =>  $member->id,
                    'register_point'    =>  0,
                    'purchase_point'    =>  $package->purchase_point,
                    'promotion_point'   =>  0,
                    'cash_point'    =>  0,
                    'md_point'  =>  0
                ]);

                if (env('APP_ENV') == 'local') { // local
                    // $wallet = $member->wallet;
                    $this->MemberRepository->addNetwork($member);
                    // $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
                } else { // production
                    // dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
                   $this->MemberRepository->addNetwork($member);
                    // dispatch(new NetworkAfterRegisterJob($member))->onQueue('queue-network-register');
               }

                // remove cache for network
               \Cache::forget('member.' . $parent->id . '.children');

               $this->BonusRepository->calculateDirect($member, $direct);
                // $this->BonusRepository->calculateOverride($member);
           }

           $this->MemberRepository->saveModel(new \App\Models\MemberDetail, [
            'member_id' =>  $member->id,
            'mobile_phone' => $data['phone'],
            'address' => $data['address'],
            'state' => $data['state'],
            'city' => $data['city'],
            'district' => $data['district'],
            'pan' => $data['pan'],
            'pin_code' => $data['pin'],
            'date_of_birth' => $data['dob'],
        ]);

           $this->MemberRepository->saveModel(new \App\Models\MemberShares, [
            'member_id' =>  $member->id,
            'amount'    =>  0,
            'share_limit'   =>  $package->share_limit,
            'max_share_sale'    =>  $package->max_share_sale
        ]);

            $lead->username = $data['username']; 
            $lead->email = $request->email; 
            $lead->phone = $request->mobile; 
            $lead->otp      = rand(100000, 999999);
            $lead->otp_time = date('Y-m-d H:m:s') ;
            $lead->save();

            $mobile = $lead->phone;
            $message = "Your%20OTP%20for%20LivinG%20Organic%20is%20$lead->otp";
            // $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
            $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=FRLVNG&dest=$mobile&msg=$message&priority=1"; 
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);
            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) {
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }
        //   if($lead){
        //     $response->message ="Registerd Successfully";
        //     $username = $data['username'];
        //     $password = $data['password'];
        //     $email = $data['email'];
        //     $sponsor_id = $data['direct_id'];
        //     $link = "<a href='http://www.4living.in/en/login'>http://www.4living.in/en/login</a>";
        //     $mobile = $data['phone'];
        //     $message = "Welcome%20to%204LivinG.%20Your%20ID%20is%20%3A%2D%20$username%20&%20Password%20%2D%20$password.%20Login%20at%20$link";

        //     $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20120003&pass=123456&send=PROMO&dest=$mobile&msg=$message&priority=1";
        //     // $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 

        //     $c = curl_init();
        //     curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        //     curl_setopt($c,CURLOPT_HTTPGET ,1);

        //     curl_setopt($c, CURLOPT_URL, $url);
        //     $contents = curl_exec($c);
        //     if (curl_errno($c)) { 
        //         echo 'Curl error: ' . curl_error($c);
        //     }
        //     else{
        //         curl_close($c);
        //     }


            //Send New user Registered mail to Sdmin

            // $content = array('Email'=>$email, 'Sponsor ID' => $sponsor_id, 'Username' => $username, 'Mobile' => $mobile);
            // Mail::send('mail', $content, function($message) use ($email, $username, $mobile, $sponsor_id)
            // {   
            //     $message->from('no-reply@help2humancf.com', 'No-Reply');
            //     $message->to('admin@help2humancf.com', 'Admin')->subject('New Member Registered');
            // });


            // return response()->json($response);
            // exit();
        // }

        

            $response->message = 'Registerd Successfully';
            $response->username = $data['username'];
    }
} catch (\Exception $e) {
    
    return \Response::json([
        'type'  =>  'error',
        'message'   =>  $e->getMessage()
    ]);
}
return response()->json($response);

    // return \Response::json([
    //     'type'  => 'success',
    //     'message' => \Lang::get('message.registerSuccess'),
    //     'redirect' => route('/', ['lang' => \App::getLocale()])
    // ]);
}

public function verifyotp(Request $request)
{
    // $sponsored = new RegistrationSponsorId;
    $response = new StdClass;
    $response->status = 200;
    $response->message = 'Something went wrong';
    
    
    $findUser = Leads::where('username',$request->username)->orderBy('id', 'DESC')->first();
    
    // echo $findUser;
    // $sponsoredId = $request->sponsoredId;
    // $sponsoredId = strtoupper($sponsoredId);

    if($findUser->verify_otp != 'verified')
    {
        if($findUser->otp == $request->otp) {                
            $response->message = 'Registered Successfully.';
            $response->user_id = $findUser->id;
            $findUser->verify_otp ='verified';
            $findUser->update();
            // $sponsored->user_id = $findUser->id;
            // $sponsored->sponsor_id = $sponsoredId;
            // $sponsored->save();
            
            
              // $link = "<a href='http://www.4living.in/en/login'>http://www.4living.in/en/login</a>";
            
           $mobile = $findUser->phone;
           $user = Member::where('username',$findUser->username)->first();
           $change = User::where('id',$user->user_id)->first();
           $change->is_ban = '0';
           $change->update();
           
           $name = strtoupper($user->name);
            $username = $user->username;
            $password = $user->password;
             $message = "Welcome%20$name%20to%204LivinG%2E%20Your%20Customer%20Registration%20ID%20is%20$username%20%26%20Password%20is%20$password%2E%20For%20more%20details%20logon%20to%20www%2E4living%2Ein%2E%20Thank%20You%2E";
            $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=FRLVNG&dest=$mobile&msg=$message&priority=1"; 
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);
            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) {
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }
            
            return response()->json($response);

        }
        else {    
            $response->message = "Wrong OTP";
            return response()->json($response);
        }
    }
    else
    {
        $response->message = "Already Verified";
        
        return response()->json($response);
    }        
}

public function checkoutForm(Request $request,$id,$package)
{      
    $lead = Leads::where('id',$id)->first();
    return view('membership-payment',compact('lead','package'));
}

public function checkoutForm1(Request $request,$id){
    $success = "";
    if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
        if(strcasecmp($contentType, 'application/json') == 0){
            $data = json_decode(file_get_contents('php://input'));
            $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
            $json=array();          
            $json['success'] = $hash;
            echo json_encode($json);        
        }
        exit(0);
    }

}

public function getSuccess(Request $request){

    $post_data = $_POST;  

    $user_email = $post_data['email'];
    $status = $post_data['txnStatus'];
    $txnid = $post_data['txnid'];


    $payment = Payment::where('txnid', $post_data['txnid'])->first();


    if($status == 'SUCCESS'){

        $lead = Leads::where('email',$user_email)->first();
        $lead->payment_status = 'success';
        $lead->update();
        if (isset($payment->user_id) && $payment->user_id){
            $user = Member::where('user_id',$payment->user_id)->first();
            $user->payment_amount = $post_data['amount'];
            $user->update();
        }
    }    

    // $data = array('Email'=>$user_email, 'TransactionID'=>$txnid, 'Payment_Status'=>$status);
    // Mail::send('mail', $data, function($message) use ($user_email, $txnid, $status)
    // {   
    //     $message->from('no-reply@abc.in', 'Mailer');
    //     $message->to('Admin@abc.in', 'Admin')->subject('Successfull Registration');
    // });



    return redirect('/en');
}


}
