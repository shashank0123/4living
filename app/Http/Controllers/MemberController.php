<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\NetworkAfterRegisterJob;
use App\Jobs\SharesAfterRegisterJob;
use App\Repositories\MemberRepository;
use App\Repositories\SharesRepository;
use App\Repositories\PackageRepository;
use App\Models\Epoint;
use App\Models\Epin;
use Mail;
use App\Leads;
use App\User;
use DB;
use StdClass;
use Hash;
use App\Models\Member;
use App\UploadProof;

class MemberController extends Controller
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;

    /**
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        SharesRepository $SharesRepository
    ) {
        $this->MemberRepository = $MemberRepository;
        $this->SharesRepository = $SharesRepository;
        $this->middleware('member', ['except' => ['postLogin']]);
    }

    /**
     * Login
     * @return [type] [description]
     */
    public function postLogin () {

        $data = \Input::get('data');
       
        try {
            $getid = explode('SR100', $data['username']);
			 
            if (isset($getid[1])){
                $user = \Sentinel::authenticate([
                    'id'  =>  $getid[1],
                    'password'  =>  $data['password'],
                ], (isset($data['remember'])));

            }
            else{
                $user = \Sentinel::authenticate([
                    'username'  =>  $data['username'],
                    'password'  =>  $data['password'],
                ], (isset($data['remember'])));
            }
            if (!$user) {
                throw new \Exception(\Lang::get('error.loginError'), 1);
                return false;
            }
            
            $permissions = $user->permissions;
			
            if (!isset($permissions['member'])) {
                throw new \Exception(\Lang::get('error.userError'), 1);
                return false;
            } else if ($permissions['member'] != 1) {
                throw new \Exception(\Lang::get('error.userError'), 1);
                return false;
            }

            // if ($user->is_ban || $user->member->package_amount==0) {
            if ($user->is_ban) {
                throw new \Exception(\Lang::get('error.userBan'), 1);
                return false;
            }
        } catch (\Exception $e) {
            \Sentinel::logout();
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        return \Response::json([
            'type'      =>  'success',
            'message'   =>  \Lang::get('message.loginSuccess'),
            'redirect'  =>  route('memberhome', ['lang' => \App::getLocale()])
        ]);
    }

    /**
     * Register new member
     * @return [type] [description]
     */
    public function postRegisterOtp(){
        $data = \Input::get('data');
        // $sponsored = new RegistrationSponsorId;
    $response = new StdClass;
    $response->status = 200;
    $response->message = 'Something went wrong';
    
    $findUser = Leads::where('username',$data['username'])->orderBy('id', 'DESC')->first();
    // echo $findUser;
    // $sponsoredId = $request->sponsoredId;
    // $sponsoredId = strtoupper($sponsoredId);

    if($findUser->verify_otp != 'verified')
    {
        if($findUser->otp == $data['otp']) {                
            $response->message = 'Registered Successfully.';
            $response->user_id = $findUser->id;
            $findUser->verify_otp ='verified';
            $findUser->update();
            // $sponsored->user_id = $findUser->id;
            // $sponsored->sponsor_id = $sponsoredId;
            // $sponsored->save();
            
            
            
           $mobile = $findUser->phone;
           $user = Member::where('username',$findUser->username)->first();
           $change = User::where('id',$user->user_id)->first();
           $change->is_ban = '0';
           $change->update();
           
           $name = strtoupper($user->name);
            $username = $user->username;
            $password = $user->password;
             $message = "Welcome%20$name%20to%204LivinG%2E%20Your%20Customer%20Registration%20ID%20is%20$username%20%26%20Password%20is%20$password%2E%20For%20more%20details%20logon%20to%20www%2E4living%2Ein%2E%20Thank%20You%2E";
            $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=FRLVNG&dest=$mobile&msg=$message&priority=1"; 
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);
            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) {
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }
            
             return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.registerSuccess'),
            'redirect' => route('member.registerSuccess', ['lang' => \App::getLocale()])
        ]);

        }
        else {    
            $response->message = "Wrong OTP";
            return response()->json($response);
        }
    }
    else
    {
        $response->message = "Already Verified";
        
        return response()->json($response);
    }        
        
    } 
     
    public function postRegister () {
        $data = \Input::get('data');
        
        $data['username'] = strtoupper(substr($data['name'],0,1)).rand(999999, 1000000000);
        
        $data['package_id'] = 1;
		
        $data['phone'] = $data['mobile_phone'];
        $response = new StdClass;
        $user = \Sentinel::getUser();
        $currentMember = $user->member;

        // if (!isset($data['terms'])) {
        //     return \Response::json([
        //         'type'  =>  'error',
        //         'message'   =>  \Lang::get('error.termsError')
        //     ]);
        // }        

        try {
            $member = $this->MemberRepository->register($data, $currentMember);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }
        
        if (env('APP_ENV') == 'local') { // local
            $wallet = $member->wallet;
            $this->MemberRepository->addNetwork($member);
            $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
        } else { // production
            $wallet = $member->wallet;
            $this->MemberRepository->addNetwork($member);
			 $this->MemberRepository->addReferalCode($member);
            // dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
            // dispatch(new NetworkAfterRegisterJob($member))->onQueue('queue-network-register');
        }
        
        



        $response->message ="Registerd Successfully";
            /* $username = $data['username'];
            $password = $data['password'];
            $email = $data['email'];
            $sponsor_id = $data['direct_id'];
            $mobile = $data['phone'];
            $message = "Congratulations.%20You%20are%20successfully%20registered%20for%20HELP2HUMANCF.%20Your%20login%20details%20are%20%3A%2D%20USERNAME%20%3A%2D%20$username%20Password%20%2D%20$password";
            $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
            
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);

            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) { 
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }


            $content = array('Email'=>$email, 'Sponsor ID' => $sponsor_id, 'Username' => $username, 'Mobile' => $mobile);
            Mail::send('success_mail', $content, function($message) use ($email, $username, $mobile, $sponsor_id)
            {   
                $message->from('no-reply@4living.com', 'No Reply');
                $message->to('admin@4living.com', 'Admin')->subject('New Member Registered');
            });
			*/

        \Cache::forget('member.' . $user->id);
        session(['last_member_register' => $member]);
        
        $lead = new Leads;
        $lead->username = $member->username; 
            $lead->email = $data['email']; 
            $lead->phone = $data['mobile_phone']; 
            $lead->otp      = rand(100000, 999999);
            $lead->otp_time = date('Y-m-d H:m:s') ;
            $lead->save();

            $mobile = $lead->phone;
            $message = "4LivinG%20one%20time%20Verification%20OTP%20is%20$lead->otp";
            // $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
            $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=FRLVNG&dest=$mobile&msg=$message&priority=1"; 
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);
            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) {
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }

        return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.registerSuccess'),
            'redirect' => route('member.registerotp', ['lang' => \App::getLocale()]),
            'password' => $data['password']
        ]);
    }


    /**
     * Update Account Settings
     * @return [type] [description]
     */
    public function postUpdateAccount (Request $request) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;

        // if($data['bank_account_number'] != $data['confirm_account']) {
        //     return \Response::json([
        //             'type'  =>  'error',
        //             'message'   =>  \Lang::get('error.bankAccountError')
        //         ]);
        // }

        if (isset($data['s'])) {
            if ($member->secret_password != $data['s']) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.securityPasswordError')
                ]);
            }
        }

        $detail = $member->detail;
        $userData = [];

        // if (isset($data['password'])) {
        //     if ($data['password'] != '') {
        //         $userData['password'] = $data['password'];
        //     }
        // }

        if (isset($data['first_name'])) {
            if ($data['first_name'] != '') {
                $userData['first_name'] = $data['first_name'];
            }
        }

        if (count($userData) > 0) {
            \Sentinel::update($user, $userData);
        }

        foreach ($detail->getAttributes() as $k => $d) {
            if (isset($data[$k])) $detail->{$k} = $data[$k];
        }

        if (isset($data['secret_password'])) {
            if ($data['secret_password'] != '') {
                $member->secret_password = $data['secret_password'];
            }
        }
       // echo $detail['member_id'];
       // echo $user['first_name'];
        // echo "<pre>";
        // $name = $request->first_name;
        // echo $name;
       // print_r($user);
        // $image = $request->file('profimage');
        // $name = $image->getClientOriginalExtension();
        // $destinationPath = public_path('images');
        // $image->move($destinationPath, $name);
        // var_dump(\Input::hasFile('first_name'));
        //die();
        // if (\Input::hasFile('profimage')) {
        //  $file = \Input::file('profimage');
        //  $destination = 'images'.'/';
        //  $ext= $file->getClientOriginalExtension();
        //  $mainFilename = str_random(6).date('h-i-s');
        //  $file->move($destination, $mainFilename.".".$ext);
        //  echo "uploaded successfully";
        // }


//         $indian_states = array (
//  'AP' => 'Andhra Pradesh',
//  'AR' => 'Arunachal Pradesh',
//  'AS' => 'Assam',
//  'BR' => 'Bihar',
//  'CT' => 'Chhattisgarh',
//  'GA' => 'Goa',
//  'GJ' => 'Gujarat',
//  'HR' => 'Haryana',
//  'HP' => 'Himachal Pradesh',
//  'JK' => 'Jammu & Kashmir',
//  'JH' => 'Jharkhand',
//  'KA' => 'Karnataka',
//  'KL' => 'Kerala',
//  'MP' => 'Madhya Pradesh',
//  'MH' => 'Maharashtra',
//  'MN' => 'Manipur',
//  'ML' => 'Meghalaya',
//  'MZ' => 'Mizoram',
//  'NL' => 'Nagaland',
//  'OR' => 'Odisha',
//  'PB' => 'Punjab',
//  'RJ' => 'Rajasthan',
//  'SK' => 'Sikkim',
//  'TN' => 'Tamil Nadu',
//  'TR' => 'Tripura',
//  'UK' => 'Uttarakhand',
//  'UP' => 'Uttar Pradesh',
//  'WB' => 'West Bengal',
// );

        $detail->save();
        $member->save();
        \Cache::forget('member.'. $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.accountUpdateSuccess')
        ]);
    }

    public function postUpdateBankAccount (Request $request) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;

        if($data['bank_account_number'] != $data['confirm_account']) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.bankAccountError')
            ]);
        }

        if (isset($data['s'])) {
            if ($member->secret_password != $data['s']) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.securityPasswordError')
                ]);
            }
        }

        $detail = $member->detail;
        $userData = [];

        if (count($userData) > 0) {
            \Sentinel::update($user, $userData);
        }

        foreach ($detail->getAttributes() as $k => $d) {
            if (isset($data[$k])) $detail->{$k} = $data[$k];
        }

        if (isset($data['secret_password'])) {
            if ($data['secret_password'] != '') {
                $member->secret_password = $data['secret_password'];
            }
        }

        $detail->save();
        $member->save();
        \Cache::forget('member.'. $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.accountUpdateSuccess')
        ]);
    }


    public function postUploadProof (Request $request) {       
        
        $proof = new UploadProof;
        
        if (\Input::hasfile('utr_photo')) {
            echo ' || yes';
            $file=\Input::file('utr_photo');
            $file->move(public_path(). '/assetsss/proof/', time().$file->getClientOriginalName());
            $proof->utr_photo=time().$file->getClientOriginalName();           
        }
        else{
            echo " || no";
        }


        $proof->member_id = $request->member_id; 
        $proof->amount = $request->amount;
        $proof->date = $request->date;
        $proof->utr_number = $request->utr_number;
        $proof->bank = $request->bank;

     $proof->save();
        // die;
     if($proof){
        return redirect()->back()->with('message',"Uploaded Successfully");          
    }
    else{
        return redirect()->back()->with('message',"Something Went Wrong");
    }
    }

    public function passwordUpdateAccount (Request $request) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;

        // echo $data['old-password'].' , ';
        // echo $data['password'].' , ';
        // echo $data['confirm-password'].' , ';


        if (isset($data['s'])) {
            if ($member->secret_password != $data['s']) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.securityPasswordError')
                ]);
            }
        }

        $userData = [];
        $userdetails = \Sentinel::authenticate([
            'username'  =>  $user->member->username,
            'password'  =>  $data['old-password'],
        ]);

        if (isset($data['password']) && $userdetails) {
            // echo "Yes Password".' , ';
            if ($data['password'] != '') {
                $userData['password'] = $data['password'];
                if (count($userData) > 0) {
                    \Sentinel::update($user, $userData);
                    
                }
            }
        }

        \Cache::forget('member.'. $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.accountUpdateSuccess')
        ]);
    }

    /**
     * Renew or upgrade package
     * @return [type] [description]
     */
    public function postUpgrade (Request $request) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = Member::where('username', $data['username'])->first();

        // if ($member->secret_password != trim($data['s'])) {
        //     return \Response::json([
        //         'type'  =>  'error',
        //         'message'   =>  \Lang::get('error.securityPasswordError')
        //     ]);
        // }
        try {
            $member = $this->MemberRepository->upgrade($member, $data);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        if (env('APP_ENV') == 'local') { // local
            $wallet = $member->wallet;
            $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
        } else { // production
            // dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
        }

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.upgradeSuccess')
        ]);
    }

    /**
     * Get all direct members - DataTable
     * @return [type] [description]
     */
    public function getRegisterHistory () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->MemberRepository->registerHistory($member);
    }

    public function getRegisterleftHistory () {
        $user = \Sentinel::getUser();
        $side = 'Left';
        $member = Member::where('position', 'left')->where('parent_id', $user->member->id)->first(); 
        // $member = Member->member;
        return $data = [];

        // dd($data);
        // return view('front.member.registerleftHistory', compact('data'));
    }

    public function getRegisterrightHistory () {
        $side = 'Right';
        $user = \Sentinel::getUser();
        $member = Member::where('position', 'right')->where('parent_id', $user->member->id)->first(); 
        // $member = Member->member;
        $data = $this->MemberRepository->findChildren($member);
        return view('front.member.registerleftHistory', compact('data', 'member', 'side'));
    }

    /**
     * Search Binary Tree - VisJS
     * @return array [array of children]
     */
    public function getBinary (Request $req) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;
        
       

        if (\Input::has('m')) { // from more link
            if (!$req->session()->has('binary.session')) {
                return \Lang::get('error.securityPasswordError');
            } else {
                if (!$target = $this->MemberRepository->findByUsername(trim(\Input::get('u')))) {
                    return \Lang::get('error.memberNotFound');
                }
                session(['binary.session' => [
                    'top' => $member,
                    'current' => $target
                ]]);

                return view('front.network.binaryTree')->with('model', $target);
            }
        }

       /* if (trim($data['s']) == $member->secret_password) {
            return \Lang::get('error.securityPasswordError');
        }*/

        if (!$target = $this->MemberRepository->findByUsername(trim($data['u']))) {
            return \Lang::get('error.memberNotFound');
        }

        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Lang::get('error.memberNotFound');
        }

        if ($target->id != $member->id) {
            $left = explode(',', $member->left_children);
            $right = explode(',', $member->right_children);

            if (!in_array($target->id, $left) && !in_array($target->id, $right)) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   => \Lang::get('error.memberNotFound')
                ]);
            }
        }

        session(['binary.session' => [
            'top' => $member,
            'current' => $target
        ]]);

        return view('front.network.binaryTree')->with('model', $target);
    }

    /**
     * Search Binary Tree Top - VisJS
     * @return array [array of children]
     */
    public function getBinaryTop (Request $req) {
        if (!$req->session()->has('binary.session')) {
            return \Lang::get('error.binarySessionError');
        }

        if (!\Input::has('type')) {
            return \Lang::get('error.binarySessionError');
        }

        $type = trim(\Input::get('type'));
        if ($type != 'top' && $type != 'up') {
            return \Lang::get('error.binarySessionError');
        }

        $sessions = $req->session()->get('binary.session');
        if ($type == 'top') {
            if (!$target = $this->MemberRepository->findByUsername($sessions['top']->username)) {
                return \Lang::get('error.memberNotFound');
            }
        } else {
            if (!$parent = $this->MemberRepository->findByUsername($sessions['current']->username)) {
                return \Lang::get('error.memberNotFound');
            }

            if (!$target = $parent->parent()) {
                return \Lang::get('error.memberNotFound');
            }
        }

        $user = \Sentinel::getUser();
        $member = $user->member;
        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Lang::get('error.memberNotFound');
        }

        session(['binary.session' => [
            'top' => $member,
            'current' => $target
        ]]);

        return view('front.network.binaryTree')->with('model', $target);
    }

    /**
     * Get Member detail from binary - VisJS
     * @return html
     */
    public function getBinaryModal (Request $req) {
        if (!$req->session()->has('binary.session')) {
            return \Lang::get('error.binarySessionError');
        }

        if (!\Input::has('u')) {
            return \Lang::get('error.binarySessionError');
        } else {
            $username = trim(\Input::get('u'));
            if (!$target = $this->MemberRepository->findByUsername(trim($username))) {
                return \Lang::get('error.memberNotFound');
            }
        }

        $user = \Sentinel::getUser();
        $member = $user->member;
        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Lang::get('error.memberNotFound');
        }

        return view('front.network.binaryModal')->with('model', $target);
    }

    /**
     * Search Hierarchy by Username - JSTree
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function getUnilevelTree () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $user->member;

        if (trim($data['s']) != $member->secret_password) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        if (!$target = $this->MemberRepository->findByUsername(trim($data['u']))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   => \Lang::get('error.memberNotFound')
            ]);
        }

        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.memberNotFound')
            ]);
        }

        if ($target->id != $member->id) {
            $left = explode(',', $member->left_children);
            $right = explode(',', $member->right_children);

            if (!in_array($target->id, $left) && !in_array($target->id, $right)) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   => \Lang::get('error.memberNotFound')
                ]);
            }
        }

        return \Response::json([
            'type'  =>  'success',
            'redirect'  =>  route('network.unilevel', ['lang' => \App::getLocale()]) . '?rid=' . $target->id
        ]);
    }

    /**
     * Search Unilevel Tree - JSTree
     * @return array [array of children]
     */
    public function getUnilevel () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        $data = [];

        if (\Input::has('pid')) {
            $id = trim(\Input::get('pid'));
            if (!$target = $this->MemberRepository->findById($id)) return $data;
            if ($target->level <= $member->level) return $data;
        } else if (\Input::has('rid') && \Input::get('rid') != 0) {
            $id = trim(\Input::get('rid'));
            if (!$target = $this->MemberRepository->findById($id)) return $data;
            if ($target->level <= $member->level && $target->username != $member->username) return $data;
        } else {
            $target = $member;
        }

        $children = $this->MemberRepository->findDirect($target);
        if (count($children) > 0) {
            foreach ($children as $child) {
                array_push($data, [
                    'id' => $child->id,
                    'text'  =>  $child->username,
                    'icon'  =>  'md md-person-add',
                    'children'  =>  true
                ]);
            }
        }
        return $data;
    }

    /**
     * Find member info when register
     * @return html
     */
    public function getMemberRegisterModal () {
        $model = false;
        if (trim(\Input::get('u')) != '') {
            $model = $this->MemberRepository->findByUsername(trim(\Input::get('u')));
        }
        return view('front.member.modalRegister')->with('model', $model);
    }
    
    public function setTerms(Request $request){
        $id = $request->id;
        $message = 'Something Went Wrong';
        
        $check = Member::where('id',$id)->first();
        if(!empty($check)){
        
        $check->terms = '1';
        $check->update();
        $message = 'Success';
        
        }
        
        return response()->json(['message' => $message]);
        
    }
}
