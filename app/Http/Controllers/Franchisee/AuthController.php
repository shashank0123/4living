<?php

namespace App\Http\Controllers\Franchisee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Franchise;
use App\User;
use Hash;

class AuthController extends Controller
{

   public function __construct(
        
    ) {
        
        $this->middleware('franchise');
    }


   	public function postLogin(Request $request){

      $data = \Input::all();
      $data = $data['data'];
     
        try {           
       
                $user = \Sentinel::authenticate([
                    'username'  =>  $data['username'],
                    'password'  =>  $data['password'],
                ], (isset($data['remember'])));
            if (!$user) {
                throw new \Exception(\Lang::get('error.loginError'), 1);
                return false;
            }
        
            $permissions = $user->permissions;
      
            if (!isset($permissions['franchisee'])) {
                throw new \Exception(\Lang::get('error.userError'), 1);
                return false;
            } else if ($permissions['franchisee'] != 1) {
                throw new \Exception(\Lang::get('error.userError'), 1);
                return false;
            }

            // if ($user->is_ban || $user->franchisee->package_amount==0) {
            if ($user->is_ban) {
                throw new \Exception(\Lang::get('error.userBan'), 1);
                return false;
            }
            else {
              $check=User::where('username',$data['username'])->first();
              session()->put('franchisee',$check);
              // var_dump(session()->get('franchisee'));
              // die;
            }

           
        } catch (\Exception $e) {
            \Sentinel::logout();
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        return \Response::json([
            'type'      =>  'success',
            'message'   =>  \Lang::get('message.loginSuccess'),
            'member'    => $user,
            'redirect'  =>  route('franchiseehome'),
        ]);

   		// $data = \Input::all();
   		// $password = $data['password'];
   		
   		// $check = Franchise::where('franchise_code',$data['username'])->where('pass',$password)->first();

   		// if($check){
     //        session()->put('auth',$check);   			
   		// 	return view('franchisee.home')->with('member' , $check);
   		// }
     //     else{
     //        return redirect()->back()->with('message','Wrong MemberID & Password');
     //     }
   	}


    public function getHome(){
            $member =  session()->get('franchisee');
            $franchisee = Franchisee::where('user_id',$member->id)->first();
     
      return view('franchisee.home',compact('member','franchisee'));
    }
}
