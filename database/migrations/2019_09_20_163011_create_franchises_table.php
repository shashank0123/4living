<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchises', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name', 50);
			$table->string('address1', 255);
			$table->string('address2', 255)->nullable();
			$table->string('city', 255);
			$table->string('state', 255);
			$table->integer('pincode');
			$table->string('phone')->unique();
			$table->string('alternet_contact')->nullable();
			$table->string('username', '20');
			$table->string('email')->unique();
			$table->string('password', 20);
			$table->string('aadhar_number','30')->nullable();
			$table->string('franchise_code', 10);
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchises');
    }
}
