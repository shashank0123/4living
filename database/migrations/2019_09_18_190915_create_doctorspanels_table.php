<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorspanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctorspanels', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name', 255)->nullable();
			$table->string('designation', 255)->nullable();
			$table->string('status')->default('1')->after('designation');
			$table->string('image')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctorspanels');
    }
}
