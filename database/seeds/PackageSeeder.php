<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = [
            [
                'package_amount' => 0,
                'direct_percent' => 0,
                'pairing_percent'   =>  0,
                'group_level'   =>  0,
                'max_pairing_bonus' => 0,
                'max_pair'  =>  0,
                'purchase_point' =>  0,
                'max_share_sale' => 0,
                'share_limit' =>  0
            ],
            [
                'package_amount' => 500,
                'direct_percent' => 20,
                'pairing_percent'   =>  20,
                'group_level'   =>  0,
                'max_pairing_bonus' => 1000,
                'max_pair'  =>  10,
                'purchase_point' =>  30,
                'max_share_sale' => 1000,
                'share_limit' =>  100
            ]
        ];

        foreach ($packages as $package) {
        	$package['created_at'] = Carbon::now();
        	$package['updated_at'] = Carbon::now();
        	\DB::table('Package')->insert($package);
        }
    }
}
