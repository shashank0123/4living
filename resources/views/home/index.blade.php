@extends('layouts/home')

@section('content')
<!-- header end -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
 <!--  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
</ol> -->
<div class="carousel-inner">
 <div class="carousel-item active">
  <img style="width: 100%;filter: brightness(60%);" src="https://images.theconversation.com/files/168886/original/file-20170511-32588-h28pca.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip" alt="...">
  <div  class="carousel-caption carouseltext">
    <h2  >Empower your Family1</h5>
        <p>Every One Donate Here</p>
    </div>
</div>
<div class="carousel-item">
  <img style="width: 100%; filter: brightness(60%);" src="https://images.theconversation.com/files/168886/original/file-20170511-32588-h28pca.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip" alt="...">
  <div class="carousel-caption carouseltext">
    <h2>Empower your Family2</h2>
    <p>Every One Donate Here</p>
</div>
</div>
<div class="carousel-item">
  <img style="width: 100%;filter: brightness(60%);" src="https://images.theconversation.com/files/168886/original/file-20170511-32588-h28pca.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip" alt="...">
  <div class="carousel-caption  carouseltext">
     <h2>Insure Children3</h2>
     <p>Every One Donate Here</p>
 </div>
</div>
</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div>

<!-- banner begin-->
       <!--  <div class="banner">
            <div class="banner-content">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-xl-6 col-lg-9 d-flex align-items-center">
                            <div class="part-text">
                                <h1>IntellIgent plan<br/> for your money</h1>
                                <p>Put your investing ideas into action with full range of investments.
                                    Enjoy real benefits and rewards on your accrue investing.</p>
                                <a href="#">Start Now</a>
                                <a href="#">View Plan</a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="part-img">
                                <img src="/home-assets/img/banner-Illustration.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- banner end -->

        <!-- counter begin-->
        <div class="counter">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-xl-4 col-lg-4 col-md-6 d-flex align-items-center">
                        <div class="single-counter">
                            <div class="part-icon">
                                <i class="flaticon-group"></i>
                            </div>
                            <div class="part-text">
                                <h3 class="count-num">50,236</h3>
                                <h4>Total Member</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-counter">
                            <div class="part-icon">
                                <i class="flaticon-money"></i>
                            </div>
                            <div class="part-text">
                                <h3 class="count-num">50,236</h3>
                                <h4>Total Deposited</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6 d-flex align-items-center">
                        <div class="single-counter">
                            <div class="part-icon">
                                <i class="flaticon-hand"></i>
                            </div>
                            <div class="part-text">
                                <h3 class="count-num">50,236</h3>
                                <h4>Total Profit</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- counter end -->

        <!-- slider section -->
        <div class="container recentsec">
         <div class="row">
          <div class="col-sm-6">

            <h2 id="cardhd">RECENT EARNERS </h2>
            <div class="recentearn">
                <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                   <div class="carousel-inner">
                    <div class="carousel-item active" data-interval="10000">
                       <div class="container">
                        <div class="row" id="usertext3">

                          <div class="col-sm-3 col-3">
                           <div class="recentuser2">
                            <img src="s1.jpeg">

                        </div>
                    </div>
                    <div class="col-sm-9 col-9">
                       <div class="usertext" >
                        <h4>Ram Sankar</h4>
                        <p>Every week 200cr</p>
                        <p>Mumbai , Nakka7</p>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="carousel-item" data-interval="2000">
      <div class="container">
        <div class="row" id="usertext3">

          <div class="col-sm-3 col-3">
           <div class="recentuser2">
            <img src="s1.jpeg">

        </div>
    </div>
    <div class="col-sm-9 col-9">
       <div class="usertext" >
         <h4>Ram Sankar</h4>
         <p>Every week 200cr</p>
         <p>Mumbai , Nakka7</p>
     </div>
 </div>
</div>

</div>

</div>
<div class="carousel-item">
  <div class="container">
    <div class="row" id="usertext3">

      <div class="col-sm-3 col-3">
       <div class="recentuser2">
        <img src="s1.jpeg">

    </div>
</div>
<div class="col-sm-9 col-9">
   <div class="usertext" >
    <h4>Ram Sankar</h4>
    <p>Every week 200cr</p>
    <p>Mumbai , Nakka7</p>
</div>
</div>
</div>

</div>

</div>
</div>
<a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
 <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
 <span class="carousel-control-next-icon" aria-hidden="true"></span>
 <span class="sr-only">Next</span>
</a>                                         </div>
</div>
</div>
<div class="col-sm-6">

    <h2 id="cardhd" >RECENTLY JOINED USERS </h2>
    <div class="container usersec">
     <marquee direction="up" style="top: 0; max-height: 200px">

      <!-- 2nd -->
      <div class="row" id="usertext2">

       <div class="col-sm-3 col-3">
        <div class="recentuser">
         <img src="s1.jpeg">

     </div>
 </div>
 <div class="col-sm-9 col-9">
    <div class="usertext2">
     <h4>Salman khan</h4>
     <h6>Delhi, &nbsp; uttam Nagar</h6>

 </div>
</div>
</div>

<div class="row" id="usertext2">

   <div class="col-sm-3 col-3">
    <div class="recentuser">
     <img src="s1.jpeg">

 </div>
</div>
<div class="col-sm-9 col-9">
    <div class="usertext2">
     <h4>Hritik</h4>
     <h6>Delhi, &nbsp; uttam Nagar</h6>
 </div>
</div>
</div>

<div class="row" id="usertext2">

   <div class="col-sm-3 col-3">
    <div class="recentuser">
     <img src="s1.jpeg">

 </div>
</div>
<div class="col-sm-9 col-9">
    <div class="usertext2" >
     <h4>Virat</h4>
     <h6>Delhi, &nbsp; uttam Nagar</h6>
 </div>
</div>
</div>

<!-- end -->

<!-- 2nd -->
<div class="row" id="usertext2">

   <div class="col-sm-3 col-3">
    <div class="recentuser">
     <img src="s1.jpeg">

 </div>
</div>
<div class="col-sm-9 col-9">
    <div class="usertext">
     <h4>Salman khan</h4>
     <h6>Delhi, &nbsp; uttam Nagar</h6>
 </div>
</div>
</div>
<!-- end -->
</marquee>
</div>


</div>
</div>
</div>



<!-- end slider section -->

<!-- about begin -->
<div class="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="section-title">
                    <h2>About Help 2 Human</h2>
                    <p>We bring the right people together to challenge established thinking and drive transformation.
                    We will show the way to successive.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="single-about">
                    <div class="heading">
                        <div class="part-icon">
                            <i class="flaticon-brain"></i>
                        </div>
                        <div class="part-text">
                            <h3>We Innovate</h3>
                        </div>
                    </div>
                    <p>We innovate systematic, continuously and success
                    innovate system, That continuously done.</p>
                    <a href="#">Read More</a>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="single-about">
                    <div class="heading">
                        <div class="part-icon">
                            <i class="flaticon-patent"></i>
                        </div>
                        <div class="part-text">
                            <h3>Licensed & Certified</h3>
                        </div>
                    </div>
                    <p>We innovate systematic, continuously and success
                    innovate system, That continuously done.</p>
                    <a href="#">Read More</a>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="single-about">
                    <div class="heading">
                        <div class="part-icon">
                            <i class="flaticon-reward"></i>
                        </div>
                        <div class="part-text">
                            <h3>We are Professional</h3>
                        </div>
                    </div>
                    <p>We innovate systematic, continuously and success
                    innovate system, That continuously done.</p>
                    <a href="#">Read More</a>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="single-about">
                    <div class="heading">
                        <div class="part-icon">
                            <i class="flaticon-donation"></i>
                        </div>
                        <div class="part-text">
                            <h3>Saving & Investments</h3>
                        </div>
                    </div>
                    <p>We innovate systematic, continuously and success
                    innovate system, That continuously done.</p>
                    <a href="#">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about end -->

<!-- we think global begin-->
<div class="we-think-global">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-10">
                <div class="part-left">
                    <h2>We were always<br/>
                    thinking global Community</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                        Ut enim ad minim veniam. Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur.</p>
                    <a href="#">Learn More</a>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-10 d-flex align-items-center">
                <div class="part-right">
                    <h3>Start Invest Now</h3>
                    <p>Quis nostrud exercitation ullamco laboris nisi utaliquip
                        commodo consequat. Duis aute feeirure dolor voluptate
                    velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <a href="#">View Plan</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- we think global end -->

<!-- choosing resons begin-->
<div class="choosing-reason">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="section-title">
                    <h2>Why choose us</h2>
                    <p>We bring the right people together to challenge established thinking and drive transformation.
                    We will show the way to successive.</p>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-reason">
                    <div class="part-img">
                        <img src="/home-assets/img/choose-1.jpg" alt="">
                    </div>
                    <div class="part-text">
                        <h3>Stable & Profitable</h3>
                        <p>Effective business support planning becomes one of the
                            most critical activities within successful small business
                            maintenance and development.Accure provides best
                        affordable plan to earn money.</p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-reason">
                    <div class="part-img">
                        <img src="/home-assets/img/choose-2.jpg" alt="">
                    </div>
                    <div class="part-text">
                        <h3>Instant Withdraw</h3>
                        <p>Effective business support planning becomes one of the
                            most critical activities within successful small business
                            maintenance and development.Accure provides best
                        affordable plan to earn money.</p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-reason">
                    <div class="part-img">
                        <img src="/home-assets/img/choose-3.jpg" alt="">
                    </div>
                    <div class="part-text">
                        <h3>Referral Provides</h3>
                        <p>Effective business support planning becomes one of the
                            most critical activities within successful small business
                            maintenance and development.Accure provides best
                        affordable plan to earn money.</p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- choosing resons end -->

<!-- transaction begin-->
<div class="transaction">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="section-title text-center">
                    <h2>Latest<span> Achiever</span></h2>
                    <p>Put your investing ideas into action with full range of investments.
                    Enjoy real benefits and rewards on your accrue investing.</p>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="transaction-area">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#deposit" role="tab"
                            aria-selected="true">Top</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#withdraw" role="tab"
                            aria-selected="false">Monthly</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="deposit" role="tabpanel" aria-labelledby="home-tab">

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Currency</th>
                                        <th scope="col">Deposit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-1.jpg" alt="">
                                            </div>
                                            <span>Olympia Ripple</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-2.jpg" alt="">
                                            </div>
                                            <span>Alison Rittichier</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-3.jpg" alt="">
                                            </div>
                                            <span>Isreal Vandy</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-4.jpg" alt="">
                                            </div>
                                            <span>Emmett Steinkellner</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-5.jpg" alt="">
                                            </div>
                                            <span>Nancee Broomes</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane fade" id="withdraw" role="tabpanel" aria-labelledby="profile-tab">

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Currency</th>
                                        <th scope="col">Deposit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-1.jpg" alt="">
                                            </div>
                                            <span>Olympia Ripple</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-2.jpg" alt="">
                                            </div>
                                            <span>Alison Rittichier</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-3.jpg" alt="">
                                            </div>
                                            <span>Isreal Vandy</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-4.jpg" alt="">
                                            </div>
                                            <span>Emmett Steinkellner</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="d-flex">
                                            <div class="user-img">
                                                <img src="/home-assets/img/user-5.jpg" alt="">
                                            </div>
                                            <span>Nancee Broomes</span>
                                        </th>
                                        <td>Oct 24,2018</td>
                                        <td>$9,00,000.00</td>
                                        <td>Dollar</td>
                                        <td>001 Days Ago</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- transaction end -->

<!-- payment begin-->
<div class="payment">
    <div class="container">
        <div class="row d-flex reorder-xs">

            <div class="col-xl-5 co-lg-5 col-md-6">
                <div class="part-form">
                    <h3>Calculate Your Profit</h3>
                    <form class="payment-form">
                        <div class="form-group">
                            <label for="InputAmount">Enter Ammount :</label>
                            <input type="text" class="form-control" id="InputAmount" placeholder="10">
                        </div>
                        <div class="form-group">
                            <label for="InputTprofit">Total Profit :</label>
                            <input type="text" class="form-control" id="InputTprofit" placeholder="$15">
                        </div>
                        <div class="form-group">
                            <label for="InputProfit">Net Profit :</label>
                            <input type="text" class="form-control" id="InputProfit" placeholder="$05">
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-xl-5 col-lg-5 col-md-6 d-flex align-items-center offset-xl-1 offset-lg-1">
                <div class="row justify-content-center">
                    <div class="col-xl-12 col-lg-12">
                        <div class="section-title">
                            <h2>We Accepted<br /><span>Payment Method</span></h2>
                            <p>Put your investing ideas into action with full range of investments.
                            Enjoy real benefits and rewards on your accrue investing.</p>
                        </div>
                    </div>
                    
                    <div class="col-xl-12 col-lg-12">
                        <div class="part-accept">
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-1.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-2.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-3.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-4.jpg" alt="">
                            </div>
                        </div>
                        <div class="part-accept">
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-5.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-6.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-7.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- payment end -->

<!-- price begin-->
        <!-- <div class="price">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6">
                        <div class="section-title text-center">
                            <h2>Grab our mega package</h2>
                            <p>Put your investing ideas into action with full range of investments.
                                Enjoy real benefits and rewards on your accrue investing.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12 col-lg-12">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-selected="true">Monthly</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="yearly-tab" data-toggle="tab" href="#yearly" role="tab" aria-selected="false">Yearly</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent2">
                            <div class="tab-pane fade show active" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price special">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="yearly" role="tabpanel" aria-labelledby="yearly-tab">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price special">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> -->
        <!-- price end -->

        <!-- inventor begin-->
        <div id="investors">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6">
                        <div class="section-title text-center">
                            <h2>Our Top Inventors</h2>
                            <p>Put your investing ideas into action with full range of investments.
                            Enjoy real benefits and rewards on your accrue investing.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="box">
                            <div class="image">
                                <img class="img-fluid" src="/home-assets/img/t1.png" alt="">
                                <div class="social_icon">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-google-plus-g"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-linkedin-in"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fas fa-envelope"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="info">
                                <h5>Tamim Ubaidah
                                </h5>
                                <p>Web Designer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="box">
                            <div class="image">
                                <img class="img-fluid" src="/home-assets/img/t2.png" alt="">
                                <div class="social_icon">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-google-plus-g"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-linkedin-in"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fas fa-envelope"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="info">
                                <h5>Mamunur Rashid</h5>
                                <p>Web Designer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="box">
                            <div class="image">
                                <img class="img-fluid" src="/home-assets/img/t3.png" alt="">
                                <div class="social_icon">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-google-plus-g"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-linkedin-in"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fas fa-envelope"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="info">
                                <h5>Rex Rifat</h5>
                                <p>Web Designer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="box">
                            <div class="image">
                                <img class="img-fluid" src="/home-assets/img/t4.png" alt="">
                                <div class="social_icon">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-google-plus-g"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-linkedin-in"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fas fa-envelope"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="info">
                                <h5>David Martin</h5>
                                <p>Web Designer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- inventor end -->

        <!-- faq begin-->
        
        <!-- faq end -->

        <!-- newsletter begin-->
        <div class="newsletter">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-7">
                        <div class="section-title text-center">
                            <h2>Our Newslatter</h2>
                            <p>We bring the right people together to challenge established thinking and drive transformation.
                            We will show the way to successive.</p>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-7">
                        <form class="newsletter-form">
                            <input type="email" placeholder="Enter your email..." required>
                            <button type="submit">Subscribe Now</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- newsletter end -->

       <!--  <div class="blog-post">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-8 col-lg-8">
                        <div class="section-title text-center">
                            <h2 class="extra-margin">Latest<span> Service Posts</span></h2>
                            <p>Put your investing ideas into action with full range of investments.
                                Enjoy real benefits and rewards on your accrue investing.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-blog">
                            <div class="part-img">
                                <img src="/home-assets/img/blog-post-1.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3><a href="/blog-details">Blog Single Image Post</a></h3>
                                <h4>
                                    <span class="admin">By Admin </span>.
                                    <span class="date">12 Nov, 2018 </span>.
                                    <span class="category">in Web Design </span>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the rinting and
                                    typesetting industry. Lorem Ipsum has been the industry's standard
                                    dummy ...</p>
                                <a class="read-more" href="#"><span><i class="fas fa-book-reader"></i></span> Read More</a>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-blog">
                            <div class="part-img">
                                <img src="/home-assets/img/blog-post-2.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3><a href="blog-details">Printer Took A Galley</a></h3>
                                <h4>
                                    <span class="admin">By Admin </span>.
                                    <span class="date">12 Nov, 2018 </span>.
                                    <span class="category">in Web Design </span>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the rinting and
                                    typesetting industry. Lorem Ipsum has been the industry's standard
                                    dummy ...</p>
                                <a class="read-more" href="#"><span><i class="fas fa-book-reader"></i></span> Read More</a>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-blog">
                            <div class="part-img">
                                <img src="/home-assets/img/blog-post-3.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3><a href="blog-details">Reader Will Distracted</a></h3>
                                <h4>
                                    <span class="admin">By Admin </span>.
                                    <span class="date">12 Nov, 2018 </span>.
                                    <span class="category">in Web Design </span>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the rinting and
                                    typesetting industry. Lorem Ipsum has been the industry's standard
                                    dummy ...</p>
                                <a class="read-more" href="#"><span><i class="fas fa-book-reader"></i></span> Read More</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> -->

        @endsection