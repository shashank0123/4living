@extends('front.app')

@section('title')
  @lang('breadcrumbs.settingsDetails') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.settingsDetails')</li>
  </ul>
 
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
  
  #accountBankForm label { font-weight: 400; color: #666; }
</style>
@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.profile1')</h1>
            <p class="lead">@lang('settings.profile2')</p>
          </div>
          <p>Once you update your account, then you cannot update again. For further updates contact to admin. For this <a href="mailto:help2humancf@gmail.com?Subject=Acount%20Update"><u>Click Here</u></a></p>
            <br>
          <div class="row m-b-40">
            <div class="col-md-6">
                <h4>Personal Information</h4>
              <div class="well" style="background-color: #E5E7E9">
                <form data-parsley-validate="" role="form" class="action-form" id="accountBasicForm" http-type="post" data-url="{{ route('account.postUpdate') }}" data-nationality="true" enctype="multipart/form-data">
                  <fieldset>
                    @if(!empty($member->user->first_name))
                    <div class="form-group">
                      <label class="control-label"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.name')</label>
                      <input type="text" class="form-control" required="" disabled="" value="{{ $member->user->first_name }}" name="first_name">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.name')</label>
                      <input type="text" class="form-control" required=""  value="{{ $member->user->first_name }}" name="first_name">
                    </div>
                    @endif

                    {{-- <div class="form-group">
                      <label for="profimage" class="control-label"> Upload an image</label>
                      <input type="file" id="profimage" name="profimage" class="form-control" >
                    </div> --}}


                    @if(!empty($member->detail->date_of_birth))
                    <div class="form-group">
                      <label class="control-label" for="inputDOB"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="{{ $member->detail->date_of_birth }}" data-date-format="YYYY-MM-DD" readonly>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputDOB"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="{{ $member->detail->date_of_birth }}" data-date-format="YYYY-MM-DD">
                    </div>
                    @endif


                    

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputId">@lang('settings.id')</label>
                      <input type="text" name="identification_number" class="form-control" id="inputId" required="" value="{{ $member->detail->identification_number }}">
                    </div> --}}
                    @if(!empty($member->detail->gender))
                    <div class="form-group">
                      <label class="control-label" for="inputGender"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender" readonly>
                        <option value="Male" @if ($member->detail->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->detail->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputGender"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender">
                        <option value="Male" @if ($member->detail->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->detail->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div>
                    @endif


                    @if(!empty($member->detail->mobile_phone))
                    <div class="form-group">
                      <label class="control-label" for="inputPhone"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.mobile')</label>
                      <input type="text" name="mobile_phone" class="form-control" id="inputPhone" required="" value="{{ $member->detail->mobile_phone }}" readonly>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputPhone"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.mobile')</label>
                      <input type="text" name="mobile_phone" class="form-control" id="inputPhone" required="" value="{{ $member->detail->mobile_phone }}">
                    </div>
                    @endif

                    

                   <!-- <?php $countries = config('misc.countries'); ksort($countries); ?>
                    @if (!is_null($member->detail->nationality))
                    <div class="form-group">
                      <label class="control-label" for="inputNationality">@lang('settings.nationality')</label>
                      <select class="form-control dd-icon">
                        <option data-imagesrc="{{ asset('assets/img/flags/' . $member->detail->nationality . '.png') }}" data-description="{{ \Lang::get('country.' . $member->detail->nationality) }}">{{ $member->detail->nationality }}</option>
                      </select>
                    </div>
                    @else
                    <div class="alert alert-danger">
                      @lang('settings.nationalityError')
                    </div>
                    @endif-->


                    @if(!empty($member->detail->address))
                    <div class="form-group">
                      <label class="control-label" for="inputAddress"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address" class="form-control" id="inputAddress" required="" readonly>{{ $member->detail->address }}</textarea>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputAddress"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address" class="form-control" id="inputAddress" required="">{{ $member->detail->address }}</textarea>
                    </div>
                    @endif

                    

                   <div class="form-group">
                      <label class="control-label" for="inputState"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;State</label>
                      <select name="state" class="form-control" id="inputState" required="" value="{{$member->detail->state}}" <?php if(!empty($member->detail->state)){echo 'readonly';} ?> >
                        <option value="">Select State</option>
                        <option @if ($member->detail->state=='AP') {{ 'selected' }} @endif value="AP">Andhra Pradesh</option>
                        <option @if ($member->detail->state=='AR') {{ 'selected' }} @endif value="AR">Arunachal Pradesh</option>
                        <option @if ($member->detail->state=='AS') {{ 'selected' }} @endif value="AS">Assam</option>
                        <option @if ($member->detail->state=='BH') {{ 'selected' }} @endif value="BH">Bihar</option>
                        <option @if ($member->detail->state=='CT') {{ 'selected' }} @endif value="CT">Chhattisgarh</option>
                        <option @if ($member->detail->state=='DL') {{ 'selected' }} @endif value="DL">Delhi</option>
                        <option @if ($member->detail->state=='GA') {{ 'selected' }} @endif value="GA">Goa</option>
                        <option @if ($member->detail->state=='GJ') {{ 'selected' }} @endif value="GJ">Gujarat</option>
                        <option @if ($member->detail->state=='HR') {{ 'selected' }} @endif value="HR">Haryana</option>
                        <option @if ($member->detail->state=='HP') {{ 'selected' }} @endif value="HP">Himachal Pradesh</option>
                        <option @if ($member->detail->state=='JK') {{ 'selected' }} @endif value="JK">Jammu and Kashmir</option>
                        <option @if ($member->detail->state=='JH') {{ 'selected' }} @endif value="JH">Jharkhand</option>
                        <option @if ($member->detail->state=='KA') {{ 'selected' }} @endif value="KA">Karnataka</option>
                        <option @if ($member->detail->state=='KL') {{ 'selected' }} @endif value="KL">Kerala</option>
                        <option @if ($member->detail->state=='MP') {{ 'selected' }} @endif value="MP">Madhya Pradesh</option>
                        <option @if ($member->detail->state=='MH') {{ 'selected' }} @endif value="MH">Maharashtra</option>
                        <option @if ($member->detail->state=='MN') {{ 'selected' }} @endif value="MN">Manipur</option>
                        <option @if ($member->detail->state=='ML') {{ 'selected' }} @endif value="ML">Meghalaya</option>
                        <option @if ($member->detail->state=='MZ') {{ 'selected' }} @endif value="MZ">Mizoram</option>
                        <option @if ($member->detail->state=='NL') {{ 'selected' }} @endif value="NL">Nagaland</option>
                        <option @if ($member->detail->state=='OR') {{ 'selected' }} @endif value="OR">Orissa</option>
                        <option @if ($member->detail->state=='Pondicherry') {{ 'selected' }} @endif value="Pondicherry">Pondicherry</option>
                        <option @if ($member->detail->state=='PB') {{ 'selected' }} @endif value="PB">Punjab</option>
                        <option @if ($member->detail->state=='RJ') {{ 'selected' }} @endif value="RJ">Rajasthan</option>
                        <option @if ($member->detail->state=='SK') {{ 'selected' }} @endif value="SK">Sikkim</option>
                        <option @if ($member->detail->state=='TN') {{ 'selected' }} @endif value="TN">Tamil Nadu</option>
                        <option @if ($member->detail->state=='TR') {{ 'selected' }} @endif value="TR">Tripura</option>
                        <option @if ($member->detail->state=='UK') {{ 'selected' }} @endif value="UK">Uttaranchal</option>
                        <option @if ($member->detail->state=='UP') {{ 'selected' }} @endif value="UP">Uttar Pradesh</option>
                        <option @if ($member->detail->state=='WB') {{ 'selected' }} @endif value="WB">West Bengal</option>
                      </select>
                    </div>

                    @if(!empty($member->detail->city))
                    <div class="form-group">
                      <label class="control-label" for="city"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;City</label>
                      <input type="text" name="city" class="form-control" id="city" required="" disabled="disabled" value="{{ $member->detail->city }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="city"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;City</label>
                      <input type="text" name="city" class="form-control" id="city" required="" value="{{ $member->detail->city }}">
                    </div>
                    @endif

                    @if(!empty($member->detail->district))
                    <div class="form-group">
                      <label class="control-label" for="district"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;District</label>
                      <input type="text" name="district" class="form-control" id="district" required="" disabled="disabled" value="{{ $member->detail->district }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="district"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;District</label>
                      <input type="text" name="district" class="form-control" id="district" required="" value="{{ $member->detail->district }}">
                    </div>
                    @endif

                    @if(!empty($member->detail->pin_code))
                    <div class="form-group">
                      <label class="control-label" for="pin_code"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Pin Code</label>
                      <input type="text" name="pin_code" class="form-control" id="pin_code" required="" disabled="disabled" value="{{ $member->detail->pin_code }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="pin_code"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Pin Code</label>
                      <input type="text" name="pin_code" class="form-control" id="pin_code" required="" value="{{ $member->detail->pin_code }}">
                    </div>
                    @endif
                   
                   @if(!empty($member->detail->adhaar))
                    <div class="form-group">
                      <label class="control-label" for="inputAdhaar"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Aadhar Number</label>
                      <input type="text" name="adhaar" class="form-control" id="inputAdhaar" required="" disabled="disabled" value="{{ $member->detail->adhaar }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputAdhaar"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Aadhar Number</label>
                      <input type="text" name="adhaar" class="form-control" id="inputAdhaar" required="" value="{{ $member->detail->adhaar }}">
                    </div>
                    @endif
                    
                    @if(!empty($member->detail->pan))
                    <div class="form-group">
                      <label class="control-label" for="inputPan"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" disabled="disabled" value="{{ $member->detail->pan }}" readonly>
                    </div>
                    @else
                     <div class="form-group">
                      <label class="control-label" for="inputPan"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" value="{{ $member->detail->pan }}">
                    </div>
                    @endif
                     
                   <!-- <div class="form-group">
                      <label class="control-label" for="inputSpouseName">@lang('settings.spouse.name')</label>
                      <input type="text" name="spouse_name" id="inputSpouseName" class="form-control" value="{{ $member->detail->spouse_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputSpouseDOB">@lang('settings.spouse.dob')</label>
                      <input type="text" name="spouse_dob" id="inputSpouseDOB" class="form-control datepicker" value="{{ $member->detail->spouse_dob }}" data-date-format="YYYY-MM-DD">
                    </div>-->  

                    <div class="form-group">
                      <label class="control-label" for="inputNomineeName">@lang('settings.nominee.name')</label>
                      <input type="text" name="beneficiary_name" id="inputNomineeName" class="form-control" value="{{ $member->detail->beneficiary_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" id="Relation_with_Beneficiary">Relation with Nominee</label>
                      <input type="text"  class="form-control" id="relation_with_beneficiary" name="relation_with_beneficiary" value="{{ $member->detail->relation_with_beneficiary }}">
                    </div> 

                    <!--<div class="form-group">
                      <label class="control-label m-b-10" for="inputBeneficiaryNationality">@lang('settings.beneficiary.nationality')</label>
                      <select id="inputBeneficiaryNationality" class="form-control dd-icon" name="beneficiary_nationality">
                        @foreach ($countries as $country => $value)
                          <option value="{{ $country }}" @if ($member->detail->beneficiary_nationality == $country) selected="" @endif data-imagesrc="{{ asset('assets/img/flags/' . $country . '.png') }}" data-description="{{ \Lang::get('country.selected') }}">@lang('country.' . $country)</option>
                        @endforeach
                      </select>
                    </div>-->

                    <div class="form-group">
                      {{-- <label class="control-label" for="inputSecret">@lang('settings.secret')</label> --}}
                      <input type="hidden" name="s" class="form-control" id="inputSecret" value="{{$member->secret_password}}">
                    </div>

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputSecret">@lang('settings.secret')</label>
                      <input type="password" class="form-control" id="inputSecret" required="">
                    </div> --}}

                   {{--  <div class="form-group">
                      <div class="alert alert-info">
                        @lang('settings.passwordNotice')
                      </div>
                    </div> --}}

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('settings.password')</label>
                      <input type="password" name="password" class="form-control" id="inputPassword" minlength="5">
                    </div> --}}

                    {{-- <div class="form-group">
                      <label class="control-label">@lang('settings.repassword')</label>
                      <input type="password" class="form-control" data-parsley-equalto="#inputPassword" minlength="5">
                    </div> --}}
                    {{-- <div class="form-group">
                      <div class="alert alert-info">
                       Empty Bank Detail fields below if you do not want to change.
                      </div>
                    </div> --}}
                    
                   {{--  @if((!empty($member->detail->bank_name)) && (!empty($member->detail->bank_account_number)) && (!empty($member->detail->ifsc)))
                    <div class="form-group">
                      <label class="control-label" for="bank_name">Bank Name</label>
                      <input type="text" class="form-control" id="bank_name" name="bank_name" disabled="disabled" value="{{ $member->detail->bank_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="account_numbe">Account Number</label>
                      <input type="text" class="form-control" id="bank_account_number" name="bank_account_number" disabled="disabled" value="{{ $member->detail->bank_account_number }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" class="form-control" id="ifsc" name="ifsc" disabled="" value="{{ $member->detail->ifsc }}">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="bank_name">Bank Name</label>
                      <input type="text" class="form-control" id="bank_name" name="bank_name" value="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="account_numbe">Account Number</label>
                      <input type="text" class="form-control" id="bank_account_number" name="bank_account_number" value="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" class="form-control" id="ifsc" name="ifsc" value="">
                    </div>
                    @endif --}}
                    
                    <!--<div class="form-group">
                      <label class="control-label" for="inputNewSecret">@lang('settings.newSecret')</label>
                      <input type="password" name="secret_password" class="form-control" id="inputNewSecret" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.reNewSecret')</label>
                      <input type="password" class="form-control" data-parsley-equalto="#inputNewSecret" minlength="5">
                    </div>-->

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <h4>Bank Details</h4>
                    <div class="well" style="background-color: #E5E7E9">
                <form data-parsley-validate="" role="form" class="form-floating action-form" id="accountBankForm" http-type="post" data-url="{{ route('account.postUpdateBank') }}">
                  <fieldset>
                    <?php $countries = config('misc.countries'); ?>
                    <div class="form-group">
                      <label class="control-label" for="inputBankName">@lang('settings.bank.name')</label>
                      <select class="form-control" name="bank_name" id="inputBankName" autocomplete>
                        @foreach ($countries as $country => $value)
                          <optgroup label="{{ \Lang::get('country.' . $country) }}">
                            @foreach ($value['banks'] as $bank)
                              <option value="{{ $bank }}" @if ($member->detail->bank_name == $bank) selected="" @endif>{{ $bank }}</option>
                            @endforeach
                          </optgroup>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAccNumber">@lang('settings.bank.number')</label>
                      <input type="text" name="bank_account_number" id="inputBankAccNumber" class="form-control" required="" value="{{ $member->detail->bank_account_number }}">
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label" for="inputBankAddress">Confirm Bank Account Number</label>
                      <input type="text" name="confirm_account" id="confirm-account" class="form-control" required="" value="">
                      
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAccHolder">@lang('settings.bank.holder')</label>
                      <input type="text" name="bank_account_holder" id="inputBankAccHolder" class="form-control" required="" value="{{ $member->detail->bank_account_holder }}">
                    </div>

                    

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputBankAddress">@lang('settings.bank.address')</label> --}}
                      <input type="hidden" name="bank_address" id="inputBankAddress" class="form-control" required="" value="India">
                      
                    {{-- </div> --}}

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" class="form-control" id="ifsc" name="ifsc" value="{{ $member->detail->bank_branch }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankBranch">@lang('settings.bank.branch')</label>
                      <input type="text" name="bank_branch" id="inputBankBranch" class="form-control" value="{{ $member->detail->bank_branch }}">
                    </div>
                    <input type="hidden" name="s" class="form-control" required="" value="{{$member->secret_password}}">
                    {{-- <div class="form-group">
                      <label class="control-label">@lang('settings.secret')</label> --}}
                      <input type="hidden" name="password" class="form-control" required="" value="{{$member->secret_password}}">
                    {{-- </div> --}}

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
                </div>
                </div>
                <br><br>
                <div class="row">
                    <h4>Reset Password</h4>
                    <div class="well" style="background-color: #E5E7E9">
                <form data-parsley-validate="" role="form" class="action-form" id="accountBasicForm" http-type="post" data-url="{{ route('member.passwordUpdate') }}" data-nationality="true" enctype="multipart/form-data">
                  <fieldset>
                    {{-- @if(!empty($member->user->first_name))
                    <div class="form-group">
                      <label class="control-label">@lang('settings.name')</label>
                      <input type="text" class="form-control" required="" disabled="" value="{{ $member->user->first_name }}" name="first_name">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label">@lang('settings.name')</label>
                      <input type="text" class="form-control" required=""  value="{{ $member->user->first_name }}" name="first_name">
                    </div>
                    @endif --}}

                    {{-- <div class="form-group">
                      <label for="profimage" class="control-label"> Upload an image</label>
                      <input type="file" id="profimage" name="profimage" class="form-control" >
                    </div> --}}

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputDOB">@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="{{ $member->detail->date_of_birth }}" data-date-format="YYYY-MM-DD">
                    </div> --}}

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputId">@lang('settings.id')</label>
                      <input type="text" name="identification_number" class="form-control" id="inputId" required="" value="{{ $member->detail->identification_number }}">
                    </div> --}}
                    @if(!empty($member->detail->gender))
                    {{-- <div class="form-group">
                      <label class="control-label" for="inputGender">@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender" >
                        <option value="Male" @if ($member->detail->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->detail->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div> --}}
                    @else 
                    {{-- <div class="form-group">
                      <label class="control-label" for="inputGender">@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender">
                        <option value="Male" @if ($member->detail->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->detail->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div> --}}
                    @endif


                    {{-- <div class="form-group">
                      <label class="control-label" for="inputPhone">@lang('settings.mobile')</label>
                      <input type="text" name="mobile_phone" class="form-control" id="inputPhone" required="" value="{{ $member->detail->mobile_phone }}">
                    </div> --}}

                   <!-- <?php $countries = config('misc.countries'); ksort($countries); ?>
                    @if (!is_null($member->detail->nationality))
                    <div class="form-group">
                      <label class="control-label" for="inputNationality">@lang('settings.nationality')</label>
                      <select class="form-control dd-icon">
                        <option data-imagesrc="{{-- {{ asset('assets/img/flags/' . $member->detail->nationality . '.png') }}" data-description="{{ \Lang::get('country.' . $member->detail->nationality) }}">{{ $member->detail->nationality }} --}}</option>
                      </select>
                    </div>
                    @else
                    <div class="alert alert-danger">
                      {{-- @lang('settings.nationalityError') --}}
                    </div>
                    @endif-->

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputAddress">@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address" class="form-control" id="inputAddress" required="">{{ $member->detail->address }}</textarea>
                    </div> --}}

                   {{-- <div class="form-group">
                      <label class="control-label" for="inputState">State</label>
                      <select name="state" class="form-control" id="inputState" required="" value="{{$member->detail->state}}">
                        <option value="">Select State</option>
                        <option @if ($member->detail->state=='AP') {{ 'selected' }} @endif value="AP">Andhra Pradesh</option>
                        <option @if ($member->detail->state=='AR') {{ 'selected' }} @endif value="AR">Arunachal Pradesh</option>
                        <option @if ($member->detail->state=='AS') {{ 'selected' }} @endif value="AS">Assam</option>
                        <option @if ($member->detail->state=='BH') {{ 'selected' }} @endif value="BH">Bihar</option>
                        <option @if ($member->detail->state=='CT') {{ 'selected' }} @endif value="CT">Chhattisgarh</option>
                        <option @if ($member->detail->state=='DL') {{ 'selected' }} @endif value="DL">Delhi</option>
                        <option @if ($member->detail->state=='GA') {{ 'selected' }} @endif value="GA">Goa</option>
                        <option @if ($member->detail->state=='GJ') {{ 'selected' }} @endif value="GJ">Gujarat</option>
                        <option @if ($member->detail->state=='HR') {{ 'selected' }} @endif value="HR">Haryana</option>
                        <option @if ($member->detail->state=='HP') {{ 'selected' }} @endif value="HP">Himachal Pradesh</option>
                        <option @if ($member->detail->state=='JK') {{ 'selected' }} @endif value="JK">Jammu and Kashmir</option>
                        <option @if ($member->detail->state=='JH') {{ 'selected' }} @endif value="JH">Jharkhand</option>
                        <option @if ($member->detail->state=='KA') {{ 'selected' }} @endif value="KA">Karnataka</option>
                        <option @if ($member->detail->state=='KL') {{ 'selected' }} @endif value="KL">Kerala</option>
                        <option @if ($member->detail->state=='MP') {{ 'selected' }} @endif value="MP">Madhya Pradesh</option>
                        <option @if ($member->detail->state=='MH') {{ 'selected' }} @endif value="MH">Maharashtra</option>
                        <option @if ($member->detail->state=='MN') {{ 'selected' }} @endif value="MN">Manipur</option>
                        <option @if ($member->detail->state=='ML') {{ 'selected' }} @endif value="ML">Meghalaya</option>
                        <option @if ($member->detail->state=='MZ') {{ 'selected' }} @endif value="MZ">Mizoram</option>
                        <option @if ($member->detail->state=='NL') {{ 'selected' }} @endif value="NL">Nagaland</option>
                        <option @if ($member->detail->state=='OR') {{ 'selected' }} @endif value="OR">Orissa</option>
                        <option @if ($member->detail->state=='Pondicherry') {{ 'selected' }} @endif value="Pondicherry">Pondicherry</option>
                        <option @if ($member->detail->state=='PB') {{ 'selected' }} @endif value="PB">Punjab</option>
                        <option @if ($member->detail->state=='RJ') {{ 'selected' }} @endif value="RJ">Rajasthan</option>
                        <option @if ($member->detail->state=='SK') {{ 'selected' }} @endif value="SK">Sikkim</option>
                        <option @if ($member->detail->state=='TN') {{ 'selected' }} @endif value="TN">Tamil Nadu</option>
                        <option @if ($member->detail->state=='TR') {{ 'selected' }} @endif value="TR">Tripura</option>
                        <option @if ($member->detail->state=='UK') {{ 'selected' }} @endif value="UK">Uttaranchal</option>
                        <option @if ($member->detail->state=='UP') {{ 'selected' }} @endif value="UP">Uttar Pradesh</option>
                        <option @if ($member->detail->state=='WB') {{ 'selected' }} @endif value="WB">West Bengal</option>
                      </select>
                    </div> --}}
                   
                   {{-- @if(!empty($member->detail->adhaar))
                    <div class="form-group">
                      <label class="control-label" for="inputAdhaar">Aadhar Number</label>
                      <input type="text" name="adhaar" class="form-control" id="inputAdhaar" required="" disabled="disabled" value="{{ $member->detail->adhaar }}">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputAdhaar">Aadhar Number</label>
                      <input type="text" name="adhaar" class="form-control" id="inputAdhaar" required="" value="{{ $member->detail->adhaar }}">
                    </div>
                    @endif --}}
                    
                    {{-- @if(!empty($member->detail->pan))
                    <div class="form-group">
                      <label class="control-label" for="inputPan">PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" disabled="disabled" value="{{ $member->detail->pan }}">
                    </div>
                    @else
                     <div class="form-group">
                      <label class="control-label" for="inputPan">PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" value="{{ $member->detail->pan }}">
                    </div>
                    @endif --}}
                     
                   <!-- <div class="form-group">
                      <label class="control-label" for="inputSpouseName">@lang('settings.spouse.name')</label>
                      <input type="text" name="spouse_name" id="inputSpouseName" class="form-control" value="{{ $member->detail->spouse_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputSpouseDOB">@lang('settings.spouse.dob')</label>
                      <input type="text" name="spouse_dob" id="inputSpouseDOB" class="form-control datepicker" value="{{ $member->detail->spouse_dob }}" data-date-format="YYYY-MM-DD">
                    </div>-->  

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputNomineeName">@lang('settings.nominee.name')</label>
                      <input type="text" name="beneficiary_name" id="inputNomineeName" class="form-control" value="{{ $member->detail->beneficiary_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" id="Relation_with_Beneficiary">Relation with Nominee</label>
                      <input type="text"  class="form-control" id="relation_with_beneficiary" name="relation_with_beneficiary" value="{{ $member->detail->relation_with_beneficiary }}">
                    </div>  --}}

                    <!--<div class="form-group">
                      <label class="control-label m-b-10" for="inputBeneficiaryNationality">@lang('settings.beneficiary.nationality')</label>
                      <select id="inputBeneficiaryNationality" class="form-control dd-icon" name="beneficiary_nationality">
                        @foreach ($countries as $country => $value)
                          <option value="{{ $country }}" @if ($member->detail->beneficiary_nationality == $country) selected="" @endif data-imagesrc="{{ asset('assets/img/flags/' . $country . '.png') }}" data-description="{{ \Lang::get('country.selected') }}">@lang('country.' . $country)</option>
                        @endforeach
                      </select>
                    </div>-->

                    <div class="form-group">
                      {{-- <label class="control-label" for="inputSecret">@lang('settings.secret')</label> --}}
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputSecret">@lang('settings.old')</label>
                      <input type="password" class="form-control" name="old-password" id="inputSecret" required="">
                    </div>

                    {{-- <div class="form-group">
                      <div class="alert alert-info">
                        @lang('settings.passwordNotice')
                      </div>
                    </div> --}}

                    <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('settings.password')</label>
                      <input type="password" name="password" class="form-control" id="inputPassword" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.repassword')</label>
                      <input type="password" name="confirm-password" class="form-control" data-parsley-equalto="#inputPassword" minlength="5">
                    </div>
                    {{-- <div class="form-group">
                      <div class="alert alert-info">
                       Empty Bank Detail fields below if you do not want to change.
                      </div>
                    </div> --}}
                    
                   {{--  @if((!empty($member->detail->bank_name)) && (!empty($member->detail->bank_account_number)) && (!empty($member->detail->ifsc)))
                    <div class="form-group">
                      <label class="control-label" for="bank_name">Bank Name</label>
                      <input type="text" class="form-control" id="bank_name" name="bank_name" disabled="disabled" value="{{ $member->detail->bank_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="account_numbe">Account Number</label>
                      <input type="text" class="form-control" id="bank_account_number" name="bank_account_number" disabled="disabled" value="{{ $member->detail->bank_account_number }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" class="form-control" id="ifsc" name="ifsc" disabled="" value="{{ $member->detail->ifsc }}">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="bank_name">Bank Name</label>
                      <input type="text" class="form-control" id="bank_name" name="bank_name" value="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="account_numbe">Account Number</label>
                      <input type="text" class="form-control" id="bank_account_number" name="bank_account_number" value="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" class="form-control" id="ifsc" name="ifsc" value="">
                    </div>
                    @endif --}}
                    
                    <!--<div class="form-group">
                      <label class="control-label" for="inputNewSecret">@lang('settings.newSecret')</label>
                      <input type="password" name="secret_password" class="form-control" id="inputNewSecret" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.reNewSecret')</label>
                      <input type="password" class="form-control" data-parsley-equalto="#inputNewSecret" minlength="5">
                    </div>-->

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
                </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
