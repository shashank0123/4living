@extends('front.app')

@section('title')
  @lang('registerHistory.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>
    <li class="active">@lang('breadcrumbs.registerHistory')</li>
  </ul>
@stop
<style>
   li,a,input,p,h2,h1,h3,h4,h5,h6,span,div { text-transform: uppercase !important; }
   ul li , ul li a { color: #fff !important; }
</style>
@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
          <div class="page-header">
            <h1><i class="md md-group-add"></i> @lang('registerHistory.title')</h1>
            <p class="lead">@lang('registerHistory.subTitle2')</p>
          </div>

          <div class="card">  
            <div>
              <div class="datatables" style="background-color: #E5E7E9">
                <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('member.registerHistoryList') }}" style="background-color: #E5E7E9">
                  <thead style="background-color: #E5E7E9">
                    <tr>
                      <th data-id="created_at">@lang('registerHistory.join')</th>
                      <th data-id="username">@lang('registerHistory.name')</th>
                      <th data-id="member_id">@lang('registerHistory.member_id')</th>
                      <th data-id="register_by">@lang('registerHistory.sponsorid')</th>
                      <th data-id="status">Status</th>
                      <th data-id="package_amount">@lang('registerHistory.package')</th>
                    </tr>
                  </thead>
                  <tbody style="background-color: #E5E7E9">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
