<?php
use App\Models\MemberDetail;

?>
@extends('front.app')
@section('title')
@lang('registerHistory.directtitle') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">@lang('breadcrumbs.directHistory')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
</style>
@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">


        <div class="page-header">
          <div class="row">
              <div class="col-sm-6">                
         <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> Direct Member</h1>
          <p class="lead">@lang('registerHistory.subTitle3')</p>
              </div>
              <div class="col-sm-6" style="text-align: right;">
                <form method="get" action="/en/my-direct-members">
    
              <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
              <button type="submit" class="btn btn-primary" >Search</button>
            </form>
              </div>
          </div>
        </div>

        <div class="page-header">
          
        </div>

        <div class="card">
          <div>
            <div class="">
             <table class="table table-full table-full-small dt-responsive display nowrap" cellspacing="0" width="100%" role="grid" data-url="{{ route('member.registerleftHistoryList') }}">
              <thead>
                <tr>
                  <th>@lang('registerHistory.sr')</th>
                  <th>@lang('registerHistory.join')</th>
                  <th>@lang('registerHistory.name')</th>
                  <th>@lang('registerHistory.member_id')</th>
                  <th>Phone</th>
                  <!--<th>Placement</th>-->
                  <th>Sponsor ID</th>
                  <th>Status</th>
                  <!--<th>@lang('registerHistory.package')</th>-->
                </tr>
              </thead>
              <tbody>
                <?php $i = 1;?>
                

                @foreach ($data as $datas)
                <?php
                $detail = MemberDetail::where('member_id',$datas->id)->first();
                ?>
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$datas->created_at}}</td>
                  <td>{{$datas->name}}</td>
                  <td>{{$datas->referral_id}}</td>
                  
                  <td>@if(!empty($detail->mobile_phone)){{$detail->mobile_phone}}@endif</td>
                  <!--<td>{{$datas->position}}</td>-->
                  <td>{{$datas->register_by}}</td>
                  <td>@if ($datas->package_id == 1) {{'Inactive'}} @else {{ 'Active' }} @endif</td>
                  <!--<td>{{$datas->package_amount}}</td>-->
                </tr>
                @endforeach
              </tbody>
            </table>

            @if($i == 1)
            <div style="text-align: center;margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif

          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
@stop
