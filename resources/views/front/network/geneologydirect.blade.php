<?php
use App\Models\Member;
use App\Models\Package;
 $roots = array(); 

  if (\Input::has('rid')) {
    $sMember = \App\Models\Member::where('id', trim(\Input::get('rid')))->first();
  } else {
    $sMember = $member;
  }
  $arrId = array();
  $arrId[0] = $member->id;
  $srno = 1;
?>

@extends('front.app')
<style>
  aside.sidebar {
    background-color: #099!important;
}
</style>
@section('title')
  @lang('breadcrumbs.directgeneology') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.levelwise')</li>
  </ul>
@stop
<style>
  ul li , ul li a { color: #fff !important; }
</style>
@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-swap-vert"></i> @lang('breadcrumbs.directgeneology')</h1>
            <p class="lead">@lang('breadcrumbs.directgeneology')</p>
          </div>

          <div class="row m-b-40">
            
            <div class="card" style="background-color: #E5E7E9">
            <div>
              <div class="datatables">
                  
                 
                        
                 <!--        <h3 style="text-align: center;"><br>Level 0</h3>-->
                 <!--<table class="table table-full">-->
                 <!--    <thead>-->
                 <!--        <th>S.No.</th>-->
                 <!--        <th>Joining Date</th>-->
                 <!--        <th>Name</th>-->
                 <!--        <th>MemberId</th>-->
                 <!--        <th>Phone</th>-->
                 <!--        <th>Refered By</th>-->
                 <!--        <th>Status</th>-->
                         <!--<th>Package</th>-->
                 <!--    </thead>-->
                     
                 <!--    <tbody>-->
                         
                         
                 <!--        <tr>-->
                 <!--        <td>{{$srno++}}</td>-->
                 <!--        <td>{{$member->created_at}}</td>-->
                 <!--        <td>{{$member->name}}</td>-->
                 <!--        <td>{{$member->username}}</td>-->
                 <!--        <td>{{$member->detail->mobile_phone}}</td>-->
                 <!--        <td>{{$member->register_by}}</td>-->
                 <!--        <td>-->
                 <!--            @if($member->package_id>0)-->
                 <!--            {{'Active'}}-->
                 <!--            @else-->
                 <!--            {{'Inactive'}}-->
                 <!--            @endif-->
                 <!--        </td>-->
                         <!--<td></td>-->
                 <!--        </tr>-->
                 <!--    </tbody>-->
                 <!-- </table>-->
                  
                  
                        
                         <!--<h3 style="text-align: center;"><br>Level </h3>-->
                 <table class="table table-full">
                     <thead>
                         <th>S.No.</th>
                         <th>Joining Date</th>
                         <th>Name</th>
                         <th>MemberId</th>
                         <th>Phone</th>
                         <th>Sponsor By</th>
                         <th>Status</th>
                         <!--<th>Package</th>-->
                     </thead>
                    
                     <tbody>
                         
                       
                        
                        
                     </tbody>
                    
                    
                  </table>
                  
                   <h1 style="margin-top: 0!important; padding: 30px;background-color: #fff; text-align: center ">Comming Soon</h1>
                  
              </div>
            </div>
          </div>
            
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
