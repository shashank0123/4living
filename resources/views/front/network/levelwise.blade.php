<?php
use App\Models\Member;
use App\Models\MemberDetail;
use App\Models\Package;
 $roots = array(); 

  if (\Input::has('rid')) {
    $sMember = \App\Models\Member::where('id', trim(\Input::get('rid')))->first();
  } else {
    $sMember = $member;
  }
  $arrId = array();
  $arrId[0] = $member->id;
  $srno = 1;
?>

@extends('front.app')
<style>
  aside.sidebar {
    background-color: #099!important;
}
</style>
@section('title')
  @lang('breadcrumbs.levelwise') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.levelwise')</li>
  </ul>
@stop
<style>
  ul li , ul li a { color: #fff !important; }
</style>
@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-swap-vert"></i> @lang('breadcrumbs.levelwise')</h1>
            <p class="lead">@lang('breadcrumbs.levelwise')</p>
          </div>

          <div class="row m-b-40">
            
            <div class="card" style="background-color: #E5E7E9">
            <div>
              <div class="datatables">
                  
                 
                        
                 <!--        <h3 style="text-align: center;"><br>Level 0</h3>-->
                 <!--<table class="table table-full">-->
                 <!--    <thead>-->
                 <!--        <th>S.No.</th>-->
                 <!--        <th>Joining Date</th>-->
                 <!--        <th>Name</th>-->
                 <!--        <th>MemberId</th>-->
                 <!--        <th>Phone</th>-->
                 <!--        <th>Refered By</th>-->
                 <!--        <th>Status</th>-->
                         <!--<th>Package</th>-->
                 <!--    </thead>-->
                     
                 <!--    <tbody>-->
                         
                         
                 <!--        <tr>-->
                 <!--        <td>{{$srno++}}</td>-->
                 <!--        <td>{{$member->created_at}}</td>-->
                 <!--        <td>{{$member->name}}</td>-->
                 <!--        <td>{{$member->username}}</td>-->
                 <!--        <td>{{$member->detail->mobile_phone}}</td>-->
                 <!--        <td>{{$member->register_by}}</td>-->
                 <!--        <td>-->
                 <!--            @if($member->package_id>0)-->
                 <!--            {{'Active'}}-->
                 <!--            @else-->
                 <!--            {{'Inactive'}}-->
                 <!--            @endif-->
                 <!--        </td>-->
                         <!--<td></td>-->
                 <!--        </tr>-->
                 <!--    </tbody>-->
                 <!-- </table>-->
                  
                  @for($i=1;$i<=10;$i++)
                         <?php
                            $srno =1;
                            $arrno = 0;
                            $rootno = 0;
                            $roots = array();
                         ?>
                         @if(!empty($arrId))
                        
                            @foreach($arrId as $id)
                        <?php
                            $getm = Member::where('parent_id',$id)->get();
                        ?>
                        @if(!empty($getm))
                            @foreach($getm as $m)
                                <?php $roots[$rootno++] = $m; ?>
                            @endforeach
                        @endif
                            @endforeach
                         @endif
                         
                         <h3 style="text-align: center;"><br>Level {{$i}}</h3>
                 <table class="table table-full">
                     <thead>
                         <th>S.No.</th>
                         <th>Joining Date</th>
                         <th>Name</th>
                         <th>MemberId</th>
                         <th>Phone</th>
                         <th>Sponsor By</th>
                         <th>Status</th>
                         <!--<th>Package</th>-->
                     </thead>
                     <?php $flag = 0;
                     unset($arrId);
                     ?>
                      @if(!empty($roots))
                     <tbody>
                        
                         @foreach($roots as $root)
                         <?php
                            $arrId[$arrno++] = $root->id;
                            $flag=1;
                            $phone = MemberDetail::where('member_id',$root->id)->first();
                            $sponsor = Member::where('id',$root->parent_id)->first();
                         ?>
                         <tr>
                         <td>{{$srno++}}</td>
                         <td>{{$root->created_at}}</td>
                         <td>{{$root->name}}</td>
                         <td>{{$root->username}}</td>
                         <td>@if(!empty($phone->mobile_phone)){{$phone->mobile_phone}}@endif</td>
                         <td>@if(!empty($sponsor))
                         @if(!empty($sponsor->username))
                         {{$sponsor->username}}
                         @endif
                         @endif
                         </td>
                         <td>
                             @if($root->package_id==1)
                             {{'Inactive'}}
                             @else
                             {{'Active'}}
                             @endif
                         </td>
                         <!--<td></td>-->
                         </tr>
                         @endforeach
                         
                        
                     </tbody>
                     @endif
                    
                  </table>
                  @if($flag==0)
                  <h2 style="text-align: center; margin-top: 0 !important; background-color: #fff; padding: 20px">No result found</h2>
                  @endif
                  
                 
                 @endfor
              </div>
            </div>
          </div>
            
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
