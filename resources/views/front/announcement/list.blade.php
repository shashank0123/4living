<?php 
  $lang = \App::getLocale();
?>

@extends('front.app')

@section('title')
  @lang('announcement.messageTitle') | {{ config('app.name') }}
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>
@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.messageTitle')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-new-releases"></i> @lang('announcement.messageTitle')</h1>
        </div>

        <div class="card" style="background-color: #E5E7E9">
          <div class="card-content">
            <div class="datatables">
                
                
                <!--<h3 style="text-align: center;"><br>Level 0</h3>-->
                 <table class="table table-full">
                     <thead>
                         <th>S.No.</th>
                         <th>Date</th>
                         <th>Message</th>
                     </thead>
                     
                     <tbody>
                         <?php $srno=1;  ?>
                         
                         @if(!empty($data))
                         
                         @foreach($data as $datas)
                         <tr>
                         <td>{{$srno++}}</td>
                         <td>{{$datas->created_at}}</td>
                         <td>{{$datas->content_en}}</td>
                         </tr>
                         @endforeach
                         @endif
                     </tbody>
                  </table>
                
                
                @if($srno == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No message </h3>
            </div>
            @endif
                
              <!--<table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" id="announcementListTable" data-url="{{ route('announcement.getList', ['lang' => \App::getLocale()]) }}">-->
              <!--  <thead style="background-color: #E5E7E9">-->
              <!--    <tr>-->
              <!--      <th data-id="created_at">@lang('announcement.listDate')</th>-->
              <!--      <th data-id="title_{{ $lang }}">@lang('announcement.listTitle')</th>-->
              <!--      <th data-id="action">@lang('announcement.listAction')</th>-->
              <!--    </tr>-->
              <!--  </thead>-->
              <!--  <tbody style="background-color: #E5E7E9">-->
              <!--  </tbody>-->
              <!--</table>-->
            </div>
          </div>
        </div>
      </section>
      </div>
    </div>
  </main>
@stop
