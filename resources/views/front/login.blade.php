@extends('front.app_living')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')



<!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
		
                  
 <div class="login-register-area pt-85 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                        <div class="login-register-wrapper">
                            <div class="login-register-tab-list nav">
                                <a class="active" data-toggle="tab" href="#lg1">
                                    <h4> login </h4>
                                </a>
                                <a  href="/register">
                                    <h4> register </h4>
                                </a>
                            </div>
                            <div class="tab-content">
                                <div id="lg1" class="tab-pane active">
                                    <div class="login-form-container">
                                        <div class="login-register-form">
                                          <form class="form-floating action-form" http-type="post" id="form" data-url="{{ route('login.post', ['lang' => \App::getLocale()]) }}">
										  
										   <input type="text" style="text-transform: uppercase;" name="username" class="form-control" id="username" placeholder="Member ID" required="">
										   
                                                <input type="password" name="password" class="form-control" id="password" required="" placeholder="PASSWORD">
												
                                            
                                                <div class="button-box">
                                                    <div class="login-toggle-btn">
													<input type="checkbox" name="remember">  <label>@lang('login.remember') </label>
                                                       
                                                        <a href="/passwordReset">Forgot Password?</a>
                                                    </div>
													<button type="submit" >
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
             @lang('login.title')
            </button>
			
                                                    
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

   
@stop
