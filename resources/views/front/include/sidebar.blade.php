<?php 
  use App\Models\MemberDetail;

  $profile = MemberDetail::where('member_id',$member->id)->first();
  ?>

<style>
  #close-nav { display: none; }
  @media screen and (max-width: 768px){
    #close-nav{ display: block; }
    #show-close {/* margin-left: 30% !important;*/ text-align: center; margin-top: 20px !important }

    .navbar-fixed-top { margin-left: 0px!important; }u
  }


  .menu-links li a { font-size: 16px }
  #clickBro { display: none; }
  span { text-transform: uppercase !important; font-weight: normal !important;  }
  #file { display: none; }
  ul li a {
    text-transform: uppercase !important; font-weight: normal !important; 
  }
  .menu-links li a {
    font-size: 14px!important;
}
aside.sidebar { background-color:#099!important;}
  ul li , ul li a { color: #fff !important; }
</style>
<aside class="sidebar fixed" style="width:280px;left:0px;">
  <span onclick="hide_bar()" id="show-close"><i class="fa fa-close" id="close-nav" style="color: #fff"> &nbsp;&nbsp;Close</i></span>
  <div class="brand-logo" style="background-color: #fff;">
    <div id="logo" style="padding: 0; margin-top: 5px">
      <a href="/en/member"><img src="{{ asset('asset/images/logo/logo-2.png') }}" width="100"></a>
    </div>
  </div>
 
  <div class="user-logged-in">
    <div class="content">

      <div class="container">

        <form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
          <input type="hidden" name="member_id" id="uid" value="{{$member->id}}">
          @if(!empty($profile->profimage))
          <span id="uploaded_image">
            <img src="/assetsss/images/AdminProduct/{{$profile->profimage}}" width="150px" height="150px">
          </span>
          @else
          <span id="uploaded_image"></span>
          @endif
          <br>
          {{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
          <label for="file" style="cursor: pointer;color: #fff;margin-left: 10px; font-weight: bold;font-size: 18px;text-decoration: underline; ">U<span style="text-transform: lowercase !important;">pload</span> P<span style="text-transform: lowercase !important;">icture</span></label>
          <input type="file" name="file" id="file" onchange='submitMe()'/>
          <input type="submit" name="submit" id="clickBro" />
        </form>
      </div>        
<br>

      <div class="user-name" style="color: #fff">{{ $member->name }} <span class="text-muted f9" style="font-size:12px;color:#90caf9">( {{ $member->referral_id }} )</span></div>
      <!--<div class="user-email" style="color: #fff">@lang('sidebar.package') - <span style="color:#fff;">Rs {{ number_format($member->package_amount, 0) }}</span></div>-->
      <div class="user-actions">
        <a class="m-r-5" href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">@lang('sidebar.profileupdate')</a>
        <!--<a href="{{ route('logout', ['lang' => \App::getLocale()]) }}">@lang('sidebar.logout')</a>-->
      </div>
    </div>
  </div>

  <ul class="menu-links">
  {{-- <li icon="md md-blur-on">
      <a href="{{ route('home', ['lang' => \App::getLocale()]) }}"><i class="md md-blur-on"></i>&nbsp;<span>@lang('sidebar.home')</span></a>
  </li> --}}

    <li>
      <a href="#" data-toggle="collapse" data-target="#MDMember" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md md-accessibility"></i>&nbsp;@lang('sidebar.registerTitle')</a>
      <ul id="MDMember" class="collapse">
        <li>
          <a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.registerLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('member.registerHistory', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.registerLink2')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('member.upgrade', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.registerLink3')</span>
          </a>
        </li>
      </ul>
    </li>
    
    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#Epin" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-lock-outline"></i>&nbsp;My Shoppy (Epin)</a>
      <ul id="Epin" class="collapse">
        
        <li>
          <a href="#">
            <span>Unused Epin</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span>Used Epin</span>
          </a>
        </li>
      </ul>
    </li> --}}

 {{--    <li>
      <a href="#" data-toggle="collapse" data-target="#Epoint" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-control-point"></i>&nbsp;My Shoppy (Epoint)</a>
      <ul id="Epoint" class="collapse">
        <li>
          <a href="{{ route('member.epoint.allepoint') }}">
            <span>All Epoint</span>
          </a>
        </li>
      </ul>
    </li>
    --}}

    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#MDSettings" aria-expanded="false" aria-controls="MDSettings" class="collapsible-header waves-effect"><i class="md md-settings"></i>&nbsp;@lang('sidebar.settingsTitle')</a>
      <ul id="MDSettings" class="collapse">
        <li>
          <a href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.settingsLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('settings.bank', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.settingsLink2')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('settings.password', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.resetpassword')</span>
          </a>
        </li>
      </ul>
    </li> --}}

 

     
    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#MDTrading" aria-expanded="false" aria-controls="MDTrading" class="collapsible-header waves-effect"><i class="md md-trending-up"></i>&nbsp;@lang('sidebar.sharesTitle')</a>
      <ul id="MDTrading" class="collapse">
        <li>
          <a href="{{ route('shares.market', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.sharesLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('shares.lock', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.sharesLink2')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('shares.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.sharesLink3')</span>
          </a>
        </li>
      </ul>
    </li> --}}

   {{--  <li>
      <a href="#" data-toggle="collapse" data-target="#MDTransaction" aria-expanded="false" aria-controls="MDTransaction" class="collapsible-header waves-effect"><i class="md md-shopping-cart"></i>&nbsp;@lang('sidebar.transactionTitle')</a>
      <ul id="MDTransaction" class="collapse">
        <li>
          <a href="{{ route('transaction.transfer', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.transactionLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('transaction.withdraw', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.transactionLink2')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('transaction.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.transactionLink3')</span>
          </a>
        </li>
      </ul>
    </li>
    --}}
	<li>
      <a href="#" data-toggle="collapse" data-target="#MBoard" aria-expanded="false" aria-controls="MDGenealogy" class="collapsible-header waves-effect"><i class="md md-swap-vert"></i>&nbsp;@lang('sidebar.messageBoard')</a>
      <ul id="MBoard" class="collapse">
        <li>
          <a href="{{ route('announcement.list', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.BoardMess')</span>
          </a>
        </li>

			
       
		
        
      </ul>
    </li>
	
	<li>
      <a href="#" data-toggle="collapse" data-target="#MDGenealogy" aria-expanded="false" aria-controls="MDGenealogy" class="collapsible-header waves-effect"><i class="md md-swap-vert"></i>&nbsp;@lang('sidebar.networkTitle')</a>
      <ul id="MDGenealogy" class="collapse">
        <li>
          <a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.networkLink1')</span>
          </a>
        </li>

		<li>
          <a href="{{ route('network.levelwise', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.levelWise')</span>
          </a>
        </li>
		
        <li>
          <a href="/en/member/direct-history{{-- {{ route('network.binary', ['lang' => \App::getLocale()]) }} --}}">
            <span>@lang('sidebar.direct')</span>
          </a>
        </li>
		 <li>
          <a href="{{ route('network.directgeneology', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.genealogyDirect')</span>
          </a>
        </li>
        <!--<li>-->
        <!--  <a href="{{ route('network.unilevel', ['lang' => \App::getLocale()]) }}">-->
        <!--    <span>@lang('sidebar.networkLink2')</span>-->
        <!--  </a>-->
        <!--</li> -->
      </ul>
    </li>
	<li>
      <a href="#" data-toggle="collapse" data-target="#AccountMange" aria-expanded="false" aria-controls="MDGenealogy" class="collapsible-header waves-effect"><i class="md md-swap-vert"></i>&nbsp;@lang('sidebar.accounts')</a>
      <ul id="AccountMange" class="collapse">
        <li>
          <a href="{{ route('payment.due', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.paymentDues')</span>
          </a>
        </li>
		<li>
          <a href="{{ route('payment.received', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.paymentRecived')</span>
          </a>
        </li>
		<li>
          <a href="{{ route('payment.summary', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.paymentSummary')</span>
          </a>
        </li>
		<li>
          <a href="{{ route('payment.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.PaymentStatement')</span>
          </a>
        </li>
		<li>
          <a href="{{ route('payment.request', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.paymentRequest')</span>
          </a>
        </li>

			
       
		
        
      </ul>
    </li>
	
   {{--  <li>
      <a href="#" data-toggle="collapse" data-target="#MDStatement" aria-expanded="false" aria-controls="MDStatement" class="collapsible-header waves-effect"><i class="md md-assessment"></i>&nbsp;@lang('sidebar.statementTitle')</a>
      <ul id="MDStatement" class="collapse">
        <li>
          <a href="{{ route('bonus.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.statementLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('summary.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.statementLink2')</span>
          </a>
        </li>
      </ul>
    </li> --}}

    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#MDCoin" aria-expanded="false" aria-controls="MDCoin" class="collapsible-header waves-effect"><i class="md md-album"></i>&nbsp;@lang('sidebar.coinTitle')</a>
      <ul id="MDCoin" class="collapse">
       <!-- <li>
          <a href="{{ route('coin.transaction', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.coinLink1')</span>
          </a>
        </li>-->
        <li>
          <a href="{{ route('coin.list', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.coinLink2')</span>
          </a>
        </li>
      </ul>
    </li> --}}

    <li>
      <a href="#" data-toggle="collapse" data-target="#transfer" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-receipt"></i>&nbsp;Transfer Epins</a>
      <ul id="transfer" class="collapse">
        <li>
          <a href="{{ route('transfer.addEpointStatement', ['lang' => \App::getLocale()]) }}">
            <span>Add Statement</span>
          </a>
        </li>
        
        <li>
          {{-- <a href="{{ route('transfer.epointlist', ['lang' => \App::getLocale()]) }}.{{$memnber->id}}"> --}}
          <a href="/en/transfer-epoints/list/{{$member->id}}">
            <span>Transfer History</span>
          </a>
        </li>

        
      </ul>
    </li>


    <li icon="md md-settings-power">
      <a href="{{ route('logout', ['lang' => \App::getLocale()]) }}"><i class="md md-settings-power"></i>&nbsp;<span>@lang('sidebar.logout')</span></a>
    </li>
  </ul>
</aside>

<aside class="sidebar-toggle hidden-xs hidden-sm">
  <button type="button" class="m-5" id="toggleNav" title="@lang('common.toggleNav')">
    <span class="sr-only">@lang('common.toggleNav')</span>
    <span class="md md-more-vert"></span>
  </button>
</aside>

@section('script')
<script>
  $(document).ready(function() {
        $('#upload_image').submit(function(e) {
          e.preventDefault();  
          $.ajax({  
            url: "/upload-image",  
            type: "POST",  
            dataType: 'JSON',
            data: new FormData(this),  
            contentType: false,  
            processData:false,  

            beforeSend:function(){
              $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
            },

            success: function(data) {
              // alert('<img src="/assetsss/images/AdminProduct/'+data.image+'" width="120px" height="120px" />');
              var profile_image = data.image;
              // $('#uploaded_image').append(data.image);
              $('#uploaded_image').html('<img src="/assetsss/images/AdminProduct/'+data.image+'" width="150px" height="150px" />'); // show response from the php script.
            }
          });
        });
      });

  function submitMe() {
    $('#file').click();
    $("#clickBro").click();
  }

  function hide_bar() {
   $("#sidenav-overlay").click();
 }
</script>
@stop