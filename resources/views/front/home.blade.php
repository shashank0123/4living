<?php
use App\Repositories\SharesRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use App\Models\Epoint;
use App\Models\Member;
use App\Models\BonusPairing;
use App\Models\BonusDirect;
$term = 0;
$cnt_lvl = 1;
$level = array();
$checkID = array();
 $dummy = array();
for($i=0;$i<=9;$i++){
    $level[$i] = 0;
}

// Level wise member count

$level[0] = Member::where('parent_id',$member->id)->count();
$getuid = Member::where('parent_id',$member->id)->get();
if(!empty($getuid)){
    foreach($getuid as $i){
        $checkID[$term++] = $i->id;
    }
    $term = 0;
}

for($k=1 ; $k<=9 ; $k++){

if(!empty($checkID)){
    $count = 0;
    $countd = 0;
    foreach($checkID as $id){
        $getM = Member::where('parent_id',$id)->count();
        $count = $count+$getM;
        
        $getd = Member::where('parent_id',$id)->get();
        if(!empty($getd)){
            foreach($getd as $d){
                $dummy[$countd++]=$d->id;
            }
        }
    }
    $countd=0;
    if(!empty($dummy)){
         unset($checkID);
        foreach($dummy as $d){
            $checkID[$countd++] = $d;
        }
    }
    unset($dummy);
    $level[$cnt_lvl++] = $count;
}
}

$allChild = explode(',', $member->right_children);
$countAll = 0;
$allChild = array_filter($allChild);

if(!empty($allChild)){
    foreach($allChild as $all){
        // echo $all."<br>";
        $check = Member::where('id',$all)->first();
        if(!empty($check)){
            if($check->package_id>1)
            $countAll++;
        }
    }
}
// die;

$email = User::where('id',$member->user_id)->first();

$sharesRepo = new SharesRepository;
$announcementModel = new \App\Models\Announcement;

$pairing = BonusPairing::where('member_id', $member->id)->first();
$bonusdirect = BonusDirect::where('member_id', $member->id)->sum('amount_cash');

if ($pairing){
  $binary = $pairing->amount_cash;
}
else $binary = 0.00;
$epoints = Epoint::where('member_id', $member->id)->first();

$count_left = 0;
$count_right = 0;
$left_count = explode(',', $member->left_children);
foreach($left_count as $left){
  $check = Member::where('id',$left)->first();
  if(!empty($check))
    if($check->package_id > '1')
   $count_left++;
 
}
$cash1 = 0;
$right_count = explode(',', $member->right_children);
foreach($right_count as $left){
  $check = Member::where('id',$left)->first();
  if(!empty($check))
    if($check->package_id > '1')
   $count_right++; 
}
if ($member->package_id > 1){
$leftdirect = Member::where('direct_id', $member->id)->where('position', 'left')->get();
$rightdirect = Member::where('direct_id', $member->id)->where('position', 'right')->get();
if (count($leftdirect) > 0 && count($rightdirect) > 0 && (count($rightdirect) > count($leftdirect) || count($rightdirect) < count($leftdirect) || count($leftdirect) > 1) ){
  $pair = min($count_left, $count_right);
  $cash1 = $pair*$member->pairing_percent;
}
}
?>

<style type="text/css">
 .modal-dialog .modal-body {
    padding: 35px 50px !important;
}
        .modal-body h3 {
    font-size: 17px;
    color: blue;
    border-bottom: 2px solid lightblue;
    font-weight: 600;
}
.modal-body p, .modal-body li { font-size: 14px !important; }

  #set-div-height { height: 100px; }
  @media screen and (max-width: 768px){
   #set-div-height { height: 150px; }
 }
 
 body {
   background: url({{ \URL::current() }}/../assets/img/login.jpg) no-repeat center center fixed;
 }

 /*.breadcrumb { background-color: #EBF5FB !important }*/
 .set-marmgin { margin-top: 20px; }
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,input,div { text-transform: uppercase !important; }
 ul li , ul li a { color: #fff !important; }

</style>
@extends('front.app')

@section('title')
{{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#" style="color: #fff">@lang('breadcrumbs.front')</a></li>
  <li class="active" style="color: #fff">@lang('breadcrumbs.dashboard')</li>
</ul>
@stop

@section('content')

<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <div class="col-md-12">
        <div class="row" style=" background-color: #EBF5FB !important ">
          
          
          @if($member->package_amount == 0 )
          <div class="col-sm-3"><br>
            <input type="button" name="activate" value="Activate Your ID" onclick="selectPackage()" class="btn btn-success">
          </div>
          @endif
        </div>



<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#check-term" style="display: none"  id="getPopUp" >
  Open modal
</button>



 <!--The Modal -->
       
      </div>
      <div class="dashboard grey lighten-3">
        <div class="row no-gutter">
          <div class="col-sm-12 col-md-12 col-lg-12" style="background-color:  #EBF5FB ;">
            <div class="p-20 clearfix">
              <div class="pull-right">
                <a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}" target="_blank" class="btn btn-round-sm btn-link" data-toggle="tooltip" title="@lang('home.qCheckHierarchy')"><i class="md md-swap-vert"></i></a>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}" class="btn btn-round-sm btn-link" data-toggle="tooltip" title="@lang('home.qAddMember')"><i class="md md-person-add"></i></a>

              </div>

              
            </div>

            <div class="p-20 no-p-t">
              @if (is_null($member->secret_password) || $member->secret_password == '')
              <div class="row" style="background-color:  #EBF5FB ">
                <div class="col-md-4">
                  <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      @lang('home.spMissing') <a href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}" class="alert-link" style="text-decoration: underline;">@lang('home.spMissingLink')</a>.
                    </div>
                  </div>
                </div>
              </div>
              @endif

                {{-- <div class="row gutter-14 kpi-dashboard">
                  <div class="col-md-4">
                    <div class="card small">
                      <div class="theme-lighten-1 p-10">
                        <div class="pull-right">
                          <div> <i class="md md-account-balance-wallet text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.pointTitle')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i> @lang('common.pointSub')</div>
                      </div>
                      <div class="card-content p-10">
                        <div class="row">
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span style="font-size: small">{{ explode(' ', $member->created_at)[0] }}</span></h3>
                            <p class="grey-text w600">@lang('common.joining_date')</p>
                          </div>
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span data-count data-start="0" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h3>
                            <p class="grey-text w600">@lang('common.register')</p>
                          </div>
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span style="font-size: small">{{ explode(' ', $member->created_at)[0] }}</span></h3>
                            <p class="grey-text w600">@lang('common.paid_date')</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> --}}

                  {{-- Total members count --}}

                  <div class="col-md-5">
                    <div class="card small">
                      <div class="green p-10" style="background-color: #099">
                        <div class="pull-right">
                          <div> <i class="md md-accessibility text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.members1')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i> @lang('common.members2')</div>
                      </div>
                      <div class="card-content p-10" style="background-color: #E5E7E9">
                        <div class="row" id="set-div-height">
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="
                            @if($member->children()->count()>0){{ $member->children()->count() }}@else{{'0'}}@endif" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.direct')</p>
                          </div>
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span data-count data-start="0" data-end="@if(!empty($member->right_children)){{ count(explode(',', $member->right_children))-1 }} @else {{0}} @endif" data-decimal="0"></span></h3>
                            <p class="grey-text w600">@lang('common.leftTotal')</p>
                          </div>
                          <div class="col-md-4 text-center">
                            <h3 class="no-margin w300"><span data-count data-start="0" data-end="{{$countAll}}" data-decimal="0"></span></h3>
                            <p class="grey-text w600">@lang('common.rightTotal')</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-1"></div>

                  {{-- Active Members --}}

                  

                  

                  {{-- Personal Information --}}

                  <div class="col-md-5">
                    <div class="card small">
                      <div class="green p-10" style="background-color: #099">
                        <div class="pull-right">
                          <div> <i class="md md-accessibility text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.membersInfo')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i>{{--  @lang('common.membersSub') --}}</div>
                      </div>
                      <div class="card-content p-10" style="background-color: #E5E7E9">
                        <div class="row" id="set-div-height">
                          <div class="col-sm-4 col-xs-6">
                            <p>Name </p>
                          </div>
                          <div class="col-sm-8 col-xs-6">
                            @if(!empty($member->name))
                            <p> : {{$member->name}}</p>
                            @endif
                          </div>
                          <div class="col-sm-4 col-xs-6">
                            <p>ID</p>
                          </div>
                          <div class="col-sm-8 col-xs-6">
                            @if(!empty($member->username))
                            <p> : {{$member->username}}</p>
                            @endif
                          </div>
                          <div class="col-sm-4 col-xs-6">
                            <p>Joining Date</p>
                          </div>
                          <div class="col-sm-8 col-xs-6">
                            @if(!empty($member->created_at))
                            <p> : {{$member->created_at}}</p>
                            @endif
                          </div>
                          
                        </div> 
                      </div>
                    </div>
                  </div>

                  <div class="col-md-1"></div>

                  

                  {{-- My Associates --}}

                  <div class="col-md-11">
                      <br><br>
                    <div class="card small">
                      <div class="green p-10" style="background-color: #099">
                        <div class="pull-right">
                          <div> <i class="md md-accessibility text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.membersTitle')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i> @lang('common.membersSub')</div>
                      </div>
                      <div class="card-content p-10" style="background-color: #E5E7E9">
                        <div class="row" id="set-div-height">
                             <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;"></div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[0] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level1')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[1] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level2')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[2] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level3')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[3] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level4')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[4] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level5')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[5] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level6')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[6] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level7')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[7] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level8')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[8] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level9')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="{{ $level[9] }}" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level10')</p>
                          </div>
                          

                        </div>
                      </div>
                    </div>
                  </div>

                  

                </div>           




                <?php $announcement = $announcementModel->orderBy('created_at', 'desc')->first(); ?>
                @if ($announcement)
                <?php
                $lang = \App::getLocale();
                $announcementTitle = null;
                $announcementContent = null;
                if (isset($announcement->{"title_$lang"})) $announcementTitle = $announcement->{"title_$lang"};
                else $announcementTitle = $announcement->title_en;
                if (isset($announcement->{"content_$lang"})) $announcementContent = $announcement->{"content_$lang"};
                else $announcementContent = $announcement->content_en;
                ?>
                <div class="panel panel-success panel-announcement">
                  <div class="panel-heading">
                    <h2 class="panel-title grey-text">
                      <i class="md md-alarm"></i> {{ $announcement->created_at->format('d F Y H:i A') }}
                    </h2>
                    <h1 class="m-t-10 m-b-5 f30">
                      <i class="md md-new-releases"></i> {{ $announcementTitle }}
                    </h1>
                    <p class="small grey-text no-margin">
                      {{ Str::limit(strip_tags($announcementContent), 100) }}
                    </p>
                    <hr>
                    <a href="{{ route('announcement.read', ['id' => $announcement->id, 'lang' => $lang]) }}" class="btn btn-primary">@lang('common.read')</a>
                  </div>
                </div>
                @endif
              </div>

              
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>

<?php
$annNew = \Cache::remember('announcement.new', 60, function () {
  return \App\Models\Announcement::whereRaw('Date(created_at) = CURDATE()')->first();
});
?>
@if ($annNew)
<div class="modal fade" tabindex="-1" role="dialog" id="annModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">@lang('announcement.newTitle')</h4>
      </div>
      <div class="modal-body">
        <p>@lang('announcement.newContent') <a href="{{ route('announcement.read', ['id' => $annNew->id, 'lang' => $lang]) }}">@lang('announcement.newLink')</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('common.close')</button>
      </div>
    </div>
  </div>
</div>
@endif

   <div class="modal" id="check-term">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header" style="text-align: center !important">
        <h4 class="modal-title" style="text-align: center !important">Terms & Conditions</h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        @lang('terms.content')
        <br>
        <div class="custom-control custom-checkbox">
    &nbsp;&nbsp;<input type="checkbox" class="custom-control-input" id="defaultUnchecked">
    <label class="custom-control-label" for="defaultUnchecked">I accept Terms & Conditions</label>
    <p id="showchecked" style="display: none; color: #ff0000"></p>
</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-danger" onclick="checkTerms({{$member->id}})" >Continue</button>
        <button type="button" class="btn btn-danger" id="successTerm" data-dismiss="modal" style="display: none">Close</button>
      </div>

    </div>
  </div>
</div>
      

<script> $('#getPopUp').click(); </script>


<script>
  function selectPackage(){
    window.location.href = "/en/member/activate-id";
  }
  
  
  function checkTerms(id){
    //   alert(id);
            if (document.getElementById('defaultUnchecked').checked) {
                var check = "yes";
                 $('#showchecked').hide();
                  $.ajax({
        type: "POST",
        url: "/en/checkTerm",
        data:{ id: id },
        success:function(msg){
          // alert(msg);
          if(msg.message == 'Success'){
            $('#successTerm').click();
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }          
        }
      });
            // alert("checked");
        } else {
            $('#showchecked').show();
            $('#showchecked').text(' *First accept terms & conditions');
        }
        }
  
  
  
  
</script>
@stop
