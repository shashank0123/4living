@extends('front.app')

@section('title')
Forget Password - {{ config('app.name') }}
@stop

@section('content')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important;
  }
  .pink-text {  font-size: 24px;  }
  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .pull-right button { margin-bottom: 20px !important }

  @media screen and (max-width: 991px) {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }
  @media screen and (max-width: 580px) {
    .card { max-width:90%;margin-left: 5%; }
  }

label { text-transform: uppercase !important; color: #fff; }
  #step2, #step3 { display: none; }

 /* #step1, #step2 , #step3 { position: absolute; }*/
  
  #login-head { width: 100% !important; background-color: #099 }
  .card.bordered .card-action {
     border-top: none !important; 
}
</style>

<div class="clearfix"></div>

<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: #099; top: 0px; position: absolute;">
    <div style="background-color: #099;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
    <ul>
      <li style="list-style: none">
        <a href="#" style="color: #fff; font-size: 14px; text-transform: uppercase !important;">Help Line:  1800-419-8447</a>
      </li>

    </ul>
  </div>
</div>

<div class="card bordered z-depth-2" >
  <div class="card-header" style="background-color: #099">
    <div class="brand-logo" style="text-align: center;">
      <img src="{{ asset('/images/logo.gif') }}" width="100">
    </div>
  </div>

  <div class="container m-b-30" style="max-width: 100%; background-color: #099" id="step1">
    {{-- //Get username $ Registered Mobile number --}}
    <form class="form-floating action-form" http-type="post" data-url="/userinfo" id="getUserInput">
      <div class="card-content">
        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong black-text" style="text-transform: uppercase !important; font-weight: bold;">Forgot Password</div>
          </div>
          <div class="form-group">
            <label for="username">Enter Username</label>
            <input type="text" style="text-transform: uppercase;" name="username" class="form-control" id="username" required="required">
          </div>
          <div class="form-group">
            <label for="username">Enter Registered Phone Number</label>
            <input type="text" name="phone" class="form-control" pattern="[789][0-9]{9}" id="phone" required="required">
          </div>

          <div class="card-action clearfix">
            <div class="pull-left">
              <a href="/en/login" class="btn btn-link black-text">              
                <span style="color:black; font-weight: bold;">Cancel</span>
              </a>
            </div>
            <div class="pull-right">
              <span id="button" onclick="showVerifyDiv()" class="btn btn-link black-text">
                <span class="btn-preloader">
                  <i class="md md-cached md-spin"></i>
                </span>
                <span style="background-color: #099;color: #fff">CONTINUE..</span>
              </span>
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>

    {{-- //Verify OTP send to a Registered Mobile Number --}}
    <div class="container m-b-30" style="max-width: 100%;" id="step2">

      <form class="form-floating action-form" http-type="post" data-url="/checkOTP" id="receiveOTP">
        <div class="card-content">

          <div class="card-content">
            <div class="m-b-30">
              <div class="card-title strong pink-text">Verify OTP</div>
            </div>

            <input type="hidden" name="verifiedphone" class="form-control" id="verifiedphone" required="required">
            <input type="hidden" name="verifiedusername" class="form-control" id="verifiedusername" required="required">

            <div class="form-group">
              <label for="username">Enter OTP</label>
              <input type="number" name="otp" class="form-control" id="otp" required="required">
            </div>        

            <div class="card-action clearfix">            
              <div class="pull-right">
                <span id="button" onclick="showResetDiv()" class="btn btn-link black-text">
                  <span class="btn-preloader">
                    <i class="md md-cached md-spin"></i>
                  </span>
                  <span>Submit</span>
                </span>
              </div>
            </div>
          </div>
        </div>
        </form>
      </div>

      {{-- //Rese Password after Successfully OTP verification --}}
      <div class="container m-b-30" style="max-width: 100%;" id="step3">
        <form class="form-floating action-form" http-type="post" data-url="/reset" id="getNewPassword">

          <div class="card-content">

            <div class="card-content">
              <div class="m-b-30">
                <div class="card-title strong pink-text">Reset Password</div>
              </div>

              <input type="hidden" name="resetphone" class="form-control" id="resetphone" required="required">
            <input type="hidden" name="resetusername" class="form-control" id="resetusername" required="required">

              <div class="form-group">
                <label for="username">Enter New Password</label>
                <input type="password" name="password" class="form-control" id="password" required="required">
              </div>

              <div class="form-group">
                <label for="username">Re-Enter New Password</label>
                <input type="password" name="compare_password" class="form-control" id="compare_password" required="required">
              </div>

              <div class="card-action clearfix">
                <div class="pull-left">
                  <a href="/en/login" class="btn btn-link black-text">              
                    <span style="color:red">Cancel</span>
                  </a>
                </div>

                <div class="pull-right">
                  <span id="button" onclick="resetPassword();" class="btn btn-link black-text">
                    <span class="btn-preloader">
                      <i class="md md-cached md-spin"></i>
                    </span>
                    <span>Reset</span>
                  </span>
                </div>
              </div>

            </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    @stop


    <script>
      //to send otp to registered mobile number
      function showVerifyDiv(){
        var request = $.ajax({
          type: "POST",
          url: "/userinfo",
          data: $('#getUserInput').serialize(),
        });
        request.done(function(msg) {
          if (msg.message == 'User Exists and updated OTP field'){

            $('#step1').hide();
            $('#verifiedphone').val($('#phone').val());
            $('#verifiedusername').val($('#username').val());
            console.log(msg);
            $('#step2').show();            
          }
          else
            Swal(msg.message);                  
        });

        request.fail(function(msg) {
          Swal(msg.message);
        });        
      }

      function showResetDiv(){
        var request = $.ajax({
          type: "POST",
          url: "/checkOTP",
          data: $('#receiveOTP').serialize(),
        });
        request.done(function(msg) {
          if (msg.message == 'Otp Verified'){

            $('#step2').hide();
            $('#resetphone').val($('#verifiedphone').val());
            $('#resetusername').val($('#verifiedusername').val());
            console.log(msg);
            $('#step3').show();            
          }
          else
            Swal(msg.message);                  
        });

        request.fail(function(msg) {
          Swal(msg.message);
        });
      }

      function resetPassword(){
        var request = $.ajax({
          type: "POST",
          url: "/reset",
          data: $('#getNewPassword').serialize(),
        });
        request.done(function(msg) {
          if (msg.message == 'Data inserted Successfully'){   
          Swal("Password Successfully Updated");
          console.log(msg);
          setTimeout(function(){ window.location.href = "/en/login"; }, 2000);              
          }
          else
            Swal(msg.message);                  
        });

        request.fail(function(msg) {
          Swal(msg.message);
        });
      }
      

    </script>