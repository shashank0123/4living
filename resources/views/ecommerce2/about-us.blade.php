@extends('layouts/ecommerce2')
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
@section('content')


<!-- Hiraola's Header Main Area End Here -->
<!-- policy content -->
<style type="text/css">
  .textpolicy{ background-color: white; margin-top: 20px; margin-bottom: 100px; text-align: justify; }
  .textpolicy p{font-weight: 100px; padding: 0px 30px  0px 30px ; font-size: 15px;
    line-height: 24px; font-family: "Lato", sans-serif;     color: #595959; letter-spacing: 0.5px;}
    .textpolicy h3{ text-align: center; margin-top: 20px; margin-bottom: 10px; font-size: 28px; font-weight:600 }
    .textpolicy h5{padding: 0px 30px  0px 30px ; font-size: 20px ; font-weight: 600; }
    .textpolicy ol {list-style-type:decimal; margin-left:  10%; margin-right: 10%;}
    .textpolicy ul {list-style-type:decimal; margin-left:  10%; margin-right: 10%}
    .table tr th , .table tr td{ text-align: center;color: #595959; padding: 15px 0 !important }
    .textpolicy ol li { line-height: 1.8; color: #595959; font-size: 15px;}
    .textpolicy ul li { line-height: 1.8; color: #595959; font-size: 15px;}

    .about-us-area .overview-content { margin-top: 15%; text-align: center; }
    .about-us-area .overview-content span { color: #37bc9b; }
    .about-us_btn { display: block;padding: 10px; background-color:  #37bc9b; color: #fff; width: 20%; }
    .hiraola-about-us_btn-area a{ margin-left: 35% }
    .count { font-size: 48px; color:  #37bc9b; }
    .team-area { text-align: center; }
    .team-area h4 { font-size: 28px; font-weight: 600; margin-bottom: 20px }
    .count-title span { font-weight: bold;color: #777; font-size: 20px }
    a { color: #555; }


  </style>
  <div class="container-fluid" style="background-color:#F1F3F6;">
    <div class="row">

     <div class="col-sm-2"></div> 
     <div  class="col-sm-8 textpolicy">
      @if($content != null)


      <h3> {{$content->page_heading}} </h3>
      <hr>

      <p><?php echo htmlspecialchars_decode($content->description); ?></p>

      @else 
      <h3>Comming Soon..</h3>

      @endif

    </div> 


    <div class="col-sm-2"></div>      
  </div>
</div>

<!-- Begin Hiraola's Project Countdown Area -->
<div class="project-count-area">
  <div class="container-fluid"> <br><br>
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="single-count text-center">
          <div class="count-icon">
            <span class="ion-ios-briefcase-outline"><i class='fas fa-briefcase' style='font-size:36px'></i></span><br><br>
          </div>
          <div class="count-title">
            <h2 class="count">3000</h2>
            <span>Product Sold</span>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="single-count text-center">
          <div class="count-icon">
            <span class="ion-ios-wineglass-outline"><i class='fas fa-wine-glass-alt' style='font-size:36px'></i></span><br><br>
          </div>
          <div class="count-title">
            <h2 class="count">300</h2>
            <span>Customers Served</span>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="single-count text-center">
          <div class="count-icon">
            <span class="ion-ios-lightbulb-outline"><i class='far fa-lightbulb' style='font-size:36px'></i></span><br><br>
          </div>
          <div class="count-title">
            <h2 class="count">768</h2>
            <span>Days Worked</span>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="single-count text-center">
          <div class="count-icon">
            <span class="ion-happy-outline"><i class='far fa-smile' style='font-size:36px'></i></span><br><br>
          </div>
          <div class="count-title">
            <h2 class="count">450</h2>
            <span>Product Served</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><br><br>
<!-- Hiraola's Project Countdown Area End Here -->


<!-- Hiraola's Product Tab Area Three End Here -->


@endsection

@section('script')
<script>
  $('.count').each(function() {
    $(this).prop('Counter', 0).animate({
      Counter: $(this).text()
    }, {
      duration: 5000,
      easing: 'swing',
      step: function(now) {
        $(this).text(Math.ceil(now));
      }
    });
  });
</script>
@endsection