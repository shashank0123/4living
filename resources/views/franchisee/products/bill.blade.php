<?php
use App\Product;

?>

@extends('franchisee.app')

@section('title')
@lang('product.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.hierarchy')</a></li>
  <li class="active">@lang('breadcrumbs.purchaserequest')</li>
</ul>
@stop

@section('content')
<style>
  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
  .custom { width: 100px!important; border: 1px solid #ddd !important; }

  .bottom-div { display: none; }
</style>
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <<section>
        <div class="page-header">
          <h1 style="font-size: 24px; "><i class="md md-person-add"></i> Checkout</h1>
         
        </div>

        <div class="row m-b-40">
          <div class="col-md-2">
            
              </div>

              <div class="col-md-8">
                <div class="well" style="background-color: #ffffff">
                  <div class="top-div">
                  <div class="form-group">
                      <label class="control-label" for="bill">Total Payable Amount</label>
                      <input type="text" class="form-control" id="bill" name="bill" value="{{$bill}}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="payment_mode">Mode Of Payment</label>
                      <select type="text" class="form-control" id="payment_mode" name="payment_mode" value="{{ $member->payment_mode }}">
                        <option value="COD">Cash on Delivery</option>
                        <option value="Bank">Banking</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="bank_name">Bank Name</label>
                      <input type="text" class="form-control" id="bank_name" name="bank_name" value="@if(!empty($member->bank_name)){{$member->bank_name}}@endif">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="account_number">Account Number</label>
                      <input type="text" class="form-control" id="account_number" name="account_number" value="@if(!empty($member->account_number)){{$member->account_number}}@endif">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="account_holder_name">Account Holder Name</label>
                      <input type="text" class="form-control" id="account_holder_name" name="account_holder_name" value="@if(!empty($member->account_holder_name)){{$member->account_holder_name}}@endif">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="phone">Phone</label>
                      <input type="text" class="form-control" id="phone" name="phone" value="@if(!empty($member->phone)){{$member->phone}}@endif">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="address">Delivery Address</label>
                      <textarea class="form-control" id="address" name="address" ></textarea>
                    </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span onclick="submit()">@lang('common.continue')</span>
                    </button>
                    <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                  </div>

                </div>

                <div class="bottom-div" style="text-align: center; margin: 150px 50px"> 
                  <h3>
                    Thank you. We will replace your order soon. 
                  </h3>
                </div>



                </div>
              </div>
            </div>
          </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">
            <span class="md md-accessibility"></span> @lang('register.modal.title')
          </h4>
        </div>
        <div class="modal-body">
          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">@lang('common.modal.load')</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">@lang('common.modal.error')</small>
          </div>

          <div id="modalContent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>
  <script>
  function submit(){
    var bill = $('#bill').val();
    var payment_mode = $('#payment_mode').val();
    var bank_name = $('#bank_name').val();
    var account_holder_name = $('#account_holder_name').val();
    var phone = $('#phone').val();
    var account_number = $('#account_number').val();
    var address = $('#address').val();

    $.ajax({
        type: "POST",
        url: "/franchisee/success",
        data:{ bill : bill,payment_mode: payment_mode,bank_name: bank_name,account_holder_name: account_holder_name, phone: phone, account_number: account_number, address: address },
        success:function(msg){
          alert('Done');
          $('.top-div').hide();
          $('.bottom-div').show();
        }
      });
  }
  </script>
  @stop
