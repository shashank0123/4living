<?php
use App\Product;
$bill = 0; 
?>

@extends('franchisee.app')

@section('title')
@lang('product.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.hierarchy')</a></li>
  <li class="active">@lang('breadcrumbs.purchaserequest')</li>
</ul>
@stop

@section('content')
<style>
  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
  .custom { width: 100px!important; border: 1px solid #ddd !important; }
</style>
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1 style="font-size: 24px"><i class="md md-person-add"></i> Checkout</h1>
          {{--  <p class="lead">@lang('product.title')</p> --}}
        </div>

        <div class="row m-b-40">
          <div class="col-md-2">
            <!--<div class="row">-->
            <!--  <div class="col-xs-6 col-md-12">-->
            <!--    <label>@lang('common.register')</label>-->
            <!--    <h2 class="theme-text" style="margin-top:0;"><span data-count data-start="0.00" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h2>-->
            <!--  </div>                -->
            <!--</div>-->
          </div>

          <div class="col-md-12">
            <div class="well" style="background-color: #ffffff">
              
                <fieldset>
                  


                  <div class="card">
          <div>
            <div class="">
             <table class="table table-full" data-url="">
              <thead>
                <tr>
                  <th>@lang('product.sr')</th>
                  <th>@lang('product.name')</th>
                  <th>@lang('product.image')</th>
                  <th>@lang('product.mrp')</th>
                  <th>@lang('product.sellingprice')</th>
                  <th>@lang('product.pv') /<br> @lang('product.bv')</th>
                  <th>@lang('product.quantity')</th>
                  <th>@lang('product.cost')</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                
                @if(!empty($data))
                @foreach ($data as $datas)
                <?php
                $product = Product::where('id',$datas->product_id)->first();
                ?>

                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$product->name}}</td>
                  <td><img src="{{asset('/assetsss/images/AdminProduct/')}}/{{$product->image1}}" style="width: 100px; height: auto"></td>
                  <td>{{$product->mrp}}</td>                  
                  <td>{{$product->sell_price}}</td>                  
                  <td>{{$product->pv}}/{{$product ->bv}}</td>      

                  <td><input class="custom" type="text" value="{{$datas->quantity}}" min="0" name="quantity" onchange="clickUpdate(<?php echo $datas->product_id; ?>)" id="quant{{$datas->product_id}}"></td>
                  <td><input class="custom" type="text" value="{{$datas->quantity * $product->sell_price}}" min="0" id="amount{{$datas->product_id}}" name="amount{{$datas->product_id}}" readonly></td>

                  <td><i class="fa fa-times"></i></td>
                  <?php $bill=$datas->quantity*$product->sell_price +$bill;?>
                </tr>
                @endforeach
                @endif

               
              </tbody>
            </table>

             <div class="row" style="text-align: right;padding-right: 45px ; font-size: 18px; font-weight: bold;">
                  Total Bill : <span id="bill">{{$bill}}</span>
                </div>

            @if($i == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif
          </div>
        </div>
                    

                    
                   
                    
                  </fieldset>
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span onclick="cartEntry()">@lang('common.checkout')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
               
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">
            <span class="md md-accessibility"></span> @lang('register.modal.title')
          </h4>
        </div>
        <div class="modal-body">
          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">@lang('common.modal.load')</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">@lang('common.modal.error')</small>
          </div>

          <div id="modalContent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>
  <script>
    var cart_id = <?php echo $cart_id; ?>;
    function clickUpdate(id){
      var value = $('#quant'+id).val();

      if(value <= 0) {
        $('#quant'+id).val('0');
      }
     
      $.ajax({
        type: "POST",
        url: "/franchisee/add-cart",
        data:{ quantity: value , id: id},
        success:function(msg){
          if(msg.message == 'Products added successfully.'){
            $('#amount'+id).val(msg.amount);
            $('#bill').val(msg.bill);
          }
        }
      });
    }

    function cartEntry(){
      $.ajax({
        type: "POST",
        url: "/franchisee/cart-entry",
       
        success:function(msg){
          if(msg.message == 'Added Successfully'){
            window.location.href = '/franchisee/payment';
          }
        }
      });
    }
    
  </script>
  @stop
