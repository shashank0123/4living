<?php
use App\Product;
?>
@extends('franchisee.app')
@section('title')
@lang('payment.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">@lang('breadcrumbs.producthistory')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
 .table {max- width:90% !important;}
</style>
@section('content')
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">

        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">                
              <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> @lang('breadcrumbs.producthistory')</h1>
              <p class="lead">@lang('breadcrumbs.producthistory')</p>
            </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/en/my-products">
               
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
            </div>
          </div>
        </div>         


        
        <div class="card">
          <div>
            <div class="">
             <table class="table table-full" data-url="">
              <thead>
                <tr>
                  <th>@lang('product.sr')</th>
                  <th>@lang('product.date')</th>
                  <th>@lang('product.description')</th>
                  <th>@lang('product.payment_mode')</th>
                  <th>@lang('product.amount')</th>
                  <th>@lang('product.status')</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                
                @if(!empty($orders))
                @foreach ($orders as $order)

                <tr>
                  <td>{{$i++}}</td>
                  <td>
                    <?php echo explode(' ',$order->created_at)[0]; ?>
                      
                    </td>
                  <td>
                    <?php 
                    $products = json_decode($order->cart);
                     foreach($products as $product){
                      $pro = Product::where('id',$product->product_id)->first();

                      if(!empty($pro)){
                         echo $pro->name." - (".$product->quantity.")<br>";
                      }
                     }
                     ?>
                       
                     </td>                  
                  <td>{{$order->payment_method}}</td>                  
                  <td>{{$order->price}}</td>                  
                  <td>{{$order->status}}</td>

                </tr>
                @endforeach
                @endif
              </tbody>
            </table>

            @if($i == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
<script>
  function getvalue(){
    var search= $('#search').val();
    alert(search);
  }
</script>
@stop
