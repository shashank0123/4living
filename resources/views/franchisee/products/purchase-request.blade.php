

@extends('franchisee.app')

@section('title')
@lang('product.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.hierarchy')</a></li>
  <li class="active">@lang('breadcrumbs.purchaserequest')</li>
</ul>
@stop

@section('content')
<style>
  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
  .custom { width: 100px!important; border: 1px solid #ddd !important; }
</style>
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-person-add"></i> @lang('product.title')</h1>
          {{--  <p class="lead">@lang('product.title')</p> --}}
        </div>

        <div class="row m-b-40">
          <div class="col-md-2">
            <!--<div class="row">-->
              <!--  <div class="col-xs-6 col-md-12">-->
                <!--    <label>@lang('common.register')</label>-->
                <!--    <h2 class="theme-text" style="margin-top:0;"><span data-count data-start="0.00" data-end="@if (isset($epoints->epoint)) {{$epoints->epoint}} @else {{ 0 }} @endif" data-decimal="0"></span></h2>-->
                <!--  </div>                -->
                <!--</div>-->
              </div>

              <div class="col-md-12">
                <div class="well" style="background-color: #ffffff">

                  <fieldset>



                    <div class="card">
                      <h4>Select Products</h4>
                      <div>
                        <div class="">
                         <table class="table table-full" data-url="">
                          <thead>
                            <tr>
                              <th>@lang('product.sr')</th>
                              <th>@lang('product.name')</th>
                              <th>@lang('product.image')</th>
                              <th>@lang('product.mrp')</th>
                              <th>@lang('product.sellingprice')</th>
                              <th>@lang('product.pv') /<br> @lang('product.bv')</th>
                              <th>@lang('product.quantity')</th>
                              <th>@lang('product.cost')</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>

                            @if(!empty($data))
                            @foreach ($data as $datas)


                            <tr>
                              <td>{{$i++}}</td>
                              <td>{{$datas->name}}</td>
                              <td><img src="{{asset('/assetsss/images/AdminProduct/')}}/{{$datas->image1}}" style="width: 100px; height: auto"></td>
                              <td>{{$datas->mrp}}</td>                  
                              <td>{{$datas->sell_price}}</td>                  
                              <td>{{$datas->pv}}/{{$datas->bv}}</td>      

                              <td><input class="custom" type="text" value="@if(!empty($show))@foreach($show as $s)@if($datas->id == $s->product_id){{$s->quantity}}@endif @endforeach @else{{0}}@endif" min="0" name="quantity" onchange="clickQuant(<?php echo $datas->id; ?>)" id="quant{{$datas->id}}"></td>
                              <td><input class="custom" type="text" value="@if(!empty($show))@foreach($show as $s)@if($datas->id == $s->product_id){{$s->quantity * $datas->sell_price}}@endif  @endforeach @else{{0}}@endif" min="0" id="amount{{$datas->id}}" name="amount{{$datas->id}}" readonly></td>

                            </tr>
                            @endforeach
                            @endif
                          </tbody>
                        </table>

                        @if($i == 1)
                        <div style="text-align: center; margin-top: 2%">
                          <h3>No result found</h3>
                        </div>
                        @endif
                      </div>
                    </div>
                                       
                  </fieldset>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span onclick="cartEntry()">@lang('common.continue')</span>
                    </button>
                    <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                  </div>

                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </main>

    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="showModalLabel">
              <span class="md md-accessibility"></span> @lang('register.modal.title')
            </h4>
          </div>
          <div class="modal-body">
            <div class="loading text-center">
              <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
              <br>
              <small class="text-primary">@lang('common.modal.load')</small>
            </div>

            <div class="error text-center">
              <i class="md md-error"></i>
              <br>
              <small class="text-danger">@lang('common.modal.error')</small>
            </div>

            <div id="modalContent"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
          </div>
        </div>
      </div>
    </div>
    <script>
      function clickQuant(id){
        var value = $('#quant'+id).val();

        if(value <= 0) {
          $('#quant'+id).val('0');
        }

        $.ajax({
          type: "POST",
          url: "/franchisee/add-cart",
          data:{ quantity: value , id: id},
          success:function(msg){
            if(msg.message == 'Product added successfully.'){
              $('#amount'+id).val(msg.amount);
            }
          }
        });
      }

      function cartEntry(){
        $.ajax({
          type: "POST",
          url: "/franchisee/cart-entry",

          success:function(msg){
            if(msg.message == 'Added Successfully'){
              alert(msg.message);
              window.location.href = '/franchisee/checkout';
            }
          }
        });
      }

    </script>
    @stop
