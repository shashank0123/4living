<?php
  use App\Product;
?>

@extends('franchisee.app')
@section('title')
@lang('payment.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">@lang('breadcrumbs.products')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
 .table {max- width:90% !important;}
</style>
@section('content')
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">

        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">                
              <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> @lang('breadcrumbs.products')</h1>
              <p class="lead">@lang('breadcrumbs.products')</p>
            </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/en/my-products">
               
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
            </div>
          </div>
        </div>         


        
        <div class="card">
          <div>
            <div class="">
             <table class="table table-full" data-url="">
              <thead>
                <tr>
                  <th>@lang('product.sr')</th>
                  <th>@lang('product.name')</th>
                  <th>@lang('product.image')</th>
                  <th>@lang('product.mrp')</th>
                  <th>@lang('product.sellingprice')</th>
                  <th>@lang('product.pv')/<br>@lang('product.bv')</th>
                  <th>@lang('product.quantity')</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                
                @if(!empty($data))
                @foreach ($data as $d)
                <?php
                $datas = Product::where('id',$d->product_id)->first();
                ?>
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$datas->name}}</td>
                   <td><img src="{{asset('/assetsss/images/AdminProduct/')}}/{{$datas->image1}}" style="width: 100px; height: auto"></td>
                  <td>{{$datas->mrp}}</td>                  
                  <td>{{$datas->sell_price}}</td>                  
                  <td>{{$datas->pv}}/{{$datas->bv}}</td>                  
                  <td>{{$d->quantity}}</td>

                </tr>
                @endforeach
                @endif
              </tbody>
            </table>

            @if($i == 1)
            <div style="text-align: center; margin-top: 2%">
              <h3>No result found</h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
<script>
  function getvalue(){
    var search= $('#search').val();
    alert(search);
  }
</script>
@stop
