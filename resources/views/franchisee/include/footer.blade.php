<?php
  $route = \Route::currentRouteName();
  if (is_null($route)) $route = 'home';
  if ($route == 'announcement.read' || $route == 'coin.wallet.detail' || $route == 'coin.transaction.detail') {
    $routeEN = route($route, ['lang' => 'en', 'id' => $model->id]);
    $routeCHS = route($route, ['lang' => 'chs', 'id' => $model->id]);
    $routeCHT = route($route, ['lang' => 'cht', 'id' => $model->id]);
  } else {
    $routeEN = route($route, ['lang' => 'en']);
    $routeCHS = route($route, ['lang' => 'chs']);
    $routeCHT = route($route, ['lang' => 'cht']);
  }
?>

<!-- footer start -->
         <footer class="footer-area" style="border-top : 3px solid #eee">
            <div class="footer-top pt-30 pb-25" style="background-color: #f5f5f5" >
                <div class="container">
                    <div class="row">
                        <div class="footer-logo">
                             <img src="{{ URL::asset('asset/images/logo/logo-1.png')}}" alt="logo" style="width:200px">
                        </div>
                        
                    </div>
                    <div class="row">
                        
                        <div class="col-lg-3 col-md-3 col-12 col-sm-6">
                            <div class="footer-widget mb-30 footer-mrg-hm1">
                                <div class="footer-title">
                                    <h3>COMPANY</h3>
                                </div>
                                <div class="footer-list">
                                    <ul> 
                                         <li><a href="<?php echo  url('/4living')?>">About 4LivinG</a></li>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="<?php echo  url('/product/ayurveda')?>">Products</a></li>
                                       
                                        <li><a href="#.html">Downloads</a></li>
										<li><a href="<?php echo  url('/privacy-policy')?>">Privacy Policy</a></li>
										<li><a href="<?php echo  url('/terms')?>">Terms & condition</a></li>
										 <li><a href="#.html">News & Events</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-2 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title">
                                    <h3>QUICK LINKS</h3>
                                </div>
                                <div class="footer-list">
                                    <ul>
									 <li><a href="<?php echo  url('/drpremprakash')?>">Dr. Prem Prakash</a></li>
									 
                                         <li><a href="<?php echo  url('/doctors-team')?>">Team 4LivinG</a></li>
                                          <li><a href="<?php echo  url('/consult-doctors')?>">Ask The Doctor</a></li>
										  <li><a href="<?php echo  url('/doctors-team')?>">Testimonials</a></li>
                                           <li><a href="<?php echo  url('/about-ayurveda')?>">About Ayurveda</a></li>
                                       
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title">
                                    <h3>CONNECT</h3>
                                </div>
                                <div class="footer-list">
                                      <ul>
                                        <li><a href="<?php echo url('/franchise/login')?>">Franchise Login</a></li>
                                        <li><a href="contact">Contact Us </a></li>
                                        <li><a href="<?php echo url('/login')?>">IBD Login</a></li>
                                        
                                        
                                    </ul>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                              <div class="footer-title">
                                    <h3>FOLLOWING 4 LivinG:</h3>
                                </div>
                                <div class="footer-social">
                                  
                                    <ul>
                                        <li><a href="#"><i class=" ti-facebook "></i></a></li>
                                        <li><a href="#"><i class=" ti-twitter-alt "></i></a></li>
                                        <li><a href="#"><i class=" ti-pinterest "></i></a></li>
                                        <li><a href="#"><i class=" ti-vimeo-alt "></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom bg-gray-2 ptb-20">
                <div class="container">
                    <div class="copyright text-center">
                        <p>Copyright © <a href="#">4LivinG</a>. All Right Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="{{ URL::asset('asset/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <!-- Modernizer JS -->
    <script src="{{ URL::asset('asset/js/vendor/jquery-3.3.1.min.js') }}"></script>
    <!-- Popper JS -->
    <script src="{{ URL::asset('asset/js/vendor/popper.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ URL::asset('asset/js/vendor/bootstrap.min.js') }}"></script>

    <!-- Slick Slider JS -->
    <script src="{{ URL::asset('asset/js/plugins/countdown.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/counterup.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/images-loaded.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/isotope.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/instafeed.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery-ui.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery-ui-touch-punch.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/magnific-popup.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/owl-carousel.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/scrollup.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/waypoints.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/slick.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/wow.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/textillate.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/elevatezoom.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/sticky-sidebar.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/smoothscroll.js') }}"></script>
    <!-- Main JS -->
    <script src="{{ URL::asset('asset/js/main.js') }}"></script>