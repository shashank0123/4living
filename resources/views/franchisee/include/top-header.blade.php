<?php
  $route = \Route::currentRouteName();
  if (is_null($route)) $route = 'home';
  if ($route == 'announcement.read' || $route == 'coin.wallet.detail' || $route == 'coin.transaction.detail') {
    $routeEN = route($route, ['lang' => 'en', 'id' => $model->id]);
    $routeCHS = route($route, ['lang' => 'chs', 'id' => $model->id]);
    $routeCHT = route($route, ['lang' => 'cht', 'id' => $model->id]);
  } else {
    $routeEN = route($route, ['lang' => 'en']);
    $routeCHS = route($route, ['lang' => 'chs']);
    $routeCHT = route($route, ['lang' => 'cht']);
  }
?>

<div class="main-wrapper">
<header class="header-area header-padding-4">
    <div class="main-header-wrap">
        <div class="header-top pdt-5 pdb-15 ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        
                    </div>
                    <div class="col-lg-8">
                        <div class="setting-wrap-2">
                            <div>
                                <span class="tollno1">Contact No.:</span><span class="tollno2"><a href=""> 8950603008</a></span> &nbsp;&nbsp;&nbsp;
                                <span class="tollno1"> Email:</span>
                                <span class="tollno2"><a href="mailto:info@4living.in"> info@4living.in</a></span>
                            </div>
                            <!--<div class="setting-content2-left">-->
                            <!--    <a class="currency-dropdown-active" href="#">Language <i class="la la-angle-down"></i></a>-->
                            <!--    <div class="currency-dropdown">-->
                            <!--        <ul>-->
                            <!--            <li><a href="#">Hindi</a></li>-->
                            <!--            <li><a href="#">English</a></li>-->
                            <!--            <li><am href="#">Marathi</a></li>-->
                            <!--            <li><a href="#">MAithili</a></li>-->
                            <!--        </ul>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle  pdt-5 pdb-15 ">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <div class="logo logosize">
                            <a href="/"><img src="{{ URL::asset('asset/images/logo/logo-2.png') }}" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="header-contact-search-wrap">
                            <!--  <div class="header-contact">
                                <ul>
                                    <li><i class="la la-phone"></i> +00 112 336 555</li>
                                    <li><i class="la la-comments-o"></i> <a href="#">demo@mail.com</a></li>
                                </ul>
                                </div> -->
                            <div class="search-style-3">
                                <form>
                                    <div class="form-search-3">
                                        <input id="search" class="input-text" value="" placeholder="Search Here" type="search">
                                        <button>
                                        <i class="la la-search"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="cart-wrap cart-wrap-3">
                            <!--<button class="">-->
                            <!--<a href="wishlist.html"> <i class="la la-heart-o"></i> <br>-->
                            <!--<span class="count-style-3">01</span></a>-->
                            <!--</button> &nbsp;&nbsp;&nbsp;-->
                            <button class="cart-active">
                                <i class="la la-shopping-cart"></i> <br>
                                <!-- <span class="mini-cart-price-3">$400.00</span> -->
                                <span class="count-style-3">01</span>
                            </button>
                            <div class="shopping-cart-content">
                                <div class="shopping-cart-top">
                                    <h4>Your Cart</h4>
                                    <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                </div>
                                <ul>
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/1.jpg') }}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Tulsi.</a></h4>
                                            <span> ₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/2.jpg') }}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Thyro Sys.</a></h4>
                                            <span> ₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/3.jpg') }}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Artho Sys.</a></h4>
                                            <span> ₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="shopping-cart-bottom">
                                    <div class="shopping-cart-total">
                                        <h4>Subtotal <span class="shop-total"> ₹ 290.00</span></h4>
                                    </div>
                                    <div class="shopping-cart-btn btn-hover default-btn text-center">
                                        <a class="black-color" href="checkout">Continue to Chackout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom sticky-bar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-center">
                        <div class="main-menu menu-common-style menu-lh-4 menu-margin-5 menu-font-2">
                           <nav id="cssmenu">
                                <ul>
                                    <li><a href="/">HOME</a>
                                    </li>
                                    <li><a href="/4living">LIVE 4LivinG </a>
                                        
                                    </li>
                                    <li class="has-sub"><span class="submenu-button"></span>
                                        <a  href="#">KNOW YOUR BODY</a>
                                        <ul class="submenu">
                                            <li><a href="/humanbody">HUMAN BODY INTRO</a></li>
                                            
                                        </ul>
                                    </li>
                                    <li class="has-sub"><span class="submenu-button"></span>
                                        <a href="javascript:void();">PRODUCT</a>
                                         <ul class="submenu">
										 <li><a href="<?php echo  url('/product/ayurveda')?>">AYURVEDA MEDICINES</a></li>
										 <li><a href="<?php echo  url('/product/nutrionals')?>">NUTRIONALS</a></li>
										 <li><a href="<?php echo  url('/product/skin-care')?>">SKIN CARE</a></li>
										 
											
                                        </ul>
                                    </li>
                                    <li class="has-sub"><span class="submenu-button"></span>
                                        <a >COMPANY</a>
                                   
                                        <ul class="submenu">
                                             <li><a href="<?php echo  url('/about-us')?>">ABOUT US </a></li>
                        <li><a href="<?php echo  url('/directors-desk')?>"">DIRECTOR'S DESK </a></li>
                        <li><a href="<?php echo url('/panel-of-doctors')?>">PANEL OF DOCTOR'S </a></li>
                        <li><a href="<?php echo  url('/consult-doctors')?>">CONSULT WITH DOCTOR'S </a></li>
                        <li><a href="<?php echo url('/contact-us')?>">CONTACT US </a></li>

                                            
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo url('/login')?>">LOGIN</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-small-mobile">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <div class="mobile-logo">
                        <a href="/">
                        <img alt="" src="{{ URL::asset('asset/images/logo/logo-1.png') }}">
                        </a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="header-right-wrap">
                        <div class="cart-wrap common-style">
                            <button class="cart-active">
                            <i class="la la-shopping-cart"></i>
                            <!--<span class="count-style">2 Items</span>-->
                            </button>
                            <div class="shopping-cart-content">
                                <div class="shopping-cart-top">
                                    <h4>Your Cart</h4>
                                    <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                </div>
                                <ul>
                                    
                                   
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/3.jpg')}}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Tulsi.</a></h4>
                                            <span>₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="shopping-cart-bottom">
                                    <div class="shopping-cart-total">
                                        <h4>Subtotal <span class="shop-total">₹ 290.00</span></h4>
                                    </div>
                                    <div class="shopping-cart-btn btn-hover default-btn text-center">
                                        <a class="black-color" href="checkout">Continue to Chackout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mobile-off-canvas">
                            <a class="mobile-aside-button" href="#"><i class="la la-navicon la-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


