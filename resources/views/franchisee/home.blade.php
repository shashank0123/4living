<?php

use App\Repositories\SharesRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use App\Models\Epoint;
use App\Models\Member;
use App\Models\BonusPairing;
use App\Models\BonusDirect;
$sharesRepo = new SharesRepository;
$announcementModel = new \App\Models\Announcement;


?>

<style type="text/css">
 .modal-dialog .modal-body {
    padding: 35px 50px !important;
}
        .modal-body h3 {
    font-size: 17px;
    color: blue;
    border-bottom: 2px solid lightblue;
    font-weight: 600;
}
.modal-body p, .modal-body li { font-size: 14px !important; }

  #set-div-height { height: 100px; }
  @media screen and (max-width: 768px){
   #set-div-height { height: 150px; }
 }
 
 body {
   background: url({{ \URL::current() }}/../assets/img/login.jpg) no-repeat center center fixed;
 }

 /*.breadcrumb { background-color: #EBF5FB !important }*/
 .set-marmgin { margin-top: 20px; }
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,input,div { text-transform: uppercase !important; }
 ul li , ul li a { color: #fff !important; }

</style>
@extends('franchisee.app')

@section('title')
{{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#" style="color: #fff">@lang('breadcrumbs.front')</a></li>
  <li class="active" style="color: #fff">@lang('breadcrumbs.dashboard')</li>
</ul>
@stop

@section('content')

<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <div class="col-md-12">
        <div class="row" style=" background-color: #EBF5FB !important ">
          
          
          {{-- @if(1)
          <div class="col-sm-3"><br>
            <input type="button" name="activate" value="Activate Your ID" onclick="selectPackage()" class="btn btn-success">
          </div>
          @endif --}}
        </div>



<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#check-term" style="display: none"  id="getPopUp" >
  Open modal
</button>



 <!--The Modal -->
       
      </div>
      <div class="dashboard grey lighten-3">
        <div class="row no-gutter">
          <div class="col-sm-12 col-md-12 col-lg-12" style="background-color:  #EBF5FB ;">
            <div class="p-20 clearfix">
              <div class="pull-right">
                <a href="{{ route('network.binary') }}" target="_blank" class="btn btn-round-sm btn-link" data-toggle="tooltip" title="@lang('home.qCheckHierarchy')"><i class="md md-swap-vert"></i></a>
                <a href="{{ route('member.register') }}" class="btn btn-round-sm btn-link" data-toggle="tooltip" title="@lang('home.qAddMember')"><i class="md md-person-add"></i></a>

              </div>

              
            </div>

            <div class="p-20 no-p-t">              

                  {{-- Total members count --}}

                  {{-- <div class="col-md-5">
                    <div class="card small">
                      <div class="green p-10" style="background-color: #099">
                        <div class="pull-right">
                          <div> <i class="md md-accessibility text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.members1')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i> @lang('common.members2')</div>
                      </div>
                      <div class="card-content p-10" style="background-color: #E5E7E9">
                        <div class="row" id="set-div-height">
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="
                            " data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.direct')</p>
                          </div>
                          <div class="col-md-4 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300"><span data-count data-start="0" data-end="" data-decimal="0"></span></h3>
                            <p class="grey-text w600">@lang('common.leftTotal')</p>
                          </div>
                          <div class="col-md-4 text-center">
                            <h3 class="no-margin w300"><span data-count data-start="0" data-end="" data-decimal="0"></span></h3>
                            <p class="grey-text w600">@lang('common.rightTotal')</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> --}}

                  <div class="col-md-1"></div>

                  {{-- Active Members --}}

                  

                  

                  {{-- Personal Information --}}

                  <div class="col-md-5">
                    <div class="card small">
                      <div class="green p-10" style="background-color: #099">
                        <div class="pull-right">
                          <div> <i class="md md-accessibility text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.membersInfo')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i>{{--  @lang('common.membersSub') --}}</div>
                      </div>
                      <div class="card-content p-10" style="background-color: #E5E7E9">
                        <div class="row" id="set-div-height">
                          <div class="col-sm-4 col-xs-6">
                            <p>Name </p>
                          </div>
                          <div class="col-sm-8 col-xs-6">
                            <p> : 
                            @if(!empty($franchisee->name))
                              {{$franchisee->name}}
                            @endif
                            </p>
                          </div>
                          <div class="col-sm-4 col-xs-6">
                            <p>ID</p>
                          </div>
                          <div class="col-sm-8 col-xs-6">
                            <p> : 
                            @if(!empty($member->username))
                            {{$member->username}}
                            @endif
                          </p>
                          </div>
                          <div class="col-sm-4 col-xs-6">
                            <p>Joining Date</p>
                          </div>
                          <div class="col-sm-8 col-xs-6">
                            <p> : 
                            @if(!empty($member->created_at))
                            {{explode(' ',$member->created_at)[0]}}
                            @endif
                          </p>
                          </div>
                          
                        </div> 
                      </div>
                    </div>
                  </div>

                  <div class="col-md-1"></div>

                  

                  {{-- My Associates --}}

                  {{-- <div class="col-md-11">
                      <br><br>
                    <div class="card small">
                      <div class="green p-10" style="background-color: #099">
                        <div class="pull-right">
                          <div> <i class="md md-accessibility text-rgb-5"></i></div>
                        </div>
                        <h4 class="no-margin white-text w600">@lang('common.membersTitle')</h4>
                        <div class="f11" style="opacity:0.8"> <i class="md md-visibility"></i> @lang('common.membersSub')</div>
                      </div>
                      <div class="card-content p-10" style="background-color: #E5E7E9">
                        <div class="row" id="set-div-height">
                             <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;"></div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level1')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level2')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level3')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level4')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level5')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level6')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level7')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level8')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level9')</p>
                          </div>
                          <div class="col-md-1 text-center" style="border-right: 1px #F0F0F0 solid;">
                            <h3 class="no-margin w300" data-count data-start="0" data-end="" data-decimal="0"></h3>
                            <p class="grey-text w600">@lang('common.level10')</p>
                          </div>
                          

                        </div>
                      </div>
                    </div>
                  </div> --}}
                </div>           




                <?php $announcement = $announcementModel->orderBy('created_at', 'desc')->first(); ?>
                @if ($announcement)
                <?php
                $lang = \App::getLocale();
                $announcementTitle = null;
                $announcementContent = null;
                if (isset($announcement->{"title_$lang"})) $announcementTitle = $announcement->{"title_$lang"};
                else $announcementTitle = $announcement->title_en;
                if (isset($announcement->{"content_$lang"})) $announcementContent = $announcement->{"content_$lang"};
                else $announcementContent = $announcement->content_en;
                ?>
                <div class="panel panel-success panel-announcement">
                  <div class="panel-heading">
                    <h2 class="panel-title grey-text">
                      <i class="md md-alarm"></i> {{ $announcement->created_at->format('d F Y H:i A') }}
                    </h2>
                    <h1 class="m-t-10 m-b-5 f30">
                      <i class="md md-new-releases"></i> {{ $announcementTitle }}
                    </h1>
                    <p class="small grey-text no-margin">
                      {{ Str::limit(strip_tags($announcementContent), 100) }}
                    </p>
                    <hr>
                    <a href="{{ route('announcement.read', ['id' => $announcement->id, 'lang' => $lang]) }}" class="btn btn-primary">@lang('common.read')</a>
                  </div>
                </div>
                @endif
              </div>

              
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>

<?php
$annNew = \Cache::remember('announcement.new', 60, function () {
  return \App\Models\Announcement::whereRaw('Date(created_at) = CURDATE()')->first();
});
?>
@if ($annNew)
<div class="modal fade" tabindex="-1" role="dialog" id="annModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">@lang('announcement.newTitle')</h4>
      </div>
      <div class="modal-body">
        <p>@lang('announcement.newContent') <a href="{{ route('announcement.read', ['id' => $annNew->id, 'lang' => $lang]) }}">@lang('announcement.newLink')</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('common.close')</button>
      </div>
    </div>
  </div>
</div>
@endif

   <div class="modal" id="check-term">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header" style="text-align: center !important">
        <h4 class="modal-title" style="text-align: center !important">Terms & Conditions</h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        @lang('terms.content')
        <br>
        <div class="custom-control custom-checkbox">
    &nbsp;&nbsp;<input type="checkbox" class="custom-control-input" id="defaultUnchecked">
    <label class="custom-control-label" for="defaultUnchecked">I accept Terms & Conditions</label>
    <p id="showchecked" style="display: none; color: #ff0000"></p>
</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-danger" onclick="checkTerms()" >Continue</button>
        <button type="button" class="btn btn-danger" id="successTerm" data-dismiss="modal" style="display: none">Close</button>
      </div>

    </div>
  </div>
</div>
      

<script> $('#getPopUp').click(); </script>


<script>
  function selectPackage(){
    window.location.href = "/en/member/activate-id";
  }
  
  
  function checkTerms(id){
    //   alert(id);
            if (document.getElementById('defaultUnchecked').checked) {
                var check = "yes";
                 $('#showchecked').hide();
                  $.ajax({
        type: "POST",
        url: "/en/checkTerm",
        data:{ id: id },
        success:function(msg){
          // alert(msg);
          if(msg.message == 'Success'){
            $('#successTerm').click();
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }          
        }
      });
            // alert("checked");
        } else {
            $('#showchecked').show();
            $('#showchecked').text(' *First accept terms & conditions');
        }
        }
  
  
  
  
</script>
@stop
