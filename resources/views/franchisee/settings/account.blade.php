@extends('franchisee.app')

@section('title')
  @lang('breadcrumbs.settingsDetails') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.settingsDetails')</li>
  </ul>
 
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th,select { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
  
  #accountBankForm label { font-weight: 400; color: #666; }
</style>
@section('content')
  <main>
    @include('franchisee.include.sidebar')
    <div class="main-container">
      @include('franchisee.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.profile1')</h1>
            <p class="lead">@lang('settings.profile2')</p>
          </div>
          <p>Once you update your account, then you cannot update again. For further updates contact to admin. For this <a href="mailto:help2humancf@gmail.com?Subject=Acount%20Update"><u>Click Here</u></a></p>
            <br>
          <div class="row m-b-40">
            <div class="col-md-6">
                <h4>Personal Information</h4>
              <div class="well" style="background-color: #E5E7E9">
                <form data-parsley-validate="" role="form" class="action-form" id="accountBasicForm" http-type="post" data-url="{{ route('franchisee.account.postUpdate') }}" data-nationality="true" enctype="multipart/form-data">
                  <fieldset>
                    @if(!empty($member->name))
                    <div class="form-group">
                      <label class="control-label"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.name')</label>
                      <input type="text" class="form-control" required="" disabled="" value="{{ $member->name }}" name="name" id="inputName">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.name')</label>
                      <input type="text" class="form-control" required=""  value="{{ $member->name }}" name="name"  id="inputName">
                    </div>
                    @endif

                    {{-- <div class="form-group">
                      <label for="profimage" class="control-label"> Upload an image</label>
                      <input type="file" id="profimage" name="profimage" class="form-control" >
                    </div> --}}


                    @if(!empty($member->date_of_birth))
                    <div class="form-group">
                      <label class="control-label" for="inputDOB"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="{{ $member->date_of_birth }}" data-date-format="YYYY-MM-DD" readonly>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputDOB"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="{{ $member->date_of_birth }}" data-date-format="YYYY-MM-DD">
                    </div>
                    @endif


                    @if(!empty($member->gender))
                    <div class="form-group">
                      <label class="control-label" for="inputGender"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender" readonly>
                        <option value="Male" @if ($member->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputGender"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender">
                        <option value="Male" @if ($member->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div>
                    @endif


                    @if(!empty($member->phone))
                    <div class="form-group">
                      <label class="control-label" for="inputPhone"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.mobile')</label>
                      <input type="text" name="phone" class="form-control" id="inputPhone" required="" value="{{ $member->phone }}" readonly>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputPhone"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.mobile')</label>
                      <input type="text" name="phone" class="form-control" id="inputPhone" required="" value="{{ $member->phone }}">
                    </div>
                    @endif       

                    @if(!empty($member->alternet_phone))
                    <div class="form-group">
                      <label class="control-label" for="inputPhone"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.mobile')</label>
                      <input type="text" name="alternet_phone" class="form-control" id="inputPhone" required="" value="{{ $member->alternet_phone }}" readonly>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputPhone"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.mobile')</label>
                      <input type="text" name="alternet_phone" class="form-control" id="inputPhone" required="" value="{{ $member->alternet_phone }}">
                    </div>
                    @endif                   

                  

                    @if(!empty($member->address1))
                    <div class="form-group">
                      <label class="control-label" for="inputAddress"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address1" class="form-control" id="inputAddress" required="" readonly>{{ $member->address1 }}</textarea>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputAddress"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address1" class="form-control" id="inputAddress" required="">{{ $member->address1 }}</textarea>
                    </div>
                    @endif

                    @if(!empty($member->address2))
                    <div class="form-group">
                      <label class="control-label" for="inputAddress"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.address2')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address2" class="form-control" id="inputAddress" required="" readonly>{{ $member->address2 }}</textarea>
                    </div>
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputAddress"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;@lang('settings.address2')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address2" class="form-control" id="inputAddress" required="">{{ $member->address2 }}</textarea>
                    </div>
                    @endif

                    

                   <div class="form-group">
                      <label class="control-label" for="inputState"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;State</label>
                      <select name="state" class="form-control" id="inputState" required="" value="{{$member->state}}" <?php if(!empty($member->state)){echo 'readonly';} ?> >
                        <option value="">Select State</option>
                        <option @if ($member->state=='AP') {{ 'selected' }} @endif value="AP">Andhra Pradesh</option>
                        <option @if ($member->state=='AR') {{ 'selected' }} @endif value="AR">Arunachal Pradesh</option>
                        <option @if ($member->state=='AS') {{ 'selected' }} @endif value="AS">Assam</option>
                        <option @if ($member->state=='BH') {{ 'selected' }} @endif value="BH">Bihar</option>
                        <option @if ($member->state=='CT') {{ 'selected' }} @endif value="CT">Chhattisgarh</option>
                        <option @if ($member->state=='DL') {{ 'selected' }} @endif value="DL">Delhi</option>
                        <option @if ($member->state=='GA') {{ 'selected' }} @endif value="GA">Goa</option>
                        <option @if ($member->state=='GJ') {{ 'selected' }} @endif value="GJ">Gujarat</option>
                        <option @if ($member->state=='HR') {{ 'selected' }} @endif value="HR">Haryana</option>
                        <option @if ($member->state=='HP') {{ 'selected' }} @endif value="HP">Himachal Pradesh</option>
                        <option @if ($member->state=='JK') {{ 'selected' }} @endif value="JK">Jammu and Kashmir</option>
                        <option @if ($member->state=='JH') {{ 'selected' }} @endif value="JH">Jharkhand</option>
                        <option @if ($member->state=='KA') {{ 'selected' }} @endif value="KA">Karnataka</option>
                        <option @if ($member->state=='KL') {{ 'selected' }} @endif value="KL">Kerala</option>
                        <option @if ($member->state=='MP') {{ 'selected' }} @endif value="MP">Madhya Pradesh</option>
                        <option @if ($member->state=='MH') {{ 'selected' }} @endif value="MH">Maharashtra</option>
                        <option @if ($member->state=='MN') {{ 'selected' }} @endif value="MN">Manipur</option>
                        <option @if ($member->state=='ML') {{ 'selected' }} @endif value="ML">Meghalaya</option>
                        <option @if ($member->state=='MZ') {{ 'selected' }} @endif value="MZ">Mizoram</option>
                        <option @if ($member->state=='NL') {{ 'selected' }} @endif value="NL">Nagaland</option>
                        <option @if ($member->state=='OR') {{ 'selected' }} @endif value="OR">Orissa</option>
                        <option @if ($member->state=='Pondicherry') {{ 'selected' }} @endif value="Pondicherry">Pondicherry</option>
                        <option @if ($member->state=='PB') {{ 'selected' }} @endif value="PB">Punjab</option>
                        <option @if ($member->state=='RJ') {{ 'selected' }} @endif value="RJ">Rajasthan</option>
                        <option @if ($member->state=='SK') {{ 'selected' }} @endif value="SK">Sikkim</option>
                        <option @if ($member->state=='TN') {{ 'selected' }} @endif value="TN">Tamil Nadu</option>
                        <option @if ($member->state=='TR') {{ 'selected' }} @endif value="TR">Tripura</option>
                        <option @if ($member->state=='UK') {{ 'selected' }} @endif value="UK">Uttaranchal</option>
                        <option @if ($member->state=='UP') {{ 'selected' }} @endif value="UP">Uttar Pradesh</option>
                        <option @if ($member->state=='WB') {{ 'selected' }} @endif value="WB">West Bengal</option>
                      </select>
                    </div>

                    @if(!empty($member->city))
                    <div class="form-group">
                      <label class="control-label" for="inputCity"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;City</label>
                      <input type="text" name="city" class="form-control" id="inputCity" required="" disabled="disabled" value="{{ $member->city }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputCity"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;City</label>
                      <input type="text" name="city" class="form-control" id="inputCity" required="" value="{{ $member->city }}">
                    </div>
                    @endif

                    

                    @if(!empty($member->pincode))
                    <div class="form-group">
                      <label class="control-label" for="inputPincode"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Pin Code</label>
                      <input type="text" name="pincode" class="form-control" id="inputPincode" required="" disabled="disabled" value="{{ $member->pincode }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputPincode"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Pin Code</label>
                      <input type="text" name="pincode" class="form-control" id="inputPincode" required="" value="{{ $member->pincode }}">
                    </div>
                    @endif
                   
                   @if(!empty($member->aadhar_number))
                    <div class="form-group">
                      <label class="control-label" for="inputAadhar"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Aadhar Number</label>
                      <input type="text" name="aadhar_number" class="form-control" id="inputAadhar" required="" disabled="disabled" value="{{ $member->aadhar_number }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputAadhar"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;Aadhar Number</label>
                      <input type="text" name="aadhar_number" class="form-control" id="inputAadhar" required="" value="{{ $member->aadhar_number }}">
                    </div>
                    @endif
                                      
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <h4>Bank Details</h4>
                    <div class="well" style="background-color: #E5E7E9">
                <form data-parsley-validate="" role="form" class="form-floating action-form" id="accountBankForm" http-type="post" data-url="{{ route('franchisee.account.postUpdateBank') }}">
                  <fieldset>
                    <?php $countries = config('misc.countries'); ?>
                    <div class="form-group">
                      <label class="control-label" for="inputBankName">@lang('settings.bank.name')</label>
                      <select class="form-control" name="bank_name" id="inputBankName" autocomplete>
                        @foreach ($countries as $country => $value)
                          {{-- <optgroup label="{{ \Lang::get('country.' . $country) }}"> --}}
                            @foreach ($value['banks'] as $bank)
                              <option value="{{ $bank }}" @if ($member->bank_name == $bank) selected="" @endif>{{ $bank }}</option>
                            @endforeach
                          </optgroup>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAccNumber">@lang('settings.bank.number')</label>
                      <input type="text" name="account_number" id="inputBankAccNumber" class="form-control" required="" value="{{ $member->account_number }}">
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label" for="inputBankAddress">Confirm Bank Account Number</label>
                      <input type="text" name="confirm_account" id="confirm-account" class="form-control" required="" value="">
                      
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAccHolder">@lang('settings.bank.holder')</label>
                      <input type="text" name="account_holder_name" id="inputBankAccHolder" class="form-control" required="" value="{{ $member->account_holder_name }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAddress">@lang('settings.bank.address')</label>
                      <input type="text" name="bank_address" id="inputBankAddress" class="form-control" required="" value="{{ $member->bank_address }}">
                    </div>


                      @if(!empty($member->pan_no))
                    <div class="form-group">
                      <label class="control-label" for="inputPan"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" required="" disabled="disabled" value="{{ $member->pan_no }}" readonly>
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputPan"><i class="fa fa-asterisk" style="color: #e91e63; font-size: 10px"></i> &nbsp;PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" required="" value="{{ $member->pan_no }}">
                    </div>
                    @endif
                      
                    {{-- </div> --}}

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" class="form-control" id="ifsc" name="ifsc" value="{{ $member->bank_branch }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="gst_no">GST Number</label>
                      <input type="text" class="form-control" id="gst_no" name="gst_no" value="{{ $member->gst_no }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankBranch">@lang('settings.bank.branch')</label>
                      <input type="text" name="bank_branch" id="inputBankBranch" class="form-control" value="{{ $member->bank_branch }}">
                    </div>
                    <input type="hidden" name="s" class="form-control" required="" value="{{$member->secret_password}}">
                    {{-- <div class="form-group">
                      <label class="control-label">@lang('settings.secret')</label> --}}
                      <input type="hidden" name="password" class="form-control" required="" value="{{$member->secret_password}}">
                    {{-- </div> --}}

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
                </div>
                </div>
                <br><br>
                <div class="row">
                    <h4>Reset Password</h4>
                    <div class="well" style="background-color: #E5E7E9">
                <form data-parsley-validate="" role="form" class="action-form" id="accountBasicForm" http-type="post" data-url="{{ route('franchisee.passwordUpdate') }}" data-nationality="true" enctype="multipart/form-data">
                  <fieldset>
                    

                   

                    <div class="form-group">
                      <label class="control-label" for="inputSecret">@lang('settings.old')</label>
                      <input type="password" class="form-control" name="old-password" id="inputSecret" required="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('settings.password')</label>
                      <input type="password" name="password" class="form-control" id="inputPassword" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.repassword')</label>
                      <input type="password" name="confirm-password" class="form-control" data-parsley-equalto="#inputPassword" minlength="5">
                    </div>                    

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
                </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
