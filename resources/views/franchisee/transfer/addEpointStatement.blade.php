@extends('franchisee.app')

@section('title')
Add Transfer Statement | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="/en/login">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">Add Funds Transfer Statement</li>
</ul>
@stop
<style>
  #member-detail { display: none; }
  #member-detail .row{ width: 30% }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
</style>

@section('content')
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">

      @if($errors->any())
      <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <br>
        <li>{{ $error }}</li>
        @endforeach
      </div>
      @endif

      @if($message = Session::get('message'))
      <div class="alert alert-primary">
        <br>
        <p style="color: #800000; font-size: 16px; padding: 20px; background-color: #eee">{{ $message }}</p>
      </div>
      @endif

      <section style="padding: 2% 10%">
        <div class="page-header">
          <h1><i class="md md-swap-vert"></i> Add Funds Transfer Statement</h1>
        </div>

        <div class="row m-b-40">
          <div class="col-md-6">
            <div class="alert alert-danger">
              Remember to UPDATE member wallet.
            </div>
            <div class="panel panel-default">
              <div class="panel-body">
                <form method="post" action="{{route('transer.epoints')}}">
                  {{-- <form role="form" onsubmit="return false;" data-parsley-validate class="action-form shares-form" id="transferStatementForm" http-type="post" data-url="{{ route('admin.transfer.add') }}"> --}}
                    <fieldset>
                      {{-- <div class="form-group">
                        <label class="control-label">From Member ID</label>
                        <input type="text" name="from" required="" class="form-control">
                      </div> --}}
                      <input type="hidden" name="sender_id" value="{{$member->id}}">
                      <div class="form-group">
                        <label class="control-label">To Member ID</label>
                        <input type="text" name="to" required="" class="form-control"  onchange="getUserDetail()" id="inputDirect">
                      </div>


                      <div class="form-group" id="member-detail">
                        <div class="container">
                          <div class="row" style="color: #000; font-weight: bold; border-bottom: 1px solid #fff">
                            &nbsp;&nbsp;&nbsp;&nbsp;<span id="uname" style="color: #000; font-weight: normal;"></span><br>                            
                          </div>
                        </div>
                      </div>

                      {{-- <div class="form-group">
                        <label class="control-label">Type</label>
                        <select class="form-control" name="type" required="">
                          <option value="cash">Cash</option>
                          <option value="promotion">Promotion</option>
                          <option value="register">Register</option>
                        </select>
                      </div>
                      --}}
                      <div class="form-group">
                        <label class="control-label">Funds</label>
                        <input type="number" step="any" class="form-control" name="epoint" required="">
                        <span class="help-block"></span>
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                          <span class="btn-preloader">
                            <i class="md md-cached md-spin"></i>
                          </span>
                          <span>Submit</span>
                        </button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <script>
    function getUserDetail(){
      var name = $('#inputDirect').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/en/user-name",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Success'){
            $('#member-detail').show();
            $('#uname').html("Name : "+msg.name);
          }
          else{
            $('#member-detail').show();
            $('#uname').html(msg.message);
          }
        }
      });
    }

  </script>
  @stop
