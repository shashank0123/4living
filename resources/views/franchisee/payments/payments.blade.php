@extends('franchisee.app')
@section('title')
@lang('payment.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
  <!--<li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>-->
  <li class="active">{{$type}} @lang('breadcrumbs.payment')</li>
</ul>
@stop
<style>
 li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
 .table {max- width:90% !important;}
</style>
@section('content')
<main>
  @include('franchisee.include.sidebar')
  <div class="main-container">
    @include('franchisee.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">

        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">                
              <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> {{ $type }} Payment</h1>
              <p class="lead">@if($type=='Due'){{'DUE Payment'}}@else   {{'All Received Payments List.'}}  @endif </p>
            </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/en/my-members">
                <input type="hidden" name="side" value="{{$type}}">
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 80%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
            </div>
          </div>
        </div>         


        
        <div class="card">
          <div>
            <div class="">
                @if($type=='Received')
             <table class="table table-full" data-url="{{ route('member.registerleftHistoryList') }}">
              <thead>
                <tr>
                  <th>@lang('payment.sr')</th>
                  <th>@lang('payment.date')</th>
                  <th>@lang('payment.amount')</th>
                  
                  
                  
                  <th>Status</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                
                @if(!empty($data))
                @foreach ($data as $datas)

                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$datas->created_at}}</td>
                  <td>{{$datas->amount}}</td>                  
                  <td>@if($type=='Due'){{'Pending'}}@else{{'Received'}}@endif</td>

                </tr>
                @endforeach
                @endif
              </tbody>
            </table>

            @if($i == 1)
            <div style="text-align: center; margin: 2%">
              <h3>No result found<br><br></h3>
            </div>
            @endif
            
            @else
            <div style="text-align: center; margin: 2%">
              <h3><br><br>No Payment Due<br><br></h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
<script>
  function getvalue(){
    var search= $('#search').val();
    alert(search);
  }
</script>
@stop
