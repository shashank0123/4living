@extends('franchisee.app')

@section('title')
  @lang('incomedirect.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.directincome')</li>
  </ul>
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>
@section('content')
  <main>
    @include('franchisee.include.sidebar')
    <div class="main-container">
      @include('franchisee.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
          <div class="page-header">
            <h1><i class="md md-group-add"></i> @lang('incomedirect.title')</h1>
            <p class="lead">@lang('incomedirect.subtitle1')</p>
          </div>

          <div class="card" style="background-color: #E5E7E9">
            <div>
              <div class="datatables" style="background-color: #E5E7E9">
                 <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('bonus.directList', ['lang' => \App::getLocale()]) }}">
                    <thead style="background-color: #E5E7E9">
                      <tr>
                        <th data-id="created_at">@lang('misc.create')</th>
                        <th data-id="amount_cash">@lang('misc.direct')</th>
                        {{-- <th data-id="amount_cash">@lang('misc.binary')</th> --}}
                        <th data-id="admin_charge">@lang('misc.admin')</th>
                        <th data-id="tds_charge">@lang('misc.tds')</th>
                        {{-- <th data-id="repurchase">@lang('misc.repurchase')</th> --}}
                        {{-- <th data-id="amount_promotion">@lang('misc.promotion')</th> --}}
                        <th data-id="total">@lang('misc.total')</th>
                      </tr>
                    </thead>
                    <tbody style="background-color: #E5E7E9">
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
