@extends('franchisee.app')

@section('title')
  @lang('success.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="/en/member">@lang('breadcrumbs.dashboard')</a></li>
    <li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>
    <li class="active">@lang('breadcrumbs.registerSuccess')</li>
  </ul>
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
</style>
@section('content')
  <main>
    @include('franchisee.include.sidebar')
    <div class="main-container">
      @include('franchisee.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-done-all"></i> @lang('success.title')</h1>
          </div>

          <div class="row m-b-40">
            <div class="col-md-8">
              <div class="well white text-center">
                <h1 class="theme-text text-uppercase w300">@lang('success.text1')</h1>
                <h3>@lang('success.text2')<span class="theme-text">{{ $model->username }}</span></h3>
                <div class="row">
                    @if(!empty($model->name))
                  <div class="col-sm-4">
                    <p>Name </p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{ $model->name}} </p>
                  </div>
                  @endif
                  
                  @if(!empty($model->username))
                  <div class="col-sm-4">
                    <p>Member ID </p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{ $model->username}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->password))
                  <div class="col-sm-4">
                    <p>Password </p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->password}}</p>
                  </div>
                  @endif
                  
                   @if(!empty($model->register_by))
                  <div class="col-sm-4">
                    <p>Reference Id</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->register_by}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->date_of_birth))
                  <div class="col-sm-4">
                    <p>DOB</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->date_of_birth}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->address))
                  <div class="col-sm-4">
                    <p>Address</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->address}} </p>
                  </div>
                  @endif
                  
                  
                   @if(!empty($model->detail->city))
                  <div class="col-sm-4">
                    <p>City</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->city}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->district))
                  <div class="col-sm-4">
                    <p>District</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->district}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->state))
                  <div class="col-sm-4">
                    <p>State</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->state}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->nationality))
                  <div class="col-sm-4">
                    <p>Country</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->nationality}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->pin_code))
                  <div class="col-sm-4">
                    <p>Pin Code</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->pin_code}} </p>
                  </div>
                  @endif
                  
                   @if(!empty($model->detail->mobile_phone))
                  <div class="col-sm-4">
                    <p>Mobile</p>
                  </div>
                  <div class="col-sm-8">
                    <p> : {{$model->detail->mobile_phone}} </p>
                  </div>
                  @endif
                  
                </div>

                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}" class="btn btn-primary btn-block m-t-50">
                  <i class="md md-navigate-before"></i> @lang('success.back')
                </a>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
