<?php
  use App\doctorspanel;
 $doctorTeams = DB::table('doctorspanels')
                ->where('status', '1')
                ->get();
				
 

?>
@extends('layouts/ecommerce2')


@section('content')
 <!-- header end here -->
  <style type="text/css">
    .teamprofilesec{ background-color: #222; }
    .teamprofile{  padding: 30px 0;margin-bottom:10px; }
    .teamprofile h4{font-size: 20px; font-weight: bold; color: white;  }
    .teamprofile p{  font-size: 14px; line-height: 16px;  color: white; }
    .teamprofile2 img { width: 100%; height: 100%!important; padding: 10px;  }
   
</style>




<div class="container-fluid mt-40 mb-50 ">
    <h2 class="sechead">Team 4LivinG </h2>
     <hr class="hrstyle">
    <p>
        Leading the Future<br>

Meet the ‘4LivinG ‘Leadership Team.
Team work has been defined as Working together to achieve a common vision for the 4LivinG team the vision has been to help everyone realize their goals, dreams and aspiration.
    </p>


     <div class="row   teamprofilesec" >
        <div class="col-lg-8 col-md-8 col-sm-8  col-12">
        <div class="teamprofile2 ">
            <img src="asset/images/premp.jpg" class="img-fluid" alt="Responsive image">
           </div>
         </div>
        
       <div class="col-lg-4 col-md-4 col-sm-4  col-12">
       <div class="teamprofile">
         <h4>Dr. Prem Prakash</h4>
         <p>Chairman of Board</p>
       <hr>
        <p>Dr. Prem Prakash having been involved with 4LivinG since the very first day , Dr. Prem Prakash has been instrumental in guiding the company in a manners that is consistent with her core values of Hard work..</p>
      </div>
      </div>
        
    </div>



    <div class="row   teamprofilesec" >
         <div class="col-lg-4 col-md-4 col-sm-4  col-12">
       <div class="teamprofile">
         <h4> R. Parshad </h4>
         <p> C.E.O </p>
       <hr>
        <p>We want to thank and welcome all of you to 4Living.Our Company has reached a significant milestones of establishing itself in the market! </p>
 <p>It's time to change lives for the better by becoming a part of the big 4Living team.It is because for most partners our company is no longer just business-it is an integral part of the life of their families.</p>   
 <p>We do not promise quick easy money and getting rich instanty.We teach people how to work efficiently.By becoming a partner of the company and helping us promote our good and service.</p>
<p>Our goal is effective development of our company’s operations across the world. Our partners’success is a great achievement for the whole company.
</p>
<p style="text-align: right; font-family: verdan">The motto of our company is “Be the best! Have the best”</p>
      </div>
      </div>
        <div class="col-lg-8 col-md-8 col-sm-8  col-12">
        <div class="teamprofile2 ">
            <img src="asset/images/rparsad.jpg" class="img-fluid" alt="Responsive image">
           </div>
         </div>
        
      
        
    </div>


 <div class="row   teamprofilesec" >
        <div class="col-lg-8 col-md-8 col-sm-8  col-12">
        <div class="teamprofile2 ">
            <img src="asset/images/manojs2.jpg" class="img-fluid" alt="Responsive image">
           </div>
         </div>
        
       <div class="col-lg-4 col-md-4 col-sm-4  col-12">
       <div class="teamprofile">
         <h4>Dr. Manoj Sharma</h4>
         <p> Board Member  </p>
       <hr>
        <p>Manoj Sharma is a Ayurvedic Doctor. From the beginning 4LivinG he has provided us new idea to make a new Ayurvedic Product.</p>
      </div>
      </div>
        
    </div>

<div class="row   teamprofilesec" >
     <div class="col-lg-4 col-md-4 col-sm-4  col-12">
       <div class="teamprofile">
         <h4>Satish Kumar </h4>
         <p> R &d Head   </p>
       <hr>
        <p>Born and raised in India, S. Kumar joined 4LivinG in 2017.He has developed a unique and enduring passion For creating grearness throughout the Company.</p>
      </div>
      </div>
        <div class="col-lg-8 col-md-8 col-sm-8  col-12">
        <div class="teamprofile2 ">
            <img src="asset/images/satishdr.png" class="img-fluid" alt="Responsive image">
           </div>
         </div>     
    </div>
</div>
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection