@extends('layouts.company')


@section('content')
 <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
        <div class="about-us-area pt-90 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="about-us-img text-center">
                            <a href="#">
                                <img src="../asset/images/product/4.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 align-self-center">
                        <div class="about-us-content">
                            <h2>Welcome To <span>4LivinG</span> Ayurvedic !</h2>
                            <p>Step back & to refresh the memories of the foundation of <b>4LivinG</b> Distributors Pvt. Ltd. It was set up in 2018. For more than 1 year, <b>4LivinG</b> Distributors Pvt. Ltd.® has been providing high quality, affordable Ayurvedic medicines & FMCG products trusted by healthcare professionals and patients across geographies.</p><p>


Business leaders Er. N Kumar joined their hands together with leading medical practitioner Dr. Prem Prakash  (M.D. Medicine) to build a marketing company. Based on the five principles of Vision, Integrity, Opportunity, Freedom, and Success, The Company has set its mission to built Healthy India by offering the best Herbo-Nutritional products for Mankind along with Organic Agricultural Products & Organic Animal Health Products.</p><p>

Millions of our satisfied customers are enjoying flexible and cost-effective healthcare solutions. The ultimate goal of the company is to reach the market with world-class ayurvedic & herbal products. The company manufactures most of its brand products with sister concern company RPS Biotech Pvt. Ltd. <b>4LivinG</b> has developed & launched Cow Colostrum capsules i.e. Imsys in 2018, which was the first time in India. In a very short period, Imsys has become India’s leading brand amongst all health care products.</p><p>


Started with just two products in 2018, Amazing product results along with marketing efforts, led the company to reach 5 products, with a huge turnover leading towards 3 million for the financial year 2018/19. The company has now set up its new targets to reach the market with a wide range of FMCG products internationally. The company distributes its products with its network of distributors in all states of India. Product results have already gathered a database of more than 1 million satisfied customers in India and this figure shows a remarkable growth with every new day. Dr.  Prem  Prakash Sir, Chairman of the company having a medical practice of 35 years is the backbone of outstanding product results. He leads the team of the R&D department of the manufacturing. The product properties are truly unique & hard to copy. Mr. Prakash Khalate, one of the Director is taking care of product manufacturing & Er.  N Kumar, one of the Director is the key person for the successful branding of the company.</p><p>

We have ground operations in India. We have covered all developed and emerging markets. The operations are exclusively guided by the company directors. The hardship of well qualified 150 employees working under directors’ guidance is a master key for the company to lead towards success. Offering High Quality, We are a vertically integrated company that develops, manufactures and markets herbal medicines. We have a large portfolio of herbs that cover multiple dosage forms including tablets, capsules, granules, creams, and liquids. In 2018, The Company has stepped into Organic agriculture products & now marketing its 7 Organic Farming products with a huge success rate across India with the banner of RPS. Recently we have started the Vighnaharta banner for Nutritional Health Support. A stronger presence in these areas will add significant depth to the existing product pipeline.</p>


                            <div class="about-us-btn btn-hover hover-border-none">
                                <a class="btn-color-white btn-color-theme-bg black-color" href="product.html">Shop now!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-area pb-90 section-padding-3">
            <div class="container">
                <div class="feature-border feature-border-about">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="../asset/images/icon-img/feature-icon-1.png" alt="">
                                <h5>Best Product</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="../asset/images/icon-img/feature-icon-2.png" alt="">
                                <h5>100% fresh</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="../asset/images/icon-img/feature-icon-3.png" alt="">
                                <h5>Secure Payment</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="../asset/images/icon-img/feature-icon-4.png" alt="">
                                <h5>Best Queality</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="team-area pt-60 pb-60">
            <div class="container">
                <div class="section-title-2 text-center">
                    <h2>Team Members</h2>
                    <img src="../asset/images/icon-img/title-shape.png" alt="icon-img">
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="../asset/images/team/team-1.jpg" alt="">
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.Mike Banding</h4>
                                <span>Manager </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="../asset/images/team/team-2.jpg" alt="">
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.Peter Pan</h4>
                                <span>Developer </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="../asset/images/team/team-3.jpg" alt="">
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Ms.Sophia</h4>
                                <span>Designer </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="../asset/images/team/team-4.jpg" alt="">
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.John Lee</h4>
                                <span>Chairmen </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="testimonial-area pt-60 pb-80">
            <div class="container">
                <div class="section-title-2 text-center">
                    <h2>Testimonials</h2>
                    <img src="../asset/images/icon-img/title-shape.png" alt="icon-img">
                </div>
                <div class="testimonial-active owl-carousel">
                    <div class="sin-testimonial">
                        <div class="client-content">
                            <p>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, laboris consequat. ”</p>
                        </div>
                        <div class="client-info">
                            <img alt="" src="../asset/images/testimonial/client-1.png">
                            <h5>Anna Miller</h5>
                            <span>Deginer</span>
                        </div>
                    </div>
                    <div class="sin-testimonial">
                        <div class="client-content">
                            <p>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, laboris consequat. ”</p>
                        </div>
                        <div class="client-info">
                            <img alt="" src="../asset/images/testimonial/client-2.png">
                            <h5>Kevin Walker</h5>
                            <span>Developer</span>
                        </div>
                    </div>
                    <div class="sin-testimonial">
                        <div class="client-content">
                            <p>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, laboris consequat. ”</p>
                        </div>
                        <div class="client-info">
                            <img alt="" src="../asset/images/testimonial/client-3.png">
                            <h5>Ruth Pierce</h5>
                            <span>Customer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection