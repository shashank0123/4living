<?php
	use App\doctorspanel;

	$directors = DB::table('doctorspanels')
                ->where('panel_type', 'directors')
                ->get();

?>
@extends('layouts/ecommerce2')


@section('content')
<style type="text/css">
    .drprofile{ margin-top: 25px; }
    .drprofile h5{  font-size: 16px; font-weight: bold; }
    .bgcolordd{background-color: #f9f9f9; padding: 8px; border-top: 1px solid #ddd; }
     .drprofile ul{  list-style-type: disc; margin-left: 20px; }

          .drprofile li, p{  font-size: 14px; }

    }
</style>
 <!-- header end here -->
        <div class=" mt-30 mb-30">
          
        </div>
<div class="container-fluid mt-40 mb-50 ">
    <h2 class="sechead">Director's Desk </h2>
     <hr class="hrstyle">
    <div class="row lrmargin pagemarginsec" >
       <div class="col-lg-8 col-md-8 col-sm-8  col-12">
        <h4 class="phead">Directors Profile :</h4>
       <hr class="hrstyle2">
       <div class="drprofile">
         <h5>Dr. Prem Prakash ( Chairman of Board )</h5>
         <div class="bgcolordd">
          <p> Dr. Prem Prakash has been instrumental in guiding the company in a manner that is consistent with her core values of Hard work Though being a practitioner of Ayurveda & Nutritional for over 50 years.</p> 
      </div>
      </div>

       <div class="drprofile">
         <h5> R. Parshad ( CEO )</h5>
         <div class="bgcolordd">
          <p>Ayurveda is an ancient knowledge cultivated by the sages of India. It has made its way to becoming one of the ancient most but modern health care techniques. The term ‘Ayurveda’ means ‘science of life’.
The theories and notions of Ayurveda have been practiced since the past 5000 years. It recommends methods for appropriate living and longevity. It includes instructions to maintain good health as well as dealing with illness through yoga, treatments, herbal medicines, correct diet and lifestyle changes.
</p>



<h5>The Ayurvedic Treatment Philosophy:</h5>
<p>According to Ayurveda it is possible for you to live a long and healthy life through intelligent co-ordination of your body (sharira), mind (mana), senses (indriya) and soul (atma). Ayurveda thus offers a unique blend of science and philosophy that balances the physical, mental, emotional and spiritual components necessary for holistic health.</p>





          </div>
      </div>

<div class="drprofile">
         <h5>Mr. N. Kumar </h5>
         <div class="bgcolordd">
          <p>Well qualified Technical and Professional degree.</p>
          <ul>
          <li> B.E(Mechanical) from KUK.</li>
           <li> Looking After Administration, Exports and Implementation ERP in the company</li>
<li>
              There is power in thoughts; in negative and in positive thinking. Choose positivity and you will enjoy positive.</li>

<p>Energy and positive results. Friend, I do believe in positivity. Not just because I listen to positive philosophers & motivators, but for the reasons that I have struggled through my personal life -always kept a position attitude & today I have achieved a position bigger than my dream.</p>

</p>
          </div>
      </div>

       </div>
        <div class="col-lg-4 col-md-4 col-sm-4  col-12">
<img class="img-responsive" src="asset/images/building.png">

         </div>
        
    </div>
    
</div>
    <!-- inventor end -->
    <!-- inventor end -->
@endsection