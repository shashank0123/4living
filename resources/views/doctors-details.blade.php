@extends('layouts/ecommerce2')

@section('content')

        <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="{{ URL::asset('asset/images/bg/breadcrumb.jpg')}}">
        </div>
        <div class="about-us-area pt-90 pb-90">
		 @if(!empty($details))
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="about-us-img text-center">
						
                           
                                <img src="<?php echo asset("assetsss/images/doctorspanel/$details->image")?>" alt="">
                          
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 align-items-center">
                        <div class="doctorgtl mt-20">
                           <h4 class="phead">{{ $details->name }}<small> (  {{ $details->designation }} ) </small></h4>

     <hr class="hrstyle2">
                           {!! nl2br($details->description) !!}

                          
                        </div>
                    </div>
                </div>
            </div>
			 @endif
        </div>
    <!-- inventor end -->
    <!-- inventor end -->
@endsection