<?php
use App\Product;
use App\Address;
use App\Country;
use App\State;
use App\City;
$address = Address::where('user_id',$member->id)->first();
$i=0;
$total = 0;
?>

@extends('layouts.wealth-more')

@section('content')

<style>
	.page-large-title { padding: 20px 0; color: #37bc9b;}
</style>

<!-- BREADCRUMBS -->
<section class="page-section breadcrumbs">
	<div class="container">
		<div class="page-header">
			<h1>Checkout</h1>
		</div>

	</div>
</section>
<!-- /BREADCRUMBS -->

<section class="page-section color">
	<div class="container">
		<form action="/checkout-page/{{$member->id}}" method="POST" class="form-delivery">
		<div class="row">
			<div class="col-sm-8">
				<h3 class="block-title alt"><i class="fa fa-angle-down"></i> Delivery address</h3>
				@if($address != null)
				
					<div class="row">
						<div class="col-md-6">
							<div class="form-group"><input class="form-control" type="text" placeholder="Address"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><input class="form-control" type="text" placeholder="Last Name"></div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><input class="form-control" type="text" placeholder="Address"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group selectpicker-wrapper">
								<select
								class="selectpicker input-price" data-live-search="true" data-width="100%"
								data-toggle="tooltip" title="Select">
								<option>Country</option>
								<option>Country</option>
								<option>Country</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group selectpicker-wrapper">
							<select
							class="selectpicker input-price" data-live-search="true" data-width="100%"
							data-toggle="tooltip" title="Select">
							<option>City</option>
							<option>City</option>
							<option>City</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group"><input class="form-control" type="text" placeholder="Postcode/ZIP"></div>
				</div>
				<div class="col-md-4">
					<div class="form-group"><input class="form-control" type="text" placeholder="Email"></div>
				</div>
				<div class="col-md-4">
					<div class="form-group"><input class="form-control" type="text" placeholder="Phone Number"></div>
				</div>
				<div class="col-md-12">
					<div class="form-group"><textarea class="form-control" placeholder="Addıtıonal Informatıon" name="name" id="id" cols="30" rows="10"></textarea></div>
				</div>
				{{-- <div class="col-md-12">
					<div class="checkbox">
						<label>
							<input type="checkbox"> Ship to  Different address for invoice
						</label>
					</div>
				</div> --}}
			</div>

			@else
			<div class="row">
						<div class="col-md-6">
							<div class="form-group"><input class="form-control" type="text" placeholder="Address"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><input class="form-control" type="text" placeholder="Last Name"></div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><input class="form-control" type="text" placeholder="Address"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group selectpicker-wrapper">
								<select
								class="selectpicker input-price" data-live-search="true" data-width="100%"
								data-toggle="tooltip" title="Select">
								<option>Country</option>
								<option>Country</option>
								<option>Country</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group selectpicker-wrapper">
							<select
							class="selectpicker input-price" data-live-search="true" data-width="100%"
							data-toggle="tooltip" title="Select">
							<option>City</option>
							<option>City</option>
							<option>City</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group"><input class="form-control" type="text" placeholder="Postcode/ZIP"></div>
				</div>
				<div class="col-md-4">
					<div class="form-group"><input class="form-control" type="text" placeholder="Email"></div>
				</div>
				<div class="col-md-4">
					<div class="form-group"><input class="form-control" type="text" placeholder="Phone Number"></div>
				</div>
				<div class="col-md-12">
					<div class="form-group"><textarea class="form-control" placeholder="Addıtıonal Informatıon" name="name" id="id" cols="30" rows="10"></textarea></div>
				</div>
				{{-- <div class="col-md-12">
					<div class="checkbox">
						<label>
							<input type="checkbox"> Ship to  Different address for invoice
						</label>
					</div>
				</div> --}}
			</div>
			
			@endif
		
	</div>

	<div class="col-sm-4">
		<h3 class="block-title alt"><i class="fa fa-angle-down"></i> Payments options</h3>
                <div class="panel-group payments-options" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel radio panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                    <span class="dot"></span> Direct Bank Transfer
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body">
                                <div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus.</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapseTwo">
                                    <span class="dot"></span> Cheque Payment
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                            <div class="panel-body">
                                Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">
                                    <span class="dot"></span> Credit Card
                                </a>
                                <span class="overflowed pull-right">
                                    <img src="/wealth-assets/img/preview/payments/mastercard-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/visa-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/american-express-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/discovery-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/eheck-2.jpg" alt=""/>
                                </span>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3"></div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    <span class="dot"></span> PayUMoney
                                </a>
                                <span class="overflowed pull-right"><img src="/wealth-assets/img/preview/payments/paypal-2.jpg" alt=""/></span>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4"></div>
                    </div>
                </div>

                <div class="overflowed">
                    <a class="btn btn-theme btn-theme-dark" href="/en">Home Page</a>
                    <a class="btn btn-theme pull-right" href="/checkout-page">Place Your Order</a>
                </div>
	</div>
</div>
</form>
</div>
</section>

@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#country_id').on('change', function() {
			var countryID = $(this).val();
			if(countryID) {
				$.ajax({
					url: 'state/'+countryID,
					type: "GET",
					data : {"_token":"{{ csrf_token() }}",country_id:countryID},
					dataType: "json",
					success:function(data) {                       
						if(data){

							$('#state_id').empty();
							$('#state_id').focus;
							$('#state_id').append('<option value="">-- Select state_id --</option>'); 
							$.each(data, function(key, value){
								$('select[name="state_id"]').append('<option value="'+ value.id +'">' + value.state+ '</option>');
							});
						}else{
							$('#state_id').empty();
						}
					}
				});
			}else{
				$('#state_id').empty();
			}
		});
	});



	$(document).ready(function() {
		$('#state_id').on('change', function() {
			var stateID = $(this).val();
			if(stateID) {
				$.ajax({
					url: 'city/'+stateID,
					type: "GET",
					data : {"_token":"{{ csrf_token() }}",state_id:stateID},
					dataType: "json",
					success:function(data) {                       
						if(data){

							$('#city_id').empty();
							$('#city_id').focus;
							$('#city_id').append('<option value="">-- Select City --</option>'); 
							$.each(data, function(key, value){
								$('select[name="city_id"]').append('<option value="'+ value.id +'">' + value.city+ '</option>');
							});
						}else{
							$('#city_id').empty();
						}
					}
				});
			}else{
				$('#city_id').empty();
			}
		});
	});
</script>
@endsection