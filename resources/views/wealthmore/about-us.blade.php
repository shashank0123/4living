<?php
use App\Product;
use App\MainCategory;
use App\Testimonial;
use App\Banner;
use App\Review;
//use DB; 
$count=0;
?>

@extends('layouts/wealth-more')

@section('content')

<!-- CONTENT AREA -->
<div class="content-area">

    <!-- BREADCRUMBS -->
    <section class="page-section breadcrumbs">
        <div class="container">
            <div class="page-header">
                <h1>About Us</h1>
            </div>
            <ul class="breadcrumb"></ul>
        </div>
    </section>
    <!-- /BREADCRUMBS -->

    <!-- PAGE -->
    <section class="page-section color">
        <div class="container">

            <h2 class="text-center lead">Welcome To <strong>Wealthmore India</strong> Store!</h2><br>
            <p class="text-center lead">We Provide quality service and product. We believe in you and your experience. There is nothing else to say apart from the thing that after using our product, if you don't like it, return and take the money back.</p><br>


            <hr class="page-divider"/>
            <div class="row">
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-team no-border no-padding">
                        <div class="media">
                            <img class="img-circle" src="/wealth-assets/img/preview/team/team-1.jpg" alt=""/>
                        </div>
                        <div class="caption">
                            <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                            <ul class="social-icons">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                            <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-team no-border no-padding">
                        <div class="media">
                            <img class="img-circle" src="/wealth-assets/img/preview/team/team-2.jpg" alt=""/>
                        </div>
                        <div class="caption">
                            <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                            <ul class="social-icons">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                            <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-team no-border no-padding">
                        <div class="media">
                            <img class="img-circle" src="/wealth-assets/img/preview/team/team-3.jpg" alt=""/>
                        </div>
                        <div class="caption">
                            <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                            <ul class="social-icons">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                            <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="thumbnail thumbnail-team no-border no-padding">
                        <div class="media">
                            <img class="img-circle" src="/wealth-assets/img/preview/team/team-4.jpg" alt=""/>
                        </div>
                        <div class="caption">
                            <h4 class="caption-title">Standard Name <small>Support team</small></h4>
                            <ul class="social-icons">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                            <div class="caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper, quam vel viverra laoreet, nibh libero adipiscing diam, sit amet dictum sem nisi ut sapien.</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /PAGE -->

    <!-- PAGE -->
    <section class="page-section">
        <div class="container">
            <div class="row">
                        {{-- <div class="col-md-4">
                            <div class="product-list">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="block-title"><span>Top Sellers</span></h4>                         
                                <div class="media">
                                    <a class="pull-left media-link" href="#">
                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-3.jpg" alt="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                        <div class="rating">
                                                <span class="star"></span>
                                                <span class="star active"></span>
                                                <span class="star active"></span>
                                                <span class="star active"></span>
                                            <span class="star active"></span>
                                        </div>
                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <?php             
                        $top_products = DB::table( 'products' )
                        ->join( 'reviews', 'reviews.product_id', '=', 'products.id' )
                        ->where( 'products.status', 'Active' )
                        ->groupBy( 'products.id' )
                        ->select( 'products.id', DB::raw( 'AVG( rating )' ) )->orderBy('AVG( rating )','DESC')
                        ->limit(3)->get();

                        ?>
                        
                        <div class="col-md-6">
                            <div class="product-list">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="block-title"><span>Top Accessories</span></h4>
                                @if($top_products != null)
                                @foreach($top_products as $top)
                                <?php $product = Product::where('id',$top->id)->first(); ?>
                                @if($product != null)
                                <div class="media">
                                    <a class="pull-left media-link" href="/productdetail/{{$product->id}}">
                                        <img class="media-object" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->name}}" style="width: 100px; height: 100px;">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                        <?php
                                        $rate = Review::where('product_id',$product->id)->avg('rating');
                                        $final_rate = floor($rate)+1;
                                        if($final_rate == 0){
                                            $final_rate = -1;
                                        }
                                        else { $final_rate = $final_rate - 5; }
                                        ?>
                                    
                                        <div class="rating">
                                           @for($i=0 ; $i<5 ; $i++)
                                           @if($final_rate>=0)
                                           <span class="star active"></span>
                                           @else
                                           <span class="star"></span>
                                           @endif
                                           <?php $final_rate++; ?>
                                           @endfor
                                       </div>
                                       <div class="price"><ins>{{$product->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                   </div>
                                   
                               </div>
                               @endif
                               @endforeach
                               @endif                                
                           </div>
                       </div>

                       <?php
                       $new_arrival = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','DESC')->limit(3)->get();
                       ?>
                       <div class="col-md-6">
                        <div class="product-list">
                            <a class="btn btn-theme btn-title-more" href="#">See All</a>
                            <h4 class="block-title"><span>Top Newest</span></h4>

                            @if($new_arrival != null)
                            @foreach($new_arrival as $new)
                            <div class="media">
                                <a class="pull-left media-link" href="/productdetail/{{$new->id}}">
                                    <img class="media-object" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $new->image1 }}" alt="{{$new->name}}" style="width: 100px; height: 100px;">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="/productdetail/{{$new->id}}">{{$new->name}}</a></h4>
                                    <?php
                                    $rate = Review::where('product_id',$new->id)->avg('rating');
                                    $final_rate = floor($rate);
                                    if($final_rate == 0){
                                        $final_rate = -1;
                                    }
                                    else { $final_rate = $final_rate - 5; }
                                    ?>
                                
                                    <div class="rating">
                                       @for($i=0 ; $i<5 ; $i++)
                                       @if($final_rate>=0)
                                       <span class="star active"></span>
                                       @else
                                       <span class="star"></span>
                                       @endif
                                       <?php $final_rate++; ?>
                                       @endfor
                                   </div>
                                   <div class="price"><ins>Rs. {{$new->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                               </div>
                           </div>
                           @endforeach
                           @endif
                       </div>
                   </div>
               </div>
           </div>
       </section>
       <!-- /PAGE -->

   </div>
   <!-- /CONTENT AREA -->

   @endsection

