<?php 
use App\Models\MemberDetail;
use App\Models\Member;
use App\Models\Package;
?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
	<li><a href="#">Front Page</a></li>
	<li><a href="{{ route('admin.home') }}">Dashboard</a></li>
	<li class="active">{{$status}} List</li>
</ul>
@stop

@section('content')
<main>
	@include('back.include.sidebar')
	<div class="main-container" >
		@include('back.include.header')

		<style type="text/css">
			#DataTables_Table_0_wrapper .row{width: 100% !important}
		</style>
		<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


			@if($errors->any())
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</div>
			@endif

			@if($message = Session::get('message'))
			<div class="alert alert-primary">
				<p>{{ $message }}</p>
			</div>
			@endif

			<section class="tables-data" style="padding: 30px">
				<div class="row">
					<div class="col-sm-6">
						<div class="page-header">
							<h1><i class="md md-group-add"></i> {{$status}} Withdraw List</h1>


							<p>{{$status}} List</p>
						</div>
					</div>
					<div class="col-sm-6">

						<div style="text-align: right;">
							<form method="get" action="/admin/statement-withdraw">
								<input type="hidden" name="status" value="{{$status}}">
								<input type="search" class="form-group" name="search" style="padding: 5px" placeholder="Enter here" />
								<button type="submit" class="btn btn-primary">Search</button>
							</form>
							@if($status == 'Pending')

							<a href="/admin/download-excel" class="btn btn-danger" style="margin-top: 1px" onclick="hide()">Downlaod</a>

							@endif

						</div>
					</div>
				</div>

				<div class="card">
					<div>
						<div class="container">

							<table  class="table table-full-small " style="text-align: center;">
								<thead>
									<tr>
										<th>Date</th>
										<th>Username</th>
										<th>Amount</th>
										<th>Admin Fee</th>
										<th>TDS Charges</th>
										<th>Credit Amount</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody id="hideBody">
									<?php $i = 1; ?>
									
									@foreach($withdraw as $draw)
									<?php $i++; ?>
									<tr>
										<td>{{$draw->created_at}}</td>
										<td>{{$draw->username}}</td>
										<td>{{$draw->amount}}</td>
										<td>{{$draw->admin}}</td>
										<td>{{$draw->amount/20}}.00</td>
										<td>{{$draw->amount-$draw->admin-$draw->amount/20}}.00</td>
										<td>{{$draw->status}}</td>
									</tr>
									@endforeach
									
								</tbody>
							</table>

							@if($i==1)
							<div style="text-align: center; padding: 10%" id="showResult">
								<h3>No Result Found</h3>
							</div>
							@endif
						</div>
					</div>

				</div>
			</section>
		</div>
	</div>
</main>

<script>
	function hide(){
		$('#hideBody').hide();
		$('#showResult').show();
		$('#showResult').html('No Result Found');
	}
</script>
@stop
