<?php 

use App\OrderItem;
use App\Product;
use App\User;
use App\Models\Franchise;


// $cnt = 0;
// if($count_member<4005)
// { $cnt = 4005; }
// else{ $cnt = $count_member; }
?>


@extends('back.app')

@section('title')
Product Purchase Requests| {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Product Purchase Request List of Franchisees</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
      
      <section class="tables-data" style="padding: 30px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Purchase Request List</h1>
          <p>( {{$user_type}}) </p>
        </div>
        <div style="text-align: right;">
            <form method="get" action="/admin/searched-purchase-request">
              <input type="search" class="form-group" name="search_username" style="padding: 5px" placeholder="Enter username here" />
              <button type="submit" class="btn btn-primary">Search</button>
            </form>

        </div>
        
        <div class="card">
          <div>
            <div class="container">

              <table  class="table table-full-small " style="text-align: center;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Franchisee ID</th>
                    <th>Products</th>
                    <th>Total Amount</th>
                    <th>Status</th>                    
                  </tr>
                </thead>
                <tbody>
                  @if(!empty($orders))

                  <?php $i=1; ?>
                  @foreach($orders as $order)
                  <?php 
                  $user = User::where('id',$order->user_id)->first();
                  $franchise = Franchise::where('user_id',$order->user_id)->first();
                   ?>
                  <tr id="order-div{{$order->id}}">
                    <td>{{$i++}}</td>
                    <td>{{$order->created_at}}</td>
                    <td>{{$franchise->name}}</td>
                    <td>{{$user->username}}</td>
                   <td>
                     <?php $order_items = OrderItem::where('order_id',$order->id)->get();
                     if(!empty($order_items)){
                      foreach($order_items as $item){
                        $product = Product::where('id',$item->item_id)->first();
                        if(!empty($product)){
                          echo $product->name." - (".$item->quantity.") <br>";                        }
                      }
                     }
                      ?>
                    </td>
                    <td>{{$order->price}}</td>
                    <td>{{$order->payment_method}}</td>
                    <td>
                      <select name="status" id="status{{$order->id}}" class="form-control" onchange="changeStatus({{$order->id}})">
                        <option value="Pending" @if($order->order_status == 'Pending'){{'selected'}}@endif>Pending</option>
                        <option value="Delivered" @if($order->order_status == 'Delivered'){{'selected'}}@endif>Delivered</option>
                        <option value="Rejected" @if($order->order_status == 'Rejected'){{'selected'}}@endif>Rejected</option>
                      </select>
                    </td>
                  </tr>
                  @endforeach
                  @endif
				  
							  
                </tbody>
					
              </table>
			   
            </div>
          </div>
			
        </div>
		
      </section>
	  
    </div>
  </div>
</main>
<script>
  function changeStatus(id){
    var data = $('#status'+id).val();
    $.ajax({
        type: "POST",
        url: "/admin/change-order-status",
        data:{ status: data , order_id: id},
        success:function(msg){
          alert(msg.message)
          if(msg.message == 'Status Changed Successfully'){
            $('#order-div'+id).hide();
                 
          }
        }
      });
  }
</script>
@stop
