@extends('back.app')

@section('title')
  @lang('incomedirect.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.weeklyincome')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">

        <section style="padding:1% 4%">

        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">
            <h1><i class="md md-group-add"></i> Weekly Income</h1>
            <p class="lead">Weekly Income</p>
          </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/admin/weekly-income" style="margin-top: 20px">       
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 40%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
            </div>
          </div>
        </div>
           
      </section>

        <section class="tables-data" style="padding:1% 5%">
          

          <div class="card">
            <div>
              <div class="datatables">
                 <table class="table table-full" >
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Cash Amount</th>
                        {{-- <th data-id="admin_charge">@lang('misc.admin')</th> --}}
                        <th>TDS Charges</th>
                        {{-- <th data-id="repurchase">@lang('misc.repurchase')</th> --}}
                        {{-- <th data-id="amount_promotion">@lang('misc.promotion')</th> --}}
                        <th>Total</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count=0; ?>
                      @if(!empty($bonus_pairing))
                      
                      @foreach($bonus_pairing as $pairing)
                      <?php $count++; ?>
                      <tr>
                        <th>{{$pairing->username}}</th>
                        <th>{{$pairing->amount_cash}}</th>
                        {{-- <th data-id="admin_charge">@lang('misc.admin')</th> --}}
                        <th>{{$pairing->amount_promotion}}</th>
                        {{-- <th data-id="repurchase">@lang('misc.repurchase')</th> --}}
                        {{-- <th data-id="amount_promotion">@lang('misc.promotion')</th> --}}
                        <th>{{$pairing->total}}</th>
                        <th>{{$pairing->created_at}}</th>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                  

                  @if($count == 0 )
                  <div style="text-align: center;margin-top: 2%">
                    <h3>No Result Found</h3>
                  </div>
                  @endif
              </div>
            </div>
          </div>
		  {{ $bonus_pairing->links("pagination::bootstrap-4") }}
        </section>
      </div>
    </div>
  </main>
@stop
