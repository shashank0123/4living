<?php 
use App\Models\FranchiseDetails;
use App\Models\Franchise;


?>


@extends('back.app')

@section('title')
Update Franchise Account | {{ config('app.name') }}
@stop



@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
     @if(!empty($message))
     <p class="alert-success" style="padding: 20px">{{$message}}</p>
     @endif

      <section style="padding:  1% 5%">
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.reset1') Franchise</h1>
            
          </div>
          @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well white">
                <form method="POST" action="/admin/franchise-password/{{$franchisPassword->id}}">
                  <fieldset>

                    <div class="form-group">
                      <label class="control-label" for="new_password">Enter New Password</label>
                      <input type="password" name="new_password" class="form-control" id="new_password" required="" value="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="confirm_password">Confirm Password</label>
                      <input type="password" name="confirm_password" class="form-control" id="confirm_password" required="" value="">
                    </div>
                    

                    
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                   
                      <a href="/admin/franchise-list" class="btn btn-danger">@lang('common.cancel')</a>
                      {{-- <button type="reset" class="btn btn-default">@lang('common.cancel')</button> --}}
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>

<script>
  function resetForm(){
      $('input[type=password]').val("");      
    }
</script>

@stop
