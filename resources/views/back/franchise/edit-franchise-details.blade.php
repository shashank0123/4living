@extends('back.app')

@section('content')

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
	<a href="/admin/franchise-list"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
	<div style="width: 60%; text-align: center !important;" >
	<h3 style="margin-left: 20%">Add Franchise Bank Details</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			<form action="/admin/franchise-bank-details/{{$details->franchise_id}}" method="POST" >
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">
						
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="keyword">GST No. <font color="red">*</font></label>
							<input type="text" class="form-control" id="gst_no" name="gst_no"  maxlength="30" value="@if(!empty($details->gst_no)){{$details->gst_no}}@endif">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Pan Number <font color="red">*</font></label>							
							<input type="text" class="form-control" id="pan_no" name="pan_no"  maxlength="20" value="@if(!empty($details->pan_no)){{$details->pan_no}}@endif" >
						
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">Bank Name <font color="red">*</font></label>							
							<input type="text" class="form-control" id="bank_name" name="bank_name" maxlength="100" value="@if(!empty($details->bank_name)){{$details->bank_name}}@endif">
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Account Holder Name <font color="red">*</font></label>							
							<input type="text" class="form-control" id="account_holder_name" name="account_holder_name"  maxlength="50" value="@if(!empty($details->account_holder_name)){{$details->account_holder_name}}@endif" >
						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="status">Account Number <font color="red">*</font></label>							
							<input type="text" class="form-control" id="account_number" name="account_number"  maxlength="30" value="@if(!empty($details->account_number)){{$details->account_number}}@endif" >
						
					</div>
					
					<div class="col-md-6">
						
							<label for="status">Bank Address <font color="red">*</font></label>							
							<input type="text" class="form-control" id="bank_address" name="bank_address"  maxlength="200" value="@if(!empty($details->bank_address)){{$details->bank_address}}@endif" >
						
					</div>

					
				</div>
				
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="status">Branch Name <font color="red">*</font></label>							
							<input type="text" class="form-control" id="bank_branch" name="bank_branch"  maxlength="50" value="@if(!empty($details->bank_branch)){{$details->bank_branch}}@endif" >
						
					</div>
					<div class="col-md-6">
						
							<label for="status">Bank IFSC Code <font color="red">*</font></label>							
							<input type="text" class="form-control"  id="bank_ifsc_code" name="bank_ifsc_code"  maxlength="20" value="@if(!empty($details->bank_ifsc_code)){{$details->bank_ifsc_code}}@endif" >
						
					</div>

				</div>
				
				<button type="submit" class="btn btn-alt-primary">Update</button>
				
			                    
                   
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

