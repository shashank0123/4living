<?php
use App\Models\Member;
?>

@extends('back.app')

@section('title')
All Epins | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">All Epins</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="padding: 2%;">
      <section>

        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">                
              <h1> All Epins</h1>
            </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/admin/searched-funds">                  
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 50%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
            </div>
          </div>
        </div>       
      </section>

      <div class="well white">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Username</th>
              <th>Userid</th>
              <th>Epins</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            @php
            $total = 0;
            $username = 0;
            $count = 0
            @endphp
            @if($epoints != null)
            @foreach($epoints as $epoints)
            @php
            $count++;
            $username = Member::where('id',$epoints->receiver_id)->first();
            @endphp
            @if($username != null)
            <tr>        
              <td>{{$username->username}}</td>
              <td>HH1{{sprintf("%05d", $username->user_id)}}</td>
              <td>{{$epoints->no_of_epoint}}</td>
              <td>{{$epoints->created_at}}</td>
            </tr>
            @endif
            @php
            $total = $epoints->no_of_epoint+$total;
            @endphp
            @endforeach
            @endif
          </tbody>
          <tr>
            <td><STRONG>Total</STRONG></td>
            <td></td>
            <td><strong>@if (isset($epoints->no_of_epoint)) {{$total}} @else {{ 0 }} @endif</strong></td>
            <td></td>
          </tr>
        </table>

        @if($count == 0)
        <div style="text-align: center; margin-top: 2%">
          <h3>No Result Found</h3>
        </div>
        @endif
      </div>
    </div>
  </div>
</main>
@stop
