@extends('back.app')

@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

  <a href="/admin/main-category"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br>
    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="/admin/edit-main-category/{{ $maincategory->id }}" method="POST" enctype="multipart/form-data">

        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="name" name="Mcategory_name" value="{{$maincategory->Mcategory_name}}">
              <label for="name">Category</label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="Mcategory_description" name="Mcategory_description" value="{{$maincategory->Mcategory_description}}">
              <label for="email">Description</label>
            </div>
          </div>
        </div><br>

        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="featured" name="featured" value="{{ $maincategory->featured }}">
              <label for="text">Featured</label>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-material floating">
              <select name="category_id" class="form-control">
                <option value=""></option>
                @foreach($categoryarray as $data)
                {{-- <option value="{{$data->id}}">{{$data->Mcategory_name}}</option> --}}
                <option value="{{ $data->id}}" {{ $maincategory->category_id==$data->id? 'selected': null }}>{{ $data->Mcategory_name  }}</option>
                @endforeach
                </select>
              <label for="mobile">Parent Category</label>
            </div>
          </div>        
        </div>

        <div class="form-group row">
         <div class="col-md-6">
            <div class="form-material ">
              <select name="status" class="form-control">
                <option value="Deactive" {{ $maincategory->status=='Deactive'? 'selected': null }}>Deactive</option>
                <option value="Active" {{ $maincategory->status=='Active'? 'selected': null }}>Active</option>
              </select>
              <label for="mobile">Status</label>
            </div>
          </div>
        </div>
        <div class="form-group row">                           
          <button type="submit" class="btn btn-alt-primary">Submit</button>      
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Page Content -->
@endsection

