@extends('back.app')

@section('content')

@include('back.include.header')
@include('back.include.sidebar')

<section style="margin-top: 50px;">
<div class="container">
  <div class="row">
    <div class="col-sm-6">
  <a href="/admin/add-image-gallery"><button type="submit" class="btn btn-alt-primary">Add Gallery Images</button> </a><br><br></div>
  
</div>
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
<div style="overflow: auto;">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>        
        <th>Event Name</th>               
        <th>Event address</th>
		<th>Status</th>
        <th>Created At</th>
        <th>Action</th>        
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>

      @foreach ($imagegallery as $row)

      <tr>
        <td>{{$count++}}</td>        
        <td>{{$row->name}}</td>   
		<td>{{$row->designation}}</td>     		
        <td>{{ $row->status=='0'? 'Deactive': 'Active' }}</td>
        <td>{{$row->created_at}}</td>
	<td>
      <a class="btn btn-primary" href="{{ url('/admin/edit-image-gallery', $row->id) }}"><i class="fa fa-edit"></i></a>
      <a class="btn btn-danger" href="{{ url('/admin/image-gallery', $row->id) }}" onclick="return myFunction();"><i class="fa fa-trash"></i></a>
    </td>
        
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>

<script>
  function myFunction() {
      if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
 </script>
@endsection
