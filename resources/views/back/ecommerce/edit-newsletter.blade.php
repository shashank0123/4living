@extends('back.app')

@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

  <a href="/admin/newsletter"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="/admin/edit-newsletter/{{$newsletter->id}}" method="POST">

        <div class="form-group row">          
          <div class="col-md-12">
            <div class="form-material ">
              <input type="text" class="form-control" id="email" name="email" value="{{$newsletter->email}}" readonly>
              <label for="email">Email</label>
            </div>
          </div>
        </div><br>       
              

        <div class="form-group row">
         <div class="col-md-12">
            <div class="form-material ">
              <select name="status" class="form-control">
                <option value="Deactive" {{ $newsletter->status=='Deactive'? 'selected': null }}>Deactive</option>
                <option value="Active" {{ $newsletter->status=='Active'? 'selected': null }}>Active</option>
              </select>
              <label for="mobile">Status</label>
            </div>
          </div>
        </div>


        <div class="form-group row">                           
          <button type="submit" class="btn btn-alt-primary" name="submit">Submit</button>      
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Page Content -->
@endsection

