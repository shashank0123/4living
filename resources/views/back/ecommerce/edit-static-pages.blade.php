@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/static-pages"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<input type="text" class="form-control" id="page_name" name="page_name" value="{{ $page->page_name }}" readonly>
							<label for="heading">Page</label>
						</div>
					</div>							
					
					<div class="col-md-12">						
						<div class="form-material floating">
							<input type="text" class="form-control" id="page_heading" name="page_heading" value="{{ $page->page_heading }}">
							<label for="heading">Page Heading</label>
						</div>
					</div>					
				</div>
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Page Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="description" class="form-control" id="description" >{{$page->description}}</textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>

				<button type="submit" class="btn btn-alt-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

