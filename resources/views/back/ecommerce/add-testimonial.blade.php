@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="testimonial"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-testimonial" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="client_name" name="client_name">
							<label for="client_name">Client Name</label>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="city" name="city">
							<label for="city">City</label>
						</div>
					</div>
				</div>



				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="number" class="form-control" id="rating" name="rating">
							<label for="rating">Rating</label>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="review" name="review">
							<label for="review">Review</label>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 15% ; width: 85%">
							<label for="text">Image</label>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
							<label for="status">Status</label>
						</div>
					</div>
				</div>
				
				<button type="submit" class="btn btn-alt-primary">Submit</button>

			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

