@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/image-gallery"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="/admin/edit-image-gallery/{{$imagegallery->id}}" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="question">Event Name</label><br>
							<input type="text" class="form-control" id="ename" name="ename" value="{{ $imagegallery->name }}">
						</div>
					</div>					
				</div>


				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="answers">Event Address</label><br>
							<input type="text" class="form-control" id="eaddress" name="eaddress" value="{{ $imagegallery->designation }}">
						</div>
					</div>

					
				</div>
				<div class="form-group row">
					<div class="col-md-12">
						  <div class="form-material floating">
							<label for="keyword">Event Image</label><br><br>
							 <div class="input-group control-group increment" >
							
							 
							  <input type="file" name="filename[]" class="form-control">
							  <div class="input-group-btn"> 
								<button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
							  </div>
							  </div>
							   <div class="clone hide">
							  <div class="control-group input-group" style="margin-top:10px">
							   <input type="file" name="filename[]" class="form-control">
							  
								<div class="input-group-btn"> 
								  <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
								</div>
							  </div>
							</div>
							   @foreach(json_decode($imagegallery->filename, true) as $key => $value)
							   <div  style="float:left; margin:10px;">
							   <img src="/assetsss/images/gallery/{{ $value}}" style="width:100px; height:100px"> <br> 
							  <a class="btn btn-danger" href="{{ url('/admin/image-gallery-delete',[ $imagegallery->id, $value]) }}" onclick="return myFunction();" style="margin-top:10px; margin-left:30px"><i class="fa fa-trash"></i></a>
							   
							 
								</div>
								@endforeach
								
								
						</div>
										
						
					</div>
					</div>
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="status">Status</label><br>
							<select name="status" class="form-control">
								<option value="0" {{ $imagegallery->status=='0'? 'selected': null }}>Deactive</option>
								<option value="1" {{ $imagegallery->status=='1'? 'selected': null }}>Active</option>
							</select>
						</div>
					</div>				
				</div>                   
				<button type="submit" class="btn btn-alt-primary">Update</button>
			</div>
		</form>
	</div>
</div>
</div>

<script type="text/javascript">
	 function myFunction() {
      if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
  
    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>
<!-- END Page Content -->
@endsection

