@extends('back.app')

@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/video-gallery"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="/admin/edit-video-gallery/{{$videoedit->id}}" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="question">Title</label><br>
							<input type="text" class="form-control" id="title" required name="title" value="{{ $videoedit->title }}">
						</div>
					</div>					
				</div>


				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="answers">Url</label><br>
							<input type="text" class="form-control" id="url" required name="url" value="{{ $videoedit->url }}">
						</div>
					</div>
					

					
				</div>
		
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="status">Status</label><br>
							<select name="status" class="form-control">
								<option value="0" {{ $videoedit->status=='0'? 'selected': null }}>Deactive</option>
								<option value="1" {{ $videoedit->status=='1'? 'selected': null }}>Active</option>
							</select>
						</div>
					</div>				
				</div>                   
				<button type="submit" class="btn btn-alt-primary">Update</button>
			</div>
		</form>
	</div>
</div>
</div>


<!-- END Page Content -->
@endsection

