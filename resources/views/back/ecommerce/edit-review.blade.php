@extends('back.app')
<?php
  use App\Product;
?>
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">


  <a href="/admin/review"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="/admin/edit-review/{{$review->id}}" method="POST">

        <div class="form-group row">

          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="name" name="name" value="{{$review->name}}">
              <label for="name">Name</label>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="email" name="email" value="{{$review->email}}" readonly>
              <label for="email">Email</label>
            </div>
          </div>
        </div><br>

        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="rating" name="rating" value="{{ $review->rating }}">
              <label for="text">Rating</label>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-material ">
              <input type="text" class="form-control" id="review" name="review" value="{{ $review->review }}">
              <label for="text">Review</label>
            </div>
          </div>
        </div>


        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material floating">
             <?php $prod = Product::where('id',$review->product_id)->first(); ?>
             <input type="text" class="form-control" id="product_id" name="product_id" value="{{ $prod->name }}" readonly>
              <label for="mobile">Product</label>
            </div>
          </div>        

        
         <div class="col-md-6">
            <div class="form-material ">
              <select name="status" class="form-control">
                <option value="Deactive" {{ $review->status=='Deactive'? 'selected': null }}>Deactive</option>
                <option value="Active" {{ $review->status=='Active'? 'selected': null }}>Active</option>
              </select>
              <label for="mobile">Status</label>
            </div>
          </div>
        </div>
        <div class="form-group row">                           
          <button type="submit" class="btn btn-alt-primary" name="submit">Submit</button>      
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Page Content -->
@endsection

