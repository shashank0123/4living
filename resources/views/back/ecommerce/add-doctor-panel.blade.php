@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

  <a href="/admin/doctor-panel"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="add-doctor-panel" method="POST" enctype="multipart/form-data">
        {{-- @csrf --}}
        
         <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Panel Type </label>
            <br>
			 <select class="form-control" name="panel_type" id="panel_type">
              <option value="consultwithdoctors">Consult with Doctors</option>
              <option value="panelofdoctors">Panel of Doctors</option>
            </select>
       
          </div>
        </div>
      </div>
        <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Doctor's Name </label>
            <br>
            <input type="text" class="form-control" id="name" name="name">
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Doctor's Designation  </label><br>
            <input type="text" class="form-control" id="designation" name="designation">
          </div>
        </div>
      </div>
	<div class="form-group row">
					<div class="col-md-12">
						  <div class="form-material floating">
							<label for="keyword">Doctor's Image</label><br>
							<input type="file" class="form-control" id="image_url" name="image_url" >
						</div>
					</div>
					</div>
					<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Doctor's Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="description" class="form-control" id="description" ></textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>
		        <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Show Rank </label>
            <br>
            <input type="text" class="form-control" id="rank" name="rank">
          </div>
        </div>
      </div>		
      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Status</label><br>
            <select class="form-control" name="status">
              <option value="1">Active</option>
              <option value="0">Deactive</option>
            </select><br>
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-alt-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
<!-- END Page Content -->
@endsection

