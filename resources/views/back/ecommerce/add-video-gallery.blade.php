@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

  <a href="/admin/video-gallery"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="add-video-gallery" method="POST" enctype="multipart/form-data">
        {{-- @csrf --}}
        
        
        <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Title </label>
            <br>
            <input type="text" class="form-control" id="title" name="title" size="50" required>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>URL  </label><br>
            <input type="url" class="form-control" id="url" name="url" required  >
          </div>
        </div>
      </div>
	
      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Status</label><br>
            <select class="form-control" name="status">
              <option value="1">Active</option>
              <option value="0">Deactive</option>
            </select><br>
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-alt-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
<!-- END Page Content -->
@endsection

