@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/blog"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				
				
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="heading" name="heading" value="{{ $blog->heading }}">
							<label for="heading">Blog Heading</label>
						</div>
					</div>
					
									
					
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-8">
								<div class="form-material floating">

									<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 15% ; width: 85%">
									<label for="image_url">Image 3</label>

								</div>
							</div>
							<div class="col-sm-4">
								<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $blog->image_url }} " class="logo-img">
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Page Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="description" class="form-control" id="description" >{{$blog->description}}</textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="status" class="form-control">
								<option value="Deactive" {{ $blog->status=='Deactive'? 'selected': null }}>Deactive</option>
								<option value="Active" {{ $blog->status=='Active'? 'selected': null }}>Active</option>
							</select>
							<label for="mobile">Status</label>
						</div>
					</div>
				</div>

                   
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                       </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

