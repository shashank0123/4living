<?php $type = \Input::get('t'); ?>
@extends('back.app')

@section('title')
All {{ ucwords($type) }} | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">{{ ucwords($type) }}List</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">

      <section class="tables-data" style="padding:10px 50px">
        <div class="page-header">
          <div class="row">
            <div class="col-sm-6">              
              <h1><i class="md md-account-balance"></i> Bonus Pairing List</h1>
              <p class="lead">Your Bonus Pairing List.</p>          
            </div>
            <div class="col-sm-6" style="text-align: right;">
              <form method="get" action="/admin/searched-bonus-list">                  
                <input type="search" class="form-group" name="search" style="padding: 5px; width: 50%" placeholder="Enter username here" id="search" required/>
                <button type="submit" class="btn btn-primary" >Search</button>
              </form>
              <a href="/admin/download" class="btn btn-danger" style="margin-top: 1px">Downlaod</a>
            </div>
          </div>
        </div>        
      </section>



      <div class="card">
        <div class="card-content">
          <div class="datatables">
            <table class="table table-full ">
              <thead>
                <tr>
                  <th data-id="created_at">Created Date</th>
                  <th data-id="username">Username</th>
                  <th data-id="total">Amount</th>
                  {{-- <th data-id="action">Action</th> --}}
                </tr>
              </thead>
              <tbody>
                <?php $count=0; ?>
                @if(!empty($bonus))
                @foreach($bonus as $b)
                <?php $count++; ?>
                <tr>
                  <td>{{$b->created_at}}</td>
                  <td>{{$b->username}}</td>
                  <td>{{$b->amount_cash}}</td>
                  {{-- <td></td> --}}
                </tr>
              </tbody>
              @endforeach
              @endif
            </table>

            @if($count == 0)
            <div style="text-align: center; margin-top: 2%">
              <h3>No Result Found</h3>
            </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</main>
@stop
