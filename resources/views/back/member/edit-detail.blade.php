<?php 
use App\Models\MemberDetail;
use App\Models\Member;
use App\User;


$user = User::where('id',$getMember->user_id)->first();

?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member Detail</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
     @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif

      <section style="padding:  1% 5%">
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.title1')</h1>
            <p class="lead">@lang('settings.subTitle1')</p>
          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well white">
                <form method="POST" action="/admin/personal-info/{{$getMember->id}}">
                  <fieldset>

                    <div class="form-group">
                      <label class="control-label" for="username">User Name</label>
                      <input type="text" name="username" class="form-control" id="username" required="" value="{{ $getMember->username }}">
                    </div>
                    

                    @if(!empty($user->first_name))
                    <div class="form-group">
                      <label class="control-label">@lang('settings.name')</label>
                      <input type="text" class="form-control" required="" value="{{ $user->first_name }}" name="first_name">
                    </div>
                    @else
                    <div class="form-group">
                      <label class="control-label">@lang('settings.name')</label>
                      <input type="text" class="form-control" required=""  value="{{ $user->first_name }}" name="first_name">
                    </div>
                    @endif

                    {{-- <div class="form-group">
                      <label for="profimage" class="control-label"> Upload an image</label>
                      <input type="file" id="profimage" name="profimage" class="form-control" >
                    </div> --}}
                    
                    @if(!empty($member))
                    @if(!empty($member->date_of_birth))
                    <div class="form-group">
                      <label class="control-label" for="inputDOB">@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="{{ $member->date_of_birth }}" data-date-format="YYYY-MM-DD">
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputDOB">@lang('settings.dob')</label>
                      <input type="text" name="date_of_birth" required="" id="inputDOB" class="form-control datepicker" value="" data-date-format="YYYY-MM-DD">
                    </div>
                   
                    @endif
                    

                    
                    @if(!empty($member))
                    @if(!empty($member->gender))
                    <div class="form-group">
                      <label class="control-label" for="inputGender">@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender" >
                        <option value="Male" @if ($member->gender == 'Male') selected="" @endif>@lang('settings.gender.male')</option>
                        <option value="Female" @if ($member->gender == 'Female') selected="" @endif>@lang('settings.gender.female')</option>
                      </select>
                    </div>
                     @endif
                    @else 
                    <div class="form-group">
                      <label class="control-label" for="inputGender">@lang('settings.gender')</label>
                      <select class="form-control" name="gender" id="inputGender">
                        <option value="Male">@lang('settings.gender.male')</option>
                        <option value="Female">@lang('settings.gender.female')</option>
                      </select>
                    </div>
                   
                    @endif

                    @if(!empty($member))
                    @if(!empty($member->mobile_phone))
                    <div class="form-group">
                      <label class="control-label" for="inputPhone">@lang('settings.mobile')</label>
                      <input type="text" name="mobile_phone" class="form-control" id="inputPhone" required="" value="{{ $member->mobile_phone }}">
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputPhone">@lang('settings.mobile')</label>
                      <input type="text" name="mobile_phone" class="form-control" id="inputPhone" required="" value="">
                    </div>
                   
                    @endif

                   
                    @if(!empty($member))
                    @if(!empty($member->address))
                    <div class="form-group">
                      <label class="control-label" for="inputAddress">@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address" class="form-control" id="inputAddress" required="">{{ $member->address }}</textarea>
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputAddress">@lang('settings.address')</label>
                      <textarea style="border:1px solid #dddddd;padding:5px" name="address" class="form-control" id="inputAddress" required=""></textarea>
                    </div>
                    @endif

                    @if(!empty($member))
                    @if(!empty($state))
                   <div class="form-group">
                      <label class="control-label" for="inputState">State</label>
                      <select name="state" class="form-control" id="inputState" required="" value="{{$member->state}}">
                        <option value="">Select State</option>
                        <option @if ($member->state=='AP') {{ 'selected' }} @endif value="AP">Andhra Pradesh</option>
                        <option @if ($member->state=='AR') {{ 'selected' }} @endif value="AR">Arunachal Pradesh</option>
                        <option @if ($member->state=='AS') {{ 'selected' }} @endif value="AS">Assam</option>
                        <option @if ($member->state=='BH') {{ 'selected' }} @endif value="BH">Bihar</option>
                        <option @if ($member->state=='CT') {{ 'selected' }} @endif value="CT">Chhattisgarh</option>
                        <option @if ($member->state=='DL') {{ 'selected' }} @endif value="DL">Delhi</option>
                        <option @if ($member->state=='GA') {{ 'selected' }} @endif value="GA">Goa</option>
                        <option @if ($member->state=='GJ') {{ 'selected' }} @endif value="GJ">Gujarat</option>
                        <option @if ($member->state=='HR') {{ 'selected' }} @endif value="HR">Haryana</option>
                        <option @if ($member->state=='HP') {{ 'selected' }} @endif value="HP">Himachal Pradesh</option>
                        <option @if ($member->state=='JK') {{ 'selected' }} @endif value="JK">Jammu and Kashmir</option>
                        <option @if ($member->state=='JH') {{ 'selected' }} @endif value="JH">Jharkhand</option>
                        <option @if ($member->state=='KA') {{ 'selected' }} @endif value="KA">Karnataka</option>
                        <option @if ($member->state=='KL') {{ 'selected' }} @endif value="KL">Kerala</option>
                        <option @if ($member->state=='MP') {{ 'selected' }} @endif value="MP">Madhya Pradesh</option>
                        <option @if ($member->state=='MH') {{ 'selected' }} @endif value="MH">Maharashtra</option>
                        <option @if ($member->state=='MN') {{ 'selected' }} @endif value="MN">Manipur</option>
                        <option @if ($member->state=='ML') {{ 'selected' }} @endif value="ML">Meghalaya</option>
                        <option @if ($member->state=='MZ') {{ 'selected' }} @endif value="MZ">Mizoram</option>
                        <option @if ($member->state=='NL') {{ 'selected' }} @endif value="NL">Nagaland</option>
                        <option @if ($member->state=='OR') {{ 'selected' }} @endif value="OR">Orissa</option>
                        <option @if ($member->state=='Pondicherry') {{ 'selected' }} @endif value="Pondicherry">Pondicherry</option>
                        <option @if ($member->state=='PB') {{ 'selected' }} @endif value="PB">Punjab</option>
                        <option @if ($member->state=='RJ') {{ 'selected' }} @endif value="RJ">Rajasthan</option>
                        <option @if ($member->state=='SK') {{ 'selected' }} @endif value="SK">Sikkim</option>
                        <option @if ($member->state=='TN') {{ 'selected' }} @endif value="TN">Tamil Nadu</option>
                        <option @if ($member->state=='TR') {{ 'selected' }} @endif value="TR">Tripura</option>
                        <option @if ($member->state=='UK') {{ 'selected' }} @endif value="UK">Uttaranchal</option>
                        <option @if ($member->state=='UP') {{ 'selected' }} @endif value="UP">Uttar Pradesh</option>
                        <option @if ($member->state=='WB') {{ 'selected' }} @endif value="WB">West Bengal</option>
                      </select>
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputState">State</label>
                      <select name="state" class="form-control" id="inputState" required="" value="">
                        <option  value="">Select State</option>
                        <option  value="AP">Andhra Pradesh</option>
                        <option  value="AR">Arunachal Pradesh</option>
                        <option  value="AS">Assam</option>
                        <option  value="BH">Bihar</option>
                        <option  value="CT">Chhattisgarh</option>
                        <option  value="DL">Delhi</option>
                        <option  value="GA">Goa</option>
                        <option  value="GJ">Gujarat</option>
                        <option  value="HR">Haryana</option>
                        <option  value="HP">Himachal Pradesh</option>
                        <option  value="JK">Jammu and Kashmir</option>
                        <option  value="JH">Jharkhand</option>
                        <option  value="KA">Karnataka</option>
                        <option  value="KL">Kerala</option>
                        <option  value="MP">Madhya Pradesh</option>
                        <option  value="MH">Maharashtra</option>
                        <option  value="MN">Manipur</option>
                        <option  value="ML">Meghalaya</option>
                        <option  value="MZ">Mizoram</option>
                        <option  value="NL">Nagaland</option>
                        <option  value="OR">Orissa</option>
                        <option  value="Pondicherry">Pondicherry</option>
                        <option  value="PB">Punjab</option>
                        <option  value="RJ">Rajasthan</option>
                        <option  value="SK">Sikkim</option>
                        <option  value="TN">Tamil Nadu</option>
                        <option  value="TR">Tripura</option>
                        <option  value="UK">Uttaranchal</option>
                        <option  value="UP">Uttar Pradesh</option>
                        <option  value="WB">West Bengal</option>
                      </select>
                    </div>
                    @endif
                    
                   
                   @if(!empty($member))
                   @if(!empty($member->adhaar))
                    <div class="form-group">
                      <label class="control-label" for="inputAdhaar">Aadhar Number</label>
                      <input type="text" name="adhaar" class="form-control" id="inputAdhaar" required=""  value="{{ $member->adhaar }}">
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputAdhaar">Aadhar Number</label>
                      <input type="text" name="adhaar" class="form-control" id="inputAdhaar" required="" value="">
                    </div>
                    @endif
                    
                    
                    @if(!empty($member))
                    @if(!empty($member->pan))
                    <div class="form-group">
                      <label class="control-label" for="inputPan">PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan"  value="{{ $member->pan }}">
                    </div>
                     @endif
                    @else
                     <div class="form-group">
                      <label class="control-label" for="inputPan">PAN Number</label>
                      <input type="text" name="pan" class="form-control" id="inputPan" value="">
                    </div>
                    @endif
                    
                     
                    
                    @if(!empty($member))
                    @if(!empty($member->beneficiary_name))
                    <div class="form-group">
                      <label class="control-label" for="inputBeneficiaryName">@lang('settings.beneficiary.name')</label>
                      <input type="text" name="beneficiary_name" id="inputBeneficiaryName" class="form-control" value="{{ $member->beneficiary_name }}">
                    </div>
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" for="inputBeneficiaryName">@lang('settings.beneficiary.name')</label>
                      <input type="text" name="beneficiary_name" id="inputBeneficiaryName" class="form-control" value="">
                    </div>
                    @endif
                    

                     @if(!empty($member))
                    @if(!empty($member->relation_with_beneficiary))
                    <div class="form-group">
                      <label class="control-label" id="Relation_with_Beneficiary">Relation with Beneficiary</label>
                      <input type="text"  class="form-control" id="relation_with_beneficiary" name="relation_with_beneficiary" value="{{ $member->relation_with_beneficiary }}">
                    </div> 
                     @endif
                    @else
                    <div class="form-group">
                      <label class="control-label" id="Relation_with_Beneficiary">Relation with Beneficiary</label>
                      <input type="text"  class="form-control" id="relation_with_beneficiary" name="relation_with_beneficiary" value="">
                    </div>
                    @endif
                    
                   <div class="form-group">
                      {{-- <label class="control-label" for="inputSecret">@lang('settings.secret')</label> --}}
                      {{-- <input type="hidden" name="s" class="form-control" id="inputSecret" value="asd123"> --}}
                    </div>

                    {{-- <div class="form-group">
                      <label class="control-label" for="inputSecret">@lang('settings.secret')</label>
                      <input type="password" class="form-control" id="Secret" required="">
                    </div> --}}

                    {{-- <div class="form-group">
                      <div class="alert alert-info">
                        @lang('settings.passwordNotice')
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('settings.password')</label>
                      <input type="password" name="password" class="form-control" id="inputPassword" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.repassword')</label>
                      <input type="password" class="form-control" data-parsley-equalto="#inputPassword" minlength="5">
                    </div> --}}
                    

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <a  class="btn btn-secondary" onclick="resetForm()">Reset</a>
                      <a href="/admin/member-list" class="btn btn-danger">@lang('common.cancel')</a>
                      {{-- <button type="reset" class="btn btn-default">@lang('common.cancel')</button> --}}
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>

<script>
  function resetForm(){
      $('input[type=text]').val("");
      $('input[type=number]').val("");
      $('textarea').val("");
      
    }
</script>

@stop
