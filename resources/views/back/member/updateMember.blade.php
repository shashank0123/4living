<?php 
use App\Models\MemberDetail;
use App\Models\Member;
use App\Models\Package;
use App\User;

// $cnt = 0;
// if($count_member<4005)
// { $cnt = 4005; }
// else{ $cnt = $count_member; }
?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member List</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">


      @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
      
      <section class="tables-data" style="padding: 30px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Member List</h1>
        </div>
        <div style="text-align: right;">
            <form method="get" action="/admin/searched-members">
              <input type="search" class="form-group" name="search_username" style="padding: 5px" placeholder="Enter username here" />
              <button type="submit" class="btn btn-primary">Search</button>
            </form>

        </div>
        
        <div class="card">
          <div>
            <div class="container">

              <table  class="table table-full-small " style="text-align: center;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Joined On</th>
                    <th>Company Serial No.</th>
                    <th>Name</th>
                    <th>Member ID</th>
                    <th>Mobile</th>
                    <th>Sponsor ID</th>
                    <th>Package</th>
                    <th>Profile Update</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($members as $member)
                  <?php
                  $mbr = MemberDetail::where('member_id',$member->id)->first();
    $user = User::where('id',$member->user_id)->first();
                  ?>
                  <tr>
                    <td>{{$i++}}</td>
                    <td>{{$member->created_at}}</td>
                    <td>{{4005 + ($count_member--)}}</td>
                    
                    <td>@if(!empty($member->name)){{$member->name}}@endif</td>
                    <td>@if(!empty($member->username)){{$member->username}}@endif</td>
                    <td>@if(!empty($mbr->mobile_phone)){{$mbr->mobile_phone}}@endif</td>
                    
                    <td>{{$member->register_by}}</td>
                    
                    <td>
                     @if($member->package_id =='2')
                     {{'2100'}}
                     @else
                     {{'0'}}
                     @endif
                    <td>
                      <?php
                      $direct = Member::where('id',$member->direct_id)->first();
                      ?>
                      @if(!empty($direct)){{$direct->username}} @else{{'-'}} @endif</td>
                    <td>
                      
                      {{$member->register_by}}</td>
                    <td>
                      <a href="/admin/personal-info/{{$member->id}}" class="btn btn-primary">Personal Info</a>
                      <br>
                      <a href="/admin/bank/{{$member->id}}" class="btn btn-primary" style="margin-top: 5px">Bank Detail</a>
                      <a href="/admin/password/{{$member->user_id}}" class="btn btn-primary" style="margin-top: 5px">Password</a>
                    </td>
                    <th>
                        <a href="/admin/member-detail/{{$member->id}}" class="btn btn-primary">Info</a>
                        <form method="POST" action="/admin/delete/{{$member->id}}" style="margin-top: 20px">
                        @if(!empty($user))
                        @if($user->is_ban == 0)
                      <button type="submit" class="btn btn-success">Block</button>
                      @else
                      <button type="submit" class="btn btn-danger">Blocked</button>
                      @endif
                      @endif
                      </form>
                      
                    </td>
                  </tr>
                  @endforeach
				  
							  
                </tbody>
					
              </table>
			   
            </div>
          </div>
			
        </div>
		{{ $members->links("pagination::bootstrap-4") }}
      </section>
	  
    </div>
  </div>
</main>
@stop
