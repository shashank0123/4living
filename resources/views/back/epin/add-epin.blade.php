 <?php
		use App\Models\Package;
		//use App\Models\Product;
		$packages = Package::all();
		//$Products = Product::all();
  ?>
@extends('back.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@section('content')

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
	<a href="pin-list"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
	<div style="width: 60%; text-align: center !important;" >
	<h3 style="margin-left: 20%">Create New Epin</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			<form action="add-epin" method="POST" >
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">
						
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="keyword">Epin Type <font color="red">*</font></label>
							 <select class="form-control" name="epin_type" id="epin_type" required>
								
								<option value="Joining">Joining</option>
								<option value="Purchase">Purchase</option>
								
							</select>
						
					</div>

					<div class="col-md-6" id="productsel">
						
							<label for="status">Product <font color="red">*</font></label>							
							<select class="form-control" name="epin_type_id" id="epin_type_id" required>
								<option value="">Select Product</option>
								<option value="1">1</option>
								<option value="2">2</option>
								
							</select>
						
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">Package</label>							
										 <select class="form-control" name="package_id" id="inputPackage" required="">
								@if (count($packages) > 0)
								@foreach ($packages as $package)
								<option value="{{ $package->id }}">
								  {{ number_format($package->package_amount, 0) }}
								</option>
								@endforeach
								@endif
							  </select>
						
					</div>

					<div class="col-md-6">
						
							<label for="status">No of Epins <font color="red">*</font></label>							
							<input type="text" class="form-control" id="no_of_epins" name="no_of_epins"  maxlength="3">
						
					</div>
				</div>
				
				
				
			
				
			
			
				
				
			         
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
	<script type="text/javascript">
		
$( document ).ready(function() {
	//$('#productsel').hide();
	
	 $("#epin_type_id").prop('disabled', 'disabled');
	  $("select[name='epin_type']").change(function(){
		  var epin_type = $(this).val();
		 if(epin_type == 'Purchase'){
			// $('#productsel').show();
			 $("#epin_type_id").removeAttr("disabled");
		 }else{
			 //$('#productsel').hide();
			 $("#epin_type_id").prop('disabled', 'disabled');
		 }
		
	  });
  });
</script>

    <!-- END Page Content -->
    @endsection

