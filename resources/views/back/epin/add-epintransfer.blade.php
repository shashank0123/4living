 <?php
		use App\Models\Package;
		
		$packages = Package::all();
		
		use App\Models\Franchise;
		$franchiseAll = Franchise::all();
  ?>
@extends('back.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@section('content')

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white;">
	<div class="row" style=" padding-left: 2%">
	<a href="pin-list"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>
	<div style="width: 60%; text-align: center !important;" >
	<h3 style="margin-left: 20%">Create New Epin</h3>
		</div>
	</div>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">

		<div class="block-content">
			<form action="add-epintransfer" method="POST" >
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">
						
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						
							<label for="keyword">Choose Franchise <font color="red">*</font></label>
							<select class="form-control" name="receiver_id" id="receiver_id" required="">
								@if (count($franchiseAll) > 0)
								@foreach ($franchiseAll as $val)
								<option value="{{ $val->id }}">
								  {{ $val->name }} - {{ $val->address1 }} - {{ $val->city }}
								</option>
								@endforeach
								@endif
							  </select>
						
					</div>

					<div class="col-md-6">
						
							<label for="status">Package</label>							
										 <select class="form-control" name="package_id" id="inputPackage" required="">
								@if (count($packages) > 0)
								@foreach ($packages as $package)
								<option value="{{ $package->id }}">
								  {{ number_format($package->package_amount, 0) }}
								</option>
								@endforeach
								@endif
							  </select>
						
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						
							<label for="status">No of Epins <font color="red">*</font></label>							
							<input type="text" class="form-control" id="no_of_epins" name="no_of_epins"  maxlength="3" required>
						
					</div>

					
				</div>
				
				
		
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
	<script type="text/javascript">
		
$( document ).ready(function() {
	//$('#productsel').hide();
	
	 $("#epin_type_id").prop('disabled', 'disabled');
	  $("select[name='epin_type']").change(function(){
		  var epin_type = $(this).val();
		 if(epin_type == 'Purchase'){
			// $('#productsel').show();
			 $("#epin_type_id").removeAttr("disabled");
		 }else{
			 //$('#productsel').hide();
			 $("#epin_type_id").prop('disabled', 'disabled');
		 }
		
	  });
  });
</script>

    <!-- END Page Content -->
    @endsection

