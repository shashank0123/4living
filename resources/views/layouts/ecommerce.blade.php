<?php 
use App\MainCategory;
use App\Product;
$products = Product::all();
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Help2Human</title>
  <meta name="robots" content="noindex, follow" />
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="/asset/images/favicon.ico">
    <!-- CSS
     ============================================ -->
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="/asset/css/vendor/bootstrap.min.css">
     <!-- Fontawesome -->
     <link rel="stylesheet" href="/asset/css/vendor/font-awesome.css">
     <!-- Fontawesome Star -->
     <link rel="stylesheet" href="/asset/css/vendor/fontawesome-stars.css">
     <!-- Ion Icon -->
     <link rel="stylesheet" href="/asset/css/vendor/ion-fonts.css">
     <!-- Slick CSS -->
     <link rel="stylesheet" href="/asset/css/plugins/slick.css">
     <!-- Animation -->
     <link rel="stylesheet" href="/asset/css/plugins/animate.css">
     <!-- jQuery Ui -->
     <link rel="stylesheet" href="/asset/css/plugins/jquery-ui.min.css">
     <!-- Lightgallery -->
     <link rel="stylesheet" href="/asset/css/plugins/lightgallery.min.css">
     <!-- Nice Select -->
     <link rel="stylesheet" href="/asset/css/plugins/nice-select.css">
     <!-- Main Style CSS (Please use minify version for better website load performance) -->
     <link rel="stylesheet" href="/asset/css/style.css">
     <!--<link rel="stylesheet" href="/asset/css/style.min.css">-->
     <style>
      body {
        text-align: center !important;
      }
      .header-bottom_area .main-menu_area > nav > ul > li { padding-right: 30px !important; }
      .header-middle_area .hm-searchbox { margin-top: 20px !important; }
      .header-middle_area { padding:  8px 30px !important; }
      #response-show { display: none !important; }
      @media screen and (max-width: 991px) {
        .header-bottom_area .header-logo { padding: 10px 0 0px !important;}     }
        .header-right_area ul li a { padding: 3px 10px 3px 10px !important }
        .fa-star-of-david { color: #ccc !important; }
        .shipping-content h6, .shipping-content p { color: #fff; }
        .container { display: block !important; width: 100% !important }

        
        @media screen and (max-width : 991px)
        {
          #response-hide { display: none; }
          #response-show { display: block !important; }
          #response-news { display: none; }
        }

        @media (min-width: 576px){
          .container { max-width: 100% !important }
        }

      </style>
    </head>

    <body class="template-color-1">
      <div class="main-wrapper">
        <div class="header-middle_area d-none d-lg-block" style="background-color: #fff">
          <div class="container-fluid" style="background-color: orange;margin-top: -10px !important;margin-left: -3%;margin-right: -3% padding: 0 !important; width: 105.5%;">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="header-right_area" >
                <ul>
                  <li>
                    <a href="#" style="color: #fff; font-size: 14px">Help Line:  1800-419-8447</a>
                  </li>
                  <li>
                    <a href="#user-view" class="wishlist-btn toolbar-btn" title="User" data-toggle="tooltip" data-placement="top">
                      <!-- <i class="ion-android-favorite-outline"></i> -->
                      <i class="fa fa-user" style="color: #fff;font-size: 14px"></i> 
                    </a>
                  </li>
                  <li>
                    <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color--white d-lg-none d-block">
                      <i class="ion-navicon" style="color: #fff"></i>
                    </a>
                  </li>
                  <li><a class="hiraola-add_compare toolbar-btn" href="/wishlist" data-toggle="tooltip" data-placement="top" title="Wishlist">
                    <i
                    class="ion-android-favorite-outline" style="color: #fff;font-size: 14px"></i></a>
                  </li>
                  <li>
                    <button onclick="showCart()"> <a href="#miniCart" class="minicart-btn toolbar-btn" data-toggle="tooltip" data-placement="top" title="MiniCart">
                      <i class="ion-bag" style="color: #fff;font-size: 14px" ></i>
                    </a></button>
                  </li>
                </ul>
              </div>
            </div>                    
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <div class="header-logo">
                  <a href="/index">
                    <img src="/images/logo.gif" alt="Hiraola's Header Logo" style="width: 150px; height: 80px; margin-top: 5px">
                  </a>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="hm-form_area">
                  <form action="/search-product" method="POST" class="hm-searchbox">
                    <select class="nice-select select-search-category" name="subcategory">
                      <option value="0">All</option>;
                      @foreach($maincategory as $row)
                      <?php $cat = MainCategory::where('category_id',$row->id)->get();
                      ?>
                      @foreach($cat as $col)
                      <?php $subcat = MainCategory::where('category_id',$col->id)->get();
                      ?>
                      @foreach($subcat as $cols )
                      <option value="{{ $cols->id }}">{{ $cols->Mcategory_name }}</option>                    
                      @endforeach           
                      @endforeach           
                      @endforeach
                    </select>
                    <input type="text" placeholder="Enter your search key ..." name="product_keyword">
                    <button type="submit" class="li-btn" name="submit"><i class="fa fa-search"></i></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-bottom_area header-sticky stick">
          <style type="text/css">
            #boxy {
              max-width: 1322px !important;
            }
            .header-bottom_area .main-menu_area > nav > ul > li > a { padding: 10px 0 !important; }                    
          </style>
          <div class="container" id="boxy">
            <div class="row">
              <div class="col-md-7 col-sm-7 d-lg-none d-block">
                <div class="header-logo">
                  <a href="index">
                    <img src="/images/logo.gif" style="width: 90px; height: 50px" alt="Hiraola's Header Logo">
                  </a>
                </div>
              </div>
              <div class="col-lg-12 d-none d-lg-block position-static" style="font-size: 13px;">
                <div class="main-menu_area">
                  <nav>
                    <ul>                                       
                      <li class="active"><a href="/index">Home</a></li>     
                      @foreach($maincategory as $row)
                      <li class="megamenu-holder"><a href="#">{{$row->Mcategory_name}}</a>
                        <ul class="hm-megamenu">
                          <?php $category=MainCategory::where('category_id',$row->id)->get(); ?>
                          @foreach($category as $col)
                          <li><span class="megamenu-title">{{$col->Mcategory_name}}</span>
                            <?php $subproduct=MainCategory::where('category_id',$col->id)->get(); ?>
                            <ul>
                              @foreach($subproduct as $cols)
                              <li><a href="/product-list/{{$cols->id}}">{{$cols->Mcategory_name}}</a>
                              </li>
                              @endforeach                            
                            </ul>
                          </li>
                          @endforeach                                 
                        </ul>
                      </li>
                      @endforeach
                      <li><a href="/about">About Us</a></li>
                      <li><a href="/contact-us">Contact</a></li>
                      @if (isset($member->id))
                      <li><a href="/en/member">Panel</a></li>
                      <li><a href="/en/logout">Logout</a></li>
                      @else
                      <li><a href="/en/login">Login</a></li>
                      @endif
                    </ul>
                  </nav>
                </div>
              </div>              
              <br>
              <div class="col-lg-12 d-none d-lg-block position-static" id="response-show" style="font-size: 13px;">
                <div class="main-menu_area">
                  <style>
                    .container {
                      display: inline-block;
                      cursor: pointer;
                    }

                    .bar1, .bar2, .bar3 {
                      width: 35px;
                      border-radius: 2px;
                      height: 4px;
                      background-color: #fff;
                      margin: 5px 5px;
                      transition: 0.4s;
                    }

                    /* Rotate first bar */
                    .change .bar1 {
                      -webkit-transform: rotate(-45deg) translate(-9px, 6px) ;
                      transform: rotate(-45deg) translate(-9px, 6px) ;
                    }

                    /* Fade out the second bar */
                    .change .bar2 {
                      opacity: 0;
                    }

                    /* Rotate last bar */
                    .change .bar3 {
                      -webkit-transform: rotate(45deg) translate(-8px, -8px) ;
                      transform: rotate(45deg) translate(-8px, -8px) ;
                    }
                    .response-menu { display: none; }
                    .response-menu ul { padding:10px 5px }
                    .response-menu ul li a{ color: #fff; padding:20px 5px; font-size: 16px;font-weight: bold !important; }
                    .response-menu ul li a:hover { color: #fff !important;  text-decoration: underline !important; }
                    .response-menu ul li { padding-bottom: 10px  }
                    .response-menu ul li .hm-megamenu { display: none; }
                    .response-menu .megamenu-holder:hover  .hm-megamenu { display: block !important; background-color: #fff}
                    #subcat li a { color: orange !important; }
                    .response-menu .megamenu-title  { color: #333; font-size: 16px !important }
                  </style> 

                  <div style="position: absolute; top:0" onclick="myFunction(this)">
                    <br> 
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div><br> 

                    <div class="response-menu" style="text-align: left !important;">

                      <nav>
                        <ul>                                       
                          <li class="active"><a href="/index">Home</a></li>     
                          @foreach($maincategory as $row)
                          <li class="megamenu-holder"><a href="#">{{$row->Mcategory_name}}</a>
                            <ul class="hm-megamenu">
                              <?php $category=MainCategory::where('category_id',$row->id)->get(); ?>
                              @foreach($category as $col)
                              <li><span class="megamenu-title">{{$col->Mcategory_name}}</span>
                                <?php $subproduct=MainCategory::where('category_id',$col->id)->get(); ?>
                                <ul id="subcat">
                                  @foreach($subproduct as $cols)
                                  <li><a href="/product-list/{{$cols->id}}">{{ strtoupper($cols->Mcategory_name)}}</a>
                                  </li>
                                  @endforeach                            
                                </ul>
                              </li>
                              @endforeach                                 
                            </ul>
                          </li>
                          @endforeach
                          <li><a href="/about">About Us</a></li>
                          <li><a href="/contact-us">Contact</a></li>
                          @if (isset($member->id))
                          <li><a href="/en/member">Panel</a></li>
                          <li><a href="/en/logout">Logout</a></li>
                          @else
                          <li><a href="/en/login">Login</a></li>
                          @endif
                        </ul>
                      </nav>
                    </div>   
                  </div>                     
                </div>
              </div> 

              <script>
                function  myFunction(){
                  $('.response-menu').toggle();
                }

              </script>

            </div>
          </div>
        </div>
        <div class="offcanvas-minicart_wrapper" id="miniCart">
          <div class="offcanvas-menu-inner">
            <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
            <div class="minicart-content">
              <div class="minicart-heading">
                <h4>Shopping Cart</h4>
              </div>
              <ul class="minicart-list">
                @if(session()->get('cart') == null)
                <?php echo "No products in the cart"; ?>
                @else
                <?php
                echo "Something in cart";
                ?>
                @endif
                <li class="minicart-product">
                  <a class="product-item_remove" href="javascript:void(0)"><i class="ion-android-close"></i></a>
                  <div class="product-item_img">
                    <img src="/asset/images/product/small-size/2-2.jpg" alt="Hiraola's Product Image">
                  </div>
                  <div class="product-item_content">
                    <a class="product-item_title" href="#">Egg White Protien</a>
                    <span class="product-item_quantity">1 x ₹12500</span>
                  </div>
                </li>
                <li class="minicart-product">
                  <a class="product-item_remove" href="javascript:void(0)"><i class="ion-android-close"></i></a>
                  <div class="product-item_img">
                    <img src="/asset/images/product/small-size/2-3.jpg" alt="Hiraola's Product Image">
                  </div>
                  <div class="product-item_content">
                    <a class="product-item_title" href="#">100% whey Protein Gold</a>
                    <span class="product-item_quantity">1 x ₹12500</span>
                  </div>
                </li>
              </ul>
            </div>
            <div class="minicart-item_total">
              <span>Subtotal</span>
              <span class="ammount">₹37500</span>
            </div>
            <div class="minicart-btn_area">
              <a href="/cart" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Cart</a>
            </div>
            <div class="minicart-btn_area">
              <a href="/checkout" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Checkout</a>
            </div>
          </div>
        </div>            
      </header>

      @if($errors->any())
      <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </div>
      @endif

      @if($message = Session::get('message'))
      <div class="alert alert-primary" style="text-align: center;">
        <p>{{ $message }}</p>
      </div>
      @endif

      @yield('content')



      <!-- footer here -->
      <!-- Begin Hiraola's Shipping Area -->
      <div class="hiraola-shipping_area" style="background-color: #333;; margin-top: 70px">
        <div class="container">
          <div class="shipping-nav">
            <div class="row">
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/1.png" alt="Hiraola's Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>30 DAYS RETURN</h6>
                    <p>money back</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/2.png" alt="Hiraola's Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>FREE SHIPPING</h6>
                    <p>on all orders over 12500</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/3.png" alt="Hiraola's Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>LOWEST PRICE</h6>
                    <p>guarantee</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/4.png" alt="Hiraola's Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>SAFE SHOPPING</h6>
                    <p>guarantee 100%</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="hiraola-footer_area">
        <div class="footer-top_area">
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <div class="footer-widgets_info">
                  <div class="footer-widgets_logo">
                    <a href="/index">
                      <img src="/images/logo.gif" alt="Hiraola's Footer Logo" style="width: 180px; height: 80px">
                    </a>
                  </div>
                  <div class="hiraola-social_link">
                    <ul>
                      <li class="facebook">
                        <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                          <i class="fab fa-facebook"></i>
                        </a>
                      </li>
                      <li class="twitter">
                        <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                          <i class="fab fa-twitter-square"></i>
                        </a>
                      </li>
                      <li class="google-plus">
                        <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                          <i class="fab fa-google-plus"></i>
                        </a>
                      </li>
                      <li class="instagram">
                        <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                          <i class="fab fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-8 col-md-8">
                <div class="footer-widgets_area">
                  <div class="row">
                    <div class="col-md-6 col-lg-6">
                      <div class="footer-widgets_title">
                        <h6>Product</h6>
                      </div>
                      <div class="footer-widgets">
                        <ul>
                          <li><a href="#">Prices drop</a></li>
                          <li><a href="#">New products</a></li>
                          <li><a href="#">Best sales</a></li>
                          <li><a href="/contact-us">Contact us</a></li>
                        </ul>
                      </div>
                    </div>                                   

                    <div class="col-md-6 col-lg-6">
                      <div class="footer-widgets_info">
                        <div class="footer-widgets_title">
                          <h6>About Us</h6>
                        </div>
                        <div class="widgets-essential_stuff">
                          <ul>
                            <li class="hiraola-address"><i class="ion-ios-location"></i><span>Address:</span> M-Biz Trading Private Limited, A-51, 2nd Floor, New Dwarka Road, 
                            Dabri Exnt, New Delhi-110045</li>
                            <li class="hiraola-phone"><i class="ion-ios-telephone"></i><span>Call Us:</span> <a href="tel://18004198447">1800 419 8447</a>
                            </li>
                            <li class="hiraola-email"><i class="ion-android-mail"></i><span>Email:</span> <a href="mailto://info@m-biz.in">info@m-biz.in</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    {{-- <div class="col-lg-4" id="response-news" >
                      <div class="instagram-container footer-widgets_area">
                        <div id="newsresponse"><h4>Thanks for This</div>
                          <div class="footer-widgets_title">
                            <h6>Sign Up For Newsletter</h6>
                          </div>
                          <div class="widget-short_desc">
                            <p>Subscribe to our newsletters now and stay up-to-date with new collections</p>
                          </div>
                          <div class="newsletter-form_wrap">
                            <form method="POST" data-url="{{ route('newsletter')}}">
                              <div id="mc_embed_signup_scroll">
                                <div id="mc-form" class="mc-form subscribe-form">
                                  <input id="mc-email" class="newsletter-input" name="email" type="email" autocomplete="off" placeholder="Enter your email" />
                                  <button type="submit" onclick="submitNewsletter()" name="send" class="newsletter-btn" id="mc-submit">
                                    <i class="ion-android-mail" aria-hidden="true"></i>
                                  </button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div> --}}
                    </div>
                  </div>
                </div>
                <div class="col-md-1 col-lg-1"></div>
              </div>
            </div>
          </div>
          <div class="footer-bottom_area">
            <div class="container">
              <div class="footer-bottom_nav">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="footer-links">
                      <ul>
                        <li><a href="returnpolicy">Return Policy</a></li>
                        <li><a href="privacypolicy">Privacy Policy</a></li>
                        <li><a href="tnc">Terms and Conditions</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="payment">
                      <a href="#">
                        <img src="/asset/images/footer/payment/1.png" alt="Hiraola's Payment Method">
                      </a>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="copyright">
                      <span>Copyright &copy; 2019 <a href="#">m-Biz.</a> All rights reserved.</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <!-- JS
      ============================================ -->
      <!-- jQuery JS -->
      <script src="/asset/js/vendor/jquery-1.12.4.min.js"></script>
      <!-- Modernizer JS -->
      <script src="/asset/js/vendor/modernizr-2.8.3.min.js"></script>
      <!-- Popper JS -->
      <script src="/asset/js/vendor/popper.min.js"></script>
      <!-- Bootstrap JS -->
      <script src="/asset/js/vendor/bootstrap.min.js"></script>
      <!-- Slick Slider JS -->
      <script src="/asset/js/plugins/slick.min.js"></script>
      <!-- Countdown JS -->
      <script src="/asset/js/plugins/countdown.js"></script>
      <!-- Barrating JS -->
      <script src="/asset/js/plugins/jquery.barrating.min.js"></script>
      <!-- Counterup JS -->
      <script src="/asset/js/plugins/jquery.counterup.js"></script>
      <!-- Nice Select JS -->
      <script src="/asset/js/plugins/jquery.nice-select.js"></script>
      <!-- Sticky Sidebar JS -->
      <script src="/asset/js/plugins/jquery.sticky-sidebar.js"></script>
      <!-- Jquery-ui JS -->
      <script src="/asset/js/plugins/jquery-ui.min.js"></script>
      <script src="/asset/js/plugins/jquery.ui.touch-punch.min.js"></script>
      <!-- Lightgallery JS -->
      <script src="/asset/js/plugins/lightgallery.min.js"></script>
      <!-- Scroll Top JS -->
      <script src="/asset/js/plugins/scroll-top.js"></script>
      <!-- Theia Sticky Sidebar JS -->
      <script src="/asset/js/plugins/theia-sticky-sidebar.min.js"></script>
      <!-- Waypoints JS -->
      <script src="/asset/js/plugins/waypoints.min.js"></script>
      <!-- Instafeed JS -->
      <script src="/asset/js/plugins/instafeed.min.js"></script>
      <!-- Instafeed JS -->
      <script src="/asset/js/plugins/jquery.elevateZoom-3.0.8.min.js"></script>

      <!-- Main JS -->
      <script src="/asset/js/main.js"></script>

      <script>
        function showCart(){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

          $.ajax({
            /* the route pointing to the post function */
            url: '/index/cart',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            dataType: 'JSON',
            data: {_token: CSRF_TOKEN, id: id},
            success: function () { 
              alert('hello');
            }
          }); 
        }

        function submitNewsletter(){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

          var request = $.ajax({
            url: '/index/news',
            type: 'POST',
            data: $('#newsletter').serialize(),

            success: function( response ) {

              $('#newsresponse').show();
            }
          }); 

          
        }
      </script>
    </body>
    </html>