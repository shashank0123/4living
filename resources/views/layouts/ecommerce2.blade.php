<?php
if(!empty($member))
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>4LivinG</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('asset/css/vendor/bootstrap.min.css') }}">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{ URL::asset('asset/css/vendor/line-awesome.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/vendor/themify.css') }}">
    <!-- othres CSS -->
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/animate.css') }}asset/css/plugins/owl-carousel.css">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/style.css') }}">
    <style>
   
    </style>
</head>

<body>
 <div class="main-wrapper">
<header class="header-area header-padding-4">
    <div class="main-header-wrap">
        <div class="header-top pdt-5 pdb-15 ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        
                    </div>
                    <div class="col-lg-8">
                        <div class="setting-wrap-2">
                            <div>
                                <span class="tollno1">Contact No.:</span><span class="tollno2"><a href=""> 8950603008</a></span> &nbsp;&nbsp;&nbsp;
                                <span class="tollno1"> Email:</span>
                                <span class="tollno2"><a href="mailto:info@4living.in"> info@4living.in</a></span>
                            </div>
                            <!--<div class="setting-content2-left">-->
                            <!--    <a class="currency-dropdown-active" href="#">Language <i class="la la-angle-down"></i></a>-->
                            <!--    <div class="currency-dropdown">-->
                            <!--        <ul>-->
                            <!--            <li><a href="#">Hindi</a></li>-->
                            <!--            <li><a href="#">English</a></li>-->
                            <!--            <li><am href="#">Marathi</a></li>-->
                            <!--            <li><a href="#">MAithili</a></li>-->
                            <!--        </ul>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle  pdt-5 pdb-15 ">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <div class="logo logosize">
                            <a href="/"><img src="{{ URL::asset('asset/images/logo/logo-2.png') }}" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="header-contact-search-wrap">
                            <!--  <div class="header-contact">
                                <ul>
                                    <li><i class="la la-phone"></i> +00 112 336 555</li>
                                    <li><i class="la la-comments-o"></i> <a href="#">demo@mail.com</a></li>
                                </ul>
                                </div> -->
                            <div class="search-style-3">
                                <form>
                                    <div class="form-search-3">
                                        <input id="search" class="input-text" value="" placeholder="Search Here" type="search">
                                        <button>
                                        <i class="la la-search"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="cart-wrap cart-wrap-3">
                            <!--<button class="">-->
                            <!--<a href="wishlist.html"> <i class="la la-heart-o"></i> <br>-->
                            <!--<span class="count-style-3">01</span></a>-->
                            <!--</button> &nbsp;&nbsp;&nbsp;-->
                            <button class="cart-active">
                                <i class="la la-shopping-cart"></i> <br>
                                <!-- <span class="mini-cart-price-3">$400.00</span> -->
                                <span class="count-style-3">01</span>
                            </button>
                            <div class="shopping-cart-content">
                                <div class="shopping-cart-top">
                                    <h4>Your Cart</h4>
                                    <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                </div>
                                <ul>
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/1.jpg') }}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Tulsi.</a></h4>
                                            <span> ₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/2.jpg') }}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Thyro Sys.</a></h4>
                                            <span> ₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/3.jpg') }}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Artho Sys.</a></h4>
                                            <span> ₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="shopping-cart-bottom">
                                    <div class="shopping-cart-total">
                                        <h4>Subtotal <span class="shop-total"> ₹ 290.00</span></h4>
                                    </div>
                                    <div class="shopping-cart-btn btn-hover default-btn text-center">
                                        <a class="black-color" href="checkout">Continue to Chackout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom sticky-bar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-center">
                        <div class="main-menu menu-common-style menu-lh-4 menu-margin-5 menu-font-2">
                            <nav id="cssmenu">
                                <ul>
                                    <li><a href="/">HOME</a>
                                    </li>
                                    <li><a href="/4living">LIVE 4LivinG </a>
                                        
                                    </li>
                                    <li class="has-sub"><span class="submenu-button"></span>
                                        <a  href="#">KNOW YOUR BODY</a>
                                        <ul class="submenu">
                                            <li><a href="/humanbody">HUMAN BODY INTRO</a></li>
                                            
                                        </ul>
                                    </li>
                                    <li class="has-sub"><span class="submenu-button"></span>
                                        <a href="javascript:void();">PRODUCT</a>
                                         <ul class="submenu">
										 <li><a href="<?php echo  url('/product/ayurveda')?>">AYURVEDA MEDICINES</a></li>
										 <li><a href="<?php echo  url('/product/nutrionals')?>">NUTRIONALS</a></li>
										 <li><a href="<?php echo  url('/product/skin-care')?>">SKIN CARE</a></li>
										 
											
                                        </ul>
                                    </li>
                                    <li class="has-sub"><span class="submenu-button"></span>
                                        <a >COMPANY</a>
                                   
                                        <ul class="submenu">
                                             <li><a href="<?php echo  url('/about-us')?>">ABOUT US </a></li>
                        <li><a href="<?php echo  url('/directors-desk')?>"">DIRECTOR'S DESK </a></li>
                        <li><a href="<?php echo url('/panel-of-doctors')?>">PANEL OF DOCTOR'S </a></li>
                        <li><a href="<?php echo  url('/consult-doctors')?>">CONSULT WITH DOCTOR'S </a></li>
                        <li><a href="<?php echo url('/contact-us')?>">CONTACT US </a></li>

                                            
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo url('/login')?>">LOGIN</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-small-mobile">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <div class="mobile-logo">
                        <a href="/">
                        <img alt="" src="{{ URL::asset('asset/images/logo/logo-1.png') }}">
                        </a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="header-right-wrap">
                        <div class="cart-wrap common-style">
                            <button class="cart-active">
                            <i class="la la-shopping-cart"></i>
                            <!--<span class="count-style">2 Items</span>-->
                            </button>
                            <div class="shopping-cart-content">
                                <div class="shopping-cart-top">
                                    <h4>Your Cart</h4>
                                    <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                </div>
                                <ul>
                                    
                                   
                                    <li class="single-shopping-cart">
                                        <div class="shopping-cart-img">
                                            <a href="#"><img alt="" src="{{ URL::asset('asset/images/product/3.jpg')}}"></a>
                                            <div class="item-close">
                                                <a href="#"><i class="sli sli-close"></i></a>
                                            </div>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="#">Tulsi.</a></h4>
                                            <span>₹ 99.00</span>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="la la-trash"></i></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="shopping-cart-bottom">
                                    <div class="shopping-cart-total">
                                        <h4>Subtotal <span class="shop-total">₹ 290.00</span></h4>
                                    </div>
                                    <div class="shopping-cart-btn btn-hover default-btn text-center">
                                        <a class="black-color" href="checkout">Continue to Chackout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mobile-off-canvas">
                            <a class="mobile-aside-button" href="#"><i class="la la-navicon la-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="mobile-off-canvas-active">
   <a class="mobile-aside-close"><i class="la la-close"></i></a>
   <div class="header-mobile-aside-wrap">
      <div class="mobile-search">
         <form class="search-form" action="#">
            <input type="text" placeholder="Search entire store…">
            <button class="button-search"><i class="la la-search"></i></button>
         </form>
      </div>
      <div class="mobile-menu-wrap">
         <!-- mobile menu start -->
         <div class="mobile-navigation">
            <!-- mobile menu navigation start -->
            <nav>
               <ul class="mobile-menu">
                  <li><a href="/">HOME</a></li>
                   <li><a href="living">LIVE 4LivinG </a>
                  <li class="menu-item-has-children ">
                     <a href="#">KNOW YOUR BODY</a>
                     <ul class="dropdown">
                         <li><a href="/humanbody">HUMAN BODY INTRO</a></li>
                                          
                     </ul>
                  </li>
                  <li class="menu-item-has-children">
                     <a href="#">COMPANY</a>
                     <ul class="dropdown">
                        <li><a href="<?php echo  url('/about-us')?>">ABOUT US </a></li>
                        <li><a href="<?php echo  url('/directors-desk')?>">DIRECTOR'S DESK </a></li>
                        <li><a href="<?php echo url('/panel-of-doctors')?>">PANEL OF DOCTOR'S </a></li>
                        <li><a href="<?php echo  url('/consult-doctors')?>">CONSULT WITH DOCTOR'S </a></li>
                        <li><a href="<?php echo url('/contact-us')?>">CONTACT US </a></li>
                      
                     </ul>
                  </li>
                
                   <li class="menu-item-has-children" >
                    <a href="#">PRODUCT</a>
                    <ul class="dropdown">
                     <li><a href="<?php echo  url('/product/ayurveda')?>">AYURVEDA MEDICINES</a></li>
										 <li><a href="<?php echo  url('/product/nutrionals')?>">NUTRIONALS</a></li>
										 <li><a href="<?php echo  url('/product/skin-care')?>">SKIN CARE</a></li>
                    </ul></li>

                  <li><a href="<?php echo url('/login')?>">LOG IN</a></li>
               </ul>
            </nav>
            <!-- mobile menu navigation end -->
         </div>
         <!-- mobile menu end -->
      </div>
      <div class="mobile-curr-lang-wrap">
         <div class="single-mobile-curr-lang">
            <a class="mobile-language-active" href="#">Language <i class="la la-angle-down"></i></a>
            <div class="lang-curr-dropdown lang-dropdown-active">
               <ul>
                  <li><a href="#">English (US)</a></li>
                  <li><a href="#">English (UK)</a></li>
                  <li><a href="#">Spanish</a></li>
               </ul>
            </div>
         </div>
         
         <div class="single-mobile-curr-lang">
            <a class="mobile-account-active" href="#">My Account <i class="la la-angle-down"></i></a>
            <div class="lang-curr-dropdown account-dropdown-active">
               <ul>
                  <li><a href="<?php echo url('/login')?>">Login</a></li>
                  <li><a href="#">Creat Account</a></li>
                  <li><a href="#">My Account</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="mobile-social-wrap">
         <a class="facebook" href="#"><i class="ti-facebook"></i></a>
         <a class="twitter" href="#"><i class="ti-twitter-alt"></i></a>
         <a class="pinterest" href="#"><i class="ti-pinterest"></i></a>
         <a class="instagram" href="#"><i class="ti-instagram"></i></a>
         <a class="google" href="#"><i class="ti-google"></i></a>
      </div>
   </div>
</div>
<!-- header end here -->
@yield('content')


<!-- footer start -->
         <footer class="footer-area" style="border-top : 3px solid #eee">
            <div class="footer-top pt-30 pb-25" style="background-color: #f5f5f5" >
                <div class="container">
                    <div class="row">
                        <div class="footer-logo">
                             <img src="{{ URL::asset('asset/images/logo/logo-1.png')}}" alt="logo" style="width:200px">
                        </div>
                        
                    </div>
                    <div class="row">
                        
                        <div class="col-lg-3 col-md-3 col-12 col-sm-6">
                            <div class="footer-widget mb-30 footer-mrg-hm1">
                                <div class="footer-title">
                                    <h3>COMPANY</h3>
                                </div>
                                <div class="footer-list">
                                    <ul> 
                                         <li><a href="<?php echo  url('/4living')?>">About 4LivinG</a></li>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="<?php echo  url('/product/ayurveda')?>">Products</a></li>
                                       
                                        <li><a href="">Downloads</a></li>
										<li><a href="<?php echo  url('/privacy-policy')?>">Privacy Policy</a></li>
										<li><a href="<?php echo  url('/terms')?>">Terms & condition</a></li>
										 <li><a href="">News & Events</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-2 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title">
                                    <h3>QUICK LINKS</h3>
                                </div>
                                <div class="footer-list">
                                    <ul>
									 <li><a href="<?php echo  url('/drpremprakash')?>">Dr. Prem Prakash</a></li>
									 
                                         <li><a href="<?php echo  url('/doctors-team')?>">Team 4LivinG</a></li>
                                          <li><a href="<?php echo  url('/consult-doctors')?>">Ask The Doctor</a></li>
										  <li><a href="<?php echo  url('/doctors-team')?>">Testimonials</a></li>
                                           <li><a href="<?php echo  url('/about-ayurveda')?>">About Ayurveda</a></li>
                                       
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title">
                                    <h3>CONNECT</h3>
                                </div>
                                <div class="footer-list">
                                      <ul>
                                        <li><a href="<?php echo url('/franchisee/login')?>">Franchise Login</a></li>
                                        <li><a href="contact-us">Contact Us </a></li>
                                        <li><a href="<?php echo url('/login')?>">IBD Login</a></li>                                          
                                    </ul>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                              <div class="footer-title">
                                    <h3>FOLLOWING 4 LivinG:</h3>
                                </div>
                                <div class="footer-social">
                                  
                                    <ul>
                                        <li><a href="#"><i class=" ti-facebook "></i></a></li>
                                        <li><a href="#"><i class=" ti-twitter-alt "></i></a></li>
                                        <li><a href="#"><i class=" ti-pinterest "></i></a></li>
                                        <li><a href="#"><i class=" ti-vimeo-alt "></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom bg-gray-2 ptb-20">
                <div class="container">
                    <div class="copyright text-center">
                        <p>Copyright © <a href="#">4LivinG</a>. All Right Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header" style="text-align: center !important">
        <h4 class="modal-title" style="text-align: center !important">Terms & Conditions</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        @lang('terms.content')
        <br>
        <div class="custom-control custom-checkbox">
    &nbsp;&nbsp;<input type="checkbox" class="custom-control-input" id="defaultUnchecked">
    <label class="custom-control-label" for="defaultUnchecked">I accept Terms & Conditions</label>
    <p id="showchecked" style="display: none; color: #ff0000"></p>
</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button class="btn btn-danger" onclick="checkTerms()" >Continue</button>
        <button type="button" class="btn btn-danger" id="successTerm" data-dismiss="modal" style="display: none">Close</button>
      </div>

    </div>
  </div>
</div>
    
    
    
    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="{{ URL::asset('asset/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <!-- Modernizer JS -->
    <script src="{{ URL::asset('asset/js/vendor/jquery-3.3.1.min.js') }}"></script>
    <!-- Popper JS -->
    <script src="{{ URL::asset('asset/js/vendor/popper.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ URL::asset('asset/js/vendor/bootstrap.min.js') }}"></script>

    <!-- Slick Slider JS -->
    <script src="{{ URL::asset('asset/js/plugins/countdown.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/counterup.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/images-loaded.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/isotope.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/instafeed.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery-ui.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/jquery-ui-touch-punch.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/magnific-popup.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/owl-carousel.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/scrollup.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/waypoints.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/slick.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/wow.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/textillate.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/elevatezoom.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/sticky-sidebar.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/smoothscroll.js') }}"></script>
    <!-- Main JS -->
    <script src="{{ URL::asset('asset/js/main.js') }}"></script>
    <script>
        
    </script>

@yield('script')
</body>

</html>