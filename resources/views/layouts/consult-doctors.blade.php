<?php
  use App\doctorspanel;
 
  $doctorTeams = DB::table('doctorspanels')
                ->where('panel_type', 'consultwithdoctors')
				->orderBy('rank', 'asc')
  
?>
@extends('layouts/ecommerce2')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>


<div class="container-fluid mt-40 mb-50 ">
    <h2 class="sechead">Consult With  Doctor's </h2>
     <hr class="hrstyle">
     <div class="row lrmargin pagemarginsec" >
        <div class="col-md-12">
      <h4 class="phead">Emergence Of Online Doctoring:</h4>
       <hr class="hrstyle2">
<p>In the 2000s, many people came to treat the internet as a first, or at least a major, source of information and communication. Health advice is now the second-most popular topic, that people search for on the internet. With the advent of broadband and videoconferencing, many individuals have turned to online doctors to receive online consultations and purchase prescription drugs. Use of this technology has many advantages for both the doctor and the patient, including cost savings, convenience, accessibility, and improved privacy and communication. In the US, a 2006 study found that searching for information on prescription or over-the-counter drugs was the fifth most popular search topic, and a 2004 study found that 4% of Americans had purchased prescription medications online. A 2009 survey conducted by Geneva-based Health On the Net Foundation found one-in-ten Europeans buys medicines from websites and one-third claim to use online consultation. In Germany, approximately seven million people buy from mail-order pharmacies, and mail-order sales account for approximately 8–10% of total pharmaceutical sales. In 2008, the Royal Pharmaceutical Society of Great Britain reported that approximately two million people in Great Britain were regularly purchasing pharmaceuticals online (both with a prescription from registered online UK doctors and without prescriptions from other websites). A recent survey commissioned by Pfizer, the Medicines and Healthcare products Regulatory Agency, RPSGB, the Patients Association and HEART UK found that 15% of the British adults asked had bought a prescription-only medicine online. In developed countries, many online doctors prescribe so-called ‘lifestyle drugs’, such as for weight loss, hair loss or erectile dysfunction. The RPSGB has identified the most popular products prescribed online as Prozac (an antidepressant), Viagra (for erectile dysfunction), Valium (a tranquilizer), Ritalin (a psychostimulant), Serostim (a synthetic growth hormone) and Provigil (a psychostimulant). A study in the USA has also shown that antibiotics are commonly available online without prescription.</p>
</div>
<hr>

</div>


<div class="row lrmargin pagemarginsec" >
    <div class="col-md-12">
      <h4 class="phead">Potential Harm:</h4>
       <hr class="hrstyle2">
<p>Traditionalist critics of online doctors argue that an online doctor cannot provide proper examinations or diagnosis either by email or video call. Such consultations, they argue, will always be dangerous, with the potential for serious disease to be missed. There are also concerns that the absence of proximity leads to treatment by unqualified doctors or patients using false information to secure dangerous drugs. Proponents argue there is little difference between an e-mail consultation and the sort of telephone assessment and advice that doctors regularly make out of hours or in circumstances where doctors cannot physically examine a patient. Laurence Buckman, chairman of the British Medical Association’s GPs’ committee, says that online consultations make life easier for doctors and patients when used properly. "Many GPs will be very happy with it and it could be useful. When it’s a regular patient you know well, it follows on from telephone consulting. Voice is essential, vision is desirable. The problem comes when I don’t know the patient". Niall Dickson, chief executive of the General Medical Council, says: "We trust doctors to use their judgment to decide whether they should see a patient in person. Online consultations will be appropriate for some patients, whereas other patients will need a physical examination or may benefit from seeing their doctor in person"..</p>
</div>
<hr>

</div>

     <div class="row lrmargin pagemarginsec" >
        <div class="col-md-12">
      <h4 class="phead">What We Follow:</h4>
       <hr class="hrstyle2">
<p>We follow certain rules and ethics for online consultation. We advise our distributors or people to visit their Doctors first for their medical problems. We think that their Doctors are capable to resolve their issues. In case of any complicated issues to be resolved, we have the expert Panel of Doctors, who will communicate with your Doctor for your medical history which may include pathological reports, X-Ray, Scanning, Ultrasound etc. After receiving it, our Panel of Doctors will advise you with our Ayurvedic Remedies.</p>
</div>
<hr>

</div>
@if (count($doctorTeams) > 0)
<div class="  lrmargin pagemarginsec">
    <div class=" ml-15">
 <h4 class=" phead">Consult With Doctor's</h4>
       <hr class="hrstyle2">
   </div>
</div>
    <div class="row lrmargin pagemarginsec" >
	@foreach ($doctorTeams as $doctors)
	
        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
           
           
<div class="panelcard">
  <img src="assetsss/images/doctorspanel/{{ $doctors->image }}" alt="{{ $doctors->name }}" style="width:100%">
  <h1>{{ $doctors->name }}</h1>
  <p class="title">({{ $doctors->designation }})</p>
  

  <p><button onclick="location.href = 'panel-of-doctors/details/{{ $doctors->id }}';">Contact</button></p>
</div>
        </div>
		 @endforeach
       
        
       
        
    </div>
    
</div>
  @endif
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection