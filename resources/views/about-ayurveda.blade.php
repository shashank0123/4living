@extends('layouts/ecommerce2')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="asset/images/bg/breadcrumb.jpg">
        </div>
<!--   -->

<div class="container-fluid mt-40 mb-50 ">
    <h2 class="sechead">Human Body Introduction </h2>
     <hr class="hrstyle">
    <div class=" lrmargin pagemarginsec ayurvedicsec" >
<p>Ayurvedic medicine also known as Ayurveda. It is one of the world's oldest holistic (whole-body) healing systems. It was developed thousands of years ago in India. It is based on the belief that health and wellness depend on a delicate balance between the mind, body, and spirit. The primary focus of Ayurvedic medicine is to promote good health, rather than fight disease. But treatments may be recommended for specific health problems.</p>
<div>
 <h4 class="phead">Ayurveda Concepts</small></h4>

     <hr class="hrstyle2"></div>


<p>According to Ayurvedic theory, everything in the universe living or not is connected. Good health is achieved when your mind, body, and spirit are in harmony with the universe. A disruption of this harmony can lead to poor health and sickness.<</p>
<p>For followers of Ayurveda, anything that affects your physical, spiritual, or emotional well-being can cause you to be out of balance with the universe. Some things that can cause a disruption include:</p> <ul>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Genetic or Birth defects</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Injuries</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Climate and seasonal changes</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Age</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Emotions</li></ul>
<p>How your body works to keep you healthy and your unique physical and psychological characteristics combine to form your body's constitution, or prakriti. Your prakriti is believed to stay the same for your entire life. However, how you digest food and eliminate waste can influence it. Every person is made of a combination of five basic elements found in the universe:</p>
<ul>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Space</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Air</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Fire</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Water</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Earth</li></ul>
<p>These elements combine in the human body to form three life forces or energies, called Doshas. They control how your body works. The three Doshas are:</p>
<ul>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Vata Dosha (space and air)</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Pitta Dosha (fire and water)</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Kapha Dosha (water and earth)</li></ul>
<p>Everyone inherits a unique mix of the three doshas. One dosha is usually more dominant. Each dosha controls a different body function. It is believed that your chances of getting sick are linked to the balance of your doshas.</p>
<div>
<h4 class="phead">Ayurvedic Medicine </h4>
     <hr class="hrstyle2">
 </div>
 <p></p>
<ol>
    <li>  Ayurveda Concepts</li>
    <li>  Vata Dosha</li>
    <li>  Pitta Dosha</li>
    <li>  Kapha Dosha</li>
    <li>  The Ayurvedic Visit</li>
    <li>  Ayurvedic Treatment</li>
    <li>  Studies on Ayurvedic Medicine</li>
</ol>
<h4 class="phead">Vata Dosha</h4>
 <hr class="hrstyle2">

<p>Vata dosha (space and air) is thought to be the most powerful of all three doshas. It controls very basic body functions, such as how cells divide. It also controls your:</p>
<ul>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>   Mind</li>

    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>   Breathing</li>

    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>   Blood Flow</li>

    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>   Heart Function</li>

     <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>   Ability to get
       rid of body waste through the intestines</li></ul>
<p>Things that can disrupt this dosha are:</p>
<ul>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Eating dry fruit</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Eating too soon after a previous meal</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Fear</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Grief</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span>Staying up too late</li></ul>
<p>If vata dosha is your main life force, you are more likely to develop:</p>
<ul>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Anxiety</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Asthma</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Heart Disease</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Nervous System Disorders</li>
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Rheumatoid</li> Arthritis
       <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"> </span> Skin Problems</li></ul>

<h4 class="phead">Pitta Dosha</h4>
 <hr class="hrstyle2">

<p>The pitta dosha (fire and water) controls:</p>

        <ul><li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Digestion</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Ability to break down foods (metabolism)</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Certain hormones linked to appetite</li></ul>
<p>Things that can disrupt this dosha are:</p>
<ul>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Eating sour foods</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Eating spicy foods</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Fatigue</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Spending too much time in the sun</li></ul>
<p>If pitta dosha is your main life force, you are more likely to develop:</p>
<ul>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Anger and negative emotions</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Crohn's Disease</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Heart disease</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Heartburn a few hours after eating</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>High blood pressure</li>
        <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>Infections</li></ul>

<h4 class="phead">Kapha Dosha</h4>
 <hr class="hrstyle2">
          
<p>The kapha dosha (water and earth) controls:</p>
<ul>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Muscle Drowth</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Body strength and stability</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Weight</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Immune System</li>

</ul>
<p>Things that can disrupt this dosha are:</p>
<ul>
     <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>   Daytime Sleeping</li>
      <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>  Eating after your stomach is full</li>
      <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>  Eating or drinking items that have too much salt or water</li>
      <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>  Eating too many sweet foods</li>
      <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>  Greed</li>

    </ul>
<p>If kapha dosha is your main life force, you are more likely to develop:</p>
<ul>
 <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>       Asthma and other breathing disorders</li>
 <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>       Cancer</li>
 <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>       Diabetes</li>
 <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>       Nausea after eating</li>
 <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>       Obesity</li>
</ul>
<h4 class="phead">The Ayurvedic Visit</h4>
 <hr class="hrstyle2">
<p>There are a few state-approved Ayurvedic schools in the U.S. However, the U.S. has no national standard training or certification program for Ayurvedic practitioners. Some practitioners may have a great deal of training or experience, others may not. Do your homework when choosing an Ayurvedic practitioner. Ask about his or her training and experience. In India, Ayurvedic training can take five or more years. Graduates receive either a Bachelor of Ayurvedic Medicine and Surgery (BAMS) or Doctor of Ayurvedic Medicine and Surgery (DEMS) degree. At your first visit, the practitioner will examine you and try to determine your primary dosha and the balance among the others. The exam will include:</p>
<ul>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Checking your weight</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Examination of urine and stools</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Feeling your pulse (each dosha theoretically creates a unique pulse)</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Listening to your speech and voice</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Looking at your eyes, teeth, tongue, and skin
</li>

    </ul>
<p>You will be asked questions about your:</p>
<ul>
     <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>   Ability to recover from an illness</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Behaviors</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Diet</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Lifestyle</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Medical history, including recent illnesses
</li>
</ul>
<h4 class="phead">Ayurvedic Treatment</h4>
 <hr class="hrstyle2">
<p>Treatment depends on your unique prakriti, your primary dosha, and the balance between all three of them. A main goal of Ayurvedic medicine is to cleanse your body of undigested food called ama, which can stick to the inside of your body and make you sick. This cleansing process is called panchakarma. It is used to reduce any symptoms and reestablish harmony and balance. Panchakarma may include:</p>
<ul>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Blood purification (either by removing blood from the body or with special teas)</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Massage</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Medical oils given through the nose</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Methods to make you vomit</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>  Use of enemas, laxatives, or purgatives to cleanse your intestines</li>

  </ul>
<p>Other treatments may also be recommended to:</p>
<ul>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Restore balance</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Improve spiritual healing</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Boost your immunity</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Reduce symptoms</li>

    </ul>
<p>The treatments may include:</p>
<ul>
     <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>   Aromatherapy</li>
     <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>   Breathing exercises</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Diet changes</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Herbs, vitamins, minerals, and metals</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Plant-based oils and spices</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Lifestyle changes</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Meditation</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Stretching</li>
    <li><span class="arrowicon"><img src="asset/images/logo/bullet.jpg"></span>    Yoga</li>

    </ul>
	<h4 class="phead">Studies on Ayurvedic Medicine</h4>
 <hr class="hrstyle2">
 <p>Some research has shown that meditation works very well in relieving stress and reducing the risk for heart disease risk factors. Other studies are looking into the ability of Ayurvedic herbs to treat cancer. Several Ayurvedic herbal treatments have been studied for a variety of medical conditions. An example is ashwagandha (Withania somnifera) that has anti-depressant, anti-anxiety, and, possibly, anti-cancer effects. Recently, scientists have reported that Ayurveda may be a valuable tool in managing obesity and diabetes.</p>
 
    </div>
    
</div>
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection