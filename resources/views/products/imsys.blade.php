@extends('layouts.ecommerce2')


@section('content')
<!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>
<!--   -->

<div class="container-fluid mt-40 mb-50">
    <h2 class="sechead">Ayurvedic Medicines: IMSYS </h2>
     <hr class="hrstyle">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            
               <div class="img-hover-zoom img-hover-zoom--xyz pdsec1_1">
  <img src="../asset/images/product/1.jpg" alt="Another Image zoom-on-hover effect" width="300" height="350">
</div>
           
        </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="pdsec1_2">
               <h4 class="phead"> IMSYS</h4>
               <hr class="hrstyle2">
			   <h4 class="phead"> MRP:  Rs. 1800/</h4><br>
               <ul class="pddtls">
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Super Food cum Super Power Medicine.</span></li>
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Improves Mental & Physical Health.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Proven Ability to Support overall System of Body.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Beneficial for all Age groups from Infants to old age.</span></li>
                     
                    

               </ul>
            </div>
        </div>
        
    </div>

      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Introduction</h4>
               <hr class="hrstyle2">
               <p>In the past few years, healthcare practitioners have been hearing a great deal about bovine colostrum, a relatively a new superpower medicine or health supplement intended to optimize the immune systems of both healthy and chronically ill individuals. Testimonials, anecdotal reports, as well as the marketing efforts of several supplement manufacturers and distributors have generated much of the excitement about colostrum. The past 20 years has also witnessed the publication of over 2,000 research papers that strongly support both colostrum and its numerous components. For me, more than 1 billion satisfied patients’ results of IMSYS define the ultimate story about colostrums. The purpose of this paper is to review the scientific evidence for the clinical application of a promising immune system modulator. Colostrum, Life's First Food, for me is that bovine colostrum "rebuilds the immune system, destroys viruses, bacteria." ", and fungi, accelerates healing of all body tissue, helps lose weight, burn fat, increase bone and lean muscle mass, and slows down and even reverses aging," According to Clark and well-known naturopathic physician Dr. Bernard Jensen", colostrum plays a therapeutic role in AIDS, cancer, heart disease, diabetes, autoimmune diseases, allergies, herpes"', bacterial", viral, and parasitic' infections, gingivitis, colds, the flu, and much more. Colostrum has antioxidant and anti-inflammatory properties, and provides many vitamins, minerals, enzymes,and amino acids.</p>
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Colostrum Rediscovered</h4>
               <hr class="hrstyle2">
               <p>Historically, Ayurvedic physicians in India have used bovine colostrum therapeutically for thousands of years. Masai tribesmen in Kenya drink bovine colostrum by the liter because they know how good it is for them. They are well-known for their toughness under extreme conditions and their healthy constitutions.</p>
			    <p style="text-align:center"><img src="../asset/images/Shloka.jpg"></p>
				 <p>In the United States and throughout the world, conventional doctors used it for antibiotic purposes prior to the introduction of sulfa drugs and penicillin. In the early 1950s, colostrum was prescribed extensively for the treatment of rheumatoid arthritis. In 1950, Dr. Albert Sabin, the polio vaccine developer, discovered that colostrum contained antibodies against polio and recommended it for children susceptible to catching polio.

</p>
<p><b>Bovine colostrum is biologically transfer-able to all mammals, including man. It is much higher in immune factors than human mother's colostrum.</b></p>
            
        </div>
      </div>
	  
	 <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> What Is Colostrum?</h4>
               <hr class="hrstyle2">
               <p>Colostrum is the mammary secretion a mammal provides its newborn within the first 24 to 48 hours. It contains numerous immune system and growth factors as well as essential nutrients, trypsin, and protease inhibitors that protect it from destruction in the gastrointestional (GI) tract. It is estimated that colostrum triggers at least 50 processes in the newborn. Bovine colostrum is biologically transferable to all mammals, including man. It is much higher in immune factors than human mother's colostrum. Laboratory analyses of immune and growth factors from bovine colostrum are identical to those found in human colostrum, except that the levels of these factors are significantly higher in the bovine version. For example, human colostrum contains 2% of IgG (the most important of the immuno-globulins found in the body), while bovine colostrum contains 86% of IgG. In addition, bovine colostrum contains a blocking hormone to prevent the calf from becoming sensitized to its own mother's immune factors. Studies indicate that all species, including man, benefit from the immune-boosting properties of bovine colostrum, with no reports of allergic or anaphylactic reactions to date. This compound is in a very limited supply because colostrum is only available for a day or two after calving. The needs of the newborn calf must be met first, and only high-quality colostrum is taken from cows that have been certified free of antibiotics, pesticides, and synthetic hormones. colostrum must be processed at low temperatures so the immune and growth factors remain biologically viable. Research has shown that the molecular structure of bovine colostrum is identical to human colostrum. The immune and growth factors found in bovine colostrum are beneficial to humans of all ages. In other words, it contains all of the immune and growth factors found in all other sources and thus it can significantly benefit all other mammals, including humans of course.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> History</h4>
               <hr class="hrstyle2">
               <p>Around the turn of century it was noted that antibody levels in the first milk after birth (colostrum) were much higher than in succeeding milks. At that time it was known that these antibodies had certain properties that evoked protection. Spolverini (1920) advocated that cow’s colostrum be utilized as an infant food to protect the infant against shared human and bovine diseases. Prior to the development of sulpha drugs and antibiotics, colostrum was majorly used worldwide by leading medical practitioners for its antibiotic properties. In fact Albert Sabin, the physician credited with developing the first polio vaccine advocated the use of colostrum and in fact originally isolated anti-polio antibodies from bovine colostrum. Campbell and Peterson (1963) were the first to develop a program whereby cows were immunized with a mixture of attenuated pathogens prior to birth of the calf. The colostrum fraction collected from these animals is referred to as immune colostrum. The prophylactic and therapeutic use of immune milks has been shown to be successful in preventing and treating enteropathogenic Escherichia coli infections, rotavirus gastroenteritis in infants, Cryptococcidiosis and diarrhoea in AIDS and other immunodeficient patients, dental caries formation, and other conditions. In all these instances colostrum containing immunoglobulins has been obtained from cows that have been hyperimmunised with specific pathogens. Kummer (1992) was the first to show that colostrum from non-immunised cows can prevent gastrointestinal disease in infants. McConnell (1998) and her colleagues at Otago University, New Zealand and New Zealand Dairy Group successfully showed that it was possible to produce a colostrum from pasture fed non-hyperimmunised cows which exhibited a greater antibody titre then that of a hyperimmunised equivalent.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Learn about the many Benefits of Colostrum</h4>
               <hr class="hrstyle2">
               <p>Colostrum is Naures perfect food! It contains all the natural nutrients you need for optimal health and vitality. It is the only 100% natural source of vital growth and healing factors and cannot be laboratory reproduced. The following are some of the many benefits of using Colostrum:</p>
			    <p>
				<ul>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Strengthens the immune system</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Fights viruses, bacteria and parasites</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Helps with better digestion and bowel function</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Rejuvinates body and mind</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Assists athletes build lean muscle and helps with recovery</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Assists athletes build lean muscle and helps with recovery</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Enhances energy and general wellbeing
</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Helps relieve chronic pain
</li>
  <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Combats the effects of aging joints

</li>
  <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Helps with disease prevention

</li>

               </ul>
				</p>
            
        </div>
      </div>
	  
	   <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Sport and Fitness</h4>
               <hr class="hrstyle2">
               <p>Colostrum is increasingly being used in bodybuilding, sports and fitness regimes. It has been shown to improve sprint capacity in runners, cyclists and other athletes who require speed and stamina. Anyone involved in sports or any kind of exercise program will benefit from Colostrum.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Helps with Nutrient Uptake</h4>
               <hr class="hrstyle2">
               <p>Colostrum enhances assimilation of nutrients through the intestines and thus increases the efficiency of carbohydrate and amino acid uptake. Growth factors in Colostrums help “seal” the lining of the small intestine and provide protection from invading organisms and from ulceration, so more of the nutrients from the food you eat can be utilized as fuel for exercise, whether of the cardiovascular or muscle-building variety. This partially accounts for the increased levels of energy users of Colostrums report.Colostrum has dozens of health related natural components of which 89 are so found & studied till date, the primary being immune and growth related. Colostrum is made up of various macro and micro constituents such as cytokines, immunoglobulins, lactoferrin, growth factors and hormones, etc. These components have various biological roles that are vital to proper health and immune function.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> How it works?</h4>
               <hr class="hrstyle2">
               <p>The main function in the new-born is in the form of transfer of passive immunity from the mother to the child. The mother has been exposed to a variety of environmental factors and organisms to which her immune system has produced antibodies during her lifetime. In transfer of passive immunity the mother passes on her complement of antibodies to these various factors onto her offspring. In humans and apes the mother passively immunises her young in utero by passage of antibodies through the placenta. In animals where maternal antibodies do not pass the placental barrier (horses, cattle, pigs, and sheep), the young are passively immunised immediately after birth by way of Colostrum. In these species the maternal antibodies present in the Colostrum are absorbed directly through the gut in the first few days following birth.Bovine Colostrum is the only form of Colostrum that is not species specific. In other words, it contains all of the immune and growth factors found in all other sources and thus it can significantly benefit all other mammals, including humans of course.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead">Major Colostrum Components</h4>
               <hr class="hrstyle2">
               <p>The most important components of colostrum can basically be broken down into two major categories: immune system factors and growth factors. Drug manufacturers have tried to copy (genetically engineer) and market several of the individual components of colostrum, most notably interferon, gamma globulin, growth hormone, IgF-1, and protease inhibitors. Biotechnology companies are currently selling IgF-1 for as much as $800 per 50 cc vial, Some of the following colostrum components may very well be next on the list of "major breakthroughs" by the pharmaceutical/nutraceutical industry: Immunoglobulins (A, D, E, G, and M) are the most abundant of the immune factors found in colostrum. IgG neutralizes toxins and microbes in the lymph and circulatory system. IgM destroys bacteria, while IgE and IgD are highly antiviral.
Lactoferrin is an antiviral, antibacterial, anti-inflammatory, iron-binding protein with therapeutic effects in cancer, HIV, cytomegalo-virus, herpes", chronic fatigue syndrome, Candida albicans; and other infections. Lactoferrin helps deprive bacteria of the iron they require to reproduce, and it also releases iron into the red blood cells, enhancing oxygenation of tissues. Lactoferrin modulates cytokine release, and its receptors have been found on most immune cells, including lymphocytes, monocytcs, macrophages, and platelets.<br>Proline-rich polypeptide (PRP) is a hormone that regulates the thymus gland, stimulating an underactive immune system. It also helps down-regulate an overactive immune,system, as seen in autoimmune diseases such as multiple sclerosis (MS), rheumatoid arthritis, lupus, scleroderma, chronic fatigue syndrome, and allergies. Growth Factors include epithelial growth factor (EgF), insulin-like growth factor-I and II (IGF-1 and IGF-II), fibroblast growth tactor (FgF), platelet-derived growth factor (PDGF), transforming growth factors A & B (TgA and B), and growth hormones (GH).<br>These all help stimulate cell and tissue growth by stimulating DNA formation." Genetically engineered versions of IGF-1 and GH are now marketed as anti-aging and AIDS drugs- with very high pricing, They are found naturally and in high concentrations in colostrums (IMSYS). Several studies show that these growth factors are capable of increasing T-cell production, accelerating healing, balancing blood glucose levels, reducing insulin need, increasing muscle and bone growth and repair, and metabolizing fat for fuel.<br>A 1990 study in The New Enqland Journal of Medicine concluded that GH treatment prevented some of the signs of aging. In his study, Dr. Daniel Rudman treated 26 men between the ages of 61 and 80 with GH. Patients experienced a decrease in overall body fat (up to 14%) and an increase in bone density and lean muscle mass. In addition, their skin was thicker and more elastic. Rudman said the changes were equivalent to those incurred over a 10- to 20-year period of aging. However, Rudman administered GH via injection. There's no evidence that ingesting it through the GI tract would offer similar benefits.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead">Clinical Applications</h4>
               <hr class="hrstyle2">
               <p>For symptomatic adults, clinicians usually prescribe 1,000 to 2,000 my twice daily of the dried, encapsulated form of colostrum, best taken on an empty stomach with eight to 12 ounces of water. Preventive doses have not been established, but I recommend continuous dosing at levels the consumer/patient has decided upon. For those who show no clinical response to colostrum, the dosage can safely be doubled or even tripled as needed until the desired results are obtained. Children can also take colostrum, but require proportioniately less. Mild reactions can occur in up to 5% of the cases, but are usually mild and disappear with intake of plenty of water & continued supplementation at the same dosage level.Through hundreds of years of use and over 2,000 clinical studies, colostrum has been demonstrated to be completely safe, without drug interactions or side effects at any level of ingestion. The following clinical conditions have been well-documented to respond favorably to colostrum supplementation:

</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead">Viral Illnesses</h4>
               <hr class="hrstyle2">
               <p>The GI component of the immune system produces about 75% of the antibodies in the human system, The ability of AIDS/HIV patients to fight infectious disease is severelv compromised, partially due to damage to the gut from chronic inflammation and diarrhea. Several recent studies report colostrum's role in the reversal of this chronic problem, stemming from opportunistic infections like Candida albicans, cryptosporidia, rotavirus, herpes simplex, pathogenic strains of E.coli, and intestinal flu infections. Colostrum handles all gut pathogens well without side effects. Colostrum is composed of numerous factors with strong antiviral activity, especially the immuno-globulins, lactoferrin, and the cytokines.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Sport and Fitness</h4>
               <hr class="hrstyle2">
               <p>Colostrum is increasingly being used in bodybuilding, sports and fitness regimes. It has been shown to improve sprint capacity in runners, cyclists and other athletes who require speed and stamina. Anyone involved in sports or any kind of exercise program will benefit from Colostrum.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead">Allergies and Autoimmune Diseases</h4>
               <hr class="hrstyle2">
               <p>PRP from colostrum can work as a regulatory substance of the thymus gland." It has been demonstrated to improve or eliminate symptomatology of both allergies and autoimmune diseases (MS, rheumatoid arthritis, lupus, myasthenia gravis). PRP inhibits the overproduction of lymphocytes and T-cells and reduces the major symptoms of allergies and autoimmune disease; pain, swelling and inflammation</p>
			   <p><b>Colostrum lactalbumin has been found to be able to cause the selective death(apoptosis) of cancer cells, leaving the surrounding non-cancerous tissues unaffected.'"</b></p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead">Heart Disease</h4>
               <hr class="hrstyle2">
               <p>Altered immunity may be the hidden cause of atherosclerosis and cardiovascular disease, For example, a type of chlamydia has been associated with arterial plaque formation in over 79% of patients with heart disease. A recent New England Journal of Medicine article indicatcd that heart disease is partially the result of immune sensitization to cardiac antigens. Immune-system-mediated injury results in myocarditis, with lymphocytes and macrophages being the predominant infiltrating cells. Colustrum PRP may have a role in reversing heart disease very much like it does with allergies and autoimmune diseases.Additionally, IgF-1 and GH in colostrum can lower LDL cholesterol while increasing HDL cholesterol concentrations. Colostrum growth factors promote the repair and regeneration of heart muscle and the regeneration of new blood vessels for collateral coronary circulation.</p>
			    
            
        </div>
      </div>
	  
	    <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead">Cancer</h4>
               <hr class="hrstyle2">
               <p>The 1985 Steven Rosenberg book, Quiet Strides in the War on Cancer, first popularized the benefits of cytokines in the treatment of cancer. Since that time, the same cytokines found in colostrum (interleukins 1, 6, 10, interferon G, and lymphokines) have been the single most researched protocols in scientific research for the cure for cancer. Colostrum lactalbumin has been found to be able to cause the selective death (apoptosis) of cancer cells, leaving the surrounding ¯noncancerous tissues unaffected. Lactoferrin has similarly been reported to possess anti-cancer activity.The mix of immune and growth factors in colostrum can inhibit the spread of cancer cells. If viruses are involved in either the initiation or the spread of cancer, colostrum could prove to be one of the best ways to prevent the disease in the first place.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Diabetes</h4>
               <hr class="hrstyle2">
               <p>Juvenile diabetes (Type I, insulin dependent) is thought to result from an autoimmune mechanism. Colostrum contains several factors that can offset this and other allergies. Colostrum IgE-1 can bind to both the insulin and IgF-1 receptors found on all cells. Human trials in 1990 reported that IgF-1 stimulates glucose utilization, effectively treating acute hypoglycemia and lessening a Type II diabetic's dependence on insulin.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Weight-loss Programs</h4>
               <hr class="hrstyle2">
               <p>The body requires IgF-1 to metabolize fat for energy through the Krebs cycle, With aging, less IgF-1 is produced in the body. Inadequate levels are associated with an increased incidence of Type II diabetes and difficulty in losing weight despite a proper nutritional intake and adequate exercise, Colostrum provides a good source of IgF-1 as a complementary therapy for successful weight loss.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Athletic Stress</h4>
               <hr class="hrstyle2">
               <p>Exhaustive workouts and athletic competition can temporarily depress the immune system, decreasing the number of I-lymphocytes and NK cells, Athletes are therefore more prone to developing infections, including chronic fatigue syndrome. Many of colostrum's immune factors can help significantly reduce the number and severity of infections caused by both physical and emotional stress.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Sport</h4>
               <hr class="hrstyle2">
               <p>Colostrum</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Leaky Gut Syndrome</h4>
               <hr class="hrstyle2">
               <p>One of the major benefits of colostrum supplementation is enhanced gut efficiency, due to the many immune enhancers that control clinical and subclinical GI infections. Colostral growth factors also play a role by keeping the intestinal mucosa sealed and impermeable to toxins. This is evidenced by colostrum's ability to control chronic diarrhea caused by gut inflammation related to dysbiosis.
Healing leaky gut syndrome reduces toxic load and helps reverse many allergic and autoimmune conditions. For the healthy individual or athlete in training, colostrum supplementation enhances the efficiency of the intestine's uptake of amino acid and carbohydrate fuel, More nutrients are made available for muscle cells and other vital tissues and organs. One of the reasons for the energy boost seen in most healthy individuals who use supplemental colostrum, is its ability to improve nutrient availability and correct subclinical leaky gut syndrome</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Wound Healing</h4>
               <hr class="hrstyle2">
               <p>Several colostrum components stimulate wound healing." Nucleotides, EgF, TgF, and IgF-1 stimulate skin growth md cellular growth and repair by direct action on DNA and RVA. These growth factors facilitate the healing of tissues damaged by ulcers, trauma, burns, surgery, or inflammatory disease, Colostrum's wound-healing properties specifically benefit the skin, muscle, cartilage, bone, and reserve cells. Powdered colostrum can be applied topically to gingivitis, sensitive teeth, aphthous ulcers, cuts, abrasions, and burns after they have been cleaned once disinfected.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Alzheimer’s Disease and Cognitive Disorders</h4>
               <hr class="hrstyle2">
               <p>As the number of senior citizens increases with improved health care and longer life expectancy, the effects of Alzheimer’s disease will continue to take a toll on our health system and our senior citizens. Colostrum contains components which have been shown to benefit those suffering from this debilitating condition of 15 Alzheimer’s patients receiving PRP in a clinical trial, 8 showed clinical improvement and the rest stabilized, whereas of those receiving selenium or a placebo, none showed improvement.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Antioxidants</h4>
               <hr class="hrstyle2">
               <p>Are antioxidants the fountain of youth? That question remains debatable, but it is known that oxidative stress and high levels of reactive oxygen species (ROS), by-products of normal metabolism which build up in the body, can cause widespread damage and increase the aging process. Colostrum contains antioxidants and the precursors to glutathione (the most powerful antioxidant known) which help clean up these pollutants from the body and reverse the damage they do to the body.</p>
			    <p><ul>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Oxidation by-products of metabolism cause extensive damage to DNA, proteins and lipids, which ultimately results in aging, cancer and other degenerative diseases.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Chronic oxidative stress can accelerate cell senescence in human endothelial cells.</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Colostrum is rich in antioxidants, including glutathione, the most powerful antioxidant known, and its precursors.
</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Colostrum reduces ferricytochrome C and suppresses myeloperoxidase and lysozyme activity in human polymorphonuclear leukocytes (PMN), activities which are important in the mediation of acute inflammation.
</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>PRP turns down lipid peroxidation, reducing the amount of reactive oxygen species (ROS) in cells.
</li>
    <li><span class="arrowicon"><img src="../asset/images/logo/bullet.jpg"></span>Lactoferrin acts as an antioxidant in the gut.
</li>
   
               </ul></p>
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> Quality Control</h4>
               <hr class="hrstyle2">
               <p>The best quality colostrum is produced organically and is free of pesticides, herbicides, anabolic hormones like rBST, steroids, antibiotics, and other chemicals. Not all colostrum products in market are biologically active. This is because of improper processing through the use of high temperatures and pasteurization or the formation of colostrum into capsules. This method requires high pressure and generates heat, destroying biological activity. Colostrum in liquid form is also less than ideal. It is not as concentrated as the powdered versions of the product, must be kept refrigerated due to its short shelf life, and preservatives must be added that further dilute and destroy its biological capabilities.

</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h3 class="phead"> Answers to the Most Commonly Asked Questions about Cow Colostrum</h3><br>
			
			<h4 class="phead">Why Cow Colostrum?</h4>
			
               <hr class="hrstyle2">
               <p>Scientific research, conducted in the last decade in major medical research centers and universities, throughout the world, has shown that the molecular combinations of the immune and growth factors in cow's colostrum are virtually identical to human. Research has also shown that only bovine colostrum is not species specific and can work in human and lower mammals.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">What is the purpose of Colostrum in nature?</h4>
			
               <hr class="hrstyle2">
               <p>In newborn mammals, the primary function for colostrum is the transfer of passive immunity from the mother to the child. During her lifetime, the mother has been exposed to a variety of environmental factors and organisms to which her immune system has produced antibodies. In transfer of passive immunity, the mother passes on her complement of antibodies to these various factors to her offspring. Humans and primates differ from other mammals in the location and timing of immune transfer. A pregnant woman or female primate passively immunizes her young in utero when her antibodies cross the placental barrier. This transfer is only about half of what can ultimately be transferred. Further immunity and growth factors transfer during breastfeeding from her colostrum and milk. For all other mammals whose antibodies do not pass through the placental barrier, the young are passively immunized immediately after birth with colostrum. The maternal antibodies present in the colostrum are directly absorbed through the gut lining in the first few days after birth. What also separates humans and primates from other mammals is that without colostrum, non primate mammals will die within 24 hours. These mammals are born without any immune system, and as they pass through the birth canal, they are immediately exposed to pathogens. The physical trauma of birth causes bruising, inflammation and pain for the animal. Colostrum helps attenuate all of these insults. It stimulates bone and tissue growth so that offspring can quickly recover and survive their hostile environments, perhaps running or swimming to avoid predators within a couple of days.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Why is Colostrum Significant?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum is the first food that is available to the newborn. It is not only highly nutritious but also contains substances (immunoglobulins and immune factors) that help to stimulate and augment the newborn’s immune system, thus helping to protect the vulnerable newborn from its new and potentially harmful environment. Furthermore, it contains substances (growth factors and cytokines) which act to stimulate the development, maturation and proliferation of various tissues and organs. Human colostrum is not as powerful as other mammalian colostrum, and the amount of colostrum decreases over time. Human development is much slower than other species, so unlike a baby gazelle, for example, a human infant does not “hit the ground running” right after birth. This necessitates mothers breastfeed their babies for at least 2 years (and perhaps as long as 4 years) in order to provide optimal growth, development, and protection against disease-causing pathogens. The significance of colostrum is best illustrated in farm animals, such as the horse, cow, goat, sheep etc. In these animals passive transmission of immunity occurs after birth in the form of colostrum. If these animals do not receive colostrum in the first 12 - 24 hours following birth they have a very good chance of not surviving (mortality rates of up to 95% have been reported). Human babies won’t die without colostrum, but they’ll certainly have more developmental problems. Limited breastfeeding or substituting formula for breast milk increases the incidence of respiratory, ear and gastrointestinal infections, lower I.Q., allergies, autoimmune conditions including diabetes, autism, ADD, etc. Colostrum also has a significant role in the first few days of an infant’s life. Babies are born with holes in their stomachs and small intestines, a natural condition known as intestinal permeability, or “leaky gut”. This allows for the immunoglobulins to easily pass through into the bloodstream. Mom’s colostrum closes these holes after a couple days of breastfeeding. If babies are not continuously breastfed for the first 2 years, the holes re-open and Leaky Gut Syndrome develops. Even if babies receive adequate breastfeeding, specific lifestyle factors and medications can cause Leaky Gut Syndrome (LGS). Leaky Gut Syndrome is a primary cause of autoimmune diseases including diabetes, autism, asthma, allergies and in infants an increase in stomach and digestive issues, sudden infant death and infections of all types, including ear infections.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">How does Colostrum differ from plain milk?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum is not milk. It is a concentration of immune and growth factors, vitamins and minerals essential amino acids designed by nature to transfer immunity, heal and prevent Leaky Gut Syndrome, initiate and sustain growth and calm and eliminate pain associated with the birth process. It has a high concentration of immunoglobulins, lactoferrin, PRPs and all of the vital growth factors. These substances are also present in milk but at negligible levels. Furthermore, colostrum has a much higher protein, vitamin, mineral content, and is lower in lactose.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">What are Anitibodies?</h4>
			
               <hr class="hrstyle2">
               <p>Antibodies are very specialized molecules that are produced by the body's immune system.They are produced in response to the host being exposed to an immunogenic or foreign substance (antigen) such as an infectious microbe. Their action is to destroy and prevent the colonization and/or neutralize disease-causing microbes. A very important feature of antibodies is that they are directed specifically to the pathogen that induced their formation.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
What are growth factors?</h4>
			
               <hr class="hrstyle2">
               <p>Growth factors are very small bio-active molecules which promote growth and maturation of various cell types and tissues. They are found in very high concentrations in colostrum. They not only stimulate normal growth and development but help regenerate and accelerate the repair of aged or injured muscle, skin, bone, cartilage and nerve tissues. Growth factors also stimulate the body to burn fat for fuel instead of muscle tissue in times of fasting or dieting. They help build lean muscle and have been shown to have a positive effect on athletic performance.In addition, transforming growth factors initiate and ensure bone health and density, and help destroy cancer cells in the body. Growth factors also ensure the growth and development of immune cells such as macrophages, NK (natural killer) cells and T cells to prevent infections of all types. It also provides the endothelial growth hormones that help maintain capillary growth and development to insure the distribution of healing and nutritional components all the way down to the cellular level and to maintain flexibility of our arterial walls to help maintain healthy blood pressure and flow to the extremities</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Why do i need Colostrum as an adult?</h4>
			
               <hr class="hrstyle2">
               <p>Research has shown that once we pass puberty, our bodies gradually produce less of the immune and growth factors that help our bodies fight off disease and heal damaged body tissue. With the loss of these vital components, we age and die. Colostrum is the only source of these life giving components ...the actual immune factors and all of our body's growth factors (hormones) in perfect combination as nature intended (synthesized and isolated hormones throw the other hormones out of balance and have been shown to have significant side effects). Research has shown that colostrum has the demonstrated ability to kill bacteria and viral invaders, stimulate tissue repair (particularly the bowel lining), stimulate fat utilization for fuel and optimize cellular reproduction (anti-aging). No other substance on the planet can provide all of these marvelous benefits.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Why can't i just get raw colostrum from the local dairy farmer?</h4>
			
               <hr class="hrstyle2">
               <p>You can, but there are limitations. First, it needs to be refrigerated and consumed before it spoils. Second, raw colostrum is not pasteurized and contains immune factors from just one cow. This differs from processed colostrum, which is pasteurized for safety and concentrated to provide a broad base of immune factors that have been pooled from hundreds and sometimes thousands of cows. There are very few producers of colostrum in the world that have the ability to accomplish this.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Should i take Colostrum in tablet or capsule form?</h4>
			
               <hr class="hrstyle2">
               <p>It takes tremendous pressure (which creates high temperatures) to form a tablet. When you apply this heat to Colostrum, you destroy the biological activity of colostrum's immune and growth factors. Some manufacturers choose this route either because they lack knowledge about Colostrum or because it is so much cheaper. So only capsuled colostrum (IMSYS) is recommended. The goal is to have colostrum reach the small intestine intact where it does its best work. It is very important to take colostrum capsules on an empty stomach with lots of water to aid its transit into the upper intestine. </p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
How safe is colostrum?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum is a food supplement & superpower medicine as well; and can be consumed by most people without any side effects. Colostrum has no known drug interactions. People with milk protein allergies should be cautious when using colostrum. In actuality, only about 1 in 1,000,000 people are truly allergic to the casein in milk. What people call “milk allergy” is the crossover of partially digested milk proteins into the bloodstream as a result of Leaky Gut Syndrome. There is no known toxicity level with colostrum.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">How does Colostrum interact with other supplements or prescription medications?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum heals the bowel. Simply, this means you are better utilizing the nutrients from the food you eat as well as anything else you put in your body.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
How much should i take and when?</h4>
			
               <hr class="hrstyle2">
               <p>The amount of colostrum you take varies from person to person. For healthy individuals who want to maintain a healthy immune system, for each 20 kg weight IMSYS 1 capsule daily is recommended. For anyone who has compromised health situation, GI or digestive health issues, auto-immune conditions, inflammatory conditions, is recovering from an injury, wants to get rid of allergies, or detects a cold or flu coming on, 1 capsule IMSYS for each 10 kg weight is recommended. For severe heath conditions 1 capsule IMSYS for each 5 kg weight is recommended.Some people may experience a healing incident as the body releases toxins (digestive problems, skin eruptions, rashes). These symptoms usually disappear in a couple of days. It's interesting that if you listen to your body, you become aware of where colostrum has gone in your body to do its work. Colostrum's growth factors may create initial pain in areas of old wounds or injuries as it heals. If this becomes uncomfortable, you may want to cut back on the colostrum for a few days and then resume at a lower quantity, increasing your intake slowly as you reach optimum levels.</p>
			    
            
        </div>
      </div>
	  
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">What if i'm pregnant or nursing</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum definitely won’t hurt you, your fetus, or your infant, but as always with anything you take during pregnancy or while you are nursing, you should check with your health care professional first. Colostrum will help supplement your immune system which takes at hit during childbirth. It will help also you recover faster and help you lose those extra pounds associated with pregnancy quicker. Usually we recommend colostrum after 5th month of pregnancy.

</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Can i give my children colostrum?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum is strongly recommended for infants who are not being breastfed for at least 2 years. Colostrum can help growing children in many ways, from better concentration, increased memory to fewer illnesses such as autism, diabetes, and juvenile arthritis. Children have a tendency to catch colds and illness, so Colostrum will help immensely.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">I can't swallow the capsule. How can i take colostrum?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum works best if it is taken in capsule form, on an empty stomach with water. If you cannot swallow capsules, you may open the capsule and mix it with water or milk. Wait at least twenty minutes before you eat.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
How can colostrum help me lose weight?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum contains IGF-1, the most powerful growth factor in our bodies. Medical research has shown that without adequate amounts of IGF-1, our bodies, during times of fast (diet), will burn muscle protein before they burn fat (catabolism). Colostrum contains the only natural source of IGF-1. Supplementing your exercise regime with colostrum can help to burn fat and build lean muscle mass. Remember that muscle weighs more than fat, so while you may not notice a change on the scale, you will see the difference in the way your clothes fit!</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">I need to gain weight, can colostrum help?</h4>
			
               <hr class="hrstyle2">
               <p>Within about two hours of eating, our body's blood glucose levels are depleted and we utilize sources of stored fuel (fat and muscle protein) for energy. It is easier for our bodies to burn muscle tissue rather than fat. By increasing food intake along with exercise and colostrum, weight gain will come through increased muscle mass (muscle tissue weighs more than fat per volume). If you are an athlete, this can be very beneficial for performance.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">How is colostrum Anti-Aging?</h4>
			
               <hr class="hrstyle2">
               <p>After puberty our body begins slowing down the production of our body's Growth Hormones. These hormones are necessary for the reproduction of virtually all of our body cellular tissue. It has been shown that by age 80 we are producing virtually no growth hormone, and so we age and die. Colostrum's growth factors are the actual hormones that stimulate the normal reproduction of body cellular tissue. Normal reproduction means just that (normal) not aged, cancerous, wrinkled, or weakened. The New England Journal of Medicine (a few years back) stated that the most effective anti-aging process would be simply the replacement of growth hormones at proper levels to slow , possibly stop and even reverse the aging processes. Does it work? Just try and take colostrum away from someone who's used colostrum for a while and you'll see.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Can Colostrum help with my depression?</h4>
			
               <hr class="hrstyle2">
               <p>Most of us are in search of a means to increase the brain's feel-good chemicals, serotonin and dopamine. To accomplish this, we look to nicotine, drugs and certain (usually unhealthy and fattening) foods. We begin to rely on these substances to maintain that ‘feel-good’ sensation. Colostrum is a natural and healthy means of stimulating the brain to release serotonin and dopamine and prolong their re-uptake. Lactalbumin in colostrum has been clinically proven to improve mood during stress.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Does colostrum help with cuts, and burns? After surgery or accidents?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum's immune factors are highly effective anti-bacterial and anti-viral agents to help reduce and eliminate infections in external injuries (cuts, burns, abrasions and surgical wounds). Colostrum also contains epithelial (skin) growth hormone that stimulates accelerated healing and powerful anti-inflammatory agents that can eliminate swelling and pain.Open the capsule and mix with sterile water or a saline solution to form a paste. This can be applied directly to the affected area. Bandage if needed. Reapply two to three times daily until healing is complete.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Does colostrum help with gingivitis?</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum's powerful antibacterial factors have been shown to help prevent and treat Gingivitis. Application is made by applying colostrum directly to the gum area just before retiring. Reapply nightly until better. Colostrum has also been shown to help control any pain before or after dental work.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">How can i afford colostrum?</h4>
			
               <hr class="hrstyle2">
               <p>How can you NOT afford colostrum? The next time you visit your neighborhood drug store and pharmacy, become aware of how many products and drugs are on the market that only treat the symptoms. Colostrum can help heal the body at the cellular level and prevent the onset of certain conditions. Colostrum is one of the most potent natural antibiotics. Most high quality colostrum product like IMSYS retail for about just Rupees 1300/-.We must remember that a cow gives birth only once a year. High quality colostrum is taken from only select hormone, pesticide and antibiotic free cows. It must then be processed without excessive heat to keep its components biologically active and encapsulated in a capsule that will dissolve in the small intestine - this processing is very expensive. With all the varying qualities of colostrum on the market, we have a perfect example of the expression - you get what you pay for.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
I'm a vegetarian, i don't eat animal foods</h4>
			
               <hr class="hrstyle2">
               <p>The Rishis (India's spiritual leaders) vegan diet has included colostrum for thousands of years. Today, in India, the milkman delivers colostrum to the wealthy. It's interesting that that the cow has been deified in India. Colostrum is not at all non- vegetable but is unique unto itself - Colostrum is the first food of life - it is the gift of new life!.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
What if i am lactose intolerant?</h4>
			
               <hr class="hrstyle2">
               <p>Most people with lactose intolerance do not experience a problem with colostrum, as there is only a small amount of lactose present. If, however you have a severe milk protein allergy, you should consult your doctor prior to taking colostrum, due to its high protein count. </p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Research</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum is one of the most highly researched supplements. Type in the word colostrum at www.pubmed.gov and you will see more than 8,000 studies strongly supportive of both Colostrum and its numerous components.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Here are quotes from some of these articles:</h4>
			
               <hr class="hrstyle2">
               <p>Immunoglobulin from bovine Colostrum effectively reduces and prevents viral and bacterial infections in immune deficient subjects: bone marrow recipients, premature babies, AIDS, etc.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">
New England Journal of Medicine</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum has a virus antibody that acts against viral invaders. A wide range of antiviral factors was acknowledged to be present in Colostrum. This research was done at the US Government’s Center for Disease Control in Atlanta, Georgia.</p>
			    
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            		
			<h4 class="phead">Drs. Skottner, Arrhenius-Nyberg, Kanje and Fryklund, Acta.Paediatric Scandinavia</h4>
			
               <hr class="hrstyle2">
               <p>Colostrum contains Non Specific Inhibitors that inhibit a wide range of respiratory illness, notably Influenza viruses. Colostrum is specifically cited for its unique effectiveness against potentially deadly outbreaks of Asian Flu viruses that emerge from animal/human mutations.</p>
			    
            
        </div>
      </div>
		  
	  
	  
	  
	  
	  

    
</div>
	@include('products.products-list');
    <!-- inventor end -->
    <!-- inventor end -->
@endsection