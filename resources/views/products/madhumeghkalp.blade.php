@extends('layouts.ecommerce2')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>

<div class="container-fluid mt-40 mb-50">
    <h2 class="sechead">Ayurvedic Medicines: MADHUMEGHKALP </h2>
     <hr class="hrstyle">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            
               <div class="img-hover-zoom img-hover-zoom--xyz pdsec1_1">
  <img src="../asset/images/product/4.jpg" alt="Another Image zoom-on-hover effect" width="300" height="350">
</div>
           
        </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="pdsec1_2">
               <h4 class="phead"> MADHUMEGHKALP</h4>
                <hr class="hrstyle2">
			   <h4 class="phead"> MRP:  Rs. 1800/</h4><br>
               <ul class="pddtls">
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Traditional Ayurvedic Supplement to regulate Blood Sugar Levels.</span></li>
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Promotes Insulin Secretion.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Delays the development of Diabetic Complications.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Rectifies Metabolic Abnormalities. </span></li>
                   
                    

               </ul>
            </div>
        </div>
        
    </div>

      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Introduction</h4>
               <hr class="hrstyle2">
               <p>Diabetes, often referred to by doctors as diabetes mellitus, describes a group of metabolic diseases in which the person has high blood glucose (blood sugar), either because insulin production is inadequate, or because the body's cells do not respond properly to insulin, or both.</p>
            
        </div>
      </div>
	   <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Ayurvedic Management of Diabetes Mellitus</h4>
               <hr class="hrstyle2">
               <p>Madhu meha which has been correlated with Diabetes Mellitus has become a global problem in spite of advances in modern science. India has been projected by WHO as the country with the fastest growing population of Diabetic patients. It is estimated that between 1995 – 2025 diabetic patients in India will increase by 195%.<br>Diabetes Mellitus is also a maharoga (major disease) because it affects most part of the body and every cell of the human physiology. The ancient Indian physicians described not only the sweetness of urine as one of the major symptoms but also the relationship of the disease with disturbance of the 5 sheaths of the body – annamaya kosha{Food sheath}, pranamaya kosha{Energy sheath}, manomaya kosha{Mind Sheath}, vijnana maya kosha{Intalectual Sheath} and anandamaya kosha{Bliss Sheath}.<br>All classical texts describes prameha. The word prameha derived from the root mih sechane meaning watering that means dilution of everything in the body not only urine).The main causes of prameha (diabetes) are lack of exercise and improper food habits in excess food intake which falls in the category of ushna, snigdha and guru are the primal cause of this disease – fish, curd are good examples. Foods that increase kapha, medhas and moothra are the etiological factors for prameha.</p>
            
        </div>
      </div>
	  <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> 
Yashcha kinchith vidhiranyepi sleshma medho moothra samjananam sa sarva: nidana vishesha</h4>
               <hr class="hrstyle2">
               <p>Ayurveda clearly indicates few herbal remedies, to get rid of diabetic complications. These herbs regulate Insulin ratio in the body & rejuvenate the body cells to work efficiently. Researches on all these ayurvedic herbs lead 4LivinG to launch Madhumeghkalp.</p>
            
        </div>
      </div>
	  
       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro2">
            <h4 class="phead"> 
Active ingredients of Madhumeghkalp</h4>
               <hr class="hrstyle2">
               <ul class="pddtls">
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >	Berberis Species</span></li>
                   <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >	PterocarpusMarsupiumRoxb</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >GymnemaSylvestre R.br.</span></li>
                    <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >TrigonellaFoenu-graecum Linn.</span></li>
					 <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >	EnicostemmalittorateBtume</span></li>
					 <li><span class="bullet2" ><img src="../asset/images/logo/pdbullet.jpg"></span>
                    <span class="pdlist" >Purified Black Bitumen</span></li>
                     
                    

               </ul>
            
        </div>
      </div>


       





      
       <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro6">

             <h4 class="phead">Dosage</h4>
              <hr class="hrstyle2">
          
           
               
               <ol>
    <li><b>MashmeghKalp:</b> &nbsp; 2 tablets three a day before breakfast and dinner</li>
   
               </ol>
            
        </div>
      </div>

    
</div>
@include('products.products-list');
        
    <!-- inventor end -->
    <!-- inventor end -->
@endsection