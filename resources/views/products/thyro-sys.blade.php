@extends('layouts.ecommerce2')


@section('content')
 <!-- header end here -->
        <div class=" mt-30 mb-30">
           <img class="img-responsive"  src="../asset/images/bg/breadcrumb.jpg">
        </div>

<div class="container-fluid mt-40 mb-50">
    <h2 class="sechead">Ayurvedic Medicines: THYRO-SYS </h2>
     <hr class="hrstyle">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            
               <div class="img-hover-zoom img-hover-zoom--xyz pdsec1_1">
  <img src="../asset/images/product/5.jpg" alt="Another Image zoom-on-hover effect" width="300" height="350">
</div>
           
        </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="pdsec1_2">
               <h4 class="phead"> THYRO-SYS</h4>
               <hr class="hrstyle2">
			   <h4 class="phead"> MRP:  Rs. 1800/</h4><br>
              
            </div>
        </div>
        
    </div>

      <div class="row align-items-center lrmargin pagemarginsec">
        <div class="product_intro">
            <h4 class="phead"> Ingredients:</h4>
               <hr class="hrstyle2">
               <p> <ol>
                   <li> Allium Sativum Linn.</li>
                    <li>Andrographispaniculate</li>
                     <li>Compound Drug</li>
                      <li>Gum</li>
					  <li>Sodium Benzoate</li>
					  <li>Starch</li>
					  <li>Magnesium sterate</li>
					  

               </ol> 
			   </p>
            
        </div>
      </div>
     

    
</div>
        @include('products.products-list');
    <!-- inventor end -->
    <!-- inventor end -->
@endsection