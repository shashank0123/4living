@extends('layouts.admin')

@section('content')
  <style type="text/css">
  	#alt-res {
  		width: 60%;
  		display: block;
  		margin: auto;
  		padding: 20px;
  		border: 1px solid #e8e8e8;
  		display: none;
  		margin-top: 40px;
  	}
  </style>
   <div class="alert alert-success" id="alt-res">
      <div id="res"></div>
   </div>
	<div class="container ftocken" style="margin-top: 50px;">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-4">
				<div>
				  <form action="genToken" method="post" id="token">
				  	@csrf
					<select name="subscription">
						<option>Select</option>
            @foreach($subscription as $subc)
						<option value="{{$subc->subscription}}">{{$subc->subscription}}</option>
            @endforeach
					</select>
				</div>
				<div style="text-align: center; margin-top: 30px">
					<button type="submit" class="btn btn-primary">Generate</button>
				</div>
			  </form>
			</div>
			<div class="col-md-5"></div>
		</div>
	</div>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	//this is the id of the form
$("#token").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var url = form.attr('action');
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {   $('#alt-res').css('display','block');
                $('#res').empty()
               $('#res').append(data); // show response from the php script.
           }
         });
    });
</script>
@endsection