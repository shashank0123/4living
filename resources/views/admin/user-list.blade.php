@extends('layouts.admin')

@section('content')


<section style="margin-top: 50px;">
<div class="container">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>
        <th>Date</th>
        <th>User Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
        {{-- <th>Status</th> --}}
        <!-- <th>User type</th>
        <th>Remember token</th>
        <th>Created at</th> -->
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
      <tr>
        <td>{{$count++}}</td>
        <td>{{ explode(' ', $user->created_at)[0] }}</td>
        <td>{{'SS000'.$user->id}}</td>
         <td>{{$user->name}}</td>       
        <td>{{$user->email}}</td>
        <td><form action="/admin/resetuserpassword" method="POST">@csrf<input type="hidden" name="user_id" value="{{$user->id}}" ><input type="submit" name="Edit" value="Reset" class="btn btn-danger"></form>
          <form action="/admin/activateuser" method="POST">@csrf<input type="hidden" name="user_id" value="{{$user->id}}" ><input type="submit" name="Activate" value="Activate" class="btn btn-danger"></form></td>

       
        
        <!-- <td>admin</td>
        <td>32</td> -->
        
      </tr>
       @endforeach
    </tbody>
  </table>
</div>
</section>


@endsection
