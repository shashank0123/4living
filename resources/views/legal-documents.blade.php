<?php
use App\Faqs;
$faqs=Faqs::where('status','Active')->orderBy('created_at','ASC')->get();
?>

@extends('layouts.ecommerce2')
@section('content')

<!-- page title begin-->
<div class="page-title">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-8">
				<h2 class="extra-margin">LEGAL DOCUMENTS</h2>
				<p>Enjoy real benefits and rewards on your accrue MEMBERSHIP.</p>
			</div>
		</div>
	</div>
</div>
<!-- page title end -->

<!-- faq begin-->
<div class="faq" style="background-color: #eee">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-10">
				<div class="section-title text-center">
					<div class="row">
						<div class="col-sm-6">
							<a href="/legal_documents/proof1.jpeg" target="blank">
								<img src="/legal_documents/proof1.jpeg" style=" margin-bottom: 20px">
								<h3 style="margin-bottom: 50px">GST Registration Certificate</h3>
							</a>
						</div>

            <div class="col-sm-6">
              <a href="/legal_documents/proof2.jpeg" target="blank">
                <img src="/legal_documents/proof2.jpeg" style=" margin-bottom: 20px">
                <h3 style="margin-bottom: 50px">Certificate Of Incorporation</h3>
              </a>
            </div>

            <div class="col-sm-6">
              <a href="/legal_documents/proof3.jpeg" target="blank">
                <img src="/legal_documents/proof3.jpeg" style=" margin-bottom: 20px">
                <h3 style="margin-bottom: 50px">TAN</h3>
              </a>
            </div>

            <div class="col-sm-6">
              <a href="/legal_documents/proof4.jpeg" target="blank">
                <img src="/legal_documents/proof4.jpeg" style=" margin-bottom: 20px">
                <h3 style="margin-bottom: 50px">PAN</h3>
              </a>
            </div>						

						{{-- <div class="col-sm-6">
							<a href="/legal_documents/pan.jpeg" target="blank"><img src="/legal_documents/pan.jpeg"></a>
							<h3 style="margin-bottom: 50px">PAN Card</h3>
						</div>

						<div class="col-sm-6">
							<a href="/legal_documents/pan1.jpeg" target="blank"><img src="/legal_documents/pan1.jpeg"></a>
							<h3 style="margin-bottom: 50px">PAN Card</h3>
						</div> --}}
					</div>
				</div>
			</div>
		</div>




	</div>
</div>
<!-- faq end -->

<!-- payment begin-->
<div class="payment" style="margin-top: -50px">
  <div class="container">
    <div class="row d-flex reorder-xs">

      <div class="col-xl-5 co-lg-5 col-md-6">
        <div class="part-form">
          <h3>Bank Detail</h3>
          <div class="row">
            <div class="col-sm-4"><p>Company</p></div>
            <div class="col-sm-8"><p>- Help2Human CF Pvt. Ltd.</p></div>
            <div class="col-sm-4"><p>Bank</p></div>
            <div class="col-sm-8"><p>- HDFC BANK</p></div>
            <div class="col-sm-4"><p>A/C No.</p></div>
            <div class="col-sm-8"><p>- 50200042179121</p></div>
            <div class="col-sm-4"><p>IFSC</p></div>
            <div class="col-sm-8"><p>- HDFC0000328</p></div>
          </div>   
        </div>
      </div>

      <div class="col-xl-5 col-lg-5 col-md-6 d-flex align-items-center offset-xl-1 offset-lg-1">
        <div class="row justify-content-center">
          <div class="col-xl-12 col-lg-12">
            <div class="section-title">
              <h2>We Accepted<br /><span>Payment Method</span></h2>
              <p>Put your investing ideas into action with full range of investments.
              Enjoy real benefits and rewards on your accrue investing.</p>
            </div>
          </div>

          <div class="col-xl-12 col-lg-12">
            <div class="">
              <div >
                <img src="/images/payumoney.png" alt="" style="width: 200px !important, height: auto">
              </div>
              {{-- <div class="single-accept">
                <img src="asset/img/accept-card-2.jpg" alt="">
              </div>
              <div class="single-accept">
                <img src="asset/img/accept-card-3.jpg" alt="">
              </div>
              <div class="single-accept">
                <img src="asset/img/accept-card-4.jpg" alt="">
              </div> --}}
            </div>
            {{-- <div class="part-accept">
              <div class="single-accept">
                <img src="asset/img/accept-card-5.jpg" alt="">
              </div>
              <div class="single-accept">
                <img src="asset/img/accept-card-6.jpg" alt="">
              </div>
              <div class="single-accept">
                <img src="asset/img/accept-card-7.jpg" alt="">
              </div>
            </div> --}}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- payment end -->

@endsection