<?php

return [
	'title' => 'Member Tree',
	'subTitle' => 'Your Tree',
	'member' => 'Member ID',
	'secret' => 'Security Password',
	'notice' => 'Please input ID first.',
	'upline' => 'Up 1 level',
	'top' => 'Back To Top',
    'register' => 'Register',
    'modal.title' => 'Member Detail',
    'modal.info' => 'Member Information',
    'modal.id' => 'User ID',
    'modal.join' => 'Join Date',
    'modal.package' => 'Package',
    'modal.sponsor' => 'Sponsor',
    'modal.info2' => 'Sales Information',
    'modal.left' => 'Left',
    'modal.right' => 'Right',
    'modal.carry' => 'Carry Forward',
    'modal.today' => 'Today Sales',
    'modal.total' => 'Total Sales'
];
