<?php

return [
	'title' => 'Withdrawal',
	'subTitle' => 'Cash point withdraw.',
	'missingNotice' => ' <h4>Bank Detail Missing!</h4> <strong>You have not set your bank detail.</strong> You need to set it in order to do make withdrawal.', // don't remove <h4></h4> and <strong></strong>
	'missingLink' => 'set now',
	'account' => 'Account Information',
	'amount' => 'Amount',
	'date' => 'Date',
	'bank'	=> 'Bank',
	'photo'	=> 'Receipt Image',
	'bank.account' => 'Account Detail',
	'amount' => 'Amount',
	'amountNotice' => 'Withdrawal amount should not be less than 500.',
	'fee' => 'Admin Fee (5%)',
	'fees' => 'Admin Fee (20%)',
	'total' => 'Total',
	'security' => 'Transaction Password',
];
