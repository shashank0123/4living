<?php

return [


    'sr' => 'S.No.',
    'date' => 'Date',
    'mrp' => 'MRP',
    'sellingprice' => 'Selling Price',
    'pv' => 'Point Value',
    'bv' => 'Busniess Value',
    'quantity' => 'Quantity',
    'cost' => 'Total Price',
    'title' => 'Product purchase request',
    'status' => 'Status',
    'payment_mode' => 'Payment Method',
    'amount' => 'Amount',
    'description' => 'Products Detail',
   
];
