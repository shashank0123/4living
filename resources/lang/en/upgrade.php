<?php

return [
	// 'title' => 'Upgrade / Renew Package',
	'title' => 'Activate ID',
	'subTitle' => '<span class="theme-text bold">10-50%</span> of your promotion point, <span class="theme-text bold">50-90%</span> of your register point.', // don't remove <span></span>
	'package' => 'Package',
	'username' => 'User Name',
	'memberid' => 'Member ID',
	'renew' => 'RENEW',
	'epin' => 'EPIN List',
	'registerPoint' => 'REGISTER POINT amount',
	'registerNotice' => 'will be used on promotion point.' // like "90% will be used on promotion point"
];
