<?php

return [
	'sr' => 'Serial No.',
	'title' => 'Registration History',
	'subTitle' => 'Your members registration history.',
	'subTitle1' => 'Your left members registration history.',
	'subTitle2' => 'Your right members registration history.',
	'subTitle3' => 'Your direct members registration history.',
	'join' => 'Joining Date/Time',
	'username' => 'Username',
	'member_id' => 'Member ID',
	'sponsorid' => 'Sponsor ID',
	'registerBy' => 'Registered By',
	'directtitle' => 'Direct Members',
	'name' => 'Name',
	'package' => 'Package',
	'mobile' => 'Phone',
	'action' => 'Action'
];
